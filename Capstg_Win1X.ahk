;****************************************************
;* 【CapsLock增强脚本-Capstg-V20240628】
;*               by Ez and Tuutg
;****************************************************
global RunAny_Plugins_Version:="1.0.5.3"
; #Include %A_ScriptDir%\RunAny_ObjReg.ahk


/*;CapsLock增强脚本，例子 {{{1
;by Ez
;v20190721 添加在tc里面中键点击打开目录
;v20190904 更新暂停等热键，直接把AutoHotkey.exe改名为capsez.exe
;v20190916 添加几种模式的开关，解决BUG10任务栏无法切换
;v20190927 添加快捷键在TC中打开资源管理器中选中的文件，添加在tc中双击右键返回上一级。自动获取TC路径
;v20191214 添加媒体播放相关快捷键和右键拖动窗口，解决一点小问题和小细节
;v20200108 修复在资管或桌面没选文件的问题。再修复一些细节
;v20200401 添加不同程序中对应不同的小菜单，增强对话框，tab组合键等
;v20210405 添加侧边键增强等等细节。
;v20210601 继续添加对浏览器和播放器和侧边键鼠标的使用增强。
;v20210801 添加对IrfanView等程序的快捷键增强，改进调用启动器popsel的方式，其他小细节等
;v20211125 添加开启或关闭随系统自动启动，以及其他很小细节的优化
;v20220401 小细节优化
;v20220626 添加tc中数字键单击和双击效果，添加鼠标长按模式，添加everything中筛选器等，添加alt空格截图的开关，tc中alt+E为F4，中键改为在对侧面板新开标签
;v20220629 修复启动的时候卡顿，以及卡键问题
;v20220710 优化tc中数字键双击和长按操作，优化微信接收文件等
;v20240526 讯飞语音模块，添加当前模式状态提示功能，添加自动绿化功能


;by Tuutg
;v20230428 添加xnview mp程序的快捷键增强.增加简易设置Gui,方便无AHK基础的普通用户使用
;v20230710 添加tagLyst.exe程序的快捷键,添加找图点击功能等
;v20230730 优化IDM的下载对话框中MButton快捷键下载逻辑,用纯AHK,无需Quicker
;v20230730 添加WPS_et 双击中键关闭/单击中键缩放100%快捷键增强
;v20230801 cad: !w::定位到文件夹; F3=^f,对象捕捉
;v20230806 excel/word中 ALT约=Ctrl
;v20230807 Listary中Listary_WidgetWin_0 | 1
;v20230903 修复snipaste快捷键强制占用,恢复其他软件(Code编辑)中F3默认功能
;v20230905 完善简易Gui设置界面。增加Runany黑名单,修复Runany快捷键`强制占用,恢复其他软件`默认功能,如Listary
;v20231207 修正各种小错误,添加BCompare程序的增强快捷键等
;v20231215 开启{Space}模式,左手通用加强模式,单击标点符号,AHK编程符号优先.双击TC模式:数字小键盘+-*/等
;v20231215 开启{Space}模式,增加Snipaste和PixPin截图贴图工具栏左手键盘点击功能,
;v20231215 增加Snipaste和PixPin黑名单功能,恢复F1,f3默认功能.修复快捷键冲突,方法为用Controlsend,在冲突软件下再定义要的功能
;v20231216 添加ACDSee.exe程序的增强快捷键.添加一键启动|激活微信|微信文件传输助手等
;v20231220 修正各种小错误,添加UCompare,WinMerge程序的增强快捷键等
;v20231221 修正小错误,去除{Blind}模式,防止意外触发部分全局热键.IDM程序增加Ctrl+A全选并复制快捷键
;v20231225 增加Win11系统默认图片查看器增强快捷键.
;v20231225 升级Listary到6.3.0.36版(Win11).新增Listary搜索框中,CapsLock={Ctrl 2},用于快速切换到ListaryPro6.3文件窗口.和常规状态相反
;v20231226 打开|保存对话框中,CapsLock=Listary_CyJp,快速跳转到Listary中.WPS打开|保存对话框中适配uTools4.4.1版本
;v20231226 设置选项中,增加微信自定义安装路径,以及微信文件接收目录设置.
;v20240105 增加微信图片编辑器增强快捷键.增加Space & MButton::=打印的增强快捷键.优化缩减微信增强代码,优化一键搜索相关代码
;v20240108 修正Snipaste和PixPin保存图像时{space}无法打字的Bug,增加QQ|TIM图片编辑器增强快捷键
;v20240114 统一Listary和everything结果列表,caps|alt+W跳转到TC和MButton|alt+s跳转默认资管等增强快捷键
;v20240116 新增Anytext和FileLocator结果列表,caps|alt+W跳转到TC和MButton|alt+s跳转默认资管等增强快捷键
;v20240116 修复微信中复制粘贴时,单按CapsLock退出微信
;v20240124 优化EzTc中{space}+双击D|F|G用于文件选择时的逻辑,相当于取消选择相同|选择相同|反选
;v20240130 优化适配RunCaptX在32位系统和U盘版中的核心功能正常工作
;v20240301 优化适配Listary6.3以上版本,IDM自动下载,修复微信中在打开|保持等弹出对话框时按caps|+W弹出文件助手的问题,更改为caps|+W=Enter
;v20240306 优化适配浏览器组Alt模式;增加Acrobat和SumatraPDF单键模式快捷键
;v20240309 优化改进{space}模式,保留Space+X|C|V=Ctrl+X|C|V,降低记忆难度.优化统一PS和pdf导出文件,PDF|JPG|PNG=+!L|M|RButton
;v20240311 改进快捷键在Wps统一模式不起作用的bug
;v20240313 单键通用看图模式,适配Win7系统默认看图器和ACDSee
;v20240314 MButton盲点下载浏览按钮,适配360X浏览器的下载
;v20240315 恢复微信联系人简写+Space或Enter功能|[但是会导致微信中打字时用鼠标连选后再按Space+x|c失效,可直接按Space+c替代(有bug,暂时修复不了)或Caps+X|C|V]
;v20240316 新增Space+c模式剪切板内容显示;健全Shift+符号模式,可替代Shift用于符号输入
;v20240317 单键通用看图模式,适配Win11系统默认看图器,画图,XMind.exe,优化改进Space+b,Code输入模式,单冒号优先,双击右键
;v20240318 单键上下左右,增强模式,适配QuickLook,EVERYTHING等软件
;v20240319 Photoshop中保留Space+X|C|V=Ctrl+X|C|V,单击是系统功能双击,是PS内部功能.Excelz中Space & q|a,文本输入模式为Home.End,其他为Pgup,Pgdn,双击加Ctrl
;v20240319 修复JPEGView.exe中双击右键不关闭的问题
;v20240320 优化适配Space独立模式在微信,excel相关代码,等同Caps模式,左手有上下左右+Sapce+X|C|V.微信主窗口非输入模式时支持单键E上D下
;v20240322 优化适配Caps+X|C|V|B,增加按住Caps不放,双击X|C|V|B直接复制路径和文件名,单击功能不变.优化Listary|uTools没启动时,保持原有Ez脚本设定功能
;v20240323 增加自动保存开关,GV_ToggleAotuSaveMode=1,Tab|Enter激活自动保存,初始未保存会有弹出提示,切换开关按键为^#+a,默认关闭,0.改进F2命名,F7新建,F8快删
;v20240324 丰富完善CapsLock在不同场景下的功能,动态赋予相应场景最高频,最需要的功能.增强纯鼠标纯鼠标剪切|复制|粘贴功能,~Lb+X|C|V或Lb+Rb复制|Lb+Mb粘贴
;v20240325 增加Enter激活自动保存功能,配套增加GUI设置项
;v20240326 完善Caps模式在excel相关代码,Space+e|d|s|f,增加单击操作同上下左右,双击快速跳转到非空数据单元格
;v20240327 完善Space独立模式在excel相关代码,Space+1|2|3|4|5,只输入数字,无符号,^f|查找时自动填充光标下内容.下载场景,Caps|Space+X|C|V|,增加双击X|C|V为整体操作
;v20240328 完善自动保存相关代码,Tab|Enter,Space|Caps+G在Code|Office|Wps...系列软件可选启用.
;v20240328 丰富完善SciTEWindow软件中,Ctrl+f|h,光标自动定位到搜索框,方便按Enter|Caps搜索. 修复微开启单键上下模式后,无法打字,或光标进入打字框上|下失效
;v20240329 Space模式适配360se.exe. 新增MButton盲点保存|打开对话框默认按钮. Space & g::^g,适配Listary路径跳转
;v20240330 完善Space独立模式在excel相关代码,Caps|Space+E|D|S|F,单击快速定位到|双击选择非空单元格.单键模式适配wps_pdf,优化Wps_office键位和窗口检测,全系适配
;v20240401 增加Tab & b键位.定义为:1-uTools优先.2-资管中复制并右侧显示clip(Clipboard内容)|复制全文件名|同CapsLock & b|3-TC中-em_BeyondCompare4文件比较
;v20240402 1.TC中Space增加左手上下左右, 默认关闭数字键跳转快速打开文件功能 2.在打开|保持|...等对话框Space开启-QuickLook预览 3.PS修复Space打汉字多出一个空格的Bug
;v20240403 1.Space & b::智能判断:鼠标右键单击|单冒号:|中键 2.微信启用原版Tab,快速输入联系人 3.优化剪映和Pr等.单独Space模式,增加单击模式,适配剪映国际版
;v20240408 1.优化改进TC中Space & F|G|R键位功能,调整为高频功能双击SP+F=F4,单击SP+G=Enter,双击SP+G=!= 2.IDM,下载优化,中键一步到位,!r=F2,重命名
;v20240410 1.浏览器组增加Space & v::^v{Enter}; 2.浏览器组,增加Space & g::CCAP电摩合格证管理系统,一键撤销
;v20240412 1.优化完善IDM|360X下载,中键一步到位,提高配合Listary自动定位目标目录的可靠性,扩展名分割符.用空格替代
;v20240415 1.Caps+E|D|S|F增加鼠标移动模拟,开关为ctrl+win+alt+M或者!#Space,默认关闭,0。Space+E|D|S|F在文本光标下调整为|方向键快速移动
;v20240417 1.GroupDiagOpenAndSave|新建保存对话框中,新增!e=新建文件,e=New. 2.;增加调用多媒体(音乐|视频)相关程序快捷键,CapsF1~F12=FN+F1~F12
;v20240418 1.修复CapsLock在GroupDiagOpenAndSave对话框中不转到Listary. 2.Listary 5 选项
;v20240419 1.Listary 5 选项中,中键点击应用 2.TGTC中Caps+w,关闭标签|TC快搜中,激活过滤;+s=^s,打开文件搜索面板
;v20240420 1.Listary 5 选项中,中键点击应用 2.TGTC中Caps+w,关闭标签|TC快搜中,激活过滤;+s=^s,打开文件搜索面板
;v20240421 TGTC中,排除TC主窗口,Caps+Q|A,重定义,Home|End, | Caps+S|F,重定义,Left|Right
;v20240423 视频剪辑组,优化X,Space+X,键位,新增!s=+s=s,吸附开关切换. 2.浏览器组采用窗口模式,避免干扰GroupDiagOpenAndSave组的!g/!w
;v20240424 修复微信看图器,图片出现二维码时,a键不等同保存的Bug. 新增m键=二维码
;v20240425 1.GroupDiagOpenAndSave对话框中加Space & v::^v{Enter} 2.机动车合格证,增加请输入车型序号对话框,自动录入参数
;v20240430 1.GroupDiagOpenAndSave对话框中微调,动态Caps|Space & g,纵坐标在文件名控件以上,为{Enter};其他情形为^g,Listary跳转到目标路径
;v20240501 1.GridMove排除ahk_class Photoshop,保持默认Space抓手功能不变
;v20240507 1.微软Office修复打印时中建默认Enter, !+d=Ctrl+d,向下填充
;v20240515 1.优化适配中汽机动车合格证打印系统,自动填写|打印. 2.优化适配Excel版电自合格证打印系统,自动打印 3.数据筛选,导出Excel
;v20240516 丰富完善listary,标签QueryReplace中搜索词增加带文件扩展名,缩小搜索范围,定位更精准
;v20240518 优化适配1.$!g::Utools_goole_翻译. 2.Utools中CapsLock::Esc
;v20240519 修复TTOTAL_CMD中,1.+g不进入G盘的Bug 2.CapsLock::关闭|取选|重置英文(新增),要在KBLAutoSwitch中设定{Rshift}=英文
;v20240520 微信中:1.修复在中文输入法下快速联系人时Tab不能填充联系人 2.取消Space作为快速联系人的激活键,改用Tab键 3.Space&C=caps&C=Ctrl+C,复制不影响打字  4.新增向日葵远程控制中键自动输入密码登入 5.新增SLDWORKS一键快速导出|另存为其他格式 6.新增双击tab,从Listary搜索框跳转回打开|保存对话框
;v20240528 尽量统一常用看图器|查看器键位(QWERT ASDFG ZXCVBH),如系统看图器,微信|QQ看图器,JpegView,IrfanView,ImageView,XnView MP,Acrobat,金山PDF,稻壳阅读器等
;v20240606 1.关闭+MButton作为打印功能,和3D软件有干扰 2.TC中重命名和文本模式下,中键用做确认 3.修正{Text}模式导致的不能输入中文的Bug,只保留右键菜单后的字母用{Text}
;V20240612 右键设置界面优化适配14"笔记本电脑屏幕
;V20240625 优化Everything键位,尽量和TCg键位保持一致. 修复一键压缩解压中,Listary拦截右键菜单选项输入的Bug
;V20240626 优化微调fupx.exe增强键位,提高操作效率. 右键菜单选项输入字母采用Controlsend+{Text},提高可靠性
;V20240628 优化微调浏览器组定义,提高可靠性. F4微调在讯飞已经启动后定义. ahk_class #32770窗口中新增Alt+A=Ctrl+A全选. 修复新版QQ中,Alt+F失效的Bug 


;定位到程序最后,用户自定义应用增强例子开始
;建议对“例子”位置进行自行修改
*/


;**************图标定义^ **************
IfExist, %A_ScriptDir%\Tg_Win1X.ico
{
	Menu, TRAY, Icon, %A_ScriptDir%\Tg_Win1X.ico, , 1
}
;**************图标定义$ **************


;管理员权限代码，放在文件开头 {{{1
loop, %0%
{
	param := %A_Index% ; Fetch the contents of the variable whose name is contained in A_Index.
	params .= A_Space . param
}
ShellExecute := A_IsUnicode ? "shell32\ShellExecute":"shell32\ShellExecuteA"
if not A_IsAdmin
{
	if A_IsCompiled
		DllCall(ShellExecute, uint, 0, str, "RunAs", str, A_ScriptFullPath, str, params , str, A_WorkingDir, int, 1)
	else
		DllCall(ShellExecute, uint, 0, str, "RunAs", str, A_AhkPath, str, """" . A_ScriptFullPath . """" . A_Space . params, str, A_WorkingDir, int, 1)
	ExitApp
}

;文件头 {{{1
;Directives	;脚本前参数指令
{
;#NoTrayIcon	;不显示托盘图标
#NoEnv	;不检查空变量为环境变量
#WinActivateForce ;强制激活窗口
#SingleInstance Force ;运行替换旧实例
#InstallKeybdHook ;强制无条件安装键盘钩子
#InstallMouseHook ;强制无条件安装鼠标钩子
#Persistent ;让脚本持久运行(关闭或ExitApp)
#MaxMem 4	 ;max memory per var use
#MaxHotkeysPerInterval 10000 ;设置的时间内按热键最大次数
#MenuMaskKey vkE8  ;将掩码键改成未分配的按键, 如 vkE8 等.20240229
;KeyHistory	;显示脚本信息和最近键击和鼠标点击的历史
ListLines,Off ;不显示最近执行的脚本行
SendMode Input ;使用更速度和可靠方式发送键鼠点击
;SendMode InputThenPlay ;速度过快导致发送不正常请注释此行.默认关闭,否则影响Listary/Utools弹出
SetBatchLines -1 ;让脚本无休眠地执行(换句话说，也就是让脚本全速运行)(默认10ms)
SetKeyDelay 0 ;设置每次Send和ControlSend发送键击后自动的延时,使用-1表示无延时
SetWinDelay,0 ;执行窗口命令后,最小自动的延时(0 默认100ms),-1 表示无延时
SetControlDelay,0 ;控件修改命令自动延时(默认20)
SetTitleMatchMode, 2 ;窗口标题模糊匹配. RegEx ;窗口标题RegEx正则匹配
SetTitleMatchMode, Slow ;窗口标题慢速模式匹配
Process Priority,,High ;线程,主,高级别
DetectHiddenWindows, On ;显示隐藏窗口
SetCapsLockState, AlwaysOff ;强制按键保持CapsLock关闭状态
CoordMode,Menu,Window ;~坐标相对活动窗口
CoordMode,Mouse,Window ;~坐标相对活动窗口
}

;************** group定义^ ****************** {{{1
{
;浏览器组,Caps+r|w,前进|后退,增强F2,F3,F4.启用Space系列快件键
; GroupAdd, Group_browser, ahk_class St.HDBaseWindow
GroupAdd, Group_browser, ahk_class IEFrame ;IE
GroupAdd, Group_browser, ahk_class ApplicationFrameWindow ;Edge
GroupAdd, Group_browser, ahk_class MozillaWindowClass ;Firefox
GroupAdd, Group_browser, ahk_class QQBrowser_WidgetWin_1 ;QQBrowser
GroupAdd, Group_browser, YywPlayerOperateFrame ahk_class XMLWnd
GroupAdd, Group_browser, ahk_class Chrome_WidgetWin_1 ;Chrome|Edge|360|vivaldi|Opera|百分
GroupAdd, Group_browser, ahk_class Chrome_WidgetWin_2 ;Chrome|Edge|360|vivaldi|Opera|百分

;主窗口模式,避免干扰GroupDiagOpenAndSave组的!g/!w/!f,20240628
GroupAdd, Group_browser, ahk_exe chrome.exe ;Chrome
GroupAdd, Group_browser, ahk_exe msedge.exe ;Edge
GroupAdd, Group_browser, ahk_exe 115chrome.exe ;115的播放器
GroupAdd, Group_browser, ahk_exe 360chrome.exe ;360Chrome
GroupAdd, Group_browser, ahk_exe 360Chromex.exe ;360ChromeX.exe
GroupAdd, Group_browser, ahk_exe 360se.exe ;360se.exe
GroupAdd, Group_browser, ahk_exe opera.exe ;Opera.exe
GroupAdd, Group_browser, ahk_exe vivaldi.exe ;vivaldi.exe


;Ctrl+Space黑名单,禁用全局快捷键Ctrl+Space等
GroupAdd, Group_disableCtrlSpace, ahk_exe excel.exe
GroupAdd, Group_disableCtrlSpace, ahk_exe pycharm.exe
GroupAdd, Group_disableCtrlSpace, ahk_exe SQLiteStudio.exe
GroupAdd, Group_disableCtrlSpace, ahk_exe gvim.exe
GroupAdd, Group_disableCtrlSpace, ahk_class NotebookFrame


;打开|保持|...等对话框组,模仿Listary跳转目标路径,!w,!g,^g,!s,!f,Caps+w
GroupAdd, GroupDiagOpenAndSave, 新建 ahk_class #32770
GroupAdd, GroupDiagOpenAndSave, 选择 ahk_class #32770
GroupAdd, GroupDiagOpenAndSave, 保存 ahk_class #32770
GroupAdd, GroupDiagOpenAndSave, 另存 ahk_class #32770
GroupAdd, GroupDiagOpenAndSave, 存储 ahk_class #32770
GroupAdd, GroupDiagOpenAndSave, 打开 ahk_class #32770
GroupAdd, GroupDiagOpenAndSave, 上传 ahk_class #32770
GroupAdd, GroupDiagOpenAndSave, 导入 ahk_class #32770
GroupAdd, GroupDiagOpenAndSave, 导出 ahk_class #32770
GroupAdd, GroupDiagOpenAndSave, 插入 ahk_class #32770
GroupAdd, GroupDiagOpenAndSave, 浏览 ahk_class #32770
GroupAdd, GroupDiagOpenAndSave, Open ahk_class #32770
GroupAdd, GroupDiagOpenAndSave, Save ahk_class #32770
GroupAdd, GroupDiagOpenAndSave, Select ahk_class #32770
GroupAdd, GroupDiagOpenAndSave, Browse ahk_class #32770

;上面为Ez原版GroupDiagOpenAndSave. 下面为Tuutg增加的组
GroupAdd, GroupDiagOpenAndSave, 并入 ahk_class #32770
GroupAdd, GroupDiagOpenAndSave, 查找 ahk_class #32770
GroupAdd, GroupDiagOpenAndSave, 发布 ahk_class #32770
GroupAdd, GroupDiagOpenAndSave, 更改 ahk_class #32770

; GroupAdd, GroupDiagOpenAndSave, 添加 ahk_class #32770 ;此项影响TC|!w-智能跳转,原因未知
GroupAdd, GroupDiagOpenAndSave, 输入 ahk_class #32770
GroupAdd, GroupDiagOpenAndSave, 输出 ahk_class #32770
GroupAdd, GroupDiagOpenAndSave, 替换 ahk_class #32770

GroupAdd, GroupDiagOpenAndSave, 位置 ahk_class #32770
GroupAdd, GroupDiagOpenAndSave, 下载 ahk_class #32770
GroupAdd, GroupDiagOpenAndSave, PDF ahk_class #32770
GroupAdd, GroupDiagOpenAndSave, XPS ahk_class #32770

;下面为Tuutg增加的特殊非标准的GroupDiagOpenAndSave组
GroupAdd, GroupDiagOpenAndSave, ahk_exe FileShred.exe
GroupAdd, GroupDiagOpenAndSave, 浏览 ahk_exe BCompare.exe
GroupAdd, GroupDiagOpenAndSave, ahk_class TSelectFolderForm
GroupAdd, GroupDiagOpenAndSave, 打开 ahk_class WindowsForms10.Window.8.app.0.3ce0bb8_r34_ad1
GroupAdd, GroupDiagOpenAndSave, 另存 ahk_class WindowsForms10.Window.8.app.0.3ce0bb8_r34_ad1
GroupAdd, GroupDiagOpenAndSave, ahk_class ATL:003F83B0 ahk_exe HRConfig.exe
GroupAdd, GroupDiagOpenAndSave, ahk_class TProfileBrowseDialog	;BCompare.exe
GroupAdd, GroupDiagOpenAndSave, 安装新插件 ahk_class #32770	;TC_Plugman.exe


;WPS-Office组,方法1,uTools: $!f::模仿Listary, 跳转目标路径,会有失败概率
GroupAdd, Group_WPS, ahk_exe wpp.exe
GroupAdd, Group_WPS, ahk_exe et.exe
GroupAdd, Group_WPS, ahk_exe wps.exe
GroupAdd, Group_WPS, ahk_exe wpspdf.exe	;金山PDF专业版
GroupAdd, Group_WPS, ahk_exe wpsoffice.exe ;Wps_4合1集成模式,20240330


;Office组, CapsLock, 默认为{Enter},20240324
GroupAdd, OfficeAndWPS, ahk_class XLMAIN ;MS_Excel和Wps_et通用
GroupAdd, OfficeAndWPS, xls ahk_class OpusApp ;Wps_Excel
GroupAdd, OfficeAndWPS, 工作簿 ahk_class OpusApp ;Wps_Excel
GroupAdd, OfficeAndWPS, ahk_class OpusApp ;MS_Word和Wps集成模式通用
GroupAdd, OfficeAndWPS, ahk_class PPTFrameClass ;MS_PPT
GroupAdd, OfficeAndWPS, ahk_class PP11FrameClass ;Wps_PPT
GroupAdd, OfficeAndWPS, PDF ahk_class Qt5QWindowIcon ;Wps_PDF专业版
GroupAdd, OfficeAndWPS, ahk_exe wpsoffice.exe ;Wps_4合1集成模式,20240330
GroupAdd, OfficeAndWPS, ahk_exe wps.exe	;Wps_4合1集成模式,20240408
GroupAdd, OfficeAndWPS, ahk_exe wpspdf.exe	;金山PDF专业版


;打印机组,快速选择打印机,单击中键=盲点Enter,打印,OK等......
GroupAdd, Group_打印, 打印 ahk_exe wps.exe	;wps
GroupAdd, Group_打印, 打印 ahk_class #32770	;通用
GroupAdd, Group_打印, ahk_class PSFloatC	;Photoshop简单弹出对话框
GroupAdd, Group_打印, 打印 ahk_exe xnviewmp.exe	;xnviewmp.exe
GroupAdd, Group_打印, 打印 ahk_class bosa_sdm_XL9	;wps_et
GroupAdd, Group_打印, Dialog ahk_class QWidget	;wpspdf.exe
GroupAdd, Group_打印, 打印 ahk_class Qt5QWindow  ;wpspdf.exe


;Office_表格组,20240330
GroupAdd, Office_Excel, Excel ahk_class XLMAIN	;微软Excel
GroupAdd, Office_Excel, 表格 ahk_class XLMAIN	;金山Excel
GroupAdd, Office_Excel, 表格 ahk_class OpusApp	;金山Excel
GroupAdd, Office_Excel, 表格 ahk_exe et.exe		;金山Excel
GroupAdd, Office_Excel, 表格 ahk_exe wps.exe	;金山Excel
GroupAdd, Office_Excel, 表格 ahk_exe wpsoffice.exe ;金山Excel
GroupAdd, Office_Excel, xls ahk_class XLMAIN	;金山Excel
GroupAdd, Office_Excel, xls ahk_class OpusApp	;金山Excel
GroupAdd, Office_Excel, xls ahk_exe et.exe		;金山Excel
GroupAdd, Office_Excel, xls ahk_exe wps.exe		;金山Excel
GroupAdd, Office_Excel, xls ahk_exe wpsoffice.exe ;金山Excel
GroupAdd, Office_Excel, xlsx ahk_class XLMAIN	;金山Excel
GroupAdd, Office_Excel, xlsx ahk_class OpusApp	;金山Excel
GroupAdd, Office_Excel, xlsx ahk_exe et.exe		;金山Excel
GroupAdd, Office_Excel, xlsx ahk_exe wps.exe	;金山Excel
GroupAdd, Office_Excel, xlsx ahk_exe wpsoffice.exe ;金山Excel
GroupAdd, Office_Excel, 工作簿 ahk_class XLMAIN	;金山Excel
GroupAdd, Office_Excel, 工作簿 ahk_class OpusApp ;金山Excel
GroupAdd, Office_Excel, 工作簿 ahk_exe et.exe	;金山Excel
GroupAdd, Office_Excel, 工作簿 ahk_exe wps.exe	;金山Excel
GroupAdd, Office_Excel, 工作簿 ahk_exe wpsoffice.exe ;金山Excel


;Office_文字组,20240330
GroupAdd, Office_Word, Word ahk_class OpusApp	;微软Word
GroupAdd, Office_Word, 文字 ahk_class XLMAIN	;金山Word
GroupAdd, Office_Word, 文字 ahk_class OpusApp	;金山Word
GroupAdd, Office_Word, 文字 ahk_exe wps.exe	;金山Excel
GroupAdd, Office_Word, 文字 ahk_exe wpsoffice.exe	;金山Word
GroupAdd, Office_Word, doc ahk_class OpusApp	;金山Word
GroupAdd, Office_Word, doc ahk_class XLMAIN	;金山Excel
GroupAdd, Office_Word, doc ahk_exe wps.exe ;金山Excel
GroupAdd, Office_Word, doc ahk_exe wpsoffice.exe ;金山Word
GroupAdd, Office_Word, docx ahk_class OpusApp	;金山Word
GroupAdd, Office_Word, docx ahk_class XLMAIN	;金山Word
GroupAdd, Office_Word, docx ahk_exe wps.exe ;金山Word
GroupAdd, Office_Word, docx ahk_exe wpsoffice.exe ;金山Word


;Office_PPT组,20240330
GroupAdd, Office_PPT, ahk_class PPTFrameClass	;微软PPT
GroupAdd, Office_PPT, ahk_class PP11FrameClass	;金山PPT
GroupAdd, Office_PPT, 演示 ahk_class XLMAIN	;金山PPT
GroupAdd, Office_PPT, 演示 ahk_class OpusApp	;金山PPT
GroupAdd, Office_PPT, 演示 ahk_exe wpp.exe	;金山PPT
GroupAdd, Office_PPT, 演示 ahk_exe wps.exe	;金山PPT
GroupAdd, Office_PPT, 演示 ahk_exe wpsoffice.exe	;金山PPT
GroupAdd, Office_PPT, ppt ahk_class OpusApp	;金山PPT
GroupAdd, Office_PPT, ppt ahk_class XLMAIN	;;金山PPT
GroupAdd, Office_PPT, ppt ahk_exe wpp.exe	;金山PPT
GroupAdd, Office_PPT, ppt ahk_exe wps.exe	;金山PPT
GroupAdd, Office_PPT, ppt ahk_exe wpsoffice.exe ;金山PPT
GroupAdd, Office_PPT, pptx ahk_class OpusApp	;金山PPT
GroupAdd, Office_PPT, pptx ahk_class XLMAIN	;金山PPT
GroupAdd, Office_PPT, pptx ahk_exe wpp.exe	;金山PPT
GroupAdd, Office_PPT, pptx ahk_exe wps.exe ;金山PPT
GroupAdd, Office_PPT, pptx ahk_exe wpsoffice.exe ;金山PPT


;OfficePDF组,金山PDF
GroupAdd, Kingsoft_PDF, pdf ahk_class QWidget	;金山专业PDF
GroupAdd, Kingsoft_PDF, ahk_class Qt5QWindowIcon	;金山PDF
GroupAdd, Kingsoft_PDF, pdf ahk_exe wpsoffice.exe	;金山PDF


;资源管理器组,模仿TC:F2,F3,F7,F8,压缩|隐藏,双击右键,返回目录.quicklook预览
; GroupAdd, Group_explorer, ahk_class TTOTAL_CMD	;TC资管
; GroupAdd, Group_explorer, ahk_exe explorer.exe	;Win-1分4
GroupAdd, Group_explorer, ahk_class CabinetWClass  	;Win1X资管
GroupAdd, Group_explorer, ahk_class ExploreWClass 	;Win7资管
GroupAdd, Group_explorer, ahk_exe Q-Dir.exe
GroupAdd, Group_explorer, ahk_exe Q-Dir32.exe
GroupAdd, Group_explorer, ahk_class 360ExplorerFrame
GroupAdd, Group_explorer, ahk_exe clover.exe
GroupAdd, Group_explorer, ahk_exe XYplorer.exe
GroupAdd, Group_explorer, ahk_exe OneCommander.exe
GroupAdd, Group_explorer, ahk_exe Multi Commander.exe
GroupAdd, Group_explorer, ahk_exe EFCW.exe
GroupAdd, Group_explorer, ahk_exe dopus.exe
GroupAdd, Group_explorer, ahk_class WinRarWindow


;桌面组,模仿TC:F2,F3,F7,F8,压缩|隐藏,双击右键,返回目录.quicklook预览
GroupAdd, Group_Desktop, ahk_class Progman 	  	;Win1X桌面
GroupAdd, Group_Desktop, ahk_class WorkerW		;Win7桌面


;资源管理器组,模仿TC:F2,F3,F7,F8,压缩|隐藏,双击右键,返回目录.quicklook预览
GroupAdd, CapsLockKeysAsBackSpace, ahk_class TTOTAL_CMD	;TC资管
GroupAdd, CapsLockKeysAsBackSpace, ahk_class CabinetWClass	;Win1X资管
GroupAdd, CapsLockKeysAsBackSpace, ahk_class ExploreWClass	;Win7资管
GroupAdd, CapsLockKeysAsBackSpace, ahk_class 360ExplorerFrame
GroupAdd, CapsLockKeysAsBackSpace, ahk_class WinRarWindow
GroupAdd, CapsLockKeysAsBackSpace, ahk_exe Q-Dir.exe


;代码编辑器组,增加注释,查找,定位,替换,保存,比较快捷键
; GroupAdd, Group_Code, ahk_class SciTEWindow
GroupAdd, Group_Code, ahk_exe AutoAHK.exe
GroupAdd, Group_Code, ahk_exe AHKEditor.exe
GroupAdd, Group_Code, Adventure ahk_class AutoHotkeyGUI	;Adventure.exe
GroupAdd, Group_Code, Code ahk_class Chrome_WidgetWin_1	;Code.exe
GroupAdd, Group_Code, GVIM ahk_class Vim	;GVim.exe
GroupAdd, Group_Code, VIM ahk_class ConsoleWindowClass	;Vim.exe
GroupAdd, Group_Code, ahk_class Notepad
GroupAdd, Group_Code, ahk_class Notepad2U
GroupAdd, Group_Code, ahk_class Notepad3U
GroupAdd, Group_Code, ahk_class Notepad3
GroupAdd, Group_Code, ahk_class Notepad++
GroupAdd, Group_Code, ahk_class Qt5152QWindowIcon ahk_exe Notepad--.exe
GroupAdd, Group_Code, ahk_class PX_WINDOW_CLASS	;Python
GroupAdd, Group_Code, uTools ahk_class Chrome_WidgetWin_1	;uTools
GroupAdd, Group_Code, ahk_class ConsoleWindowClass	;cmd.exe
GroupAdd, Group_Code, ahk_exe SourceTree.exe
GroupAdd, Group_Code, ahk_exe GitHubDesktop.exe
GroupAdd, Group_Code, ahk_class SunAwtFrame	;webstorm64.exe|pycharm64.exe
GroupAdd, Group_Code, ahk_exe EmEditor.exe
GroupAdd, Group_Code, ahk_class TkTopLevel	;ahk_exe pythonw.exe
GroupAdd, Group_Code, ahk_exe BCompare.exe	;Beyond Compare
GroupAdd, Group_Code, ahk_exe uc.exe	;UltraCompare
GroupAdd, Group_Code, ahk_exe WinMergeU.exe


;代码编辑器组中的代码比较组,20240403
GroupAdd, Code_Compare, ahk_exe BCompare.exe	;Beyond Compare
GroupAdd, Code_Compare, ahk_exe uc.exe	;UltraCompare
GroupAdd, Code_Compare, ahk_exe WinMergeU.exe

;代码编辑器组中的查找|替换|定位对话框
GroupAdd, Group_CodeFind, 定位 ahk_class #32770
GroupAdd, Group_CodeFind, 查找 ahk_class #32770
GroupAdd, Group_CodeFind, 替换 ahk_class #32770


;Snipaste和PixPin黑名单,恢复被截图软件占用的全局快捷键F1,F3等
GroupAdd, Group_disableSnipaste, ahk_exe Adventure.exe
GroupAdd, Group_disableSnipaste, ahk_class SciTEWindow
GroupAdd, Group_disableSnipaste, ahk_exe regedit.exe


;截图软件等中启用单独Space模式用于工具切换,而非默认的符号数字输入模式
;恢复Adobe软件等中原始Space功能或其他单独Space模式,而非符号输入模式
GroupAdd, Group_SingleSpace, ahk_exe Snipaste.exe	;Space模式用于工具切换
GroupAdd, Group_SingleSpace, ahk_exe PixPin.exe	;Space模式用于工具切换
GroupAdd, Group_SingleSpace, 图像 ahk_class #32770	;恢复Snipaste|PixPin另存为打字功能
{	;Excel,;微软|金山Excel|独立Space模式用于快选|工具|数字,20240331
GroupAdd, Group_SingleSpace, Excel ahk_class XLMAIN	;微软Excel|独立Space模式用于快选|工具|数字
GroupAdd, Group_SingleSpace, 表格 ahk_class XLMAIN	;金山Excel|独立Space模式用于快选|工具|数字
GroupAdd, Group_SingleSpace, 表格 ahk_class OpusApp	;金山Excel|独立Space模式用于快选|工具|数字
GroupAdd, Group_SingleSpace, 表格 ahk_exe et.exe	;金山Excel|独立Space模式用于快选|工具|数字
GroupAdd, Group_SingleSpace, 表格 ahk_exe wps.exe	;金山Excel|独立Space模式用于快选|工具|数字
GroupAdd, Group_SingleSpace, 表格 ahk_exe wpsoffice.exe ;金山Excel|独立Space模式用于快选|工具|数字
GroupAdd, Group_SingleSpace, xls ahk_class XLMAIN	;金山Excel|独立Space模式用于快选|工具|数字
GroupAdd, Group_SingleSpace, xls ahk_class OpusApp	;金山Excel|独立Space模式用于快选|工具|数字
GroupAdd, Group_SingleSpace, xls ahk_exe et.exe		;金山Excel|独立Space模式用于快选|工具|数字
GroupAdd, Group_SingleSpace, xls ahk_exe wps.exe	;金山Excel|独立Space模式用于快选|工具|数字
GroupAdd, Group_SingleSpace, xls ahk_exe wpsoffice.exe ;金山Excel|独立Space模式用于快选|工具|数字
GroupAdd, Group_SingleSpace, xlsx ahk_class XLMAIN	;金山Excel|独立Space模式用于快选|工具|数字
GroupAdd, Group_SingleSpace, xlsx ahk_class OpusApp	;金山Excel|独立Space模式用于快选|工具|数字
GroupAdd, Group_SingleSpace, xlsx ahk_exe et.exe	;金山Excel|独立Space模式用于快选|工具|数字
GroupAdd, Group_SingleSpace, xlsx ahk_exe wps.exe	;金山Excel|独立Space模式用于快选|工具|数字
GroupAdd, Group_SingleSpace, xlsx ahk_exe wpsoffice.exe ;金山Excel|独立Space模式用于快选|工具|数字
GroupAdd, Group_SingleSpace, 工作簿 ahk_class XLMAIN	;金山Excel|独立Space模式用于快选|工具|数字
GroupAdd, Group_SingleSpace, 工作簿 ahk_class OpusApp ;金山Excel|独立Space模式用于快选|工具|数字
GroupAdd, Group_SingleSpace, 工作簿 ahk_exe et.exe	;金山Excel|独立Space模式用于快选|工具|数字
GroupAdd, Group_SingleSpace, 工作簿 ahk_exe wps.exe	;金山Excel|独立Space模式用于快选|工具|数字
GroupAdd, Group_SingleSpace, 工作簿 ahk_exe wpsoffice.exe ;金山Excel|独立Space模式用于快选|工具|数字
}
GroupAdd, Group_SingleSpace, ahk_class Photoshop	;原始Space功能和替代Ctrl+...
GroupAdd, Group_SingleSpace, ahk_class Premiere Pro ;Pr-20240321,单独Space模式
GroupAdd, Group_SingleSpace, ahk_exe JianyingPro.exe ;剪映-20240321,单独Space模式
GroupAdd, Group_SingleSpace, ahk_exe CapCut.exe	;剪映国际版-20240404,单独Space模式
GroupAdd, Group_SingleSpace, QUICKSEARCH ahk_class TQUICKSEARCH ;20240309,Ez模式
GroupAdd, Group_SingleSpace, ahk_exe WeChat.exe	;快速联系人用原始Space,会影响Space+X|C
GroupAdd, Group_SingleSpace, ahk_exe XMind.exe	;20240322,单独Space模式
; GroupAdd, Group_SingleSpace, ahk_exe Animate.exe ;原始Space功能,单独Space模式
; GroupAdd, Group_SingleSpace, ahk_class AcrobatSDIWindow ;原始Space功能
; GroupAdd, Group_SingleSpace, ahk_exe Dreamweaver.exe 	;原始Space功能
; GroupAdd, Group_SingleSpace, ahk_exe AfterFX.exe	;原始Space功能
; GroupAdd, Group_SingleSpace, ahk_class illustrator ;原始Space功能
; GroupAdd, Group_SingleSpace, ahk_exe QQ.exe ;Space模式用于工具切换,20240108
; GroupAdd, Group_SingleSpace, ahk_exe TIM.exe ;Space模式用于工具切换,20240108
; GroupAdd, Group_SingleSpace, ahk_class SciTEWindow ;符号|数字输入模式,20240311


;视频剪辑组,单独Space模式,箭头光标为原版正常模式,其他光标为Tg定制模式
GroupAdd, MediaEditor_SgSpace, ahk_class Premiere Pro ;Pr-20240321,单独Space模式
GroupAdd, MediaEditor_SgSpace, ahk_exe JianyingPro.exe ;剪映-20240321,单独Space模式
GroupAdd, MediaEditor_SgSpace, ahk_exe CapCut.exe	;剪映国际版-20240404,单独Space模式


;Space符号模式,快速输入>符号,TC中数字小键盘等
GroupAdd, SpaceKeysAsShiftInTC, ahk_exe Listary.exe
GroupAdd, SpaceKeysAsShiftInTC, ahk_class TTOTAL_CMD
GroupAdd, SpaceKeysAsShiftInTC, ahk_class EVERYTHING
GroupAdd, SpaceKeysAsShiftInTC, ahk_exe ATGUI.exe
GroupAdd, SpaceKeysAsShiftInTC, ahk_exe fupx.exe	;20240626
GroupAdd, SpaceKeysAsShiftInTC, ahk_exe FileLocatorPro.exe


;Runany黑名单,禁用全局快捷键`
; GroupAdd, Group_disableRunany, ahk_exe mainfree.exe
; GroupAdd, Group_disableRunany, ahk_exe Tdxw.exe
; GroupAdd, Group_disableRunany, ahk_exe dzh2.exe
GroupAdd, Group_disableRunany, ahk_exe Snipaste.exe	;鼠标穿透
GroupAdd, Group_disableRunany, ahk_exe PixPin.exe	;鼠标穿透


;中键(MButton)关闭标签组,单击中键缩放100%->保存/双击中键关闭
GroupAdd, Group_MButtonDoubleClose, ahk_exe Notepad++.exe
GroupAdd, Group_MButtonDoubleclose, ahk_exe MindMaster.exe
GroupAdd, Group_MButtonDoubleclose, ahk_exe XMind.exe
GroupAdd, Group_MButtonDoubleclose, ahk_class AcrobatSDIWindow
GroupAdd, Group_MButtonDoubleclose, pdf ahk_class QWidget ;wpspdf主窗口
GroupAdd, Group_MButtonDoubleclose, ahk_class Photoshop
GroupAdd, Group_MButtonDoubleclose, ahk_class DOCBOX_PDF_FRAME
GroupAdd, Group_MButtonDoubleclose, ahk_class Notepad ;Win11记事本


;中键(MButton)盲点默认不是“确定”或者OK的按钮,特殊对话框
GroupAdd, Group_MButtonClickOk, ahk_class TCheckMsgBox	;tc
GroupAdd, Group_MButtonClickOk, ahk_class TInpComboDlg	;tc
GroupAdd, Group_MButtonClickOk, ahk_class TExtMsgForm	;tc
GroupAdd, Group_MButtonClickOk, ahk_class TOverWriteForm
GroupAdd, Group_MButtonClickOk, ahk_exe QuickJump.exe
GroupAdd, Group_MButtonClickOk, 收藏 ahk_class Chrome_WidgetWin_2 ;360|msedge
GroupAdd, Group_MButtonClickOk, 书签 ahk_class Chrome_WidgetWin_1 ;chrome.exe
GroupAdd, Group_MButtonClickOk, Adobe Photoshop ahk_class PSDialogBox ;PSDialogBox
GroupAdd, Group_MButtonClickOk, Total Commander ahk_class TCOMBOINPUT ;F7新建文件夹
GroupAdd, Group_MButtonClickOk, 工作表 ahk_class bosa_sdm_XL9	;MS_Excel设置|撤销工作表保护
GroupAdd, Group_MButtonClickOk, 确认密码 ahk_class bosa_sdm_XL9 ;MS_Excel设置工作表保护密码
GroupAdd, Group_MButtonClickOk, 确认密码 ahk_class Qt5QWindowIcon ;WPS_Excel设置工作表保护密码


;中键(MButton)盲点,快速自动输入U-Key等密码口令
GroupAdd, MButtonClickPassWord, 阿里云盘分享 ahk_class Chrome_WidgetWin_1
GroupAdd, MButtonClickPassWord, 口令 ahk_class #32770 ahk_exe Acrobat.exe
GroupAdd, MButtonClickPassWord, 文档属性 ahk_class #32770 ahk_exe Acrobat.exe


;中键(MButton)盲点,快速自动输入Cqc-U-Key密码口令
GroupAdd, Group_CQCCAPassWord, inputpasswdui ahk_class #32770
GroupAdd, Group_CQCCAPassWord, 核对数字证书口令 ahk_class #32770
GroupAdd, Group_CQCCAPassWord, ahk_class Dialog ahk_exe certTool.exe
GroupAdd, Group_CQCCAPassWord, Verify PIN ahk_class #32770


;微信中的页面,增加单键翻页,双击右键,关闭退出
GroupAdd, 微信中的页面, 微信 ahk_class CefWebViewWnd
GroupAdd, 微信中的页面, 微信 ahk_class Chrome_WidgetWin_0
GroupAdd, 微信中的页面, 公众号 ahk_class H5SubscriptionProfileWnd
GroupAdd, 微信中的页面, ahk_class FileManagerWnd


;Ctrl+A增强组,增加全选后带默认复制动作
GroupAdd, Group_ableCtrlAC, ahk_exe IDMan.exe


;EZTC-32|64位版中禁用右键(RButton)双击返回上级目录. EZTC自带此功能,配合设定鼠标模式为Win右键
GroupAdd, Group_disableRButtonBackspace, 10.50 - Frank Zheng ahk_exe TOTALCMD.EXE	;32位
GroupAdd, Group_disableRButtonBackspace, 11.02 - Frank Zheng ahk_exe TOTALCMD.EXE	;32位
GroupAdd, Group_disableRButtonBackspace, 11.03 - Frank Zheng ahk_class TTOTAL_CMD	;32|64位


;Listary5|6搜索框增强.功能:双右键=ESC,双Caps={Ctrl 2},单击Caps={Ctrl},结果列表跳转到资管等
GroupAdd, Group_ListarySearchBox, ahk_class Listary_WidgetWin_0	;Listary5搜索框
GroupAdd, Group_ListarySearchBox, ahk_class Listary_WidgetWin_1	;Listary5搜索框
GroupAdd, Group_ListarySearchBox, Search ahk_exe Listary.exe	;Listary6搜索框
GroupAdd, Group_ListarySearchBox, SearchBarWindow ahk_exe Listary.exe ;Listary6
GroupAdd, Group_ListarySearchBox, LauncherSearchWindow ahk_exe Listary.exe ;Listary6
GroupAdd, Group_ListarySearchBox, Listary ahk_exe Listary.exe ;Listary6.3文件搜索窗口


;不支持Space+LBM|RBM模式切换窗口的组,20240325
GroupAdd, DisableGridMove, ahk_class Photoshop
; GroupAdd, DisableGridMove, ahk_exe Acrobat.exe


;支持Ctrl+双击X|C|V,复制文件名,路径的资管和搜索|程序,20240322
GroupAdd, CtrlCDbCopyName_Path, ahk_exe explorer.exe
GroupAdd, CtrlCDbCopyName_Path, ahk_exe Listary.exe
GroupAdd, CtrlCDbCopyName_Path, ahk_class TTOTAL_CMD
GroupAdd, CtrlCDbCopyName_Path, ahk_class EVERYTHING
GroupAdd, CtrlCDbCopyName_Path, ahk_exe ATGUI.exe
GroupAdd, CtrlCDbCopyName_Path, ahk_exe FileLocatorPro.exe


;CapsLock|Tab|Enter禁用自动保存的组,如Listary|搜索|资管等,20240325
GroupAdd, DisableAutoSave, ahk_exe explorer.exe
GroupAdd, DisableAutoSave, ahk_exe Listary.exe
; GroupAdd, DisableAutoSave, ahk_class TTOTAL_CMD	;单独,取消选择+ESC
; GroupAdd, DisableAutoSave, ahk_class EVERYTHING	;单独,点击过滤按钮
GroupAdd, DisableAutoSave, ahk_exe ATGUI.exe
GroupAdd, DisableAutoSave, ahk_exe FileLocatorPro.exe


;Enter激活自动保存的组,如Code|Office|Wps系列软件的组,20240325
GroupAdd, EnterableAutoSave, ahk_class SciTEWindow


;3D建模软件,关闭Shift+MButton打印功能,用于模型查看,20240520
GroupAdd, 3DDisableMButtonPrint, ahk_exe parametric.exe	;Proe|Core
GroupAdd, 3DDisableMButtonPrint, ahk_exe SLDWORKS.exe	;SolidWorks
GroupAdd, 3DDisableMButtonPrint, ahk_exe ugraf.exe	;Ug
GroupAdd, 3DDisableMButtonPrint, ahk_exe CNEXT.exe	;Catia
GroupAdd, 3DDisableMButtonPrint, ahk_exe Rhino.exe	;犀牛
GroupAdd, 3DDisableMButtonPrint, ahk_exe CDRAFT_M.exe	;Caxa
GroupAdd, 3DDisableMButtonPrint, ahk_exe Edge.exe	;SolidEdge
GroupAdd, 3DDisableMButtonPrint, ahk_exe acad.exe	;acad
GroupAdd, 3DDisableMButtonPrint, ahk_exe zwacad.exe	;中望acad
GroupAdd, 3DDisableMButtonPrint, ahk_exe Inventer.exe	;Inventer
GroupAdd, 3DDisableMButtonPrint, ahk_exe 3dsmax.exe	;3dsmax
GroupAdd, 3DDisableMButtonPrint, ahk_exe Revit.exe	;Revit


;恢复微信,输入法,Listary搜索等软件中原始Tab功能或其他单独Tab模式,而非通用模式,20240520
GroupAdd, Group_SingleTab, ahk_exe WeChat.exe 	;启用原始Tab激活填充快速联系人
GroupAdd, Group_SingleTab, ahk_class BaseGui	;讯飞拼音,原始Tab激活填充快速联系人
GroupAdd, Group_SingleTab, ahk_class SoPY_Comp	;搜狗拼音,原始Tab激活填充快速联系人
GroupAdd, Group_SingleTab, ahk_class WanNengWBMerge	;万能五笔,原始Tab激活填充快速联系人
GroupAdd, Group_SingleTab, ahk_class mscandui21.candidate	;微软拼音,原始Tab激活填充快速联系人
GroupAdd, Group_SingleTab, ahk_class wetype.flutter.setting ;微信拼音,原始Tab激活填充快速联系人
GroupAdd, Group_SingleTab, ahk_class BAIDU_CLASS_IME_87C946A9-47CC-4068-A02B-9381C1F11B24 ;百度拼音
GroupAdd, Group_SingleTab, ahk_class ahk_class ApplicationFrameWindow	;新微软拼音,原始Tab激活填充快速联系人


;（注：ahk_class后面是AHK检测出的class名）
;************** group定义$ **************
}

;**************脚本初始设置^ ****************** {{{1
{
; Label_DefVar: ; 初始化变量|设置
global INI := % A_ScriptDir "\Capstg_Win1X.ini" ; 配置文件
global GV_ToggleWheelOnCursor,COMMANDER_PATH,SOFTDIR,ScreenShotPath,RunCaptX,QQTimFPath

;设定5分钟重启一次脚本，防止卡键 1000*60*15
GV_ReloadTimer := % 1000*60*5
GV_ToggleReload := 1
gosub,Label_ReadINI
gosub,AutoReloadInit
gosub,CreatTrayMenu

;Esc键的作用，默认WinClose，作为alt+f4关闭程序，可选CapsLock，作为切换大小写
;GV_EscKeyAs := "WinClose"
GV_EscKeyAs := "Escape"
;GV_EscKeyAs := "CapsLock"
;GV_EscKeyAs := "BackSpace"

;启动器选择，可选为popsel和qsel
;GV_PopSel_QSel := "popsel"
GV_PopSel_QSel := "qsel"

;是否启用光标下滚轮, 默认关闭,0 (Win7设置为1)
; GV_ToggleWheelOnCursor := 0

;tab系列组合键，适合左键右鼠，启用后直接按tab会感觉有一点延迟，默认开启，开关为ctrl+win+alt+花号
GV_ToggleTabKeys := 1

;启用`花号键，与ranany冲突，默认关闭,0
GV_ToggleHhjKeys := 0

;启用左手CapsLock+EDSF,鼠标移动模拟,默认关闭,0,开关为ctrl+win+alt+M
GV_ToggleCapsMouseMove := 0

;启用空格系列快捷键，Code等文本编辑器启用,会影响打字，在tc中会不能按住连选文件，默认开启，开关为ctrl+win+alt+空格
GV_ToggleSpaceKeys := 1

;空格键模式切换, 默认和Capslock模式一致,启用1
;Capslock模式,通用,Space+EDSF=上下左右,约等于Ctrl
GV_SpaceKeysAsCapsLock := 1

;Shift符号模式,在Code|TC等中,左手符号|数字快速输入,启用1
GV_SpaceKeysAsShiftInTC := 1

;在浏览器中启用空格系列快捷键, 默认开启,1
GV_GroupBrowserToggleSpaceKeys := 1

;在浏览器中切换滚轮模式，默认关闭,0
;视频中滚轮为左右快进，主要是用来看视频网站，开关默认为左边alt加空格或者双击侧边键1
global GV_GroupBrowserToggleWheelModeLeftRight := 0

;页面中滚轮为翻页，几行和一页间切换
global GV_GroupBrowserToggleWheelModeUpDown := 0

;在浏览器中切换侧边键作为中键模式
global GV_GroupBrowserToggleMButtonMode := 0

;在Totalcmd中使用数字键，单击快速打开文件，双击跳转标签, 默认关闭,0
GV_TotalcmdToggleJumpByNumber := 0	;20240402,默认关闭,便于小白用户使用

;单键模式，开关按键为caps+/, 默认关闭,0
GV_ToggleKeyMode := 0

;置顶模式，开关按键为#F1, 默认关闭,0
GV_ToggleAlwaysontopKeys := 0

;截图文件临时变量
global SSFileName

;截图的时候同时进剪贴板
global GV_ScreenShot2Clip := 1

;64位的Win7下，在输入框中是148003967
GV_CursorInputBox_64Win710 := 148003967

;正常鼠标指针
GV_CursorNormal_64Win710 := 124973738

;超链接鼠标指针
GV_CursorClick_64Win710 := 1197314685

GV_CursorInputBox := GV_CursorInputBox_64Win710

GV_CursorClick := GV_CursorClick_64Win710

GV_CursorNormal := GV_CursorNormal_64Win710

;处于编辑状态
GV_Edit_Mode := 0

;东方财富F10
gv_url_tdx_f10 := "http://data.eastmoney.com/notices/stock/"
gv_url_html := ".html"

;****************************************************
;设置TC主目录在本机的位置
;global COMMANDER_PATH := % A_ScriptDir
;global COMMANDER_PATH := "D:\RunCaptX\GreenTools\Totalcmd\SoftDir\totalcmd_ez"
;global COMMANDER_PATH := "D:\RunCaptX\SoftDir\totalcmd_ez"	;
if A_Is64bitOS AND FileExist(COMMANDER_PATH . "\" . "TOTALCMD64.EXE"){
	COMMANDER_NAME := "TOTALCMD64.EXE"
} else {
	COMMANDER_NAME := "TOTALCMD.EXE"
}
global COMMANDER_EXE := COMMANDER_PATH . "\" . COMMANDER_NAME
EnvSet,COMMANDER_PATH, %COMMANDER_PATH%
EnvSet,COMMANDER_EXE, %COMMANDER_EXE%
EnvSet,RunCaptX ,%RunCaptX%
; EnvSet,RunCaptX , D:\RunCaptX

;GV_ToolsPath := % GF_GetSysVar("ToolsPath")
GV_TempPath := % GF_GetSysVar("TEMP")
RunCaptX := % GF_GetSysVar("RunCaptX")

;绿软根目录SoftDir，默认在tc目录的上一层，这里是脚本内的环境变量，所有从ahk中启动的程序都会继承这个变量，
;如果电脑相对固定，则可以考虑在右键菜单中选添加系统的环境变量固定下来
;用EnvUpdate 会导致卡顿
SOFTDIR := % GF_GetSysVar("SoftDir")
if !SOFTDIR
{
	SOFTDIR := RegExReplace(COMMANDER_PATH,"\\[^\\]+\\?$")
	EnvSet,SoftDir, % SOFTDIR
}
;****************************************************
;执行自动绿化,脚本开机自启
;GV_AutoGreenPath := 1
;Gosub,AutoGreenPath

;GV_AutoPrepareMenuTab := 1
;Gosub,PrepareMenuTab

GV_AutoStart := 0	;by Tuutg,20240622
if GV_AutoStart
	Gosub,AutoStart	;开机自启


;默认双击快捷键间隔300微秒
GV_KeyTimer := 300
GV_MouseTimer := 400
GV_KeyClickAction1 :=
GV_KeyClickAction2 :=
GV_KeyClickAction3 :=

;长按的按钮，0为默认不管，1左键2右键3中键
GV_MouseButton := 0
GV_LongClickAction :=

TC_Msg := 1075
CM_OpenDrives := 2122
CM_OpenDesktop := 2121
CM_OpenPrinters := 2126
CM_OpenNetwork := 2125
CM_OpenControls := 2123
CM_OpenRecycled := 2127
CM_CopySrcPathToClip := 2029
CM_CopyFullNamesToClip := 2018
CM_ConfigSaveDirHistory := 582

;ScreenShotPath := "C:\Temp\"

;Tim中座标位置
Tim_Start_X := 100
Tim_Start_Y := 100
Tim_Bar_Height := 60

;QQ中座标位置
QQ_Start_X := 100
QQ_Start_Y := 30
QQ_Bar_Height := 45

WX_Start_X := 180
WX_Start_Y := 100
WX_Bar_Height := 62

TG_Start_X := 100
TG_Start_Y := 110
TG_Bar_Height := 62

;****************************************************

;自动保存开关,Tab|Enter激活自动保存,初始未保存会有弹出提示,开关按键为^#+a,默认关闭,0
; GV_ToggleAotuSaveMode := 0

;****************************************************
;用ramdisk的时候，有时候不能自动的建立Temp目录
;FileDelete,% GV_TempPath
;FileCreateDir, % GV_TempPath
;Run nircmd execmd mkdir "%GV_TempPath%"
;FileCreateDir, % GV_TempPath . "\ChromeCache"
;**************脚本初始设置$ ******************

;************** 在光标下方滚轮^ ************** {{{1
;Autoexecute code
MinLinesPerNotch := 1
MaxLinesPerNotch := 5
AccelerationThreshold := 100
AccelerationType := "L" ;Change to "P" for parabolic acceleration
StutterThreshold := 10

;************** 在光标下方滚轮开始^ ************** {{{2
;Function definitions
;See above for details on parameters
FocuslessScroll(MinLinesPerNotch, MaxLinesPerNotch, AccelerationThreshold, AccelerationType, StutterThreshold)
{
	SetBatchLines, -1 ;Run as fast as possible
	CoordMode, Mouse, Screen ;All coords relative to screen

	;Stutter filter: Prevent stutter caused by cheap mice by ignoring successive WheelUp/WheelDown events that occur to close together.
	If(A_TimeSincePriorHotkey < StutterThreshold) ;Quickest succession time in ms
		If(A_PriorHotkey = "WheelUp" Or A_PriorHotkey ="WheelDown")
			Return

	MouseGetPos, m_x, m_y,, ControlClass2, 2
	ControlClass1 := DllCall( "WindowFromPoint", "int64", (m_y << 32) | (m_x & 0xFFFFFFFF), "Ptr") ;32-bit and 64-bit support

	lParam := (m_y << 16) | (m_x & 0x0000FFFF)
	wParam := (120 << 16) ;Wheel delta is 120, as defined by MicroSoft

	;Detect WheelDown event
	If(A_ThisHotkey = "WheelDown" Or A_ThisHotkey = "^WheelDown" Or A_ThisHotkey = "+WheelDown" Or A_ThisHotkey = "*WheelDown")
		wParam := -wParam ;If scrolling down, invert scroll direction

	;Detect modifer keys held down (only Shift and Control work)
	If(GetKeyState("Shift","p"))
		wParam := wParam | 0x4
	If(GetKeyState("Ctrl","p"))
		wParam := wParam | 0x8

	;Adjust lines per notch according to scrolling speed
	Lines := LinesPerNotch(MinLinesPerNotch, MaxLinesPerNotch, AccelerationThreshold, AccelerationType)

	If(ControlClass1 != ControlClass2)
	{
		loop %Lines%
		{
			SendMessage, 0x20A, wParam, lParam,, ahk_id %ControlClass1%
			SendMessage, 0x20A, wParam, lParam,, ahk_id %ControlClass2%
		}
	}
	else
	{
		SendMessage, 0x20A, wParam * Lines, lParam,, ahk_id %ControlClass1%
	}
}

;All parameters are the same as the parameters of FocuslessScroll()
;Return value: Returns the number of lines to be scrolled calculated from the current scroll speed.
LinesPerNotch(MinLinesPerNotch, MaxLinesPerNotch, AccelerationThreshold, AccelerationType)
{
	T := A_TimeSincePriorHotkey

	if((T > AccelerationThreshold) Or (T = -1)) ;T = -1 if this is the first Hotkey ever Run
	{
		Lines := MinLinesPerNotch
	}
	else
	{
		if(AccelerationType = "P")
		{
			A := (MaxLinesPerNotch-MinLinesPerNotch)/(AccelerationThreshold**2)
			B := -2 * (MaxLinesPerNotch - MinLinesPerNotch)/AccelerationThreshold
			C := MaxLinesPerNotch
			Lines := Round(A*(T**2) + B*T + C)
		}
		else
		{
			B := (MinLinesPerNotch-MaxLinesPerNotch)/AccelerationThreshold
			C := MaxLinesPerNotch
			Lines := Round(B*T + C)
		}
	}
	Return Lines
}
}
;在任务栏上滚轮调整音量等 {{{2
#If MouseIsOver("ahk_class Shell_TrayWnd") or MouseIsOver("ahk_class Shell_SecondaryTrayWnd")
WheelUp::gosub,Sub_volUp
WheelDown::gosub,Sub_volDown

;中键静音
$MButton::gosub,Sub_volMute
$!MButton::gosub,Sub_volMute

;启动svv
;~LButton::
	;GV_LongClickAction := "gosub,Sub_sv"
	;GV_MouseButton := 1
	;gosub,Sub_ButtonLongPress
;return
;****************************************************
$!q::ControlSend,,#q	;Windows1X_搜索中心

CapsLock & w:: ;启动激活微信|微信文件传输助手
	IfWinNotExist,ahk_exe WeChat.exe
	{
		gosub,RunWechat	;启动激活微信
		WinActivate, ahk_exe WeChat.exe
		WinWait, ahk_exe WeChat.exe
	}
	else {
		SendInput,^!w	;激活微信,20231216
		Sleep,100
		gosub,微信文件传输助手
	}
return

$!e::Run,%RunCaptX%\SoftDir\AutoHotkey\SciTE\SciTE.exe

$!r::	;启动激活Rolan
	IfWinNotExist,Rolan ahk_exe Rolan.exe
	{
		Run,%RunCaptX%\SoftDir\Rolan\Rolan.exe
		WinWait, Rolan ahk_exe Rolan.exe
		SendInput,!r
		}
	else {
		SendInput,!r
	}
return

$!t::	;呼出任务管理器窗口|tagLyst主窗口
	if WinExist("ahk_exe tagLyst.exe"){
		SendInput,^#t	;呼出/隐藏tagLyst主窗口
		WinActivate, tagLyst ahk_class Chrome_WidgetWin_1
		WinWait, tagLyst ahk_class Chrome_WidgetWin_1
		WinMove, tagLyst ahk_class Chrome_WidgetWin_1, , 0, 0, A_ScreenWidth/2-1, A_ScreenHeight-75
	}
	else {
		SendInput,^+{Esc}	;呼出任务管理器窗口
	}
return
;****************************************************
; $!a::SendInput,!a ;微信截图|一键Anytext搜索|Listray_智能命令

$!s::
	if WinExist("ahk_exe WindowSpy.exe"){
		WinActivate, Window Spy ahk_class AutoHotkeyGUI
	}
	else {
		Run,%RunCaptX%\SoftDir\AutoHotkey\WindowSpy.exe
		WinActivate, Window Spy ahk_class AutoHotkeyGUI
	}
return

$!d::SendInput,#d	;呼出/隐藏桌面

$!f::gosub,TextToEverything	;一键everything搜索

$!g:: ;SendInput,#!t	;呼出Utools_goole_翻译;设定翻译-#!t,20240518
	if WinExist("ahk_exe Utools.exe"){
			SendInput,#!t	;呼出Utools_goole_翻译
		}
		else {		;呼出Ra插件管理,by Tuutg,20240518
			SendInput,{Blind}#!g
		}
return
;****************************************************
;$!z::	;预留
$!x::	;启动AnyTXT Searcher
	IfWinNotExist,ahk_exe ATGUI.exe
	{
	Run,"%RunCaptX%\SoftDir\AnyTXT Searcher\ATGUI.exe"
	WinActivate, ATGUI.exe
	WinWait, ATGUI.exe
	}
return

$!c:: ;gosub,DbRButtonGoDesktop	;预留给UTools,OCR_识别复制|显示桌面
	if (WinExist("ahk_exe Loom.exe") or WinExist("ahk_exe uTools.exe")){
		SendInput,!c	;呼出Loom|Utools_OCR_识别复制
	}
	else {	;显示桌面
		SendInput,#d	;显示桌面
	}
return

$!v::
	IfExist, D:\Program Files\Microsoft VS Code\Code.exe
		Run,"D:\Program Files\Microsoft VS Code\Code.exe"	;呼出Code.exe
	else IfExist,%RunCaptX%\SoftDir\Notepad--\notepad--.exe
		Run,%RunCaptX%\SoftDir\Notepad--\notepad--.exe	;呼出Notpad--
	else Run,notepad.exe ;系统自带notepad.exe
return

$!b::	;呼出Bandicam
	IfExist, %RunCaptX%\SoftDir\SCapture\Bandicam\Bandicam.exe
	Run,%RunCaptX%\SoftDir\SCapture\Bandicam\Bandicam.exe
return
;****************************************************
$!w::	;激活TOTAL_CMD,并开启半屏显示
	IfWinNotExist,ahk_class TTOTAL_CMD
	{
		Run, %COMMANDER_EXE%
		WinActivate, ahk_class TTOTAL_CMD
		WinWait, ahk_class TTOTAL_CMD
		WinMove, ahk_class TTOTAL_CMD, , 0, 0, A_ScreenWidth/2-1, A_ScreenHeight-75
	}
	else {
		WinActivate, ahk_class TTOTAL_CMD
		WinWait, ahk_class TTOTAL_CMD
		WinMove, ahk_class TTOTAL_CMD, , 0, 0, A_ScreenWidth/2-1, A_ScreenHeight-75
	}
return

;任务栏,双击右键,显示桌面
$RButton::	;20240317
	;GV_MouseTimer := 300
	GV_KeyClickAction1 := "SendInput,{Click,Right}"	;系统默认,RButton
	GV_KeyClickAction2 := "SendInput,#d" ;显示桌面
	GV_LongClickAction := "SendInput,{Click,Right}"	;系统默认,RButton
	gosub,Sub_MouseClick123
return
#If

Sub_volDown:
	SetTimer,SliderOff,1000
	SoundSet,-2
	gosub,DisplaySlider
return

Sub_volUp:
	SetTimer,SliderOff,1000
	SoundSet,+2
	gosub,DisplaySlider
return

Sub_volMute:
	SetTimer,SliderOff,1000
	SoundSet, +1, , mute
	SoundGet, master_mute, , mute
	if master_mute = Off
		gosub,DisplaySlider
	else if master_mute = On
		Progress,0,0, ,音量大小
return

SliderOff:
	Progress,Off
return

DisplaySlider:
	SoundGet,Volume
	Volume:=Round(Volume)
	Progress,%Volume%,%Volume%, ,音量大小
return

Sub_sv:
	Run, SndVol.exe
return

#If WinActive("ahk_exe SndVol.exe")
	$RButton::WinClose A
	$MButton::WinClose A
#If

Sub_svv:
	Run, soundvolumeview.exe
return

#If WinActive("ahk_exe soundvolumeview.exe")
$MButton::SendInput, ^{6 10}

;5%
k::SendInput, ^4
j::SendInput, ^3
WheelUp::SendInput, ^4
WheelDown::SendInput, ^3

; 1%
!k::SendInput, ^2
!j::SendInput, ^1
!WheelUp::SendInput, ^2
!WheelDown::SendInput, ^1

; 10%
^k::SendInput, ^4
^j::SendInput, ^3
^WheelUp::SendInput, ^4
^WheelDown::SendInput, ^3

; Toggle mute
m::SendInput, {F9}
Esc::SendInput, !fx

$RButton::
	;GV_MouseTimer := 400
	GV_KeyClickAction1 := "SendInput,{Click,Right}"	;系统默认,RButton
	GV_KeyClickAction2 := "SendInput,!fx"
	gosub,Sub_MouseClick123
return
#If

;Win10里面已经不需要光标下滚轮这个功能
#If (GV_ToggleWheelOnCursor=1) and (A_OSVersion in WIN_2003,WIN_XP,WIN_7)
{
WheelUp::FocuslessScroll(MinLinesPerNotch, MaxLinesPerNotch, AccelerationThreshold,AccelerationType, StutterThreshold)
WheelDown::FocuslessScroll(MinLinesPerNotch, MaxLinesPerNotch, AccelerationThreshold,AccelerationType, StutterThreshold)
^WheelUp::SendInput,^{WheelUp}
^WheelDown::SendInput,^{WheelDown}
!WheelUp::SendInput,!{WheelUp}
!WheelDown::SendInput,!{WheelDown}
}
#If

;************** 在光标下方滚轮结束 ************** {{{2


;************** 定时重启脚本部分，别动位置^ ************** {{{1
AutoReloadInit:
	SetTimer, ForceSelfReload, % GV_ReloadTimer
return

SelfReload:
	if (GV_ToggleReload && !GV_GroupBrowserToggleMButtonMode && !GV_GroupBrowserToggleWheelModeLeftRight && !GV_GroupBrowserToggleWheelModeUpDown)
	{
		SendInput,{Space Up}
		SendInput,{Tab up}
		SendInput,{CapsLock Up}
		SendInput,{ScrollLock Up}

		SendInput,{LWin Up}
		SendInput,{RWin Up}

		SendInput,{Shift Up}
		SendInput,{LShift Up}
		SendInput,{RShift Up}

		SendInput,{Alt Up}
		SendInput,{LAlt Up}
		SendInput,{RAlt Up}

		SendInput,{Control Up}
		SendInput,{LControl Up}
		SendInput,{RControl Up}

		SendInput,{Volume_Down Up}
		SendInput,{Volume_Up Up}
		;SendInput,{Volume_Mute Up}
		Sleep 100
		Reload
	}
return

ForceSelfReload:
	SendInput,{Space Up}
	SendInput,{Tab up}
	SendInput,{CapsLock up}
	SendInput,{ScrollLock Up}

	SendInput,{LWin Up}
	SendInput,{RWin Up}

	SendInput,{Shift Up}
	SendInput,{LShift Up}
	SendInput,{RShift Up}

	SendInput,{Alt Up}
	SendInput,{LAlt Up}
	SendInput,{RAlt Up}

	SendInput,{Control Up}
	SendInput,{LControl Up}
	SendInput,{RControl Up}

	SendInput,{F1 Up}
	SendInput,{F2 Up}
	SendInput,{F3 Up}
	SendInput,{F4 Up}
	SendInput,{F5 Up}
	SendInput,{F6 Up}
	SendInput,{F7 Up}
	SendInput,{F8 Up}
	SendInput,{F9 Up}
	SendInput,{F10 Up}
	SendInput,{F11 Up}
	SendInput,{F12 Up}

	SendInput,{Volume_Down Up}
	SendInput,{Volume_Up Up}
	;SendInput,{Volume_Mute Up}
	Sleep 100
	Reload
return
;************** 定时重启脚本部分，别动位置$ **************


;************** 自动绿化,脚本开机自启部分 ************** {{{1
;AutoGreenPath:	;自动绿化
;	if not GV_AutoGreenPath
;		return
;	open_folder_command2=$exec("D:\Tools\totalcmd11_ez\TOTALCMD.EXE" /A /T /O /S /L="%1")
;	$exec("D:\Tools\totalcmd11_ez\TOTALCMD.EXE" /A /T /O /S /L="%1")
;	8 $exec("
;	4 .exe
;	IniRead, OutputEv, %A_scriptDir%\Everything.ini, Everything,open_folder_command2
;	IniRead, OutputEv, %COMMANDER_PATH%\Everything.ini, Everything,open_folder_command2
;	iniexe := SubStr(OutputEv,8,InStr(OutputEv,".exe")-4)
;	if (iniexe = COMMANDER_EXE) {
;		return
;	}
;	else {
;		; run,%A_scriptDir%\Tools\nircmd.exe killprocess Everything.exe
;		run,%COMMANDER_PATH%\Tools\nircmd.exe killprocess Everything.exe
;		Gosub,Menu_GreenPath
;		Eztip("已自动绿化",2)
;	}
;return

/*
PrepareMenuTab:	;TC收藏夹
	if not GV_AutoPrepareMenuTab
		return
	; mtini := % A_ScriptDir . "\USER\MenuTab_" . GF_GetSysVar("COMPUTERNAME") . ".INI"
	mtini := % COMMANDER_PATH . "\USER\MenuTab_" . GF_GetSysVar("COMPUTERNAME") . ".INI"
	IfExist, %mtini%
		return
	else
		; FileCopy,%A_ScriptDir%\USER\MenuTab.INI,%mtini%
		FileCopy,%COMMANDER_PATH%\USER\MenuTab.INI,%mtini%
		; eztip("已准备Totalcmd的收藏夹和标签",2)
return
*/

AutoStart:	;脚本开机自启. by Tuutg,20240622
	if A_Is64bitOS
		SetRegView 64
	; RegRead, OutputVar, HKEY_LOCAL_MACHINE, Software\Microsoft\Windows\CurrentVersion\Run, Capsez
	RegRead, OutputVar, HKEY_LOCAL_MACHINE, Software\Microsoft\Windows\CurrentVersion\Run, Capstg
	if OutputVar
	{
		; RegDelete, HKEY_LOCAL_MACHINE, Software\Microsoft\Windows\CurrentVersion\Run, Capsez
		; RegDelete, HKEY_LOCAL_MACHINE, Software\Microsoft\Windows\CurrentVersion\Run, Capstg
		; EzTip("已经是Capstg随系统自动启动",2)
		return
	}
	else
	{
		; RegWrite, REG_SZ, HKEY_LOCAL_MACHINE, Software\Microsoft\Windows\CurrentVersion\Run, Capsez, "%A_AhkPath%"
		RegWrite, REG_SZ, HKEY_LOCAL_MACHINE, Software\Microsoft\Windows\CurrentVersion\Run, Capstg, "%A_AhkPath%"
		EzTip("已设置Capstg随系统自动启动",2)
	}
return
;************** 自动绿化,脚本开机自启部分$ **************


;************** caps+鼠标滚轮调整窗口透明度^ ************** {{{1
;caps+鼠标滚轮调整窗口透明度（设置30-255的透明度，低于30基本上就看不见了，如需要可自行修改）
;~LShift & WheelUp::
CapsLock & WheelUp::
	;透明度调整，增加。
	WinGet, Transparent, Transparent,A
	if (Transparent="")
		Transparent=255
		Transparent_New:=Transparent+20
	if (Transparent_New > 254)
		Transparent_New =255
		WinSet,Transparent,%Transparent_New%,A

		ToolTip 原透明度: %Transparent_New% `n新透明度: %Transparent%
		SetTimer, RemoveToolTip_transparent_Lwin, 1500
return

CapsLock & WheelDown::
	;透明度调整，减少。
	WinGet, Transparent, Transparent,A
	if (Transparent="")
		Transparent=255
	Transparent_New:=Transparent-20
	if (Transparent_New < 30)
		Transparent_New = 30
	WinSet,Transparent,%Transparent_New%,A
	ToolTip 原透明度: %Transparent_New% `n新透明度: %Transparent%
	SetTimer, RemoveToolTip_transparent_Lwin, 1500
return

;设置CapsLock 加侧边键 直接恢复透明度到255。没有侧边键的就算了，毕竟滚轮滚一下也快得很
;CapsLock & XButton1::
	;WinGet, Transparent, Transparent,A
	;WinSet,Transparent,255,A
	;ToolTip 恢复透明度
	;SetTimer, RemoveToolTip_transparent_Lwin, 1500
;return

RemoveToolTip_transparent_Lwin:
	ToolTip
	SetTimer, RemoveToolTip_transparent_Lwin, Off
return

;************caps+鼠标滚轮调整窗口透明度$***********


;************** 按住Caps拖动鼠标^    ************** {{{1
;按住caps加左键拖动窗口
Capslock & LButton::
;Escape & LButton::
	CoordMode, Mouse  ; Switch to screen/absolute coordinates.
	MouseGetPos, EWD_MouseStartX, EWD_MouseStartY, EWD_MouseWin
	WinGetPos, EWD_OriginalPosX, EWD_OriginalPosY,,, ahk_id %EWD_MouseWin%
	WinGet, EWD_WinState, MinMax, ahk_id %EWD_MouseWin%
	if EWD_WinState = 0 ; Only if the window isn't maximized
		SetTimer, EWD_WatchMouse, 10 ; Track the mouse as the user drags it.
return

EWD_WatchMouse:
	GetKeyState, EWD_LButtonState, LButton, P
	if EWD_LButtonState = U ; Button has been released, so drag is complete.
	{
		SetTimer, EWD_WatchMouse, off
		return
	}

	;GetKeyState, EWD_EscapeState, Escape, P
	;if EWD_EscapeState = D  ; Escape has been pressed, so drag is cancelled.
	;{
	;	SetTimer, EWD_WatchMouse, off
	;	WinMove, ahk_id %EWD_MouseWin%,, %EWD_OriginalPosX%, %EWD_OriginalPosY%
	;	return
	;}

	;Otherwise, reposition the window to match the change in mouse coordinates
	;caused by the user having dragged the mouse:
	CoordMode, Mouse
	MouseGetPos, EWD_MouseX, EWD_MouseY
	WinGetPos, EWD_WinX, EWD_WinY,,, ahk_id %EWD_MouseWin%
	SetWinDelay, -1 ; Makes the below move faster/smoother.
	WinMove, ahk_id %EWD_MouseWin%,, EWD_WinX + EWD_MouseX - EWD_MouseStartX, EWD_WinY + EWD_MouseY - EWD_MouseStartY
	EWD_MouseStartX := EWD_MouseX ; Update for the next timer-call to this subroutine.
	EWD_MouseStartY := EWD_MouseY
return

;按住caps加右键放大和缩小窗口
Capslock & RButton::
;Escape & RButton::
	CoordMode, Mouse, Screen ; Switch to screen/absolute coordinates.
	MouseGetPos, EWD_MouseStartX, EWD_MouseStartY, EWD_MouseWin
	WinGetPos, EWD_OriginalPosX, EWD_OriginalPosY, EWD_WinWidth, EWD_WinHeight, ahk_id %EWD_MouseWin%
	EWD_StartPosX := EWD_WinWidth - EWD_MouseStartX
	EWD_StartPosY := EWD_WinHeight - EWD_MouseStartY

	if ((EWD_MouseStartX - EWD_OriginalPosX)/EWD_WinWidth)<0.5 && ((EWD_MouseStartY - EWD_OriginalPosY)/EWD_WinHeight)<0.5
		LeftUpCorner = 1
	else
		LeftUpCorner = 0
		WinGet, EWD_WinState, MinMax, ahk_id %EWD_MouseWin%
	if EWD_WinState = 0 ; Only if the window isn't maximized
		SetTimer, EWD_ResizeWindow, 10 ; Track the Mouse as the user drags it.
return

EWD_ResizeWindow:
	if not GetKeyState("RButton", "P"){
		SetTimer, EWD_ResizeWindow, off
		return
	}
		CoordMode, Mouse, Screen ; switch to Screen/absolute coordinates.
		MouseGetPos, EWD_MouseX, EWD_MouseY
		SetWinDelay, -1 ; Makes the below move faster/smoother.
	if LeftUpCorner
		WinMove, ahk_id %EWD_MouseWin%,, EWD_OriginalPosX-(EWD_MouseStartX-EWD_MouseX), EWD_OriginalPosY-(EWD_MouseStartY-EWD_MouseY), EWD_WinWidth+(EWD_MouseStartX-EWD_MouseX),EWD_WinHeight+(EWD_MouseStartY-EWD_MouseY)
	else
		WinMove, ahk_id %EWD_MouseWin%,, EWD_OriginalPosX, EWD_OriginalPosY, EWD_StartPosX + EWD_MouseX, EWD_StartPosY + EWD_MouseY
return
;************** 按住Caps拖动窗口$    **************

;按住Caps加中键窗口最大化|常规大小循环切换
CapsLock & MButton::gosub,Sub_MaxRestore

;LWin & LButton::gosub,Sub_MaxRestore
;Win加右键给了popsel作为启动器，关键是滥用置顶并不好，所以给难受的中键

;对于置顶最好用快捷键来的更准确一点,加入提示,方便置顶查看状态
LWin & MButton::
#F1::
	WinSet, Alwaysontop, Toggle, A
	GV_ToggleAlwaysontopKeys := !GV_ToggleAlwaysontopKeys
	if(GV_ToggleAlwaysontopKeys == 1)
	ToolTip 置顶启用
	else
	ToolTip 置顶关闭
	Sleep 1000
	ToolTip
return

;从默认Ctrl＋W是关闭标签上修改一点成关闭程序。
;Escape & LButton::WinClose A
#w::WinClose A

;按住Win加滚轮来调整音量大小
LWin & WheelUp::gosub,Sub_volUp
LWin & WheelDown::gosub,Sub_volDown
;************** 按住Caps拖动窗口$ ********************


;************** 自定义方法^ ************** {{{1
;自定义方法/函数
MouseIsOver(WinTitle){
	MouseGetPos,,, Win
return WinExist(WinTitle . " ahk_id " . Win)
}

;fp,全路径文件名0 1路径,2全文件名,3仅文件名,4扩展名,5添加64字样
GetFileInfo(fp,act){
	;D:\Tools\Office\EverEdit\eeie.exe
	;InStr(Haystack, Needle [, CaseSensitive = false, StartingPos = 1, Occurrence = 1]):
	;SubStr(String, StartingPos [, Length])

	dot := InStr(fp,".",false,0,1)	;返回指定字符在字符串中位置编号
	slash := InStr(fp,"\",false,0,1)

	if(act==0)
		return % fp	;全路径文件名
	else if(act==1)
		return % SubStr(fp,1,slash)	;截取字符串中一个或多个字符
	else if(act==2)
		return % SubStr(fp,slash+1)
	else {
		;当文件名没有后缀名
		if(dot==0){
			if(act==3)
				return % SubStr(fp,slash+1)
			else if(act==4)
				return ""
			else if(act==5){
				if(A_Is64bitOS)
					return % fp . "64"
				else
					return % fp
				}
		}
		else {
			if(act==3)
				return % SubStr(fp,slash+1,dot-slash-1)
			else if(act==4)
				return % SubStr(fp,dot+1)
			else if(act==5){
				if(A_Is64bitOS)
					return % SubStr(fp,1,dot-1) . "64" . SubStr(fp,dot)
				else
					return % fp
				}
		}
	}
}

AscSend(str){
	SetFormat, Integer, H
	for k,v in StrSplit(str)
	out.="{U+ " Ord(v) "}"
	SendInput % out
}

EzTip(tip,s){
	s:=(s>0) ? s*1000 : 2000
	ToolTip % tip
	Sleep % s
	ToolTip
}

MenutrayIcon(){	;20240529
	if (!GV_GroupBrowserToggleMButtonMode && !GV_GroupBrowserToggleWheelModeLeftRight && !GV_GroupBrowserToggleWheelModeUpDown) {
		Menu,tray,Icon,%A_AhkPath%, 1
	}
	else {
		Menu,tray,Icon,%A_AhkPath%, 2
	}
}

;适合单/双击和移动统一调用,C-点击次数,0移动,1单/双击,2双击,S时间,100ms
CoordWinXClick(x,y,c,s){
	CoordMode, Mouse, Window
	Click %x%, %y%, %c%
	Sleep, % 100*s
}

;适合单击直接调用
CoordWinClick(x,y){
	CoordMode, Mouse, Window
	Click %x%, %y%
}

;适合移动直接调用
CoordWinMove(x,y){
	CoordMode, Mouse, Window
	Click %x%, %y%, 0
}

;适合双击直接调用
CoordWinDbClick(x,y){
	CoordMode, Mouse, Window
	Click %x%, %y%, 2
}

;在调用的过程前面统一加上一句 , CoordMode, Mouse, Window 较好，下同
ClickSleep(x,y,s){
	CoordMode, Mouse, Window
	Click %x%, %y%
	Sleep, % 100*s
}

;适合鼠标步进单击直接调用,by tuutg,20240415
MouseMoveClick(x,y,s1,s2){
	CoordMode, Mouse, Window
	Sleep, % 100*s1
	MouseMove, %x%, %y%, 0, R
	Sleep, % 100*s2
	MouseGetPos,xpos, ypos
	Click %xpos%, %ypos%
}

;适合鼠标步进移动直接调用,by tuutg,20240415
MouseMoveSleep(x,y,s1,s2){
	CoordMode, Mouse, Window
	Sleep, % 100*s1
	MouseMove, %x%, %y%, 0, R
	Sleep, % 100*s2
	MouseGetPos,xpos, ypos
	; Click %xpos%, %ypos%
}

ControlClickSleep(ctl,s){
	CoordMode, Mouse, Window
	ControlClick, %ctl%
	Sleep, % 100*s
}

MyWinWaitActive2(title){
	CoordMode, Mouse, Window
	WinWait, %title%,
	IfWinNotActive, %title%, , WinActivate, %title%,
	WinWaitActive, %title%,
	WinMove, %title%, , 0, 0, A_ScreenWidth/2-1, A_ScreenHeight-75
	Sleep,500
	ControlSend,,{Click},%title% ;选中,即光标在文件上,20240114
}

MyWinWaitActive(title){
	CoordMode, Mouse, Window
	WinWait, %title%,
	IfWinNotActive, %title%, , WinActivate, %title%,
	WinWaitActive, %title%,
	WinShow, %title%,
	; WinMove, %title%, , 0, 0, A_ScreenWidth/2-1, A_ScreenHeight-75
	; Sleep,500
	; ControlSend,,{Click},%title% ;选中,即光标在文件上,20240114
}

GetCursorShape(){ ;获取光标特征码 by nnrxin
	VarSetCapacity( PCURSORINFO, 20, 0) ;为鼠标信息 结构 设置出20字节空间
	NumPut(20, PCURSORINFO, 0, "UInt") ;*声明出 结构 的大小cbSize = 20字节
	DllCall("GetCursorInfo", "Ptr", &PCURSORINFO) ;获取 结构-光标信息
	if ( NumGet( PCURSORINFO, 4, "UInt")="0" ) ;当光标隐藏时，直接输出特征码为0
		return, 0
		VarSetCapacity( ICONINFO, 20, 0) ;创建 结构-图标信息
		DllCall("GetIconInfo", "Ptr", NumGet(PCURSORINFO, 8), "Ptr", &ICONINFO) ;获取 结构-图标信息
		VarSetCapacity( lpvMaskBits, 128, 0) ;创造 数组-掩图信息（128字节）
		DllCall("GetBitmapBits", "Ptr", NumGet( ICONINFO, 12), "UInt", 128, "UInt", &lpvMaskBits) ;读取 数组-掩图信息
		loop, 128{ ;掩图码
		MaskCode += NumGet( lpvMaskBits, A_Index, "UChar") ;累加拼合
	}
	if (NumGet( ICONINFO, 16, "UInt")<>"0"){ ;颜色图不为空时（彩色图标时）
		VarSetCapacity( lpvColorBits, 4096, 0) ;创造 数组-色图信息（4096字节）
		DllCall("GetBitmapBits", "Ptr", NumGet( ICONINFO, 16), "UInt", 4096, "UInt", &lpvColorBits) ;读取 数组-色图信息
		loop, 256{ ;色图码
		ColorCode += NumGet( lpvColorBits, A_Index*16-3, "UChar") ;累加拼合
		}
	} else
		ColorCode := "0"
		DllCall("DeleteObject", "Ptr", NumGet( ICONINFO, 12)) ; *清理掩图
		DllCall("DeleteObject", "Ptr", NumGet( ICONINFO, 16)) ; *清理色图
		VarSetCapacity( PCURSORINFO, 0) ;清空 结构-光标信息
		VarSetCapacity( ICONINFO, 0) ;清空 结构-图标信息
		VarSetCapacity( lpvMaskBits, 0) ;清空 数组-掩图
		VarSetCapacity( lpvColorBits, 0) ;清空 数组-色图
return, % MaskCode//2 . ColorCode ;输出特征码
}


; ----------------------------------------------------------------------------------------------------------------------
; Function ......: TrayIcon_GetInfo
; Description ...: Get a series of useful information about tray icons.
; Parameters ....: sExeName  - The exe for which we are searching the tray icon data. Leave it empty to receive data for
; ...............:             all tray icons.
; Return ........: oTrayInfo - An array of objects containing tray icons data. Any entry is structured like this:
; ...............:             oTrayInfo[A_Index].idx     - 0 based tray icon index.
; ...............:             oTrayInfo[A_Index].idcmd   - Command identifier associated with the button.
; ...............:             oTrayInfo[A_Index].pid     - Process ID.
; ...............:             oTrayInfo[A_Index].uid     - Application defined identifier for the icon.
; ...............:             oTrayInfo[A_Index].msgid   - Application defined callback message.
; ...............:             oTrayInfo[A_Index].hicon   - Handle to the tray icon.
; ...............:             oTrayInfo[A_Index].hwnd    - Window handle.
; ...............:             oTrayInfo[A_Index].class   - Window class.
; ...............:             oTrayInfo[A_Index].process - Process executable.
; ...............:             oTrayInfo[A_Index].tray    - Tray Type (Shell_TrayWnd or NotifyIconOverflowWindow).
; ...............:             oTrayInfo[A_Index].tooltip - Tray icon tooltip.
; Info ..........: TB_BUTTONCOUNT message - http://goo.gl/DVxpsg
; ...............: TB_GETBUTTON message   - http://goo.gl/2oiOsl
; ...............: TBBUTTON structure     - http://goo.gl/EIE21Z
; ----------------------------------------------------------------------------------------------------------------------

TrayIcon_GetInfo(sExeName := "")
{
	d := A_DetectHiddenWindows
	DetectHiddenWindows, On

	oTrayInfo := []
	For key,sTray in ["Shell_TrayWnd", "NotifyIconOverflowWindow"]
	{
	idxTB := TrayIcon_GetTrayBar(sTray)
	WinGet, pidTaskbar, PID, ahk_class %sTray%

	hProc := DllCall("OpenProcess",    UInt,0x38, Int,0, UInt,pidTaskbar)
	pRB   := DllCall("VirtualAllocEx", Ptr,hProc, Ptr,0, UPtr,20, UInt,0x1000, UInt,0x04)

	szBtn := VarSetCapacity(btn, (A_Is64bitOS ? 32 : 20), 0)
	szNfo := VarSetCapacity(nfo, (A_Is64bitOS ? 32 : 24), 0)
	szTip := VarSetCapacity(tip, 128 * 2, 0)

	; TB_BUTTONCOUNT = 0x0418
	SendMessage, 0x0418, 0, 0, ToolbarWindow32%idxTB%, ahk_class %sTray%
	Loop, %ErrorLevel%
	{
		; TB_GETBUTTON 0x0417
		SendMessage, 0x0417, A_Index-1, pRB, ToolbarWindow32%idxTB%, ahk_class %sTray%

		DllCall("ReadProcessMemory", Ptr,hProc, Ptr,pRB, Ptr,&btn, UPtr,szBtn, UPtr,0)

		iBitmap := NumGet(btn, 0, "Int")
		idCmd   := NumGet(btn, 4, "Int")
		fsState := NumGet(btn, 8, "UChar")
		fsStyle := NumGet(btn, 9, "UChar")
		dwData  := NumGet(btn, (A_Is64bitOS ? 16 : 12), "UPtr")
		iString := NumGet(btn, (A_Is64bitOS ? 24 : 16), "Ptr")

		DllCall("ReadProcessMemory", Ptr,hProc, Ptr,dwData, Ptr,&nfo, UPtr,szNfo, UPtr,0)

		hWnd  := NumGet(nfo, 0, "Ptr")
		uId   := NumGet(nfo, (A_Is64bitOS ?  8 :  4), "UInt")
		msgId := NumGet(nfo, (A_Is64bitOS ? 12 :  8), "UPtr")
		hIcon := NumGet(nfo, (A_Is64bitOS ? 24 : 20), "Ptr")

		WinGet, nPid, PID, ahk_id %hWnd%
		WinGet, sProcess, ProcessName, ahk_id %hWnd%
		WinGetClass, sClass, ahk_id %hWnd%

		If ( !sExeName || sExeName == sProcess || . == nPid )
		{
		DllCall("ReadProcessMemory", Ptr,hProc, Ptr,iString, Ptr,&tip, UPtr,szTip, UPtr,0)
		oTrayInfo.Push({ "idx"     : A_Index-1
				   , "idcmd"   : idCmd
				   , "pid"     : nPid
				   , "uid"     : uId
				   , "msgid"   : msgId
				   , "hicon"   : hIcon
				   , "hwnd"    : hWnd
				   , "class"   : sClass
				   , "process" : sProcess
				   , "tooltip" : StrGet(&tip, "UTF-16")
				   , "tray"    : sTray })
		}
	}
	DllCall("VirtualFreeEx", Ptr,hProc, Ptr,pRB, UPtr,0, UInt,0x8000)
	DllCall("CloseHandle",   Ptr,hProc)
	}
	DetectHiddenWindows, %d%
	Return oTrayInfo
}

; ----------------------------------------------------------------------------------------------------------------------
; Function .....: TrayIcon_GetTrayBar
; Description ..: Get the tray icon handle.
; Parameters ...: sTray - Traybar to retrieve.
; Return .......: Tray icon handle.
; ----------------------------------------------------------------------------------------------------------------------
TrayIcon_GetTrayBar(sTray:="Shell_TrayWnd")
{
	d := A_DetectHiddenWindows
	DetectHiddenWindows, On
	WinGet, ControlList, ControlList, ahk_class %sTray%
	RegExMatch(ControlList, "(?<=ToolbarWindow32)\d+(?!.*ToolbarWindow32)", nTB)
	Loop, %nTB%
	{
	ControlGet, hWnd, hWnd,, ToolbarWindow32%A_Index%, ahk_class %sTray%
	hParent := DllCall( "GetParent", Ptr, hWnd )
	WinGetClass, sClass, ahk_id %hParent%
	If !(sClass == "SysPager" || sClass == "NotifyIconOverflowWindow" )
		Continue
	idxTB := A_Index
	Break
	}
	DetectHiddenWindows, %d%
	Return idxTB
}

; ----------------------------------------------------------------------------------------------------------------------
; Function .....: TrayIcon_Button
; Description ..: Simulate mouse button click on a tray icon.
; Parameters ...: sExeName - Executable Process Name of tray icon.
; ..............: sButton  - Mouse button to simulate (L, M, R).
; ..............: bDouble  - True to double click, false to single click.
; ..............: nIdx     - Index of tray icon to click if more than one match.
; ----------------------------------------------------------------------------------------------------------------------
TrayIcon_Button(msgid, uid, hwnd, sButton:="L", bDouble:=False, nIdx:=1) {
	d := A_DetectHiddenWindows
	DetectHiddenWindows, On
	WM_MOUSEMOVE      = 0x0200
	WM_LBUTTONDOWN    = 0x0201
	WM_LBUTTONUP      = 0x0202
	WM_LBUTTONDBLCLK  = 0x0203
	WM_RBUTTONDOWN    = 0x0204
	WM_RBUTTONUP      = 0x0205
	WM_RBUTTONDBLCLK  = 0x0206
	WM_MBUTTONDOWN    = 0x0207
	WM_MBUTTONUP      = 0x0208
	WM_MBUTTONDBLCLK  = 0x0209
	sButton := "WM_" sButton "BUTTON"
	If ( bDouble )
	PostMessage, msgid, uid, %sButton%DBLCLK,, % "ahk_id " hwnd
	Else {
	PostMessage, msgid, uid, %sButton%DOWN,, % "ahk_id " hwnd
	PostMessage, msgid, uid, %sButton%UP,, % "ahk_id " hwnd
	}
	DetectHiddenWindows, %d%
	Return
}


; /*
fun_KeyClickAction123(act){
	if RegExMatch(act,"i)^(Run,)",m){
		Run,% SubStr(act,StrLen(m1)+1)
	}
	else if RegExMatch(act,"i)^(Send,)",m){
		Send,% SubStr(act,StrLen(m1)+1)
	}
	else if RegExMatch(act,"i)^(SendInput,)",m){
		SendInput,% SubStr(act,StrLen(m1)+1)
	}
	else if RegExMatch(act,"i)^(gosub,)",m){
		gosub,% SubStr(act,StrLen(m1)+1)
	}
	else if RegExMatch(act,"i)^(GoFun,)",m){
		funString := % SubStr(act,StrLen(m1)+1)
		funName := SubStr(funString,1,InStr(funString,"(")-1)
		funPara := SubStr(funString,InStr(funString,"(")+1,InStr(funString,")")-InStr(funString,"(")-1)
		RetVal := funName.(funPara)
	}
}

;%A_YYYY%-%A_MM%-%A_DD%-%A_MSec%
;msgbox % fun_GetFormatTime("yyyy-MM-dd-HH-mm-ss")
fun_GetFormatTime(f,t=""){
	;FormatTime, TimeString, 200504, 'Month Name': MMMM`n'Day Name': dddd
	;FormatTime, TimeString, ,'Month Name': MMMM`n'Day Name': dddd
	FormatTime, TimeString, %t% ,%f%
	return %TimeString%
}

GF_GetSysVar(sys_var_name){
	EnvGet, sv,% sys_var_name
	return % sv
}

Decimal_to_Hex(var){
	SetFormat, integer, hex
	var += 0
	SetFormat, integer, d
	return var
}
*/

;使用剪貼簿插入文字
AutoInput(InputStr){
	clipboard_save = %ClipboardAll%
	Clipboard:=
	Clipboard = %InputStr%
	ClipWait
	Send ^v
	Sleep, 150
	Clipboard = %clipboard_save%
}

;判断连续双击按键
GV_KeyClick_Continuous(tout){ ;大于判定双击
	return (A_ThisHotkey = A_PriorHotkey) and (A_TimeSincePriorHotkey < tout)
}
;****************************************************
GV_KeyClick_single(ms){ ;大于判定单击
	return (A_ThisHotkey = A_PriorHotkey) and (A_TimeSincePriorHotkey > ms)
}
;****************************************************
;点击PixPin钉图工具坐标
PixPinToolsClick(i){
	WinGetPos,x,y,lengthA,hightA, A
	; MouseGetPos,x, y,VarWin,VarControl, 1
	; Xpos :=lengthA-41*13
	; Ypos :=hightA-18
	; ΔX :=(lengthA-x)/1
	; ΔY :=hightA-y
	; MsgBox, %x%`,%y%`,%ΔX%`,%ΔY%`,%lengthA%`,%hightA%
	; MsgBox, %VarWin%`,%VarControl%`,%lengthA%`,%hightA%
	if (i<=5){
		Xn :=lengthA-50-(i-1)*40
		Yn :=hightA-18
	}else if(6<=i){
		Xn :=lengthA-233-(i-6)*33
		Yn :=hightA-18
	}
	CoordMode, Mouse, Window
	Click %Xn%, %Yn%
}
;****************************************************
;****************************************************
;自定义标签
; /*
Sub_ButtonLongPress:
	if ButtonLongPress{
		ButtonLongPress += 1
		return
	}
	ButtonLongPress = 1
	;SetTimer, ButtonLongPress, -250
	SetTimer, ButtonLongPress, % GV_MouseTimer
return

ButtonLongPress:
	IfEqual, ButtonLongPress, 1
	{
		MouseGetPos, x0, y0
	if GV_MouseButton = 1
		KeyWait, LButton, T0.4
	else if GV_MouseButton = 2
		KeyWait, RButton, T0.4
	else if GV_MouseButton = 3
		KeyWait, MButton, T0.4
		MouseGetPos, x1, y1
	if (ErrorLevel && (x0 = x1 && y0 = y1))
		fun_KeyClickAction123(GV_LongClickAction)
	}
	ButtonLongPress = 0
return
*/

Sub_MouseClick123:
	if winc_presses > 0 ; SetTimer 已经启动, 所以我们记录键击.
	{
		winc_presses += 1
		return
	}
	; 否则, 这是新开始系列中的首次按下. 把次数设为 1 并启动
	; 计时器：
	winc_presses = 1
	SetTimer, KeyWinC, % GV_MouseTimer ; 在 400 毫秒内等待更多的键击.
return

Sub_KeyClick123:
	if winc_presses > 0 ; SetTimer 已经启动, 所以我们记录键击.
	{
		winc_presses += 1
		return
	}
	; 否则, 这是新开始系列中的首次按下. 把次数设为 1 并启动
	; 计时器：
	winc_presses = 1
	SetTimer, KeyWinC, % GV_KeyTimer ; 在 400 毫秒内等待更多的键击.
return

KeyWinC:
	SetTimer, KeyWinC, off
	if winc_presses = 1 ; 此键按下了一次.
	{
	if GV_MouseButton = 0
	{
		fun_KeyClickAction123(GV_KeyClickAction1)
	}
	else {
		MouseGetPos, x0, y0
		if GV_MouseButton = 1
			KeyWait, LButton, T0.4
			else if GV_MouseButton = 2
			KeyWait, RButton, T0.4
			else if GV_MouseButton = 3
			KeyWait, MButton, T0.4
			MouseGetPos, x1, y1
		if (ErrorLevel && (x0 = x1 && y0 = y1))
		{
			fun_KeyClickAction123(GV_LongClickAction)
			;重置为0
			GV_MouseButton = 0
		}
		else {
			fun_KeyClickAction123(GV_KeyClickAction1)
		}
	}
	}
	else if winc_presses = 2 ; 此键按下了两次.
	{
		fun_KeyClickAction123(GV_KeyClickAction2)
	}
	else if winc_presses > 2
	{
		fun_KeyClickAction123(GV_KeyClickAction3)
		;MsgBox, Three or more clicks detected.
	}
	; 不论触发了上面的哪个动作, 都对 count 进行重置
	; 为下一个系列的按下做准备:
	winc_presses = 0
return

/*
fun_KeyClickAction123(act){
	if RegExMatch(act,"i)^(Run,)",m) {
		Run,% SubStr(act,StrLen(m1)+1)
	}
	else if RegExMatch(act,"i)^(Send,)",m) {
		Send,% SubStr(act,StrLen(m1)+1)
	}
	else if RegExMatch(act,"i)^(SendInput,)",m) {
		SendInput,% SubStr(act,StrLen(m1)+1)
	}
	else if RegExMatch(act,"i)^(gosub,)",m) {
		gosub,% SubStr(act,StrLen(m1)+1)
	}
	else if RegExMatch(act,"i)^(GoFun,)",m) {
		funString := % SubStr(act,StrLen(m1)+1)
		funName := SubStr(funString,1,InStr(funString,"(")-1)
		funPara := SubStr(funString,InStr(funString,"(")+1,InStr(funString,")")-InStr(funString,"(")-1)
		RetVal := funName.(funPara)
	}
}

Sub_ButtonLongPress:
	if ButtonLongPress {
		ButtonLongPress += 1
		return
	}
	ButtonLongPress = 1
	;SetTimer, ButtonLongPress, -250
	SetTimer, ButtonLongPress, % GV_MouseTimer
return

ButtonLongPress:
	IfEqual, ButtonLongPress, 1
	{
		MouseGetPos, x0, y0
		if GV_MouseButton = 1
			KeyWait, LButton, T0.4
		else if GV_MouseButton = 2
			KeyWait, RButton, T0.4
		else if GV_MouseButton = 3
			KeyWait, MButton, T0.4
		MouseGetPos, x1, y1
		if (ErrorLevel && (x0 = x1 && y0 = y1))
			fun_KeyClickAction123(GV_LongClickAction)
	}
	ButtonLongPress = 0
return

;%A_YYYY%-%A_MM%-%A_DD%-%A_MSec%
;msgbox % fun_GetFormatTime("yyyy-MM-dd-HH-mm-ss")
fun_GetFormatTime(f,t="")
{
	;FormatTime, TimeString, 200504, 'Month Name': MMMM`n'Day Name': dddd
	;FormatTime, TimeString, ,'Month Name': MMMM`n'Day Name': dddd
	FormatTime, TimeString, %t% ,%f%
	return %TimeString%
}

GF_GetSysVar(sys_var_name)
{
	EnvGet, sv,% sys_var_name
	return % sv
}
*/
;****************************************************
;****************************************************
Sub_CapsLock_SgC:
Sub_ClipAppend: 	;Ctrl+C复制并右侧显示clip(Clipboard内容)
	;SendInput,^{Home}^+{End}^c	;整篇复制
	SendInput,^c
	Sleep ,300
	StringLeft,clipboard_left,Clipboard,500
	ToolTip,%clipboard_left% ;在鼠标右侧显示clip(Clipboard内容)
	Sleep ,1000
	ToolTip
	FileRead, ClipboardContent,%COMMANDER_PATH%\剪切板\ClipAppend.txt
	ClipboardContent = %Clipboard%`r`n`r`n%ClipboardContent%
	FileDelete, %COMMANDER_PATH%\剪切板\ClipAppend.txt
	FileAppend, %ClipboardContent%, %COMMANDER_PATH%\剪切板\ClipAppend.txt
return

;****************************************************

;获取全文件路径|整行复制
Sub_CapsLock_DbC:
Sub_ClipAppendCopyFPath:
	; if (WinActive("ahk_group Group_explorer") or WinActive("ahk_group CtrlCDbCopyName_Path") or WinActive("ahk_group GroupDiagOpenAndSave")){
	if (A_Cursor !=""){	;选择模式,非输入模式,20240402
		SendInput,^c
		Sleep,300
		Clipboard = %Clipboard%
		; EzTip(Clipboard,1)
		Clipboard := GetFileInfo(Clipboard,0)	;获取全文件路径
	}
	else {
		SendInput,{Home}+{End}^c	;整行复制
		Sleep,300
	}
	StringLeft,clipboard_left,Clipboard,500
	ToolTip,%clipboard_left% ;在鼠标右侧显示clip(Clipboard内容)
	Sleep ,1000
	ToolTip
	FileRead, ClipboardContent,%COMMANDER_PATH%\剪切板\ClipAppend.txt
	ClipboardContent = %Clipboard%`r`n`r`n%ClipboardContent%
	FileDelete, %COMMANDER_PATH%\剪切板\ClipAppend.txt
	FileAppend, %ClipboardContent%, %COMMANDER_PATH%\剪切板\ClipAppend.txt
return

;****************************************************

;只获取文件路径|整篇复制
Sub_CapsLock_DbX:
Sub_ClipAppendCopyOPath:
	; if (WinActive("ahk_group Group_explorer") or WinActive("ahk_group CtrlCDbCopyName_Path") or WinActive("ahk_group GroupDiagOpenAndSave")){
	if (A_Cursor !=""){	;选择模式,非输入模式,20240402
		SendInput,^c
		Sleep,300
		Clipboard = %Clipboard%
		; EzTip(Clipboard,1)
		Clipboard :=GetFileInfo(Clipboard,1)	;只获取文件路径
	}
	else {
		SendInput,^{Home}^+{End}^c	;整篇复制
		Sleep,300
	}
	StringLeft,clipboard_left,Clipboard,500
	ToolTip,%clipboard_left% ;在鼠标右侧显示clip(Clipboard内容)
	Sleep ,1000
	ToolTip
	FileRead, ClipboardContent,%COMMANDER_PATH%\剪切板\ClipAppend.txt
	ClipboardContent = %Clipboard%`r`n`r`n%ClipboardContent%
	FileDelete, %COMMANDER_PATH%\剪切板\ClipAppend.txt
	FileAppend, %ClipboardContent%, %COMMANDER_PATH%\剪切板\ClipAppend.txt
return

;****************************************************

;获取文件全名,带后缀|粘贴
Sub_CapsLock_DbB:
Sub_ClipAppendCopyFName:
	; if (WinActive("ahk_group Group_explorer") or WinActive("ahk_group CtrlCDbCopyName_Path") or WinActive("ahk_group GroupDiagOpenAndSave")){
	if (A_Cursor !=""){	;选择模式,非输入模式,20240402
		SendInput,^c
		Sleep,300
		Clipboard = %Clipboard%
		; EzTip(Clipboard,1)
		Clipboard :=GetFileInfo(Clipboard,2)	;获取文件全名,带后缀
	}
	else {
		SendInput,{Down 4}{Home}{Shift Down}+{End}{Up 5}{End}{Shift Up}^c	;后5行复制
		Sleep,300
	}
	StringLeft,clipboard_left,Clipboard,500
	ToolTip,%clipboard_left% ;在鼠标右侧显示clip(Clipboard内容)
	Sleep ,1000
	ToolTip
	FileRead, ClipboardContent,%COMMANDER_PATH%\剪切板\ClipAppend.txt
	ClipboardContent = %Clipboard%`r`n`r`n%ClipboardContent%
	FileDelete, %COMMANDER_PATH%\剪切板\ClipAppend.txt
	FileAppend, %ClipboardContent%, %COMMANDER_PATH%\剪切板\ClipAppend.txt
return

;****************************************************

;只获取文件名,无后缀|前5行复制
Sub_CapsLock_SgB:
Sub_ClipAppendCopyOName:
	; if (WinActive("ahk_group Group_explorer") or WinActive("ahk_group CtrlCDbCopyName_Path") or WinActive("ahk_group GroupDiagOpenAndSave")){
	if (A_Cursor !=""){	;选择模式,非输入模式,20240402
		SendInput,^c
		Sleep,300
		Clipboard = %Clipboard%
		; EzTip(Clipboard,1)
		Clipboard :=GetFileInfo(Clipboard,3)	;只获取文件名
	}
	else {
		SendInput,{Up 4}{Home}{Shift Down}+{End}{Down 5}{End}{Shift Up}^c	;前5行复制
		Sleep,300
	}
	StringLeft,clipboard_left,Clipboard,500
	ToolTip,%clipboard_left% ;在鼠标右侧显示clip(Clipboard内容)
	Sleep ,1000
	ToolTip
	FileRead, ClipboardContent,%COMMANDER_PATH%\剪切板\ClipAppend.txt
	ClipboardContent = %Clipboard%`r`n`r`n%ClipboardContent%
	FileDelete, %COMMANDER_PATH%\剪切板\ClipAppend.txt
	FileAppend, %ClipboardContent%, %COMMANDER_PATH%\剪切板\ClipAppend.txt
return

;****************************************************
;****************************************************

Sub_MaxRestore:
	WinGet, Status_minmax,MinMax,A
	if (Status_minmax=1){
		WinRestore A
	}
	else {
		WinMaximize A
	}
return

;****************************************************

Sub_MaxAllWindows:
	WinGet, Window_List, List ; Gather a list of running programs
	loop, %Window_List%
	{
		wid := Window_List%A_Index%
		WinGetTitle, wid_Title, ahk_id %wid%
		WinGet, Style, Style, ahk_id %wid%
		;(WS_CAPTION 0xC00000| WS_SYSMENU 0x80000| WS_MAXIMIZEBOX 0x10000) | WS_SIZEBOX 0x40000
		if (!(Style & 0xC90000) or !(Style & 0x40000) or (Style & WS_DISABLED) or !(wid_Title)) ;skip unimportant windows ;! wid_Title or
			continue
			;MsgBox, % (Style & 0x40000)
			WinGet, es, ExStyle, ahk_id %wid%
			Parent := Decimal_to_Hex( DllCall( "GetParent", "uint", wid ) )
			WinGet, Style_parent, Style, ahk_id %Parent%
			Owner := Decimal_to_Hex( DllCall( "GetWindow", "uint", wid , "uint", "4" ) ) ; GW_OWNER = 4
			WinGet, Style_Owner, Style, ahk_id %Owner%

		if (((es & WS_EX_TOOLWINDOW) and !(Parent)) ; filters out program manager, etc
			or ( !(es & WS_EX_APPWINDOW)
			and (((Parent) and ((Style_parent & WS_DISABLED) =0)) ; These 2 lines filter out windows that have a parent or owner window that is NOT disabled -
			or ((Owner) and ((Style_Owner & WS_DISABLED) =0))))) ; NOTE - some windows result in blank value so must test for zero instead of using NOT operator!
			continue
			WinGet, Status_minmax ,MinMax,ahk_id %wid%
		if (Status_minmax!=1){
			WinMaximize,ahk_id %wid%
		}
		;MsgBox, 4, , Visiting All Windows`n%A_Index% of %Window_List%`n`n%wid_Title%`nContinue?
		;IfMsgBox, NO, break
	}
return

;****************************************************

Sub_WindowNoCaption:
	WinGetPos, xTB, yTB,lengthTB,hightTB, ahk_class Shell_TrayWnd
	;msgbox %xTB%
	;msgbox %yTB%
	;msgbox %lengthTB%
	;msgbox %hightTB%
	bd := 8 ;win8Border = 4
	lW := A_ScreenWidth
	hW := A_ScreenHeight
	if(xTB == 0){ ;左边和上、下面的情况
		if(yTB == 0){ ;任务栏在上和左
			if(lengthTB == A_ScreenWidth){ ;在上
			xW := 0
			yW := hightTB
			lW := A_ScreenWidth
			hW := A_ScreenHeight - hightTB
			}
			else { ;在左
			xW := lengthTB
			yW := 0
			lW := A_ScreenWidth - lengthTB
			hW := A_ScreenHeight
			}
		}
		else { ;在下
			xW := 0
			yW := 0
			lW := A_ScreenWidth
			hW := A_ScreenHeight - hightTB
		}
	}
	else { ;在右
		xW := 0
		yW := 0
		lW := A_ScreenWidth - lengthTB
		hW := A_ScreenHeight
	}
	WinSet, Style, ^0xC00000, A
return
;****************************************************
/*
Decimal_to_Hex(var)
{
	SetFormat, integer, hex
	var += 0
	SetFormat, integer, d
	return var
}
*/
;****************************************************
;打开剪贴板中多个链接
OpenClipURLS:
	loop, Parse, Clipboard, `n, `r ;在 `r 之前指定 `n, 这样可以同时支持对 Windows 和 Unix 文件的解析.
	{
		cu := A_loopField
		if(RegExMatch(A_LoopField,"^http")){
			Sleep 200
			Run, "%A_LoopField%"
		}
		else if(RegExMatch(A_LoopField,"(^[a-zA-Z]:\\)|(^file:\/\/\/[a-zA-Z]:\/)")){
			Sleep 200
			Run,"%COMMANDER_EXE%" /A /T /O /S /R="%A_LoopField%"
		}
	}
return

;****************************************************
;map tc :tabnew<cr>"+P
;map <F1> :tabnew<CR>

Sub_CopyAllVim:
	SendInput,^{Home}^+{End}^c
	Sleep 500
	if not WinExist("ahk_class Vim")
		Run %COMMANDER_PATH%\TOOLS\vim\gvim.exe, %COMMANDER_PATH%\TOOLS\vim
		WinActivate
		Sleep 500
		SendInput,{F1}^v
	return
;****************************************************
Sub_CopyVim:
	SendInput,^c
	Sleep,300
	if not WinExist("ahk_class Vim")
		Run %COMMANDER_PATH%\TOOLS\vim\gvim.exe, %COMMANDER_PATH%\TOOLS\vim
		WinActivate
		Sleep 500
		SendInput,{Esc}
		Sleep 200
		AscSend("tc")
return
;****************************************************
uTools: ;按2次Alt激活uTools
{
	SendInput,{RAlt Down}
	Sleep,50
	SendInput,{RAlt Up}
	Sleep,50
	SendInput,{RAlt Down}
	Sleep,50
	SendInput,{RAlt Up}
}
return
;****************************************************
Listary: ;按2次Ctrl激活Listary
{
	SendInput,{Ctrl Down}
	Sleep,50
	SendInput,{Ctrl Up}
	Sleep,50
	SendInput,{Ctrl Down}
	Sleep,50
	SendInput,{Ctrl Up}
}
return
;****************************************************
TextToEverything:	;以复制+后处理方式获取文件(夹)名
	; SendInput,{F2}
	; Sleep,50
	SendInput,^c
	Sleep,300
	clip:=
	if (WinActive("ahk_group Group_explorer") or WinActive("ahk_group Group_Desktop")){
		clip:=GetFileInfo(Clipboard,3)	;只获取文件名
	}
	else {
		clip:=Clipboard
	}
	Sleep,100

	; EzTip(Clipboard,1)
	;直接搜索|Alt+space激活Everything,20240130
	; Run,%COMMANDER_PATH%\Everything.exe -s %clip%
	IfExist,C:\Windows\SysWOW64\
		Run,"%RunCaptX%\Everything\Everything.exe" -s %clip%
	else
		Run,"%RunCaptX%\Everything\Everything-x86\Everything.exe" -s %clip%
return
;****************************************************
中键设定_Office:	;单击中键缩放100%/双击中键关闭
	if (GV_KeyClick_Continuous(GV_MouseTimer)){
		;office双击中键关闭
		SendInput,^s
		Sleep ,100
		SendInput,^w
		Sleep ,100
		EzTip("文件保存并关闭成功!",1)
	}
	else {
		if WinActive("ahk_group Group_打印"){
			SendInput,{Enter} 	;快速打印
		}
		else if MouseUnder("NetUIHWND1"){
			SendInput,{Enter} 	;微软打印,20240507
		}
		else {
			;office单击中键缩放100%
			SendInput,!w	;视图
			Sleep ,200		;WPS延时
			SendInput,{Text}j	;缩放100%,wps_excel不适用
			Sleep ,100
			SendInput,^s
			Sleep ,100
			;SendInput,^{Home}
			;EzTip("文件保存成功!",1)
		}
	}
return
;************************** 自定义方法结束$ ****************************


;************** Youdao_网络翻译^ ********* {{{1
;语音+弹窗  翻译-中英互译	by天甜	From:Cando_有道翻译+剪贴板函数+Splash函数+判断调试

$<#y::
	原值:=Clipboard
	Clipboard =
	Send ^c
	Sleep 300
	if (WinActive("ahk_group Group_explorer") or WinActive("ahk_group Group_Desktop")){
		Clipboard:=GetFileInfo(Clipboard,3)	;只获取文件名
	}
	else {
		Clipboard:=Clipboard
	}
	gosub sound
return

sound:
	ClipWait,0.5
	if(ErrorLevel)
	{
		InputBox,varTranslation,请输入,你想翻译啥，我来说
		if !ErrorLevel
		{
			Youdao译文:=YouDaoApi(varTranslation)
			Youdao_网络释义:= json(Youdao译文, "web.value")
			SplashYoudaoMsg("Youdao_网络翻译", Youdao_网络释义)
			spovice:=ComObjCreate("sapi.spvoice")
			spovice.Speak(Youdao_网络释义)
			Sleep, 3000
			SplashTextOff
		}
	}
	else
	{
		varTranslation:=Clipboard
		Youdao译文:=YouDaoApi(varTranslation)
		Youdao_网络释义:= json(Youdao译文, "web.value")
		SplashYoudaoMsg("Youdao_网络翻译", Youdao_网络释义)
		spovice:=ComObjCreate("sapi.spvoice")
		spovice.Speak(Youdao_网络释义)
		Sleep, 3000
		SplashTextOff
	}
	;Clipboard:=原值
	Clipboard:=Youdao_网络释义
return

SplashYoudaoMsg(title, content){
	;SoundBeep 750, 500
	MouseGetPos, MouseX, MouseY ;获得鼠标位置x,y
	MouseZ := MouseX + 100
	SplashTextOn , 400, 60, %title%, %content%
	WinMove, %title%, , %MouseZ%, %MouseY%
	WinSet, Transparent, 200, %title%
}

YouDaoApi(KeyWord)
{
	;KeyWord:=SkSub_UrlEncode(KeyWord,"utf-8")
	url:="http://fanyi.youdao.com/fanyiapi.do?keyfrom=qqqqqqqq123&key=86514254&type=data&doctype=json&version=1.1&q=" . KeyWord
	WebRequest := ComObjCreate("WinHttp.WinHttpRequest.5.1")
	WebRequest.Open("GET", url)
	WebRequest.Send()
	result := WebRequest.ResponseText
	return result
}

json(ByRef js, s, v = "")
{
	j = %js%
	loop, Parse, s, .
	{
		p = 2
		RegExMatch(A_LoopField, "([+\-]?)([^[]+)((?:\[\d+\])*)", q)
		loop{
			if (!p := RegExMatch(j, "(?<!\\)(""|')([^\1]+?)(?<!\\)(?-1)\s*:\s*((\{(?:[^{}]++|(?-1))*\})|(\[(?:[^[\]]++|(?-1))*\])|"
			. "(?<!\\)(""|')[^\7]*?(?<!\\)(?-1)|[+\-]?\d+(?:\.\d*)?|true|false|null?)\s*(?:,|$|\})", x, p))
			return
			else if (x2 == q2 or q2 == "*"){
			j = %x3%
			z += p + StrLen(x2) - 2
			if (q3 != "" and InStr(j, "[") == 1){
				StringTrimRight, q3, q3, 1
				loop, Parse, q3, ], [
				{
				z += 1 + RegExMatch(SubStr(j, 2, -1), "^(?:\s*((\[(?:[^[\]]++|(?-1))*\])|(\{(?:[^{\}]++|(?-1))*\})|[^,]*?)\s*(?:,|$)){" . SubStr(A_LoopField, 1) + 1 . "}", x)
				j = %x1%
				}
			}
			break
			}
			else p += StrLen(x)
			}
	}
	if v !=
	{
		vs = "
		if (RegExMatch(v, "^\s*(?:""|')*\s*([+\-]?\d+(?:\.\d*)?|true|false|null?)\s*(?:""|')*\s*$", vx)
			and (vx1 + 0 or vx1 == 0 or vx1 == "true" or vx1 == "false" or vx1 == "null" or vx1 == "nul"))
		vs := "", v := vx1
		StringReplace, v, v, ", \", All
		js := SubStr(js, 1, z := RegExMatch(js, ":\s*", zx, z) + StrLen(zx) - 1) . vs . v . vs . SubStr(js, z + StrLen(x3) + 1)
	}
return, j == "false" ? 0 : j == "true" ? 1 : j == "null" or j == "nul"
? "" : SubStr(j, 1, 1) == """" ? SubStr(j, 2, -1) : j
}
;************** Youdao_网络翻译$ *********


;************** 组合快捷键部分^ ************** {{{1
;************** Escape相关^ ************** {{{2
; +HJKL 表示左下右上方向
Escape & j:: SendInput,{Blind}{Down}
Escape & k:: SendInput,{Blind}{Up}
Escape & h:: SendInput,{Blind}{Left}
Escape & l:: SendInput,{Blind}{Right}

Escape & f:: SendInput,{Blind}^{Right}
Escape & s:: SendInput,{Blind}^{Left}

Escape & a::SendInput,{Blind}{PgDn} ;by tuutg 2022/11/13
Escape & q::SendInput,{Blind}{PgUp}

;************** u,i单击双击^ **************
;Escape & u:: SendInput,{Blind}^{End}
;Escape & i:: SendInput,{Blind}^{Home}
;Escape & n:: SendInput,{Blind}{PgDn}
;Escape & m:: SendInput,{Blind}{PgUp}

Escape & u::
	GV_KeyClickAction1 := "SendInput,{End}"
	GV_KeyClickAction2 := "SendInput,^{End}"
	gosub,Sub_KeyClick123
return

Escape & i::
	GV_KeyClickAction1 := "SendInput,{Home}"
	GV_KeyClickAction2 := "SendInput,^{Home}"
	gosub,Sub_KeyClick123
return

Escape & n::
	GV_KeyClickAction1 := "SendInput,{PgDn}"
	GV_KeyClickAction2 := "SendInput,^{PgDn}"
	gosub,Sub_KeyClick123
return

Escape & m::
	GV_KeyClickAction1 := "SendInput,{PgUp}"
	GV_KeyClickAction2 := "SendInput,^{PgUp}"
	gosub,Sub_KeyClick123
return
;************** u,i单击双击$ **************

;***************** 剪贴板相关^ ************** {{{2
Escape & v::
	if EscapeV_presses > 0
	{
	EscapeV_presses += 1
	return
	}
	EscapeV_presses = 1
	SetTimer, KeyEscapeV, 175
return

KeyEscapeV:
	SetTimer, KeyEscapeV, off
	if EscapeV_presses = 1
	{
	gosub,PastePureText
	}
	else if EscapeV_presses = 2
	{
	gosub,EzOtherMenuShow	;弹出Ez快捷菜单
	}
	EscapeV_presses = 0
return

Escape & x::	;20240401
; CapsLock & x::	;复制并右侧显示clip(Clipboard内容)|路径
	GV_KeyClickAction1 := "SendInput,^x"	;原版剪切
	GV_KeyClickAction2 := "gosub,Sub_ClipAppendCopyOPath"	;复制路径
	gosub,Sub_KeyClick123
return

Escape & c::
	gosub,Sub_ClipAppend	;复制并右侧显示clip(Clipboard内容)
return

;#z::Menu, MyMenu, Show  ; i.e. press the Win-Z hotkey to show the menu.

;***************** 剪贴板相关$ **************

;关闭和刷新
Escape & w::SendInput,{Blind}^w ;by tuutg 2022/11/13
Escape & r::SendInput,{Blind}^r
;切换tab
Escape & o::SendInput,{Blind}^+{Tab}
Escape & p::SendInput,{Blind}^{Tab}
;右键和DEL
;Escape & y::SendInput,{AppsKey}
Escape & y::SendInput,{Click,Right}
Escape & d::SendInput,{Delete}
;Alttab，Win8下暂时不能用
Escape & .::AltTab
Escape & ,::ShiftAltTab

Escape & `;::WinClose A

;Enter 回车窗口最大化
;Escape & Enter::WinMaximize A
Escape & Enter::gosub,Sub_MaxRestore
Escape & Space::WinMinimize A
^!#Up::gosub,Sub_MaxAllWindows
^#u::gosub,OpenClipURLS

^Escape::SendInput,^{Escape}
+Escape::SendInput,+{Escape}
$!Escape::SendInput,!{Escape}
^+Escape::SendInput,^+{Escape}
^!Escape::SendInput,^!{Escape}
$!+Escape::SendInput,!+{Escape}
^!+Escape::SendInput,^!+{Escape}

;最后一行恢复自身功能，重要

Escape::
	if (GV_EscKeyAs = "WinClose") {
		WinClose A
	}
	if (GV_EscKeyAs = "BackSpace") {
		SendInput,{BackSpace}
	}
	if (GV_EscKeyAs = "Escape") {
		SendInput,{Escape}
	}
	else if (GV_EscKeyAs = "CapsLock") {
		GetKeyState t, CapsLock, T
		IfEqual t,D, SetCapsLockState AlwaysOff
	else SetCapsLockState AlwaysOn
	}
return

/*
~Escape::
	if (A_PriorHotkey=A_ThisHotkey && A_TimeSincePriorHotkey<200){
		;MsgBox You double taped %A_ThisHotkey%
		WinClose A
	}
	else {
		; Sleep 200
		;msgbox press %A_ThisHotkey% for %A_TimeSinceThisHotkey%
		;if (A_TimeSinceThisHotkey > 200 && A_TimeSinceThisHotkey < 1000){
		SendInput {Escape}
		;}
	}
return
*/
;************** Escape相关$     **************


;************** CapsLock相关^ ************** {{{2
;win+caps+按键
;Capslock & e::
	;state := GetKeyState("LWin", "T")  ; 当 CapsLock 打开时为真, 否则为假.
	;if state
		;msgbox handle！
	;else
		;send #e
;return

;左右手结合,上下左右|↑↓→ ←, 开启原CapsLocK开关, 用^Caps(Ctrl+CapsLocK)
CapsLock & k::SendInput,{Blind}{Up}
CapsLock & j::SendInput,{Blind}{Down}
CapsLock & h::SendInput,{Blind}{Left}
CapsLock & l::SendInput,{Blind}{Right}

CapsLock & e::gosub,CapsLock_SgE
; CapsLock & e::	;SendInput,{Blind}{Up} ;by tuutg 2022/11/13
	; GV_KeyClickAction1 := "gosub,CapsLock_SgE"	;{Up}|向上↑
	; GV_KeyClickAction2 := "GoFun,"MouseMoveSleep(0,-20,0.1,0.1)""
	; gosub,Sub_KeyClick123
; return

CapsLock & d::gosub,CapsLock_SgD
; CapsLock & d::	;SendInput,{Blind}{Down} ;by tuutg 2022/11/13
	; GV_KeyClickAction1 := "gosub,CapsLock_SgD"	;{Down}|向下↓
	; GV_KeyClickAction2 := "GoFun,"MouseMoveSleep(0,+20,0.1,0.1)""
	; gosub,Sub_KeyClick123
; return

CapsLock & s::gosub,CapsLock_SgS
; CapsLock & s::	;SendInput,{Blind}{Left} ;by tuutg 2022/11/13
	; GV_KeyClickAction1 := "gosub,CapsLock_SgS"	;{Right}|向左←
	; GV_KeyClickAction2 := "GoFun,"MouseMoveSleep(-85,0,0.1,0.1)""
	; gosub,Sub_KeyClick123
; return

CapsLock & f::gosub,CapsLock_SgF
; CapsLock & f::	;SendInput,{Blind}{Right} ;by tuutg 2022/11/13
	; GV_KeyClickAction1 := "gosub,CapsLock_SgF"	;{Right}|向右→
	; GV_KeyClickAction2 := "GoFun,"MouseMoveSleep(+85,0,0.1,10.1)""
	; gosub,Sub_KeyClick123
; return
;***********************************************
CapsLock_SgE:	;20240415
	if (GV_ToggleCapsMouseMove==1){
		MouseMoveSleep(0,-25,0.1,0.1)
		Sleep, 100
		SendInput,{Click}
	}
	else
	{
		SendInput,{Up}
		; MouseMove, 0, -20, 0, R
		; MoveToNextIcon("Up")
	}
return

CapsLock_SgD:	;20240415
	if (GV_ToggleCapsMouseMove==1)
	{
		MouseMoveSleep(0,+25,0.1,0.1)
		Sleep, 100
		SendInput,{Click}
	}
	else
	{
		SendInput,{Down}
		; MouseMove, 0, +20, 0, R
		; MoveToNextIcon("Down")
	}
return

CapsLock_SgS:	;20240415
	if (GV_ToggleCapsMouseMove==1)
	{
		MouseMoveSleep(-15,0,0.1,0.1)
		Sleep, 100
		SendInput,{Click}
	}
	else
	{
		SendInput,{Left}
		; MouseMove, -25, 0, 0, R
		; MoveToNextIcon("Left")
	}
return

CapsLock_SgF:	;20240415
	if (GV_ToggleCapsMouseMove==1)
	{
		MouseMoveSleep(+15,0,0.1,0.1)
		Sleep, 100
		SendInput,{Click}
	}
	else
	{
		SendInput,{Right}
		; MouseMove, +25, 0, 0, R
		; MoveToNextIcon("Right")
	}
return
;***********************************************
;***********************************************
; 定义一个函数来移动到下一个图标
MoveToNextIcon(direction) {
	; 获取当前光标下图标的大小(步进步长)
	CoordMode, Mouse, Window
	MouseGetPos,X0, Y0
	SendInput, {F2}{Blind}^{Home}
	X1 := A_CaretX
	Y1 := A_CaretY
	Sleep,500
	; MsgBox, %X1%,%Y1%
	SendInput,{Esc}

	; 模拟按下方向键
	SendInput, {%direction%}
	Sleep,50

	SendInput, {F2}{Blind}^{Home}
	X2 := A_CaretX
	Y2 := A_CaretY
	Sleep,500
	; MsgBox, %X2%,%Y2%
	SendInput,{Esc}

	; 计算图标的大小(步进步长)
	iconWidth  := Abs(X1 - X2)
	iconHeight := Abs(Y1 - Y2)
	; MsgBox, %iconWidth%,%iconHeight%

	; 根据方向,移动鼠标到下一个图标的位置
	if (direction = "Right") {
	MouseMove,+iconWidth,0,R
	} else if (direction = "Left") {
	MouseMove,-iconWidth,0,R
	} else if (direction = "Up") {
	MouseMove,0,-iconWidth,0,R
	} else if (direction = "Down") {
	MouseMove,0,+iconWidth,0,R
	} else {
	Eztip("方向不明",1)
	return
	}
}
;***********************************************
;适合鼠标箭头和方向键同步移动,by tuutg,20240415
ArrowMoveNextIcon(direction,x,y,s1,s2){
	CoordMode, Mouse, Window
	MouseGetPos,x1pos, y1pos
	Sleep, % 100*s1
	SendInput,{%direction%}
	MouseMove, %x%, %y%, 0, R
	Sleep, % 100*s2
	MouseGetPos,xpos, ypos
	; Click %xpos%, %ypos%
}
;***********************************************
;***********************************************
CapsLock & q::
	GV_KeyClickAction1 := "gosub,CapsLock_SgQ"	;{Home}|标签向左<-
	GV_KeyClickAction2 := "SendInput,{PgUp}"	;20231223
	gosub,Sub_KeyClick123
return

CapsLock_SgQ:	;20240323
	if (A_Cursor = "IBeam") or (A_CaretX !="")
	{
		SendInput,{Home}
	}
	else
	{
		SendInput,^+{tab}
	}
return

CapsLock & w:: ;双击关闭/单击保存
	if (GV_KeyClick_Continuous(GV_KeyTimer)){
		SendInput,^s		;先保存
		Sleep,100
		SendInput,^w		;关闭窗口 by Tuutg 2022/11/13
		EzTip("文件保存并关闭成功!",1)
		return
	}
	else {
		if (A_Cursor = "IBeam") or (A_CaretX !=""){
			SendInput,{Click}^s ;单击, 保存
		}
		else
		{
			SendInput,{Click}^s ;鼠标,单击,20240417
		}
		return
	}
return

CapsLock & r::SendInput,{Blind}{Delete}	;记忆remove
; CapsLock & r::SendInput,{Delete}{Enter} ;自动秒删,20240323
CapsLock & t::SendInput,{Blind}{BackSpace} ;by Tuutg 2022/10/17

CapsLock & a::		;by tuutg 2023/08/02 22:46
	GV_KeyClickAction1 := "gosub,CapsLock_SgA"	;{End}|标签向右->
	GV_KeyClickAction2 := "SendInput,{Blind}{PgDn}"	;20231223
	gosub,Sub_KeyClick123
return
;***********************************************
CapsLock_SgA:	;20240323
	if (A_Cursor = "IBeam") or (A_CaretX !="")
	{
		SendInput,{Blind}{End}
	}
	else
	{
		SendInput,{Blind}^{tab}
	}
return

; CapsLock & g::SendInput,{Blind}{Enter} ;by tuutg 2024/03/11
CapsLock & g:: ;{Enter}优先|Ctrl+G,跳转|编组;;自动保存,只在如Code|Office|Wps系列软件启用,20240327
if (WinActive("ahk_group OfficeAndWPS") or WinActive("ahk_group Group_Code") or WinActive("ahk_group EnterableAutoSave")){
	if (GV_ToggleAotuSaveMode==1) and !WinActive("ahk_group Group_CodeFind"){	;自动保存,20240328
			GV_KeyClickAction1 := "SendInput,{Enter}^s" ;Enter|保存
			GV_KeyClickAction2 := "SendInput,^g" ;跳转|编组
			gosub,Sub_KeyClick123
		}
	else {
		GV_KeyClickAction1 := "SendInput,{Blind}{Enter}" ;Enter
		GV_KeyClickAction2 := "SendInput,{Blind}^g" ;跳转|编组
		gosub,Sub_KeyClick123
	}
}
else {
	GV_KeyClickAction1 := "SendInput,{Blind}{Enter}" ;Enter
	GV_KeyClickAction2 := "SendInput,{Blind}^g" ;跳转|编组
	gosub,Sub_KeyClick123
}
return

CapsLock & z::SendInput,{Blind}^z	;撤销
$^+z::SendInput,^y	;重做
; $!+z::ControlSend,,^y	;重做,屏蔽火绒弹窗拦截冲突
; CapsLock & x::SendInput,{Blind}^x ;by Tuutg 2022/10/17,剪切
; CapsLock & x::SendInput,{Blind}{Del}
; CapsLock & c::SendInput,{Blind}^c	;原版复制
; CapsLock & v::SendInput,{Blind}^v	;原版粘贴
; CapsLock & b::gosub,Sub_uToolsSelectTxt	;一键uTools搜索

;右键菜单,任何位置,20240324
;CapsLock & y::SendInput,{AppsKey}
;CapsLock & y::SendInput,{RButton}
;CapsLock & y::Click , , Right , 1
CapsLock & y::SendInput,{Click,Right}

;媒体相关
CapsLock & 9::SendInput,{Media_Prev}
CapsLock & 0::SendInput,{Media_Next}

CapsLock & -::gosub,Sub_volDown
CapsLock & =::gosub,Sub_volUp
CapsLock & Del::gosub,Sub_volMute

CapsLock & BackSpace::SendInput,{Media_Play_Pause}

CapsLock & PgUp::SendInput,{Media_Prev}
CapsLock & PgDn::SendInput,{Media_Next}

;移动鼠标光标，例如用在屏幕取色
CapsLock & Up::MouseMove, 0, -1, 0, R
CapsLock & Down::MouseMove, 0, 1, 0, R
CapsLock & Left::MouseMove, -1, 0, 0, R
CapsLock & Right::MouseMove, 1, 0, 0, R
CapsLock & '::SendInput,{Click} ;鼠标左键单击

;************** u,i单击双击^ **************
;CapsLock & i::SendInput,{Blind}^{Home}
;CapsLock & u::SendInput,{Blind}^{End}
;CapsLock & n::SendInput,{Blind}{PgDn}
;CapsLock & m::SendInput,{Blind}{PgUp}

CapsLock & u::
	GV_KeyClickAction1 := "SendInput,{Blind}{End}"
	GV_KeyClickAction2 := "SendInput,{Blind}^{End}"
	gosub,Sub_KeyClick123
return

CapsLock & i::
	GV_KeyClickAction1 := "SendInput,{Blind}{Home}"
	GV_KeyClickAction2 := "SendInput,{Blind}^{Home}"
	gosub,Sub_KeyClick123
return

CapsLock & n::
	GV_KeyClickAction1 := "SendInput,{Blind}{PgDn}"
	GV_KeyClickAction2 := "SendInput,{Blind}^{PgDn}"
	gosub,Sub_KeyClick123
return

CapsLock & m::
	GV_KeyClickAction1 := "SendInput,{Blind}{PgUp}"
	GV_KeyClickAction2 := "SendInput,{Blind}^{PgUp}"
	gosub,Sub_KeyClick123
return

;************** u,i单击双击$ ******************

;***************** 剪贴板相关^ ****************
CapsLock & v::	;单击粘贴,双击纯文本粘贴|Ez右键菜单面板
	if (CapsLockV_presses > 0)
	{
		CapsLockV_presses += 1
		return
	}
		CapsLockV_presses = 1
		SetTimer, KeyCapsLockV, 300
return

; CapsLock & v::
	; GV_KeyClickAction1 := "GoSub,PastePureText"
	; GV_KeyClickAction2 := "gosub,EzOtherMenuShow"
	; gosub,Sub_KeyClick123
; return

;调用的标签
KeyCapsLockV:
	SetTimer, KeyCapsLockV, off
	if CapsLockV_presses = 1
	{
		; GoSub,PastePureText	;纯文本粘贴
		SendInput,{Blind}^v	;单击直接粘贴
	}
	else if CapsLockV_presses = 2
	{
		;Menu, MyMenu, Show
		;EzOtherMenuShow()
		;gosub,EzOtherMenuShow	;弹出Ez快捷菜单面板
		gosub,PastePureText	;双击纯文本粘贴
		;gosub,Sub_ClipAppendCopyOName	;只获取文件名,无后缀
	}
	CapsLockV_presses = 0
return

EzOtherMenuShow:
	Menu, MyMenu, UseErrorLevel
	Menu, MyMenu, DeleteAll
	Menu, MyMenu, Add, &B一键百度搜索, Sub_SearchSelectTxt
	Menu, MyMenu, Add, &Everything搜索, Sub_EverythingSelectTxt
	Menu, MyMenu, Add, &Anytext搜索, Sub_AnytextSelectTxt
	Menu, MyMenu, Add, &G-Loom搜索, Sub_LoomSelectTxt
	Menu, MyMenu, Add, u&Tools搜索, Sub_uToolsSelectTxt
	Menu, MyMenu, Add  ; 添加分隔线.
	Menu, MyMenu, Add, &B播放器打开, Sub_OpenUrlByPlayer
	Menu, MyMenu, Add  ; 添加分隔线.
	Menu, MyMenu, Add, &S纯文本粘贴, PastePureText
	Menu, MyMenu, Add, &Z转换后粘贴, JoinAndPaste
	Menu, MyMenu, Add  ; 添加分隔线.
	Menu, MyMenu, Add, 添加&Quote, PasteQuote
	Menu, MyMenu, Add, 添加&Code, PasteCode
	Menu, MyMenu, Add, 添加磁头&Magnet, PasteMagnet

	Menu, MyMenu, Add ; 添加分隔线.
	GoSub,EzOtherMenu_MyApps
	Menu, MyMenu, Add ; 添加分隔线.

	Menu, MyMenu, Add, 取消[&X],EzOtherMenu_DeleteAll
	Menu, MyMenu, Show
return

EzOtherMenu_DeleteAll:
	Menu, MyMenu, DeleteAll
return


EzOtherMenu_MyApps:
	try {
		MyAppsIni := % A_ScriptDir . "\capsez_mymenus.ini"

		cur_menus := ""
		current_app := ""
		IniRead, apps, %MyAppsIni%, MyMenus
		arr_app := StrSplit(apps, "`n", "`r")
		loop % arr_app.MaxIndex()
		{
			;Notepad=ahk_class Notepad
			app := StrSplit(arr_app[A_Index],"=")
			if WinActive(app[2]){
				current_app := app[1]
				break
			}
			else {
				continue
			}
		}

		IniRead, cur_menus, %MyAppsIni%, %current_app%
		arr_cur_menus := StrSplit(cur_menus, "`n", "`r")
		loop % arr_cur_menus.MaxIndex()
		{
			;[Notepad]
			;去掉尾部文字=notepad_trim
			;将空格和tab删除=notepad_cleanWhitespace
			cm := StrSplit(arr_cur_menus[A_Index],"=")
			cm_name := cm[1]
			cm_cmd := cm[2]
			Menu, MyMenu, Add, %cm_name%, %cm_cmd%
		}
	} catch e {
	}
return

PastePureText:
	if WinActive("ahk_class ConsoleWindowClass")
	{
		; SendInput,!{Space}ep
		SendInput,{Click,Right}	;cmd粘贴
	}
	else
	{
		Clipboard = %Clipboard% ;转成纯文本
		SendInput,{Blind}^v{Enter}	;原版,20240411
	}
return

JoinAndPaste:
	clip:=Clipboard
	;例子1
	;对tc中文件复制文件名之后处理，添加双引号并且把多行合并成一行并用竖杠连接
	;clip := RegExReplace(clip, "(.+)(`r`n)?", """$1""`|")
	;例子2
	;对everything的搜索结果进行处理，去掉双引号并且把多行合并成一行并用tab分隔
	clip := RegExReplace(clip, "("".+"")(`r`n)?", "$1`t")
	StringTrimRight, clip, clip, 1
	Clipboard = %clip%
	send,{Blind}^v
return


PasteQuote:
	SendInput,{Blind}^c<quote>{Blind}^v</quote>
return

PasteCode:
	SendInput,{Blind}^c<code>{Blind}^v</code>
return

PasteMagnet:
	SendInput,^cmagnet:?xt=urn:btih:{Blind}^v
return

#include %A_ScriptDir%\capsez_myapps.ahk

;一键百度搜索
Sub_SearchSelectTxt:
	SendInput,^c
	Sleep,300
	clip:=
	if (WinActive("ahk_group Group_explorer") or WinActive("ahk_group Group_Desktop")){
		clip:=GetFileInfo(Clipboard,3)	;只获取文件名
	}
	else {
		clip:=Clipboard
	}
	if RegExMatch(clip, "^\d{6}$"){
		Out := gv_url_tdx_f10 . clip . gv_url_html
		Run,%Out%	;股票软件F10
	}
	else {
		Run,http://www.baidu.com/s?ie=utf-8&wd=%clip%
	}
return

;一键Ev搜索
Sub_EverythingSelectTxt:
	SendInput,^c
	Sleep,300
	clip:=
	if (WinActive("ahk_group Group_explorer") or WinActive("ahk_group Group_Desktop")){
		clip:=GetFileInfo(Clipboard,3)	;只获取文件名
	}
	else {
		clip:=Clipboard
	}
	; Run,%COMMANDER_PATH%\Everything.exe -s %clip%
	IfExist,C:\Windows\SysWOW64\
		Run,"%RunCaptX%\Everything\Everything.exe" -s %clip%
	else
		Run,"%RunCaptX%\Everything\Everything-x86\Everything.exe" -s %clip%
return

;一键mpv.exe播放
Sub_OpenUrlByPlayer:
	SendInput,^c
	Sleep,300
	clip:=
	clip:=Clipboard
	Run,%COMMANDER_PATH%\Plugins\WLX\vlister\mpv.exe "%clip%"
return

;一键Anytext搜索, atgui /s {query}
Sub_AnytextSelectTxt:
	SendInput,^c
	Sleep,100
	clip:=
	if (WinActive("ahk_group Group_explorer") or WinActive("ahk_group Group_Desktop")){
		clip:=GetFileInfo(Clipboard,3)	;只获取文件名
	}
	else {
		clip:=Clipboard
	}
	Run,"%RunCaptX%\SoftDir\AnyTXT Searcher\ATGUI.exe" atgui /s %clip%
return

;***********************************************
;20240322
CapsLock & x::	;复制并右侧显示clip(Clipboard内容)|路径
	GV_KeyClickAction1 := "SendInput,{Blind}^x"	;原版剪切
	GV_KeyClickAction2 := "gosub,Sub_ClipAppendCopyOPath"	;复制路径
	gosub,Sub_KeyClick123
return

;***********************************************
;20240322
CapsLock & c::	;复制并右侧显示clip(Clipboard内容)|复制全文件名
	GV_KeyClickAction1 := "gosub,Sub_ClipAppend"	;原版复制|右侧显示
	GV_KeyClickAction2 := "gosub,Sub_ClipAppendCopyFPath"	;复制全文件名
	gosub,Sub_KeyClick123
return

;***********************************************
;20240322
; CapsLock & v::	;复制并右侧显示clip(Clipboard内容)|单文件名
	; GV_KeyClickAction1 := "SendInput,{Blind}^v"	;原版粘贴
	; GV_KeyClickAction2 := "Sub_ClipAppendCopyOName"	;复制单文件名
	; gosub,Sub_KeyClick123
; return

;***********************************************
;CapsLock & g:: SendInput,{Blind}^w	;Ez原版
;***************** 剪贴板相关$ ******************

;**************CapsLock窗口相关^ ****************
CapsLock & o::SendInput,{Blind}^+{Tab} ;标签向左<-
CapsLock & p::SendInput,{Blind}^{Tab} ;标签向右->
CapsLock & Tab::SendInput,{Blind}+^{Tab} ;按住Caps不放，再按Alt,标签向右->,20240329
CapsLock & Alt::SendInput,{Blind}^{Tab} ;按住Caps不放，再按Alt,标签向左<-,20240329

;AltTabMenu 用处不大
CapsLock & .::AltTab
CapsLock & ,::ShiftAltTab

;分号最小化/关闭窗口
;CapsLock & `;::WinClose A
CapsLock & `;:: WinMinimize A

;Enter 回车窗口最大化
CapsLock & Enter::gosub,Sub_MaxRestore
;CapsLock & Space:: WinMinimize A
;**************CapsLock窗口相关$ ****************

;自定义 , By Tuutg
;***************** CapsLock自定义^ **************
;将单击caps替换为esc,双击caps映射双击Ctrl激活Listary
CapsLock::	;已适配的12种关联用法|...20240324
	Suspend Permit
	if (GV_KeyClick_Continuous(GV_KeyTimer)){
		gosub,Listary	;1.{Ctrl S50 2},激活Listary搜索框
		Sleep,300	;移动鼠标到Listary搜索框
		MouseMove,A_CaretX,A_CaretY,0 ;0-瞬时移动鼠标到目标位置
	}else if (WinActive("ahk_group Group_browser")){
		; SendInput,{BackSpace}	;2.浏览器退回上页
		SendInput,!{Left}		;2.浏览器退回上页
	}else if (WinActive("ahk_exe mspaint.exe")){
		SendInput,^s	;先保存
		Sleep,50
		SendInput,{Escape}	;取消
		Sleep,50
		SendInput,!hser	;3.回到选择工具|同PS
	}else if (WinActive("ahk_class Photoshop")){
		SendInput,{RShift}	;4.英文输入法,重置英文,要KBLAutoSwitch支持,20240520
		SendInput,^s	;先保存
		Sleep,50
		SendInput,{Escape} ;取消
		Sleep,50
		SendInput,^d	;取消选区选择
		Sleep,50
		SendInput,{Text}v	;4.回到移动工具
	}else if (WinActive("ahk_exe explorer.exe")){	;20240401
		if (A_Cursor != "IBeam"){	 ;非输入模式
			SendInput,{Click}	;5.{Click}左键单击,可用于重命名
		}
		else {	;输入模式
			SendInput,{Escape} ;5.取消,20240416
		}
	}else if (WinActive("ahk_class TTOTAL_CMD")){	;关闭|取选|重置英文
		SendInput,{Escape}	;6.退出|关闭
		Sleep,50
		TcSendUserCommand("cm_ClearAll")	;取消选择
		Sleep,50
		SendInput,{RShift}	;6.英文输入法,重置英文,要KBLAutoSwitch支持,20240520
	}else if (WinActive("ahk_group DisableAutoSave")){
		SendInput,{Escape}	;7.退出|关闭
	}else if (WinActive("ahk_class #32770") and !WinActive("ahk_group GroupDiagOpenAndSave") and !WinActive("ahk_group Group_CodeFind")){
		SendInput,{Escape}	;8.退出|关闭
	}else if (WinActive("ahk_class #32770") and WinActive("ahk_group Group_CodeFind")){	;20240324,查找对话框中
		; SendInput,{Enter}	;9.Enter|默认按钮=F3
		SendInput,{F3}	;9.Enter|默认按钮=F3
	}else if WinActive("ahk_group OfficeAndWPS"){	;20240324
		SendInput,{Escape}	;10.{Esc}
		Sleep,50
		if (GV_ToggleAotuSaveMode==1){
			SendInput,^s	;10.改动后保存,20240325
		}
	}else if (WinActive("ahk_exe BCompare.exe") or WinActive("ahk_exe uc.exe") or WinActive("ahk_exe WinMergeU.exe")){
		;20240325
		SendInput,{Escape}	;11.{Escape}
	}else {
		if (GV_ToggleAotuSaveMode==1){
			SendInput,^s	;12.先保存
			Sleep,50
		}
		SendInput,{Escape}	;12.退出|关闭
		Sleep,50
		SendInput,{RShift}	;12.英文输入法,在KBLAutoSwitch中设定,{Rshift}=英文
	}
return

;***********************************************

;暂停
CapsLock & Pause::
	Suspend Permit
	Suspend Toggle
return

;***********************************************

; +CapsLock:: CapsLock ;"之前的写法
; ^PrintScreen::
; control + CapsLock to Toggle CapsLock.  alwaysoff/On so that the key does not blink
<^CapsLock::	;开/关CapsLock,可选右侧Ctrl+CapsLock.防止和Listary激活冲突
; !CapsLock::
	GetKeyState t, CapsLock, T
	IfEqual t,D, SetCapsLockState AlwaysOff
	else SetCapsLockState AlwaysOn
return

;***************** CapsLock自定义$ **************

;************** 全局一键搜索相关^ ***************
;映射成双击Ctrl激活Listary后搜索选定文件(夹)名,并保留搜索词语
CapsLock & Space::	;20240322
if WinExist("ahk_exe Listary.exe") {
	gosub,Sub_ListarySelectTxt
	}
else
	WinMinimize A
return
;***********************************************
;一键Listary搜索
Sub_ListarySelectTxt:
	;方法1:Listary/quicek中需设定好激活键Win+Alt+S
	;SendInput,#!s
	;方法2,无需设定,保持Listary初始设定,映射成双击Ctrl激活Listary
	;以复制+后处理方式获取文件(夹)名,
	SendInput,^c
	Sleep,300
	clip:=
	if (WinActive("ahk_group Group_explorer") or WinActive("ahk_group Group_Desktop")){
		clip:=GetFileInfo(Clipboard,3)	;只获取文件名
	}
	else {
		clip:=Clipboard
	}
	Sleep,100
;***********************************************
	;双击Ctrl激活Listary
	SendInput, {Ctrl Down}
	Sleep,50
	SendInput, {Ctrl Up}
	Sleep,50
	SendInput, {Ctrl Down}
	Sleep,50
	SendInput, {Ctrl Up}
	Sleep,100
	SendInput, ^a	;全选
	Sleep,50
	; SendInput, {Del}	;清空搜索框
	; Sleep,50
	SendInput, %clip%
	; Sleep,50
	; clip:=
	; Clipboard :=
return
;***********************************************
;***********************************************
;一键uTools搜索,20240322
;uTools优先复制并右侧显示clip(Clipboard内容)|复制全文件名
CapsLock & b::
	GV_KeyClickAction1 := "gosub,Sub_KeyCapsLockB"	;uTools优先|复制无后缀文件名|前5行复制
	GV_KeyClickAction2 := "gosub,Sub_ClipAppendCopyFName"	;复制全文件名,有后缀|后5行复制
	gosub,Sub_KeyClick123
return

;***********************************************

;uTools|获取无后缀文件名,20240401
Sub_KeyCapsLockB:
if WinExist("ahk_exe uTools.exe"){
	gosub,Sub_uToolsSelectTxt
}
else {
	gosub,Sub_ClipAppendCopyOname	;复制无后缀文件名
}
return

;***********************************************
;uTools 搜索选定文件(夹)名,并保留搜索词语
Sub_uToolsSelectTxt:
	;以复制+后处理方式获取文件(夹)名
	SendInput,^c
	Sleep,300
	clip:=
	if (WinActive("ahk_group Group_explorer") or WinActive("ahk_group Group_Desktop")){
		clip:=GetFileInfo(Clipboard,3)	;只获取文件名
	}
	else {
		clip:=Clipboard
	}
	Sleep,100
;***********************************************
	;双击Alt激活uTools
	SendInput, {RAlt Down}
	Sleep,50
	SendInput, {RAlt Up}
	Sleep,50
	SendInput, {RAlt Down}
	Sleep,50
	SendInput, {RAlt Up}
	Sleep,50
	WinActivate, uTools ahk_class Chrome_WidgetWin_1
	Sleep,100
	SendInput, ^a	;全选
	Sleep,50
	; SendInput, {Del}	;清空搜索框
	; Sleep,50
	SendInput, %clip%
	; Sleep,50
	; clip:=
	; Clipboard :=
return
;***********************************************
;***********************************************
;一键Loom搜索
;Loom 搜索选定文件(夹)名,并保留搜索词语
Sub_LoomSelectTxt:
; $!c:: ;gosub,Loom
	;以复制+后处理方式获取文件(夹)名
	SendInput,^c
	Sleep,300
	clip:=
	if (WinActive("ahk_group Group_explorer") or WinActive("ahk_group Group_Desktop")){
		clip:=GetFileInfo(Clipboard,3)	;只获取文件名
	}
	else {
		clip:=Clipboard
	}
	Sleep,100
;***********************************************
	;!c激活Loom
	SendInput, !c
	WinActivate, ahk_class Window Class ahk_exe Loom.exe
	Sleep,100
	SendInput, ^a	;全选
	Sleep,50
	SendInput, %clip%
	Sleep,50
	; clip:=
	; Clipboard :=
return
;************** 全局一键搜索相关$ ***************
;*****************  CapsLock相关$  **************


;************** 分号;相关^ ************** {{{2
`; & j:: SendInput,{Blind}{Down}
`; & k:: SendInput,{Blind}{Up}
`; & h:: SendInput,{Blind}{Left}
`; & l:: SendInput,{Blind}{Right}
`; & n:: SendInput,{Blind}{PgDn}
`; & m:: SendInput,{Blind}{PgUp}

`; & Space::SendInput,{Delete}

`; & z::
	GV_KeyClickAction1 := "SendInput,{BackSpace}"
	GV_KeyClickAction2 := "SendInput,+{Home}{BackSpace}"
	gosub,Sub_KeyClick123
return

`; & b::	;20240401
; `; & x::	;20240401
	GV_KeyClickAction1 := "SendInput,{Delete}"
	GV_KeyClickAction2 := "SendInput,+{End}{Delete}"
	gosub,Sub_KeyClick123
return

`; & c::
	GV_KeyClickAction1 := "SendInput,{Blind}^c"
	GV_KeyClickAction2 := "SendInput,^{Home}^+{End}^c"
	gosub,Sub_KeyClick123
return

; `; & b::	;20240401
`; & x::	;20240401
	GV_KeyClickAction1 := "SendInput,{Blind}^x"
	GV_KeyClickAction2 := "SendInput,^{Home}^+{End}^x"
	gosub,Sub_KeyClick123
return

`; & v::
	GV_KeyClickAction1 := "SendInput,^v"
	GV_KeyClickAction2 := "SendInput,^{Home}^+{End}^v"
	gosub,Sub_KeyClick123
return

;粘贴然后回车，多用在搜索框等输入的位置，一个双手，一个单手
`; & g::
	GV_KeyClickAction1 := "SendInput,^v{Enter}"
	GV_KeyClickAction2 := "SendInput,^{Home}^+{End}^v{Enter}"
	gosub,Sub_KeyClick123
return

;搜索选中的文本
`; & s::gosub,Sub_SearchSelectTxt

;清空复制粘贴
`; & d::SendInput,{Home 2}+{End}{BackSpace}
`; & a::SendInput,^{Home}^+{End}{Delete}

;任何地方编辑，复制后转到Vim中粘贴再继续编辑
`; & i::
	GV_KeyClickAction1 := "gosub,Sub_CopyVim"
	GV_KeyClickAction2 := "gosub,Sub_CopyAllVim"
	gosub,Sub_KeyClick123
return

;粘贴并转到,多数浏览器和tc中都可用
`; & u::SendInput,^t!d^v{Enter}
;`; & u::SendInput,^t!dwww.^v{Enter}

`; & 1::AscSend(fun_GetFormatTime("yyyyMMdd"))
;`; & 1::AscSend(fun_GetFormatTime("yyyy-MM-dd"))
`; & 2::AscSend(fun_GetFormatTime("HH:mm"))
`; & 3::AscSend("#" . fun_GetFormatTime("MMdd"))

;恢复分号自身功能
$`;::SendInput,`;

;`;::SendInput,`;
^`;::SendInput,^`;
+`;::SendInput,+`;
^+`;::SendInput,^+`;
!`;::SendInput,!`;
:::SendInput,:
;************** 分号;相关结束$ **************


;************** Space空格键相关^ ************** {{{2
; vim等中冲突排除，但在tc中不能连续空格.左手区设为符号区,切换开关,^!#Space
; 右手通用模式,同Caps模式,+shift符号模式
#If (GV_ToggleSpaceKeys==1)
;右手通用模式
Space & j:: SendInput,{Blind}{Down}
Space & k:: SendInput,{Blind}{Up}
Space & h:: SendInput,{Blind}{Left}
Space & l:: SendInput,{Blind}{Right}

; Space & b::SendInput,{Blind}^{Left}
; Space & w::SendInput,{Blind}^{Right}
; Space & a::SendInput,{Blind}{PgDn}
; Space & q::SendInput,{Blind}{PgUp}

; Space & c::
	; GV_KeyClickAction1 := "SendInput,{Blind}^c"
	; GV_KeyClickAction2 := "SendInput,^{Home}^+{End}^c"
	; gosub,Sub_KeyClick123
; return

Space & y::SendInput,{Click,Right} ;鼠标右(Y)键单击
; Space & '::SendInput,{Click} ;单引号'鼠标左键单击
Space & o::SendInput,^+{Tab} ;标签向左<-
Space & p::SendInput,^{Tab} ;标签向右->

Space & u::
	GV_KeyClickAction1 := "SendInput,{End}"
	GV_KeyClickAction2 := "SendInput,^{End}"
	gosub,Sub_KeyClick123
return

Space & i::
	GV_KeyClickAction1 := "SendInput,{Home}"
	GV_KeyClickAction2 := "SendInput,^{Home}"
	gosub,Sub_KeyClick123
return

Space & n::
	GV_KeyClickAction1 := "SendInput,{PgDn}"
	GV_KeyClickAction2 := "SendInput,^{PgDn}"
	gosub,Sub_KeyClick123
return

Space & m::
	GV_KeyClickAction1 := "SendInput,{PgUp}"
	GV_KeyClickAction2 := "SendInput,^{PgUp}"
	gosub,Sub_KeyClick123
return

;恢复默认打字上屏功能,开启后微信不能Space输入联系人,可以Sapce+X|C|V
; $Space::SendInput,{Blind}{Space}
; $Space::Space	;恢复默认打字上屏功能
^Space::^Space
+Space::+Space

;***********************************************
/*;Space左手通用模式,会干扰到其他模式,关闭掉,在其他模式单独写
if (GV_SpaceKeysAsCapsLock==0){

Space & q:: ;到首部|上翻页,20240320
if (GV_KeyClick_Continuous(GV_KeyTimer)){
	if (A_Cursor = "IBeam") or (A_CaretX !="")
		SendInput,^{Home} ;到首部
	else
		SendInput,{PgUp} ;上翻页
} else{
	SendInput,{Home} ;到首部
}
return

Space & w::SendInput,{Click}^s	;单击保存

Space & e::SendInput,{Up} ;{Up}

Space & r::SendInput,{Del} ;输入{Del}

Space & t::SendInput,{Backspace} ;输入{Backspace}

;***********************************************

Space & a:: ;到末尾|上翻页,20240320
if (GV_KeyClick_Continuous(GV_KeyTimer)){
	if (A_Cursor = "IBeam") or (A_CaretX !="")
		SendInput,^{End} ;下翻页
	else
		SendInput,{PgDn} ;下翻页
} else{
	SendInput,{End} ;到末尾
}
return

Space & s::SendInput,{Left}	;单击{Left}

Space & d::SendInput,{Down}	;单击{Down}

Space & f::SendInput,{Right} ;单击{Right}

Space & g::SendInput,{Enter}^s ;输入{Enter}

;***********************************************

Space & z::SendInput,^z ;输入Ctrl+Z

Space & x::gosub,Space_SgX ;剪切+内容显示

Space & c::gosub,Space_SgC ;复制+内容显示

Space & v::SendInput,^v ;原版粘贴

Space & b::	;鼠标右(Y)键单击|输入单冒号:,20240318
	GV_KeyClickAction1 := "gosub,Space_SgB0" ;冒号:|鼠标右(Y)键单击
	GV_KeyClickAction2 := "SendInput,{MButton}" ;中键单击,20240331
	gosub,Sub_KeyClick123
return

}
*/

;***********************************************

;关闭,20240327,会和独立模式干涉
; Space & 1::	;输入0|!键,相加=11
	; GV_KeyClickAction1 := "SendInput,{0}"
	; GV_KeyClickAction2 := "SendInput,{Text}!"
	; gosub,Sub_KeyClick123
; return

; Space & 2::	;输入2|@键,相加=11
	; GV_KeyClickAction1 := "SendInput,{9}"
	; GV_KeyClickAction2 := "SendInput,{Text}@"
	; gosub,Sub_KeyClick123
; return

; Space & 3::	;输入3|#键,相加=11
	; GV_KeyClickAction1 := "SendInput,{8}"
	; GV_KeyClickAction2 := "SendInput,{Text}#"
	; gosub,Sub_KeyClick123
; return

; Space & 4::	;输入4|$键,相加=11
	; GV_KeyClickAction1 := "SendInput,{7}"
	; GV_KeyClickAction2 := "SendInput,{Text}$"
	; gosub,Sub_KeyClick123
; return

; Space & 5::	;输入5|%键,相加=11
	; GV_KeyClickAction1 := "SendInput,{6}"
	; GV_KeyClickAction2 := "SendInput,{Text}%"
	; gosub,Sub_KeyClick123
; return

;通用符号模式,等于按Shift的效果
Space & 6:: SendInput,{Text}^ ;输入^键
Space & 7:: SendInput,`& ;输入&键
Space & 8:: SendInput,`* ;输入*键
Space & 9:: SendInput,`( ;输入(键
Space & 0:: SendInput,`) ;输入)键

Space & -:: SendInput,`_ ;输入_键
Space & =:: SendInput,{NumpadAdd} ;输入+键
Space & [:: SendInput,{Text}{ ;输入{键
Space & ]:: SendInput,{Text}} ;输入}键
Space & `;:: SendInput,`: ;输入:键
Space & ':: SendInput,`" ;输入"键
Space & \:: SendInput,`| ;输入|键
Space & /:: SendInput,`? ;输入)键
Space & ,:: SendInput,`< ;输入)键
Space & .:: SendInput,`> ;输入)键

#If

;****************************************************
; Space & g 排除打开|保持|...等对话框组,用于Listary路径跳转模式
; 通用Space增强模式,约等于Caps,Space+EDSF=上下左右通用|文本模式带Ctrl效果,启用设为1.20240415
#If (GV_SpaceKeysAsCapsLock==1) and !WinActive("ahk_group Group_SingleSpace") and !WinActive("ahk_group Group_browser") and !WinActive("ahk_group SpaceKeysAsShiftInTC")

Space & q:: ;到首部|上翻页,20240320
if (GV_KeyClick_Continuous(GV_KeyTimer)){
	if (A_Cursor = "IBeam") or (A_CaretX !="")
		SendInput,^{Home} ;到首部
	else
		SendInput,{PgUp} ;上翻页
}
else {
	SendInput,{Home} ;到首部
}
return

Space & w::	;输入^|&键
	GV_KeyClickAction1 := "gosub,Space_SgW0"  ;单击保存|资管左键
	GV_KeyClickAction2 := "SendInput,{Text}&" ;逻辑与,&
	gosub,Sub_KeyClick123
return

Space & e::	;{Up}|输入-键,20240415,自带Ctrl效果,快速移动
	GV_KeyClickAction1 := "gosub,Space_SgE0"
	GV_KeyClickAction2 := "SendInput,`-"
	gosub,Sub_KeyClick123
return

Space & r::	;输入{del}|=|+键
	GV_KeyClickAction1 := "SendInput,{Del}"
	GV_KeyClickAction2 := "SendInput,{NumpadAdd}" ;+
	gosub,Sub_KeyClick123
return

Space & t::		;输入{Backspace}|\键
	GV_KeyClickAction1 := "SendInput,{Backspace}"
	GV_KeyClickAction2 := "SendInput,`|" ;逻辑或,|
	gosub,Sub_KeyClick123
return

;***********************************************

Space & a:: ;到末尾|下翻页,20240328
if (GV_KeyClick_Continuous(GV_KeyTimer)){
	if (A_Cursor = "IBeam") or (A_CaretX !="")
		SendInput,^{End} ;到末尾
	else
		SendInput,{PgDn} ;下翻页
} else{
	SendInput,{End} ;到末尾,同Excel模式
}
return

Space & s::	;输入{Left}/(数字/),20240415,自带Ctrl效果,快速移动
	GV_KeyClickAction1 := "gosub,Space_SgS0"
	GV_KeyClickAction2 := "SendInput,`/" ;{?/}键
	gosub,Sub_KeyClick123
return

Space & d::	;单击{Down},双击输入逗(D)号,20240415,自带Ctrl效果,快速移动
	GV_KeyClickAction1 := "gosub,Space_SgD0"
	GV_KeyClickAction2 := "SendInput,`,"
	gosub,Sub_KeyClick123
return

Space & f::	;单击{Right},双击输入分号(F);键,20240415,自带Ctrl效果,快速移动
	GV_KeyClickAction1 := "gosub,Space_SgF0"
	GV_KeyClickAction2 := "SendInput,`;" ;分号
	gosub,Sub_KeyClick123
return

Space & g:: ;输入句号(g).键,自动保存,20240328;自动保存,只在如Code|Office|Wps系列软件启用
	if (WinActive("ahk_group OfficeAndWPS") or WinActive("ahk_group Group_Code") or WinActive("ahk_group EnterableAutoSave")){
		if (GV_ToggleAotuSaveMode==1) and !WinActive("ahk_group Group_CodeFind"){	;自动保存,20240328
			GV_KeyClickAction1 := "SendInput,{Enter}^s" ;Enter|保存
			GV_KeyClickAction2 := "SendInput,`." ;小数点.|句号
			gosub,Sub_KeyClick123
		}
		else {	;20240330,GV_ToggleAotuSaveMode==0时,恢复为Enter
			GV_KeyClickAction1 := "SendInput,{Enter}" ;Enter
			GV_KeyClickAction2 := "SendInput,`." ;小数点.|句号
			gosub,Sub_KeyClick123
		}
	}else if(WinActive("ahk_group GroupDiagOpenAndSave")){	;20240430
		GV_KeyClickAction1 := "gosub,Space_SgGinDiagOAS" ;Listary,跳转
		GV_KeyClickAction2 := "SendInput,{Enter}" ;Enter
		gosub,Sub_KeyClick123
	}
	else {
		GV_KeyClickAction1 := "SendInput,{Enter}" ;Enter
		GV_KeyClickAction2 := "SendInput,`." ;小数点.|句号
		gosub,Sub_KeyClick123
	}
return

;***********************************************

Space & z::
	GV_KeyClickAction1 := "gosub,Space_SgZ" ;输入""键
	GV_KeyClickAction2 := "SendInput,`'" 	;',单引号
	gosub,Sub_KeyClick123
return

Space & x::
	GV_KeyClickAction1 := "gosub,Space_SgX"	;剪切|独立模式,复制增强
	GV_KeyClickAction2 := "gosub,Space_DbX" ;{}|]
	gosub,Sub_KeyClick123
return

Space & c::
	GV_KeyClickAction1 := "gosub,Space_SgC"	;复制|独立模式,复制增强
	GV_KeyClickAction2 := "gosub,Space_DbC" ;()|'>,Listary->cmd命令行
	gosub,Sub_KeyClick123
return

Space & v::
	GV_KeyClickAction1 := "gosub,Space_SgV0" ;原版粘贴|+Enter,20240425
	GV_KeyClickAction2 := "gosub,Space_DbV" ;{}|]
	gosub,Sub_KeyClick123
return

Space & b::	;智能判断: 鼠标右(Y)键单击|输入单冒号:|中键,20240403
	GV_KeyClickAction1 := "gosub,Space_SgB0" ;鼠标右(Y)键单击|单冒号:
	GV_KeyClickAction2 := "gosub,Space_DbB0" ;鼠标右(Y)键单击|中键
	gosub,Sub_KeyClick123
return

;***********************************************

Space & 1::	;输入0|!键,相加=11
	GV_KeyClickAction1 := "SendInput,{0}"
	GV_KeyClickAction2 := "SendInput,{Text}!"
	gosub,Sub_KeyClick123
return

Space & 2::	;输入2|@键,相加=11
	GV_KeyClickAction1 := "SendInput,{9}"
	GV_KeyClickAction2 := "SendInput,{Text}@"
	gosub,Sub_KeyClick123
return

Space & 3::	;输入3|#键,相加=11
	GV_KeyClickAction1 := "SendInput,{8}"
	GV_KeyClickAction2 := "SendInput,{Text}#"
	gosub,Sub_KeyClick123
return

Space & 4::	;输入4|$键,相加=11
	GV_KeyClickAction1 := "SendInput,{7}"
	GV_KeyClickAction2 := "SendInput,{Text}$"
	gosub,Sub_KeyClick123
return

Space & 5::	;输入5|%键,相加=11
	GV_KeyClickAction1 := "SendInput,{6}"
	GV_KeyClickAction2 := "SendInput,{Text}%"
	gosub,Sub_KeyClick123
return

Space & F1:: SendInput,{F11} ;加+10
Space & F2:: SendInput,{F12} ;加+10
Space & F3:: SendInput,{F8} ;相加=11
Space & F4:: SendInput,{F7} ;相加=11
Space & F5:: SendInput,{F6} ;相加=11

; $Space::SendInput,{Blind}{Space} ;恢复默认打字上屏功能
$Space::Space ;恢复空格打字上屏,by Tuutg,20240425
#If
;****************************************************

;****************************************************
;Space通用增强模式跳转标签
;鼠标在微信打字框内
MouseInTextBox(i){
	WinGetPos,wxx,wxy,wxw,wxh, ahk_class WeChatMainWndForPC
	wxwb := wxw - 80
	wxhb := wxh - 60
	if ((wxx>=350) and (wxy>=662))
return, true
}

;增强复制,鼠标右侧显示clip(clipboard内容)
Space_SgC: ;2024/03/16,显示clip(clipboard内容)
	if WinActive("ahk_class WeChatMainWndForPC"){	;20240520
		WinGetPos, wxx, wxy,wxw,wxh, ahk_class WeChatMainWndForPC
		MouseGetPos, x, y, WhichWindow, WhichControl
		; x >= 256 and wxh-y <= 87	;聊天打字框X,Y
		; x > 91 and y < 49	;联系人打字框坐标
		if (A_Cursor = "IBeam") and ((x >= 256 and wxh-y <= 87 ) or ( x >= 91 and y <= 49 ))
		{ ;鼠标聊天在打字框内
			SendInput,{Home}+{End}^c ;全选复制,修复空格不能直接复制
			; SendInput,{Blind}^c ;复制,修复空格不能直接复制,20240520
			Sleep,300 	;等待0.3s, 强制机械等待剪贴板出现内容
			;clip:=clipboard
			StringLeft,clipboard_left,clipboard,500
			Tooltip,%clipboard_left%    ;在鼠标右侧显示clip(clipboard内容)
			Sleep,1000
			Tooltip
			FileAppend, %Clipboard% `n, %COMMANDER_PATH%\剪切板\ClipAppend.txt
			gosub, ClipAppend
		}
		else if (A_Cursor = "IBeam") and (x >= 256 and wxh-y > 87 ){	;在记录框中
			; SendInput,{Home}+{End}^c ;全选复制,修复空格不能直接复制
			SendInput,{Blind}^c ;复制,修复空格不能直接复制,20240520
			Sleep,300 	;等待0.3s, 强制机械等待剪贴板出现内容
			;clip:=clipboard
			StringLeft,clipboard_left,clipboard,500
			Tooltip,%clipboard_left%    ;在鼠标右侧显示clip(clipboard内容)
			Sleep,1000
			Tooltip
			FileAppend, %Clipboard% `n, %COMMANDER_PATH%\剪切板\ClipAppend.txt
			gosub, ClipAppend
		}
		else {
			;点右键一键复制,20240520
			SendInput,{Click,Right}
			Sleep 100
			SendInput,{Up 1}{Enter}
			Sleep,300 	;等待0.3s, 强制机械等待剪贴板出现内容
		}
	}
	else {
		SendInput,{Blind}^c ;复制
		Sleep,300 	;等待0.3s, 强制机械等待剪贴板出现内容
		;clip:=clipboard
		StringLeft,clipboard_left,clipboard,500
		Tooltip,%clipboard_left%    ;在鼠标右侧显示clip(clipboard内容)
		Sleep,1000
		Tooltip
		;FileAppend, %Clipboard% `n, %COMMANDER_PATH%\剪切板\ClipAppend.txt
		gosub, ClipAppend
	}
return

;增强剪贴,鼠标右侧显示clip(clipboard内容)
Space_SgX: ;2024/03/16,显示clip(clipboard内容)
	if WinActive("ahk_class WeChatMainWndForPC"){	;20240520
		SendInput,{Home}+{End}^x ;全选剪切
		Sleep,300 	;等待0.3s, 强制机械等待剪贴板出现内容
		; clip:=clipboard
		StringLeft,clipboard_left,clipboard,500
		Tooltip,%clipboard_left%    ;在鼠标右侧显示clip(clipboard内容)
		Sleep,1000
		Tooltip
		; FileAppend, %Clipboard% `n, %COMMANDER_PATH%\剪切板\ClipAppend.txt
		gosub, ClipAppend
	}
	else {
		SendInput,^x ;全选剪切
		Sleep,300 	;等待0.3s, 强制机械等待剪贴板出现内容
		;clip:=clipboard
		StringLeft,clipboard_left,clipboard,500
		Tooltip,%clipboard_left%    ;在鼠标右侧显示clip(clipboard内容)
		Sleep,1000
		Tooltip
		;FileAppend, %Clipboard% `n, %COMMANDER_PATH%\剪切板\ClipAppend.txt
		gosub, ClipAppend
	}
return

;Space+W增强,单击&保存
Space_SgW0: ;除TC外的资管
	if (WinActive("Group_explorer") or WinActive("ahk_exe explorer.exe")){
			SendInput,{Click}	;点击左键,可快速重命名文件
		}
	else {
		SendInput,{Click}^s 	;默认{Click}^s,单击&保存
	}
return

;通用场景,冒号优先|右(Y)键|Office快速打印,20240403
Space_SgB0:
	if (A_Cursor = "IBeam"){
		if (WinActive("ahk_group Group_Code") or WinActive("ahk_class SciTEWindow")){
			SendInput,{Text}:: ;输入模式|Code,双冒号::优先,by Tuutg,20240416
		}
		else {
			SendInput,{Text}: ;输入模式|其他,单冒号:优先
		}
	}
	else {
		if WinActive("ahk_group OfficeAndWPS"){
			SendInput,^p	;快速打印,b|P旋转90度,20240408
		}
		else {
			SendInput,{Click,Right}	;鼠标右(Y)键单击
		}
	}
return

;通用场景,右(Y)键优先|中键,20240403
Space_DbB0:
	if (A_Cursor = "IBeam"){
		SendInput,{Click,Right}	;鼠标右(Y)键单击
	}
	else {
		SendInput,{MButton}	;鼠标中键单击
	}
return

;自带Ctrl效果,快速↑移动
Space_SgE0:
	if (A_Cursor = "IBeam"){
		SendInput,{Up 5}	;快速↑移动|自带Ctrl效果
	}
	else {
		SendInput,{Up}	;通用模式
	}
return

;自带Ctrl效果,快速↓移动
Space_SgD0:
	if (A_Cursor = "IBeam"){
		SendInput,{Down 5}	;快速↓移动|自带Ctrl效果
	}
	else {
		SendInput,{Down}	;通用模式
	}
return

;自带Ctrl效果,快速←移动
Space_SgS0:
	if (A_Cursor = "IBeam"){
		SendInput,^{Left}	;快速←移动|自带Ctrl效果
	}
	else {
		SendInput,{Left}	;通用模式
	}
return

;自带Ctrl效果,快速→移动
Space_SgF0:
	if (A_Cursor = "IBeam"){
		SendInput,^{Right}	;快速→移动|自带Ctrl效果
	}
	else {
		SendInput,{Right}	;通用模式
	}
return

Space_SgV0:	;原版粘贴|+Enter,20240425
	if WinActive("ahk_group GroupDiagOpenAndSave"){
		SendInput,^{Home}^+{End}^v{Enter}	;原版粘贴+Enter
	}
	else {
		SendInput,^v	;原版粘贴
	}
return

;在GroupDiagOpenAndSave中,动态:Enter|Listary跳转到目标路径,20240430
Space_SgGinDiagOAS:
	WinGetPos, x0, y0, w0, h0, A
	if (A_Cursor ="IBeam") or (A_CaretX !=""){
		if (A_CaretY<(h0-150)){
			SendInput,{Enter}	;20240430,纵坐标在文件名控件以上,{Enter}
		}
		else {
			SendInput,^g	;纵坐标在文件名控件及以下,Listary跳转转目标路径
		}
	}
	else {
		SendInput,^g	;纵坐标在文件名控件及以下,Listary跳转转目标路径
	}
return
;****************************************************

;****************************************************
;Space左手(Shift符号输入)+TC模式,单击:AHK|编程等用标点符号优先.双击:TC模式,数字小键盘
#If (GV_SpaceKeysAsShiftInTC==1) and WinActive("ahk_group SpaceKeysAsShiftInTC")
Space & q::	;输入!键
	GV_KeyClickAction1 := "gosub,Space_SgQ"	;常用文件!v(`)|!
	GV_KeyClickAction2 := "SendInput,{Text}``"	;'Runany
	gosub,Sub_KeyClick123
return

Space & w::	;输入^|&键
	GV_KeyClickAction1 := "gosub,Space_SgW"	;EzTc-(除法)/键|^
	GV_KeyClickAction2 := "SendInput,{Text}&"	;逻辑与,&
	gosub,Sub_KeyClick123
return

Space & e::	;输入↑键|(减法)-键
	; GV_KeyClickAction1 := "gosub,Space_SgE"	;EzTc-(减法)-键|#
	GV_KeyClickAction1 := "SendInput,{Up}"	;↑键,{Up}
	GV_KeyClickAction2 := "SendInput,{NumpadSub}" ;(减法)-键|_|-
	gosub,Sub_KeyClick123
return

Space & r::	;输入$键
	GV_KeyClickAction1 := "gosub,Space_SgR"	;EzTc-(加法)+键|$
	GV_KeyClickAction2 := "gosub,Space_DbR" ;EzTc-Alt+＝,选择相同扩展名的文件|＝
	gosub,Sub_KeyClick123
return

Space & t::		;输入*键|%键
	GV_KeyClickAction1 := "gosub,Space_SgT"	;EzTc-(乘法)*键|%
	GV_KeyClickAction2 := "SendInput,`|" ;逻辑或,|,20240412
	;GV_KeyClickAction2 := "SendInput,`~"		;~
	gosub,Sub_KeyClick123
return

;****************************************************

Space & a::	;输入{\|}键
	GV_KeyClickAction1 := "SendInput,`\" ;{\|}键,默认
	GV_KeyClickAction2 := "SendInput,`|" ;{\|}键,Shift+\=|
	gosub,Sub_KeyClick123
return

Space & s::	;输入/(数字/)
	; GV_KeyClickAction1 := "SendInput,`/" ;{?/}键,新建夹文件路径分割符
	GV_KeyClickAction1 := "SendInput,{Left}" ;→键,{Left}
	GV_KeyClickAction2 := "SendInput,{NumpadDiv}"	;EzTc-(数字/)|恢复选择
	gosub,Sub_KeyClick123
return

Space & d::	;单击输入逗(D)号,双击-(数字减号)键
	; GV_KeyClickAction1 := "SendInput,`,"	;EzTc,取消全选
	GV_KeyClickAction1 := "SendInput,{Down}"	;↓,{Down}
	GV_KeyClickAction2 := "gosub,Space_DbD" ;EzTc-Alt+-|-(数字减号)
	gosub,Sub_KeyClick123
return

Space & f::	;输入分号(F);键
	; GV_KeyClickAction1 := "SendInput,`;"	;EzTc,F4=cm_Edit
	GV_KeyClickAction1 := "SendInput,{Right}"	;→,{Right}
	GV_KeyClickAction2 := "gosub,Space_DbF" ;分号(F);键,20240408
	gosub,Sub_KeyClick123
return

Space & g:: ;Enter|输入句号(g).键
	GV_KeyClickAction1 := "gosub,Space_SgG" ;智能:Enter|TC-句号|(g).
	GV_KeyClickAction2 := "gosub,Space_DbG" ;句号.|Enter
	gosub,Sub_KeyClick123
return

;****************************************************

Space & z::
	GV_KeyClickAction1 := "gosub,Space_SgZ" ;TC中^Z|输入""键
	GV_KeyClickAction2 := "SendInput,`'" 	;',单引号
	gosub,Sub_KeyClick123
return

Space & x::
	GV_KeyClickAction1 := "gosub,Space_SgX" ;剪切并显示内容|兼容微信
	GV_KeyClickAction2 := "gosub,Space_DbX" ;]|{}
	gosub,Sub_KeyClick123
return

Space & c::
	GV_KeyClickAction1 := "gosub,Space_SgC" ;复制并显示内容|兼容微信
	GV_KeyClickAction2 := "gosub,Space_DbC" ;>,Listary->cmd命令行|()
	gosub,Sub_KeyClick123
return

Space & v::
	GV_KeyClickAction1 := "SendInput,^v" ;原版粘贴
	GV_KeyClickAction2 := "gosub,Space_DbV" ;]|{}
	gosub,Sub_KeyClick123
return

Space & b::	;TC内中键优先|双击右(Y)键单击|输入单冒号:,和普通模式相反
	GV_KeyClickAction1 := "gosub,Space_SgB" ;TC-中键优先,20240331
	GV_KeyClickAction2 := "SendInput,{Click,Right}"	;鼠标右(Y)键|单冒号:
	gosub,Sub_KeyClick123
return

;****************************************************

Space & 1::	;输入0|!键,相加=11
	GV_KeyClickAction1 := "SendInput,{0}"
	GV_KeyClickAction2 := "SendInput,{Text}!"
	gosub,Sub_KeyClick123
return

Space & 2::	;输入2|@键,相加=11
	GV_KeyClickAction1 := "SendInput,{9}"
	GV_KeyClickAction2 := "SendInput,{Text}@"
	gosub,Sub_KeyClick123
return

Space & 3::	;输入3|#键,相加=11
	GV_KeyClickAction1 := "SendInput,{8}"
	GV_KeyClickAction2 := "SendInput,{Text}#"
	gosub,Sub_KeyClick123
return

Space & 4::	;输入4|$键,相加=11
	GV_KeyClickAction1 := "SendInput,{7}"
	GV_KeyClickAction2 := "SendInput,{Text}$"
	gosub,Sub_KeyClick123
return

Space & 5::	;输入5|%键,相加=11
	GV_KeyClickAction1 := "SendInput,{6}"
	GV_KeyClickAction2 := "SendInput,{Text}%"
	gosub,Sub_KeyClick123
return

Space & 6:: SendInput,{Text}^ ;输入^键
Space & 7:: SendInput,`& ;输入&键
Space & 8:: SendInput,`* ;输入*键
Space & 9:: SendInput,`( ;输入(键
Space & 0:: SendInput,`) ;输入)键

Space & -:: SendInput,`_ ;输入_键
Space & =:: SendInput,{NumpadAdd} ;输入+键
Space & [:: SendInput,{Text}{ ;输入{键
Space & ]:: SendInput,{Text}} ;输入}键
Space & `;:: SendInput,`: ;输入:键
Space & ':: SendInput,`" ;输入"键
Space & \:: SendInput,`| ;输入|键
Space & /:: SendInput,`? ;输入)键
Space & ,:: SendInput,`< ;输入)键
Space & .:: SendInput,`> ;输入)键

Space & F1:: SendInput,{F10} ;相加=11
Space & F2:: SendInput,{F9} ;相加=11
Space & F3:: SendInput,{F8} ;相加=11
Space & F4:: SendInput,{F7} ;相加=11
Space & F5:: SendInput,{F6} ;相加=11

; $Space::SendInput,{Blind}{Space} ;恢复默认打字上屏功能
$Space::Space ;恢复空格打字上屏,by Tuutg,20240425
#If
;****************************************************

;****************************************************
;Space左手(Shift符号)输入+TC模式跳转标签
Space_SgQ:
	if WinActive("ahk_class TTOTAL_CMD"){
			SendInput,!v	;EzTc-(~)键,激活常用文件
		}
	else {
		SendInput,{Text}! 	;默认!
	}
return

Space_SgW:
	if WinActive("ahk_class TTOTAL_CMD"){
			SendInput,{NumpadDiv}	;EzTc-(除法)/键
		}
	else {
		SendInput,{Text}^ 	;默认^
	}
return

Space_SgE:
	if WinActive("ahk_class TTOTAL_CMD"){
		SendInput,{Up}	;↑键,{Up}
		; SendInput,{NumpadSub}	;EzTc-(减法)-键
		}
	else {
		SendInput,{NumpadSub}	;EzTc-(减法)-键
		; SendInput,{Text}# 	;默认#
	}
return

Space_SgR:
	if WinActive("ahk_class TTOTAL_CMD"){
			SendInput,{NumpadAdd}	;EzTc-(加法)+键
		}
	else {
		SendInput,{Text}$ 	;默认$
	}
return

Space_DbR:	;20240408
	if WinActive("ahk_class TTOTAL_CMD"){
			SendInput,!`=	;EzTc-Alt+＝,选择相同扩展名的文件
		}
	else {
		SendInput,`= ;等号＝;
	}
return

Space_SgT:
	if WinActive("ahk_class TTOTAL_CMD"){
			SendInput,{NumpadMult}	;EzTc-(乘法)*键
		}
	else {
		SendInput,`% 	;默认%
	}
return

Space_DbT:	;20240408
	if WinActive("ahk_class TTOTAL_CMD"){
			SendInput,!`-	;EzTc-Alt+-,取消选择相同扩展名的文件
		}
	else {
		SendInput,`_ ;下划线_;
	}
return

;****************************************************

Space_SgG:	;20240408
	if (A_Cursor = "IBeam"){
		SendInput,{Enter} ;输入模式|{Enter}优先
	}
	else {
		if WinActive("ahk_class TTOTAL_CMD"){
			if (GV_TotalcmdToggleJumpByNumber == 1)
				SendInput,`.	;EzTc-句号(g).键,激活快搜,适用于开启TC数字跳转模式
			else
				SendInput,{Enter} ;输入模式|{Enter}优先,20240408
		}
		else {
			SendInput,{Enter} ;不在TC中|{Enter}优先
		}
	}
return

Space_DbG:	;20240408
	if WinActive("ahk_class TTOTAL_CMD"){
			SendInput,!`=	;EzTc-Alt+＝,选择相同扩展名的文件
		}
	else {
		SendInput,`.	;句号(g).键
	}
return

;****************************************************

Space_DbD:
	if WinActive("ahk_class TTOTAL_CMD"){
			SendInput,!`-	;EzTc-Alt+-,取消选择相同扩展名的文件
		}
	else {
		; SendInput,{NumpadSub} ;-(数字|减号)
		SendInput,SendInput,`, ;逗(D)号,
	}
return

Space_DbF:
	if WinActive("ahk_class TTOTAL_CMD"){
		SendInput,`; ;分(F)号,TC-F4; ,20240408
		}
	else {
		; SendInput,{NumpadAdd} ;+(数字|加号)
		SendInput,`; ;分(F)号,TC-F4; ,20240408
	}
return

;****************************************************

Space_SgZ:
	if WinActive("ahk_class TTOTAL_CMD"){
		; SendInput,`'	; EzTc-',单引号=右键
		SendInput,^z	; EzTc-^z,撤销操作,20240402
	}
	else {
		SendInput,`"`"	;输入""键
		Sleep,100
		SendInput,{Left}
	}
return

Space_DbX:
	if WinActive("ahk_class TTOTAL_CMD"){
		SendInput,`[	;输入[键
	}
	else {
		SendInput,`[`]	;输入[]键
		Sleep,100
		SendInput,{Left}
	}
return

Space_DbC:
	if WinActive("ahk_group SpaceKeysAsShiftInTC") {
		SendInput,`>	;输入>键
	}
	else {
		SendInput,{Text}()	;输入()键
		Sleep,100
		SendInput,{Left}
	}
return

Space_DbV:
	if WinActive("ahk_class TTOTAL_CMD"){
		SendInput,`]	;输入]键
	}
	else {
		SendInput,{Text}{}	;输入{}键
		Sleep,100
		SendInput,{Left}
	}
return

Space_SgB:	;TC内中键优先,20240331
	if (A_Cursor = "IBeam"){
		SendInput,{Text}: ;输入模式|Code,单冒号:优先
	}
	else {
		if WinActive("ahk_class TTOTAL_CMD"){
			SendInput,{Click}	;点击激活
			Sleep 50
			TcSendPos(3004)	;新建标签,20240401
		}else if WinActive("ahk_class EVERYTHING") or WinActive("ahk_class EVERYTHING_(1.5a)"){
			SendInput,^e	;浏览路径,20240401
		}
		else {
			SendInput,{MButton}	;中键优先
		}
	}
return

;************** Space空格键相关$ **************


;************** `花号键相关^ ************** {{{2
;这个位置顺手，主要是在按住做了选择之后，再去按ctrl或者；分号等就显得远了,默认自带Shift效果
;切换开关, Escape & /
#If (GV_ToggleHhjKeys ==1)	;默认关闭0,和Runany有冲突
{
	` & 1::SendInput,{Blind}^x
	` & 2::SendInput,{Blind}^c
	` & 3::SendInput,{Blind}^v
	` & 4::SendInput,{Blind}{Del}
	` & `;::SendInput,{Blind}{Home}+{End}

	` & e::SendInput,{Blind}^+{Up} ;by tuutg 2022/11/13
	` & d::SendInput,{Blind}^+{Down} ;by tuutg 2022/11/13
	` & s::SendInput,{Blind}^+{Left} ;by tuutg 2022/11/13
	` & f::SendInput,{Blind}^+{Right} ;by tuutg 2022/11/13

	` & j:: SendInput,{Blind}+{Down}
	` & k:: SendInput,{Blind}+{Up}
	` & h:: SendInput,{Blind}+{Left}
	` & l:: SendInput,{Blind}+{Right}

	` & b::SendInput,{Blind}^+{Left}
	` & w::SendInput,{Blind}^+{Right}

	` & o::SendInput,{Blind}^{PgUp}
	` & p::SendInput,{Blind}^{PgDn}
	` & n:: SendInput,{Blind}+{PgDn}
	` & m:: SendInput,{Blind}+{PgUp}
	;` & y:: SendInput,{Blind}{Home}+{End}
	;` & u:: SendInput,{Blind}+{End}
	;` & i:: SendInput,{Blind}+{Home}

	` & u::
		GV_KeyClickAction1 := "SendInput,+{End}"
		GV_KeyClickAction2 := "SendInput,^+{End}"
		gosub,Sub_KeyClick123
	return

	` & i::
		GV_KeyClickAction1 := "SendInput,+{Home}"
		GV_KeyClickAction2 := "SendInput,^+{Home}"
		gosub,Sub_KeyClick123
	return

	` & y::
		GV_KeyClickAction1 := "SendInput,{Blind}{Home}+{End}"
		GV_KeyClickAction2 := "SendInput,{Blind}^{Home}^+{End}"
		gosub,Sub_KeyClick123
	return

	;点不是默认的“确定”或者OK按钮，如果没有就点第一个Button1，适用与那种简单的对话框，比如TC的备注
	` & Enter::
		try{
			SetTitleMatchMode RegEx
			SetTitleMatchMode Slow
			;ControlClick, i).*确定|OK.*, A
			ControlClick, i).*确 *定|OK.*, A
		} catch e{
			ControlClick, Button1, A
		}
	return


	$+`::SendInput,~
	$`::SendInput,``
	$^`::SendInput,^``
	$!`::SendInput,!``
	$+!`::SendInput,+!``
	;`::EzMenuShow()
}
#If
;************** `花号键相关$ **************


;************** Alttab相关^ ************** {{{2
;按住左键再进行滚轮，在AltaTab菜单中，可以点击右键或者按空格进行确认选择。
;多用在把文件拖到别的程序中打开，或者类似于qq微信传文件。也可以将浏览器中的图片直接拖到文件管理器中保存
~LButton & WheelUp::ShiftAltTab
~LButton & WheelDown::AltTab
;就没必要还用这个了
;LWin & WheelUp::ShiftAltTab
;LWin & WheelDown::AltTab

;鼠标中操作
#If WinActive("ahk_class TaskSwitcherWnd") or WinActive("任务切换")
{
	;Win10自己已经支持alttab中按空格选择程序
	if A_OSVersion in WIN_2003, WIN_XP, WIN_7
	{
		$!Space::SendInput,{Alt Up}
		$Space::SendInput,{Alt Up}
	}

	;在alttab的菜单中，点右键选中对应的程序
	$!RButton::SendInput,{Alt Up}
	~LButton & RButton::SendInput,{Alt Up}

	;alt+shift+tab，切换到上一个窗口功能，放在一起共用TaskSwitcherWnd算了
	;<+Tab::ShiftAltTab

	;左手
	$!x::SendInput,{Blind}{Del}
	$!q::ControlSend,,{Blind}{Left}
	$!s::SendInput,{Blind}{Down}
	$!w::SendInput,{Blind}{Up}
	$!a::SendInput,{Blind}{Left}
	$!d::SendInput,{Blind}{Right}

	;右手
	$!j::SendInput,{Blind}{Down}
	$!k::SendInput,{Blind}{Up}
	$!h::SendInput,{Blind}{Left}
	$!l::SendInput,{Blind}{Right}
	$!u::SendInput,{Blind}{End}
	$!i::SendInput,{Blind}{Home}
	$!,::SendInput,{Blind}{Left}
	$!.::SendInput,{Blind}{Right}
}
#If

;Win10改成了MultitaskingViewFrame,Win11改成了XamlExplorerHostIslandWindow
#If WinActive("ahk_class MultitaskingViewFrame") or WinActive("ahk_class XamlExplorerHostIslandWindow")
{
	$!RButton::SendInput,{Alt Up}
	~LButton & RButton::SendInput,{Alt Up}
	;左手
	$!q::ControlSend,,{Blind}{Left}
	$!s::SendInput,{Blind}{Down}
	$!w::SendInput,{Blind}{Up}
	$!a::SendInput,{Blind}{Left}
	$!d::SendInput,{Blind}{Right}

	;右手
	$!j::SendInput,{Blind}{Down}
	$!k::SendInput,{Blind}{Up}
	$!h::SendInput,{Blind}{Left}
	$!l::SendInput,{Blind}{Right}
	$!u::SendInput,{Blind}{End}
	$!i::SendInput,{Blind}{Home}
	$!,::SendInput,{Blind}{Left}
	$!.::SendInput,{Blind}{Right}
}
#If
;************** Alttab相关$ **************


;************** tab相关^ ************** {{{2
;基本操作上下左右，还可以扩展，主要用在左键右鼠的操作方式,默认自带Ctrl效果
;开关为ctrl+win+alt+花号/^!#`, 排除微信和输入法,微信中用系统原版Tab,快速输入联系人,2024050
;Listary.exe启用独立Tab,增加双击从Listary搜索框,跳回到打开|保存对话框,2024050
#If (GV_ToggleTabKeys==1) and !WinActive("ahk_group Group_SingleTab") and !WinActive("ahk_group Group_ListarySearchBox")

	Tab & e::SendInput,^{Up} ;by tuutg 2022/11/13
	Tab & d::SendInput,^{Down} ;by tuutg 2022/11/13
	Tab & s::SendInput,^{Left} ;by tuutg 2022/11/13
	Tab & f::SendInput,^{Right} ;by tuutg 2022/11/13
	Tab & q::SendInput,^{PgUp}	;Ez原版定义
	Tab & a::SendInput,^{PgDn} ;by tuutg 2022/11/13

	;对应任务栏上固定的前5个程序快速切换
	Tab & 1::SendInput,#1
	Tab & 2::SendInput,#2
	Tab & 3::SendInput,#3
	Tab & 4::SendInput,#4
	Tab & 5::SendInput,#5

	;常用的四个按键
	Tab & r::SendInput,{Del}
	Tab & w::SendInput,^s
	Tab & Space::WinMinimize A

	;uTools优先复制并右侧显示clip(Clipboard内容)|复制全文件名|同CapsLock & b|TC-文件比较
	Tab & b::	;20240401
		if WinActive("ahk_class TTOTAL_CMD"){
			TcSendUserCommand("em_BeyondCompare4")	;文件比较,20240401
		}
		else {
			GV_KeyClickAction1 := "gosub,Sub_KeyCapsLockB"	;uTools优先|复制无后缀文件名|前5行复制
			GV_KeyClickAction2 := "gosub,Sub_ClipAppendCopyFName"	;复制全文件名,有后缀|后5行复制
			gosub,Sub_KeyClick123
		}
	return

	;这个位置按起来不舒服，但先留着
	Tab & x::SendInput,^x
	Tab & t::SendInput,{BackSpace}
	Tab & z::SendInput,^z

	;右手模式，和caps一样，随便按哪一个都行，自由发挥吧
	Tab & j::SendInput,^{Down}
	Tab & k::SendInput,^{Up}
	Tab & h::SendInput,^{Left}
	Tab & l::SendInput,^{Right}
	Tab & n::SendInput,^{PgDn}
	Tab & m::SendInput,^{PgUp}
	Tab & u::SendInput,^{End}
	Tab & i::SendInput,^{Home}

	;各个程序对应小菜单
	Tab & v::gosub,EzOtherMenuShow	;弹出Ez快捷菜单

	;粘贴然后回车，多用在搜索框等输入的位置，一个双手，一个单手
	Tab & g::
		GV_KeyClickAction1 := "SendInput,^v{Enter}"
		GV_KeyClickAction2 := "SendInput,^{Home}^+{End}^v{Enter}"
		gosub,Sub_KeyClick123
	return

	;转到Vim进行编辑
	Tab & c::
		GV_KeyClickAction1 := "gosub,Sub_CopyVim"
		GV_KeyClickAction2 := "gosub,Sub_CopyAllVim"
		gosub,Sub_KeyClick123
	return

	;重要的alttab菜单
	<!Tab::AltTab

	;恢复tab自身功能,用~恢复Tab,用于热字符串激活键.GV_ToggleAotuSaveMode==1,+自动保存功能,20240403
	$Tab::	;自动保存,只在如Code|Office|Wps系列软件启用
		if (GV_ToggleAotuSaveMode==1)
		{
			if WinActive("ahk_group OfficeAndWPS") or WinActive("ahk_group EnterableAutoSave"){
				SendInput,{Tab}^s	;自动保存
			}
			else if (WinActive("ahk_group Group_Code") or WinActive("ahk_class SciTEWindow")) and !WinActive("ahk_group Code_Compare"){
				SendInput,{Tab}^s	;自动保存,20240520
			}
			else {
				SendInput,{Blind}{Tab}	;恢复自身Tab功能
			}
			return
		}
		else
		{
			SendInput,{Blind}{Tab}	;恢复自身Tab功能
		}
	return

	;Tab::	;双击tab，会明显减慢tab的响应速度，不用
		;GV_KeyTimer := 150
		;GV_KeyClickAction1 := "SendInput,{Tab}"
		;GV_KeyClickAction2 := "SendInput,#{Tab}"
		;gosub,Sub_KeyClick123
	;return

	$#Tab::SendInput,{Blind}#{Tab}
	$+Tab::SendInput,{Blind}+{Tab}
	;CapsLock & Alt::	;按住Caps不放，再按Alt.2023/08/24 08:25
	$^Tab::SendInput,{Blind}^{Tab}
	$^+Tab::SendInput,{Blind}^+{Tab}

#If
;************** tab相关$ **************


;************** 单键模式^ ************** {{{2
;开关按键为caps+/
#If (GV_ToggleKeyMode==1)
{
	j::SendInput,{Blind}{Down}
	k::SendInput,{Blind}{Up}
	h::SendInput,{Blind}{Left}
	l::SendInput,{Blind}{Right}
	y::SendInput,{Click,Right}

	u::
		GV_KeyClickAction1 := "SendInput,{End}"
		GV_KeyClickAction2 := "SendInput,^{End}"
		gosub,Sub_KeyClick123
	return

	i::
		GV_KeyClickAction1 := "SendInput,{Home}"
		GV_KeyClickAction2 := "SendInput,^{Home}"
		gosub,Sub_KeyClick123
	return

	n::
		GV_KeyClickAction1 := "SendInput,{PgDn}"
		GV_KeyClickAction2 := "SendInput,^{PgDn}"
		gosub,Sub_KeyClick123
	return

	m::
		GV_KeyClickAction1 := "SendInput,{PgUp}"
		GV_KeyClickAction2 := "SendInput,^{PgUp}"
		gosub,Sub_KeyClick123
	return

	o::SendInput,{Blind}^+{Tab}
	p::SendInput,{Blind}^{Tab}
	.::SendInput,^w
	;w::SendInput,^w

	`;::SendInput,{Click}
	,::SendInput,{Escape}

	q::SendInput,{F3} ;quicklook
	w::SendInput,^w
	e::SendInput,{Enter}
	r::SendInput,{F2}
	t::SendInput,{BackSpace}

	a::SendInput,^+s ;另存
	s::SendInput,^s
	d::SendInput,{del}{Enter}
	f::SendInput,^f
	g::SendInput,^g

	b::SendInput,^h
	v::SendInput,^v
	c::SendInput,^c
	x::SendInput,^x
	z::SendInput,^z
}
#If
;************** 单键模式$ **************


;******* 截图小功能,截图并打开保存路径^******** {{{2
>!Space::fun_NircmdScreenShot(1)	;ActiveWin
#^PrintScreen::fun_NircmdScreenShot(0)	;全屏截图
#+PrintScreen::fun_NircmdScreenShot(1)	;ActiveWin
;前提是用管理员运行nircmd.exe->拷进系统System32目录
; ScreenShotPath := "D:\RunCaptX\PixPinTg截图"

fun_NircmdScreenShot(wd)
{
	;1 ActiveWin ,0 WholeDesktop
	;右边alt+空格截图的默认保存位置
	;ScreenShotPath := "D:\"
	if(wd = 1){
		SSFileName := % ScreenShotPath . "\SSAW-" . fun_GetFormatTime( "yyyy-MM-dd HH-mm-ss" ) . ".png"
		Run nircmd savescreenshotwin "%SSFileName%"
		if(GV_ScreenShot2Clip = 1){
			Sleep,1000
			Run,nircmd Clipboard copyimage "%SSFileName%"
		}
	}
	else {
		SSFileName := % ScreenShotPath . "\SSWD-" . fun_GetFormatTime( "yyyy-MM-dd HH-mm-ss" ) . ".png"
		Run nircmd savescreenshot "%SSFileName%"
		if(GV_ScreenShot2Clip = 1){
			Sleep,1000
			Run,nircmd Clipboard copyimage "%SSFileName%"
		}
	}
	Sleep,1000
	EzTip(SSFileName,2)
	Run,%ScreenShotPath%
}
;************** 截图小功能$ **************


;************** 窗口相关^ ************** {{{2
;去掉标题栏
#f11::
	;WinSet, Style, ^0xC00000, A ;用来切换标题行，主要影响是无法拖动窗口位置。
	;WinSet, Style, ^0x40000, A ;用来切换sizing border，主要影响是无法改变窗口大小。
	gosub, Sub_WindowNoCaption
return
;************** 窗口相关$ **************


;************** Mouse鼠标相关^ ************** {{{2
;鼠标侧边键XButton2,ahk只认这两个，可以自行去掉注释
XButton2::SendInput,{PgUp}
XButton1::SendInput,{PgDn}
XButton2 & XButton1::SendInput,{Escape}
XButton1 & XButton2::SendInput,{Escape}

;**************************************************
;鼠标自定义代码,添加到此处
;快速打印,排除3D建模软件,看图软件等.注释掉,有干扰,20240606
/*$+MButton::
	if WinActive("ahk_group 3DDisableMButtonPrint")	;20240520
		return
	else
		SendInput,^p ;打印
return
*/

Space & MButton::	;20240105
	SendInput,^p ;打印
return

; #if WinActive("ahk_group Group_explorer") or WinActive("ahk_group Group_Desktop")	,20240531
; $^p::
; $+MButton::
; Space & MButton::	;右键->打印
	; Clipboard := ""
	; SendInput,{Blind}^c
	; ClipWait, 1 ; 等待一秒看是否有内容被复制到剪贴板
	; Clipboard = %Clipboard%
	; If Clipboard {
		; If FileExist(Clipboard) { ; 检查是否为有效的文件路径
			; PrintFile(Clipboard)
		; }
	; }
; Return
; #if

PrintFile(File) {
	; 使用默认关联程序打开文件并尝试打印
	Run, "%ComSpec% /c start "" """ File """ /p", , Min
	Sleep, 500 ; 简单延迟以避免冲突
	Clipboard := ""
}

;****************************************************
;直接打印/选定打印机打印
#If WinActive("ahk_group Group_打印")
{
	$MButton::SendInput,{Enter} ;盲点打印

	$+LButton::	;杨磊彩色打印机
	{
		SendInput,{Home}
		Sleep,100
		SendInput,{Right}
		Sleep,100
	}
	return

	$!LButton::SendInput,{Left}{up}	;选定MP 2014打印机
	$!RButton::SendInput,{up 4}	;选定Acrobat打印机
}
#If
;************** Mouse鼠标相关$ *******************


;********** 几种模式的开关，暂停重启^ ********** {{{2
;单键模式启用/关闭
$ScrollLock::
CapsLock & /::
	GV_ToggleKeyMode := !GV_ToggleKeyMode
	if(GV_ToggleKeyMode == 1)
		ToolTip 单键模式启用
	else
		ToolTip 单键模式关闭
		Sleep 2000
		ToolTip
return
;****************************************************
;Space启用/关闭
^!#Space::
	GV_ToggleSpaceKeys := !GV_ToggleSpaceKeys
	if(GV_ToggleSpaceKeys == 1)
		ToolTip 空格组合键启用
	else
		ToolTip 空格组合键关闭
	Sleep 2000
	ToolTip
return
;****************************************************
;Tab启用/关闭
;直接用ctrl+win+alt+tab键会引发alttab，不合适。而caps和花号是不考虑模式都在的。故用花号。
^!#`::
	GV_ToggleTabKeys := !GV_ToggleTabKeys
	if(GV_ToggleTabKeys == 1)
		ToolTip Tab组合键启用
	else
		ToolTip Tab组合键关闭
	Sleep 2000
	ToolTip
return
;****************************************************
;暂停热键，可以再按恢复
$Pause::
^!#t::
;Escape & Pause::
;CapsLock & Pause::
	Suspend Permit
	Suspend Toggle
return
;****************************************************
;暂停脚本，可以右键菜单选择或者用重启脚本恢复
^!#z::
	Suspend Permit
	Pause Toggle
return
;****************************************************
;重启脚本,CapsLock & F5::
^!#r::
$#F5::
; CapsLock & F5::
	gosub,ForceSelfReload
return
;****************************************************
;解决Win10中任务栏无法切换的臭毛病
^!#e::Run,nircmd shellrefresh
;****************************************************
;`花号键，与ranany冲突，默认关闭,一键启用/关闭,默认自带Shift效果
Escape & /::
	GV_ToggleHhjKeys := !GV_ToggleHhjKeys
	if (GV_ToggleHhjKeys == 1)
		ToolTip 花号组合键启用
	else
		ToolTip 花号组合键关闭
	Sleep 2000
	ToolTip
return
;****************************************************
;自动保存开关,Tab|Enter激活自动保存,初始未保存会有弹出提示,开关按键为^#+a,默认关闭,0
^#+a::
	GV_ToggleAotuSaveMode := !GV_ToggleAotuSaveMode
	if (GV_ToggleAotuSaveMode == 1)
		ToolTip 自动保存已启用-按Tab|Enter激活自动保存
	else
		ToolTip 自动保存已关闭-请您及时手动保存文件
	Sleep 2000
	ToolTip
return
;****************************************************
;启用左手CapsLock+EDSF,鼠标移动模拟,开关为ctrl+win+alt+M,默认关闭,0
$#!Space::
^#!m::	;20240415

	GV_ToggleCapsMouseMove := !GV_ToggleCapsMouseMove
	if (GV_ToggleCapsMouseMove == 1)
		ToolTip 启用左手Caps+EDSF,鼠标移动模拟
	else
		ToolTip 关闭左手Caps+EDSF,鼠标移动模拟
	Sleep 2000
	ToolTip
return
;************ 几种模式的开关，暂停重启$ *************


;************** 应用程序相关^ ************** {{{1
;************** Group_browser^相关^ ******** {{{2	;by Tuutg,20240628
;以360X极速浏览器X模板,其他浏览器个别按键可能有不起作用,排除GroupDiagOpenAndSave组和新版QQ,避免干扰!g/!w/!f
#If WinActive("ahk_group Group_browser") and !WinActive("ahk_group GroupDiagOpenAndSave") and !WinActive("ahk_exe QQ.exe")
{
	CapsLock & r::	;->前进,20231116
	if (A_Cursor = "IBeam" and A_CaretX = "")
	{
		SendInput,{Del} ;打字时
	}
	else
	{
		SendInput,!{Right}
	}
	return

	CapsLock & w::SendInput,!{Left}		;<-后退,20231116

	; $F1::SendInput,{Blind}^t	;预留给PinPix截图
	$F2::SendInput,{Blind}^+{Tab}	;标签左切
	$F3::SendInput,{Blind}^{Tab}	;标签右切
	$F4::SendInput,{Blind}^w	;关闭档当前标签
	$F9::ControlSend,,{F9}	;油小猴超级翻译助手

	$!q::ControlSend,,!q	;Chrono Power Action|屏蔽Ra
	$!w::SendInput,!w	;沉浸式翻译-翻译网页/显示原文
	$!e::SendInput,^+t	;恢复最后关闭页面
	$!r::ControlSend,,!a	;沉浸式翻译-翻译页面全部区域|屏蔽微信
	$!t::SendInput,!t	;篡改猴测试版-Open dashboard|设置面板
	$+!t::SendInput,^+m	;打开Chrome设置面板|360X浏览器静音

	$!a::SendInput,^a^c	;全选+复制,20240322
	; $!s::SendInput,!s	;RA-搜索
	$!+s::ControlSend,,!+s ;ChatGPT Shortcut|屏蔽Ra
	; $!d::		;聚集地址框
	$!+d::ControlSend,,!+d ;Dark Reader|屏蔽Ra
	$!f::SendInput,^f	;网页内查找
	$!g::SendInput,^j	;自带下载

	$!z::SendInput,^0	;缩放100%
	$!x::SendInput,^h	;历史记录
	$+!x::SendInput,^+{del} ;清除历史记录
	$!c::SendInput,{F11}	;全屏
	$!v::SendInput,{F12}	;开发者模式
	; CapsLock & c::SendInput,{Blind}^c ;20240322
	; CapsLock & v::SendInput,^v{Enter} ;20240322
	CapsLock & b::SendInput,^d	;添加到书签
	$!b::SendInput,^d	;添加到书签

	`;::
	;msgbox % GetCursorShape()
	;64位的Win7下，在输入框中是148003967
	if (GetCursorShape() = GV_CursorInputBox) ;I型光标
		SendInput,`;
	else
		SendInput,{Click}	;左键单击
	return

	!`;::SendInput,{Click,Right}	;右键单击
	^`;::SendInput,`;		;输入分号;

	;按住左键点中键发送Ctrl+t新建标签
	~LButton & MButton::SendInput,{Blind}^t

	;按住Shift,文内换行|Alt+左键,确认完成,编辑网页,20240528
	$+LButton::SendInput,+{Enter}	;文内换行
	$!LButton::SendInput,^{Enter}	;确认完成
	CapsLock & g::
		GV_KeyClickAction1 := "SendInput,{Enter}"	;确认完成
		GV_KeyClickAction2 := "SendInput,^{Enter}"	;确认完成
		gosub,Sub_KeyClick123
	return

	;~LButton::
		;GV_LongClickAction := "SendInput,{MButton}"
		;GV_MouseButton := 1
		;gosub,Sub_ButtonLongPress
	;return

	XButton1 & RButton::SendInput,{Blind}^c
	XButton2 & RButton::SendInput,{Blind}^c

	XButton2 & XButton1::gosub,Sub_Idm2Mpv
	XButton1 & XButton2::gosub,Sub_Idm2Mpv

	XButton1 & WheelUp::SendInput,{Blind}{Left}
	XButton2 & WheelUp::SendInput,{Blind}{Left}
	XButton1 & WheelDown::SendInput,{Blind}{Right}
	XButton2 & WheelDown::SendInput,{Blind}{Right}

	;先鼠标双击一个单词之后，这样选词方便，再调用vimium中的v模式，然后先选中两个单词，后面可以按w继续选择或别的按键
	; !w::SendInput,v2w
	; !e::SendInput,$^c
	!RButton::SendInput,v2w

	Tab & WheelUp:: SendInput,{Blind}{Left}
	Tab & WheelDown::SendInput,{Blind}{Right}

	XButton2::
		p := ClickAndLongClick()
		if (p = "0") {
			;单击
			if GV_GroupBrowserToggleWheelModeLeftRight
			SendInput,{MButton}
			else
			Send,{PgUp}
		} else if (p = "00") {
			;双击
			GV_GroupBrowserToggleWheelModeLeftRight := !GV_GroupBrowserToggleWheelModeLeftRight
			EzTip("切换鼠标滚轮模式" . GV_GroupBrowserToggleWheelModeLeftRight,2)
			MenutrayIcon()
		}
	return

	XButton1::
	p := ClickAndLongClick()
	if (p = "0") {
		;单击
		if GV_GroupBrowserToggleMButtonMode
		SendInput,{MButton}
		else
		Send,{PgDn}
	} else if (p = "00") {
		;双击
		GV_GroupBrowserToggleMButtonMode := !GV_GroupBrowserToggleMButtonMode
		EzTip("切换侧边键作为中键" . GV_GroupBrowserToggleMButtonMode,2)
			MenutrayIcon()
	} else if (p = "01") {
		;双击再按住
		GV_GroupBrowserToggleWheelModeUpDown := !GV_GroupBrowserToggleWheelModeUpDown
		EzTip("切换滚轮为翻页" . GV_GroupBrowserToggleWheelModeUpDown,2)
			MenutrayIcon()
	}
	return


	;浏览器中切换滚轮模式，主要是方便看视频，西瓜和B站
	<!Space::
		GV_GroupBrowserToggleWheelModeLeftRight := !GV_GroupBrowserToggleWheelModeLeftRight
		EzTip("鼠标滚轮模式切换" . GV_GroupBrowserToggleWheelModeLeftRight,2)
		MenutrayIcon()
	return

	WheelUp::
		if GV_GroupBrowserToggleWheelModeLeftRight {
			SendInput,{Blind}{Left}
		} else if GV_GroupBrowserToggleWheelModeUpDown {
			SendInput,{Blind}{PgUp}
		} else {
			SendInput,{WheelUp}
		}
	return

	WheelDown::
		if GV_GroupBrowserToggleWheelModeLeftRight {
			SendInput,{Blind}{Right}
		} else if GV_GroupBrowserToggleWheelModeUpDown {
			SendInput,{Blind}{PgDn}
		} else {
			SendInput,{WheelDown}
		}
	return

	$!WheelUp::SendInput,!{Left}	;<-,20231116
	$!WheelDown::SendInput,!{Right}	;->,20231116

	; ~保留鼠标手势,双击右键，发送退格，返回上一级网页|默认最小化网页
	~RButton:: ;~保留鼠标手势,默认最小化网页,20240401
		KeyWait,RButton
		KeyWait,RButton, d T0.4
		if ! Errorlevel
		{
			WinMinimize,A
		}
	return
}
#If
;****************************************************
#If WinActive("ahk_group Group_browser") and (A_Cursor != "IBeam")
	CapsLock & s::	;点击右键->编辑(书签等)
		SendInput,{Click,Right}
		Sleep,100
		SendInput,{Text}e
	return

	CapsLock & f::SendInput,{Click,Right} ;点击右键
#If
;****************************************************
;在浏览器中单独启用空格组合键
;一键启用/关闭 Ctrl+Win+Alt+Space
;按住 Space 后，搭配其它键，你可以实现以下功能：
#If WinActive("ahk_group Group_browser") and (GV_GroupBrowserToggleSpaceKeys == 1)
{
	Space & j:: SendInput,{Blind}{Down}
	Space & k:: SendInput,{Blind}{Up}
	Space & h:: SendInput,{Blind}{Left}
	Space & l:: SendInput,{Blind}{Right}

	Space & WheelUp::SendInput,{Blind}{Left}{Space up}
	Space & WheelDown::SendInput,{Blind}{Right}{Space up}

	Space & q::SendInput,{Blind}^{PgUp}	;切换至下个标签
	Space & w::SendInput,{Blind}!{Left}	;<-,20231116
	Space & e::SendInput,{Blind}{PgUp}	;向上翻页
	Space & r::SendInput,{Blind}!{Right}	;->,20231116
	;Space & r::SendInput,{Blind}{Del}	;20231116
	Space & t::SendInput,{Blind}{BackSpace} ;20231116

	;先将鼠标光标停在链接上，在链接上右键菜单，然后选另存为
	Space & a::
		SendInput,{Click,Right}
		Sleep,200
		SendInput,{Text}k
	return

	Space & s::SendInput,{Blind}^v{Enter}
	Space & d::SendInput,{Blind}{PgDn} 	;向下翻页
	Space & f::gosub,Sub_Idm2Mpv

	Space & g::	;电摩合格证管理系统,一键撤销,20240410
		GV_KeyClickAction1 := "SendInput,{Blind}{Enter}"
		GV_KeyClickAction2 := "gosub,Space_SgGinHgz"	;一键撤销
		gosub,Sub_KeyClick123
	return

	Space & z::SendInput,{Blind}^0	;缩放100%
	Space & x::
		GV_KeyClickAction1 := "gosub,Space_SgX"
		GV_KeyClickAction2 := "SendInput,^{Home}^+{End}^x"
		gosub,Sub_KeyClick123
	return

	Space & c::
		GV_KeyClickAction1 := "gosub,Space_SgC"
		GV_KeyClickAction2 := "SendInput,^{Home}^+{End}^c"
		gosub,Sub_KeyClick123
	return

	Space & v::
		GV_KeyClickAction1 := "SendInput,{Blind}^v{Enter}"	;粘贴+OK-20240410
		GV_KeyClickAction2 := "SendInput,^{Home}^+{End}^v{Enter}"
		gosub,Sub_KeyClick123
	return

	Space & b::SendInput,{Blind}!{Right}	;->,20231116
	; Space & \::SendInput,{Blind}^a^v{Enter}

	Space & u::
		GV_KeyClickAction1 := "SendInput,{End}"
		GV_KeyClickAction2 := "SendInput,^{End}"
		gosub,Sub_KeyClick123
	return

	Space & i::
		GV_KeyClickAction1 := "SendInput,{Home}"
		GV_KeyClickAction2 := "SendInput,^{Home}"
		gosub,Sub_KeyClick123
	return

	Space & n::
		GV_KeyClickAction1 := "SendInput,{PgDn}"
		GV_KeyClickAction2 := "SendInput,^{PgDn}"
		gosub,Sub_KeyClick123
	return

	Space & m::
		GV_KeyClickAction1 := "SendInput,{PgUp}"
		GV_KeyClickAction2 := "SendInput,^{PgUp}"
		gosub,Sub_KeyClick123
	return

	;复制文本,注释掉.用于GridMove窗口控制
	;Space & RButton::SendInput,{Blind}^c

	;粘贴
	XButton2 & LButton::
	; Space & LButton::	注释掉.用于GridMove窗口控制
	if (GetCursorShape() = GV_CursorInputBox){
		SendInput,{Click}	;点击激活
		Sleep,100
		SendInput,^v{Enter}
	} else if(GetCursorShape() = GV_CursorClick) {
		SendInput,{MButton}
	} else {
		SendInput,{Enter}
	}
	return

	;连击3下用来选中文本段落，然后复制
	Space & XButton1::
		SendInput,{Click 3}
		Sleep 100
		SendInput,{Blind}^c
		Sleep 300
	return

	; $Space::SendInput,{Blind}{Space}	;恢复默认打字上屏功能
	$Space::Space ;恢复空格打字上屏,by Tuutg,20240425
	^Space::^Space
	+Space::+Space

	;***********************************************
	;20240419,右手数字|符号输入|FN
	Space & 1:: SendInput,{0} ;相加=11
	Space & 2:: SendInput,{9} ;相加=11
	Space & 3:: SendInput,{8} ;相加=11
	Space & 4:: SendInput,{7} ;相加=11
	Space & 5:: SendInput,{6} ;相加=11
	Space & 6:: SendInput,{5} ;相加=11
	Space & 7:: SendInput,{4} ;相加=11
	Space & 8:: SendInput,{3} ;相加=11
	Space & 9:: SendInput,{2} ;相加=11
	Space & 0:: SendInput,{1} ;相加=11

;***********************************************

	Space & F1:: SendInput,{F11} ;加+10
	Space & F2:: SendInput,{F12} ;加+10
	Space & F3:: SendInput,{F8} ;相加=11
	Space & F4:: SendInput,{F7} ;相加=11
	Space & F5:: SendInput,{F6} ;相加=11

;***********************************************

	Space & -:: SendInput,`_ ;输入_键
	Space & =:: SendInput,{NumpadAdd} ;输入+键
	Space & [:: SendInput,{Text}{ ;输入{键
	Space & ]:: SendInput,{Text}} ;输入}键
	Space & `;:: SendInput,`: ;输入:键
	Space & ':: SendInput,`" ;输入"键
	Space & \:: SendInput,`| ;输入|键
	Space & /:: SendInput,`? ;输入?键
	Space & ,:: SendInput,`< ;输入<键
	Space & .:: SendInput,`> ;输入>键
}
#If

Sub_Idm2Mpv:
	;先点击IDM浮动
	ControlGetPos, x, y, w, h, IDM Download Button class1
	ControlClick, IDM Download Button class1, , , Left, 1, x12 y8
	Sleep 500
	MouseMove, x,y
	;再来处理，自己选择具体哪一条清晰度等
	Sleep 3000

	WinWait, 下载文件信息 ahk_class #32770, , 20
	IfWinNotActive, 下载文件信息 ahk_class #32770, , WinActivate, 下载文件信息 ahk_class #32770
		WinWaitActive, 下载文件信息 ahk_class #32770, , 20

	;WinWaitActive, 下载文件信息 ahk_class #32770, , 20
	if !ErrorLevel
	{
		;ControlGetText,Out,Edit1,下载文件信息 ahk_class #32770 ahk_exe IDMan.exe
		ControlGetText,Out,Edit1,下载文件信息 ahk_class #32770
		WinClose,下载文件信息 ahk_class #32770
		;留到以后下载里面
		ControlClick, Button2
		Run,%COMMANDER_PATH%\Plugins\WLX\vlister\mpv.exe "%Out%"
	}
return
;****************************************************
Space_SgGinHgz:	;机动车合格证管理系统,一键撤销,20240410
	SendInput,^v{Enter}		;粘贴
	Sleep,500
	CoordWinClick(815,380)	;查询
	Sleep,5000
	CoordWinClick(835,575)	;撤销
	Sleep,2500
	AutoInput("系统原因")	;原因
	Sleep,1000
	SendInput,{Enter}		;确定
return
;****************************************************

#If WinActive("ahk_group Group_disableCtrlSpace")
	^Space::ControlSend,,^{Space}
	+Space::ControlSend,,+{Space}
#If

;totalcmd中特殊的按住左键点右键移动
;#IfWinNotActive ahk_class TTOTAL_CMD
;~LButton & RButton::
	;opera 等少数软件之中都可以有自己的按住左键点右键功能
	;if not WinActive("ahk_class OperaWindowClass") and not WinActive("GreenBrowser"){
	;Send ^w
	;}
;return
;#IfWinNotActive
;************** Group_browser^相关$ *******


;************** 记事本 ************** {{{1
;启动记事本并去标题等 {{{3
$!#n::
	Run "%COMMANDER_PATH%\Tools\notepad\Notepad3.exe" /f "%COMMANDER_PATH%\Tools\notepad\Lite.ini", , , OutputVarPID
	Sleep 100
	WinWait ahk_pid %OutputVarPID%
	if ErrorLevel
	{
		ToolTip 超时了，再试一下？
		Sleep 2000
		ToolTip
		return
	}
	else
	{
		PID = %OutputVarPID%
		WinGet, ThisHWND, ID, ahk_pid %PID%
		;设置位置和大小, x,y,width,height
		;WinMove, ahk_id %ThisHWND%,, 700,400,550,350
		WinMove, ahk_id %ThisHWND%,, 700,600,310,144
		;WinMove, ahk_pid %PID%,, 700,400,550,350
		;去标题
		;WinSet, Style, ^0xC00000, ahk_pid %PID%
		;不能改变大小
		;WinSet, Style, ^0x40000, ahk_pid %PID%
		;去菜单
		DllCall("SetMenu", "Ptr", ThisHWND, "Ptr", 0)
		;顶端
		;Winset, Alwaysontop, On,  ahk_pid %PID%
	}
return
;****************************************************
;启动记事本并去标题等，并收集剪贴板 {{{3
$^#n::
	Run "%COMMANDER_PATH%\Tools\notepad\Notepad3.exe" /b /f "%COMMANDER_PATH%\Tools\notepad\Lite.ini", , , OutputVarPID
	Sleep 100
	WinWait ahk_pid %OutputVarPID%
	if ErrorLevel
	{
		ToolTip 超时了，再试一下？
		Sleep 2000
		ToolTip
		return
	}
	else
	{
		PID = %OutputVarPID%
		WinGet, ThisHWND, ID, ahk_pid %PID%
		;设置位置和大小, x,y,width,height
		;WinMove, ahk_id %ThisHWND%,, 700,400,550,350
		WinMove, ahk_id %ThisHWND%,, 700,600,310,144
		;WinMove, ahk_pid %PID%,, 700,400,550,350
		;去标题
		;WinSet, Style, ^0xC00000, ahk_pid %PID%
		;不能改变大小
		;WinSet, Style, ^0x40000, ahk_pid %PID%
		;去菜单
		DllCall("SetMenu", "Ptr", ThisHWND, "Ptr", 0)
		;顶端
		;Winset, Alwaysontop, On,  ahk_pid %PID%
	}
return
;************** 记事本$ **************


;************ 例子,建议从这里修改^ ************ {{{1
;建议的绿色便携的小菜单程序PopSel,Qsel，这两个二选一即可
$+#p::
$+#q::
	if(GV_PopSel_QSel="popsel") {
		Run %COMMANDER_PATH%\Tools\popsel\PopSel.exe /pc /T500
		Sleep 500
		MyWinWaitActive("PopSel - ahk_class WindowClass_0")
	}
	else if(GV_PopSel_QSel="qsel") {
		Run,"%COMMANDER_PATH%\Tools\qsel\Qsel.exe", %COMMANDER_PATH%\Tools\qsel
		Sleep 500
		MyWinWaitActive("Qsel ahk_class WindowClass_0")
	}
return

$!#RButton::
	if(GV_PopSel_QSel="popsel") {
		Run %COMMANDER_PATH%\Tools\popsel\PopSel.exe /i /T500
		Sleep 500
		MyWinWaitActive("PopSel - ahk_class WindowClass_0")
	}
	else if(GV_PopSel_QSel="qsel") {
		Run,"%COMMANDER_PATH%\Tools\qsel\Qsel.exe", %COMMANDER_PATH%\Tools\qsel
		Sleep 500
		MyWinWaitActive("Qsel ahk_class WindowClass_0")
	}
return

;#z::
	; Run %COMMANDER_PATH%\Tools\popsel\PopSel.exe /pc /T500
	; Sleep 500
	; MyWinWaitActive("PopSel - ahk_class WindowClass_0")
;return

;#RButton::
	; Run %COMANDER_PATH%\Tools\popsel\PopSel.exe /i /T500
	; Sleep 500
	; MyWinWaitActive("PopSel - ahk_class WindowClass_0")
;return

;建议的绿色便携的小菜单程序Qsel，这两个二选一即可
;#z::
	; Run,"%COMMANDER_PATH%\Tools\qsel\Qsel.exe", %COMMANDER_PATH%\Tools\qsel
	; Sleep 500
	; MyWinWaitActive("Qsel  ahk_class WindowClass_0")
;return

;#RButton::
	; Run,"%COMMANDER_PATH%\Tools\qsel\Qsel.exe", %COMMANDER_PATH%\Tools\qsel
	; Sleep 500
	; MyWinWaitActive("Qsel  ahk_class WindowClass_0")
;return

; #f::
	; Run,%COMMANDER_PATH%\Everything.exe
	; IfExist,C:\Windows\SysWOW64\
		; Run,"%RunCaptX%\Everything\Everything.exe" -s "%getZz%"
	; else
		; Run,"%RunCaptX%\Everything\Everything-x86\Everything.exe" -s "%getZz%"
	; Sleep 500
	; MyWinWaitActive("ahk_class EVERYTHING")
; return


;Win10中默认Win+h是语音输入模块，远不好用，所以替换成讯飞输入模块,20240529
;讯飞语音输入，语音悬浮窗 iFlyVoice.exe
;讯飞输入法语音悬浮窗
#h::
	GV_KeyClickAction1 := "GoSub,Sub_StartStopXF"	;单击启动
	GV_KeyClickAction2 := "GoSub,Sub_KillXF"	;双击关闭
	GoSub,Sub_KeyClick123
return

Sub_StartStopXF:
	xfHwnd := WinExist("ahk_class BaseGui ahk_exe iFlyVoice.exe")
	if not xfHwnd
	{
		; run,D:\Tools\System\FlyVoice\iFlyVoice.exe
		run,%SoftDir%\WTools\iFlyIME\iFlyVoice.exe
		Sleep,500
		WinGetPos, X, Y, W, H, ahk_class BaseGui ahk_exe
		MouseMove,X + W / 2, Y + H / 2
	}
	else {
		;是否已经最小化，先尝试获取
		oTrayInfo := TrayIcon_GetInfo("iFlyVoice.exe")
		if(oTrayInfo.MaxIndex()) {
			TrayIcon_Button(oTrayInfo[1].msgid, oTrayInfo[1].uid, oTrayInfo[1].hwnd, sButton:="L", 0, 1)
		}
		else {
			MyWinWaitActive("ahk_class BaseGui ahk_exe iFlyVoice.exe")
			WinGetPos, x, y,w,h, A
			;msgbox %x% , %y% ,%w% , %h%
			CoordWinClick(w/2,h/2)
			Sleep,300
			SendInput,!{Esc}
		}
	}
Return

Sub_KillXF:
	WinGet,pid,PID,ahk_class BaseGui ahk_exe iFlyVoice.exe
	; run,%A_scriptDir%\Tools\nircmd.exe killprocess /%pid%
	run,%COMMANDER_PATH%\Tools\nircmd.exe killprocess /%pid%
Return

;开cmd命令行
;$!#c::Run, cmd
;管理员权限cmd
$^#c::Run, *RunAs cmd
;在记事本中打开剪贴板中内容
$^!#n::Run, %COMMANDER_PATH%\Tools\notepad\Notepad3.exe /c

;可以改成自己的软件目录,默认在TC的SoftDir
;要求Office默认安装到系统盘,否则改为自己的安装路径
#F6::
	IfExist, %SoftDir%\Office\MicroOffice\MicroWord.exe
		Run, %SoftDir%\Office\MicroOffice\MicroWord.exe
	else
		run winword.exe
return

#F7::
	IfExist, %SoftDir%\Office\MicroOffice\microexcel.exe
		Run, %SoftDir%\Office\MicroOffice\microexcel.exe
	else
		run excel.exe	;
return

#F8::
	IfExist, %SoftDir%\Office\MicroOffice\SmartPPT.exe
		Run, %SoftDir%\Office\MicroOffice\SmartPPT.exe
	else
		run powerpnt.exe	;
return
;************** 例子,建议从这里修改$ **************


;************* 各程序快捷键或功能^ ************* {{{1
;调用任务栏相关程序快捷键1-8 {{{2
;用鼠标中键作为组合键来进行切换，默认注释掉
; ~$MButton::SendInput,^s	;增加保存功能
;$MButton & Tab::
XButton1 & CapsLock::
XButton2 & CapsLock::
CapsLock & 1::	;20240417
`; & CapsLock::
	;Totalcmd
	SendInput,#1
return

XButton1 & Tab::
XButton2 & Tab::
CapsLock & 2::	;20240417
`; & Tab::
	;Vim
	SendInput,#2
return

XButton1 & q::
XButton2 & q::
CapsLock & 3::	;20240417
`; & q::
	;QQ
	SendInput,#3
return

XButton1 & w::
XButton2 & w::
CapsLock & 4::	;20240417
`; & w::
	;微信
	SendInput,#4
return

XButton1 & e::
XButton2 & e::
CapsLock & 5::	;20240417
`; & e::
	SendInput,#5
return

XButton1 & r::
XButton2 & r::
CapsLock & 6::	;20240417
`; & r::
	SendInput,#6
return

XButton1 & t::
XButton2 & t::
CapsLock & 7::	;20240417
`; & t::
	SendInput,#7
return

XButton1 & 8::
XButton2 & 8::
CapsLock & 8::	;20240417
	SendInput,#8
return

XButton1 & 9::
XButton2 & 9::
	SendInput,#9
return

XButton1 & 0::
XButton2 & 0::
	SendInput,10
return
;调用任务栏相关程序快捷键$


;调用多媒体(音乐|视频)相关程序快捷键^ {{{2
;FN+F1~F12,20240417
CapsLock & F1::	;SendInput,{Launch_Media}	;默认播放器
	if WinActive("ahk_class Shell_TrayWnd"){
		gosub,Menu_Document	;AHK网络帮助
	}
	else {
		SendInput,{Launch_Media}	;上一首
	}
return
CapsLock & F2::SendInput,{Volume_Down}	;调低音量V-
CapsLock & F3::SendInput,{Volume_Up}	;增加音量V+
CapsLock & F4::SendInput,{Volume_Mute}	;音量静音

CapsLock & F5::	;SendInput,{Launch_Media}	;上一首
	if WinActive("ahk_exe explorer.exe"){
		gosub,ForceSelfReload	;重启Capstg脚本
	}
	else {
		SendInput,{Launch_Media}	;上一首
	}
return
CapsLock & F6::SendInput,{Media_Prev}	;下一首
CapsLock & F7::SendInput,{Media_Play_Pause}	;播放/暂停
CapsLock & F8::SendInput,{Media_Stop}	;停止

CapsLock & F9::SendInput,{Browser_Home}	;主页
CapsLock & F10::SendInput,{Launch_Mail}	;电子邮件
CapsLock & F11::SendInput,{Volume_Up}	;我的电脑
CapsLock & F12::SendInput,{Browser_Favorites}	;收藏夹
;调用多媒体(音乐|视频)相关程序快捷键$


;***********全局热键Tc|Ev|资管|定位l************
;Ctrl+Alt+左键点击，定位程序到对应的目录
$^!LButton::
$+!MButton::	;20240306
	SendInput,{Click}	;点击激活
	WinGet, ProcessPath, ProcessPath, A
	; Run Explorer /select`, %ProcessPath%	;系统资管定位文件
	Run,"%COMMANDER_EXE%" /A /T /O /S /L="%ProcessPath%"	;TC资管定位文件
	WinActivate, ahk_class TTOTAL_CMD
	WinWait, ahk_class TTOTAL_CMD
	WinMove, ahk_class TTOTAL_CMD, , 0, 0, A_ScreenWidth/2-1, A_ScreenHeight-75
return
;****************************************************
;Win+Alt+左键点击,定位文件到对应的目录,Quicker设定好+!s为定位文件目录动作
$#!LButton::
	SendInput,{Click}	;点击激活
	if WinExist("ahk_exe Quicker.exe"){
		SendInput,+!s	;Quicker设定好+!s为定位文件目录动作
	}
	else {
		SendInput,#!d	;Ra_XiaoYao_plus设定好#!d为定位文件目录动作
	}
return
;****************************************************
$!#z::gosub,TextToEverything ;一键everything搜索
;****************************************************


;把资源管理器中选中的文件用tc打开   {{{2
;Win10|Win7的资源管理器+360文件管理器等
#If WinActive("ahk_group Group_explorer") ;20240228
; #If WinActive("ahk_class CabinetWClass") or WinActive("ahk_class ExploreWClass")
{
	$!q::SendInput,!{Esc}	;切换任务活动窗口|20240601

	CapsLock & w:: ;启动激活微信|微信文件传输助手->发送文件
		IfWinNotExist,ahk_exe WeChat.exe
		{
			gosub,RunWechat	;启动激活微信
			WinActivate, ahk_exe WeChat.exe
			WinWait, ahk_exe WeChat.exe
		}
		else {	;微信文件传输助手->发送文件
			SendInput,{Click}	;点击激活
			SendInput,{Blind}^c
			Sleep,300
			clip:=GetFileInfo(Clipboard,3)	;只获取文件名
			if (clip!=""){
				SendInput,^!w	;激活微信,20231216
				Sleep,100
				gosub,微信文件传输助手
				Sleep,1000
				SendInput,^v
			}
			else {
				SendInput,^!w	;激活微信,20231216
				Sleep,100
				gosub,微信文件传输助手
			}
		}
	return

	$!e::SendInput,#e	;打开系统资管
	; $!e::Run,%RunCaptX%\SoftDir\AutoHotkey\SciTE\SciTE.exe

	$!r::SendInput,{F2}	;重命名

	$!t::	;呼出任务管理器窗口|tagLyst主窗口
		if WinExist("ahk_exe tagLyst.exe")
		{
			SendInput,^#t	;呼出/隐藏tagLyst主窗口
			WinActivate, tagLyst ahk_class Chrome_WidgetWin_1
			WinWait, tagLyst ahk_class Chrome_WidgetWin_1
			WinMove, tagLyst ahk_class Chrome_WidgetWin_1, , 0, 0, A_ScreenWidth/2-1, A_ScreenHeight-75
		}
		else {
			SendInput,^+{Esc}	;呼出任务管理器窗口
		}
	return
;****************************************************
	$!a::	;by Tuutg,20240420
		if WinActive("ahk_class ahk_class #32770"){
			SendInput,^a	;替代全选,Ctrl+A
		}
		else if (WinExist("ahk_exe WeChat.exe") and !WinActive("ahk_exe Listary.exe")){
			SendInput,!a	;微信截图
		}
		else if (WinExist("ahk_exe ATGUI.exe") and !WinExist("ahk_exe WeChat.exe")){
			gosub,Sub_AnytextSelectTxt	;一键Anytext搜索
		}
		else {
			SendInput,^a	;替代全选,Ctrl+A
		}
	return

	; $!s::Run,%RunCaptX%\SoftDir\AutoHotkey\WindowSpy.exe	;预留给Ra搜索
	$!s::
	if WinExist("ahk_exe WindowSpy.exe"){
		WinActivate, Window Spy ahk_class AutoHotkeyGUI
	}
	else {
		Run,%RunCaptX%\SoftDir\AutoHotkey\WindowSpy.exe
		WinActivate, Window Spy ahk_class AutoHotkeyGUI
	}
	return

	; $!d::SendInput,#d	;呼出/隐藏桌面|保持系统默认,聚集地址栏

	$!f::gosub,TextToEverything	;一键everything搜索

	; $!g::SendInput,!g	;Listary浏览程序路径|Ra插件管理
;****************************************************
	$!z::SendInput,^z	;撤销

	; $!x::	;预留激活AnyTXT Searcher,要在AnyTXT设置!x激活热键

	$!c::SendInput,#d	;显示桌面|呼出Loom|Utools_OCR_识别复制

	$!v::
		if WinExist("ahk_exe QuickJump.exe"){
			SendInput,^d	;QuickJump,^d,弹出菜单列表
		}else if (WinActive("ahk_exe Listary.exe")){
			SendInput,!v	;Listary,智能动作,20240329
		}
		else {	;打开编辑器
			IfExist, D:\Program Files\Microsoft VS Code\Code.exe
				Run,"D:\Program Files\Microsoft VS Code\Code.exe"	;呼出Code.exe
			else IfExist,%RunCaptX%\SoftDir\Notepad--\notepad--.exe
				Run,%RunCaptX%\SoftDir\Notepad--\notepad--.exe	;呼出Notpad--
			else Run,notepad.exe ;系统自带notepad.exe
		}
	return

	$!b::	;呼出Bandicam
		IfExist, %RunCaptX%\SoftDir\SCapture\Bandicam\Bandicam.exe
		Run,%RunCaptX%\SoftDir\SCapture\Bandicam\Bandicam.exe
	return
;****************************************************
	$!w::	;explorer->TC,并开启半屏显示
		if(COMMANDER_EXE="")
			return
		selected := Explorer_Get("",true)

		;如果没有选中文件，那就直接用当前目录
		if(selected = "")
		{
			WinGetText, CurWinAllText
			loop, Parse, CurWinAllText, `n, `r
			{
				if RegExMatch(A_LoopField, "^地址: "){
					curWinPath := SubStr(A_LoopField,5)
					break
				}
			}
			selected := curWinPath
		}

		selected := """" selected """"
		;msgbox % selected
		;WinClose A  ;把当前资源管理器关闭
		Run, %COMMANDER_EXE% /T /O /S /A /L=%selected%
		WinActivate, ahk_class TTOTAL_CMD
		WinWait, ahk_class TTOTAL_CMD
		WinMove, ahk_class TTOTAL_CMD, , 0, 0, A_ScreenWidth/2-1, A_ScreenHeight-75
	return
}
#If
;****************************************************
;Win10_11桌面|Win7
#If WinActive("ahk_class Progman") or WinActive("ahk_class WorkerW")
{
	; $!q::SendInput,!{Esc}	;Runany_启动管理|切换任务窗口,20240601

	CapsLock & w:: ;启动激活微信|微信文件传输助手
		IfWinNotExist,ahk_exe WeChat.exe
		{
			gosub,RunWechat	;启动激活微信
			WinActivate, ahk_exe WeChat.exe
			WinWait, ahk_exe WeChat.exe
		}
		else {
			SendInput,^!w	;激活微信,20231216
			; Sleep,100
			; gosub,微信文件传输助手
		}
	return

	$!e::SendInput,#e	;打开系统资管
	; $!e::Run,%RunCaptX%\SoftDir\AutoHotkey\SciTE\SciTE.exe

	$!r::SendInput,{F2}	;重命名,20240328

	$!t::	;呼出任务管理器窗口|tagLyst主窗口
		if WinExist("ahk_exe tagLyst.exe")
		{
			SendInput,^#t	;呼出/隐藏tagLyst主窗口
			WinActivate, tagLyst ahk_class Chrome_WidgetWin_1
			WinWait, tagLyst ahk_class Chrome_WidgetWin_1
			WinMove, tagLyst ahk_class Chrome_WidgetWin_1, , 0, 0, A_ScreenWidth/2-1, A_ScreenHeight-75
		}
		else {
			SendInput,^+{Esc}	;呼出任务管理器窗口
		}
	return
;****************************************************
	; $!a::gosub,Sub_AnytextSelectTxt	;微信截图|一键Anytext搜索|Listray_智能命令

	; $!s::Run,%RunCaptX%\SoftDir\AutoHotkey\WindowSpy.exe	;预留给Ra搜索
	$!s::
		if WinExist("ahk_exe WindowSpy.exe"){
			WinActivate, Window Spy ahk_class AutoHotkeyGUI
		}
		else {
			Run,%RunCaptX%\SoftDir\AutoHotkey\WindowSpy.exe
			WinActivate, Window Spy ahk_class AutoHotkeyGUI
		}
	return

	$!d::SendInput,#d	;呼出/隐藏桌面

	$!f::gosub,TextToEverything	;一键everything搜索

	$!g::	;SendInput,#!t	;呼出Utools_goole_翻译;设定翻译-#!t,20240518
		if WinExist("ahk_exe Utools.exe"){
			SendInput,#!t	;呼出Utools_goole_翻译
		}
		else {
			SendInput,{Blind}#!g	;呼出Ra插件管理,by Tuutg,20240518
		}
	return
;****************************************************
	; $!z::	;预留|=^Z,撤销

	; $!x::	;预留|一键Anytext搜索

	$!c::gosub,DbRButtonGoExplorer	;桌面双击右键,打开资管左侧半屏显示
		; if (WinExist("ahk_exe Loom.exe") or WinExist("ahk_exe uTools.exe"))
		; {
			; SendInput,!c	;呼出Loom|Utools_OCR_识别复制
		; }
		; else {	;开启左侧半屏显示
			; gosub,DbRButtonGoExplorer	;桌面双击右键,打开资管左侧半屏显示
		; }
	; return

	$!v::
		if WinExist("ahk_exe QuickJump.exe"){
			SendInput,^d	;QuickJump,^d,弹出菜单列表
		}else if (WinActive("ahk_exe Listary.exe")){
			SendInput,!v	;Listary,智能动作,20240329
		}
		else {	;打开编辑器
			IfExist, D:\Program Files\Microsoft VS Code\Code.exe
				Run,"D:\Program Files\Microsoft VS Code\Code.exe"	;呼出Code.exe
			else IfExist,%RunCaptX%\SoftDir\Notepad--\notepad--.exe
				Run,%RunCaptX%\SoftDir\Notepad--\notepad--.exe	;呼出Notpad--
			else Run,notepad.exe ;系统自带notepad.exe
		}
	return

	$!b::	;呼出Bandicam
		IfExist, %RunCaptX%\SoftDir\SCapture\Bandicam\Bandicam.exe
		Run,%RunCaptX%\SoftDir\SCapture\Bandicam\Bandicam.exe
	return
;****************************************************
	$!w:: ;explorer->TC,并开启半屏显示
		if(COMMANDER_EXE="")
			return
		selected := Explorer_Get("",true)
		if(selected = ""){
		selected := """" A_Desktop """"
			Run, %COMMANDER_EXE% /T /O /A /S /L=%selected%
			WinActivate, ahk_class TTOTAL_CMD
			WinWait, ahk_class TTOTAL_CMD
			WinMove, ahk_class TTOTAL_CMD, , 0, 0, A_ScreenWidth/2-1, A_ScreenHeight-75
			Sleep 200
			selected := """" A_DesktopCommon """"
			Run, %COMMANDER_EXE% /T /O /A /S /R=%selected%
			WinActivate, ahk_class TTOTAL_CMD
			WinWait, ahk_class TTOTAL_CMD
			WinMove, ahk_class TTOTAL_CMD, , 0, 0, A_ScreenWidth/2-1, A_ScreenHeight-75
		}
		else {
			selected := """" selected """"
			Run, %COMMANDER_EXE% /T /O /S /A /L=%selected%
			WinActivate, ahk_class TTOTAL_CMD
			WinWait, ahk_class TTOTAL_CMD
			WinMove, ahk_class TTOTAL_CMD, , 0, 0, A_ScreenWidth/2-1, A_ScreenHeight-75
		}
	return

	;桌面双击右键,打开资管左侧半屏显示
	$RButton::	;20240108
		;GV_MouseTimer := 300
		GV_KeyClickAction1 := "SendInput,{Click,Right}"	;系统默认,RButton
		GV_KeyClickAction2 := "gosub,DbRButtonGoExplorer" ;资管左侧半屏显示
		GV_LongClickAction := "SendInput,{Click,Right}"	;系统默认,RButton
		gosub,Sub_MouseClick123
	return

	$F4:: ;F4Menu,by Tuutg,20240628
		if xfHwnd	;讯飞已经启动
			SendInput,{F4}
		else
		{
			SendInput,{Click,Right}
			Sleep 50
			SendInput,{Text}f ;F4Menu
		}
	return
}
#If
;****************************************************
;用到的跳转标签,桌面双击右键,打开资管左侧半屏显示
DbRButtonGoExplorer:
	BlockInput On
	SendInput,#e
	Sleep,1000
	SendInput,^#!2	;开启左侧半屏显示,需要Gridmove支持
	BlockInput Off
return

;用到的跳转标签,任务栏双击右键,显示桌面
DbRButtonGoDesktop:
if (WinExist("ahk_exe Loom.exe") or WinExist("ahk_exe uTools.exe")){
	SendInput,!c	;呼出Loom|Utools_OCR_识别复制
}
else {	;开启左侧半屏显示
	BlockInput On
	SendInput,#d
	Sleep,1000
	BlockInput Off
}
return
;****************************************************
;用到的函数
Explorer_GetPath(hwnd="")
{
	if !(window := Explorer_GetWindow(hwnd))
		return ErrorLevel := "ERROR"
	if (window="desktop")
		return A_Desktop
		path := window.LocationURL
		path := RegExReplace(path, "ftp://.*@","ftp://")
		StringReplace, path, path, file:///
		StringReplace, path, path, /, \, All
		loop
		if RegExMatch(path, "i)(?<=%)[\da-f]{1,2}", hex)
			StringReplace, path, path, `%%hex%, % Chr("0x" . hex), All
		else break
		return path
}

Explorer_GetWindow(hwnd="")
{
	WinGet, Process, processName, % "ahk_id" hwnd := hwnd? hwnd:WinExist("A")
	WinGetClass class, ahk_id %hwnd%

	if (Process!="explorer.exe")
		return
	if (class ~= "(Cabinet|Explore)WClass")
	{
		try {
			for window in ComObjCreate("Shell.Application").Windows
			if (window.hwnd==hwnd)
			return window
		}
		catch {
			return ""
		}
	}
	else if (class ~= "Progman|WorkerW")
	return "desktop"
}


Explorer_Get(hwnd="",selection=false)
{
	if !(window := Explorer_GetWindow(hwnd))
		return ErrorLevel := "ERROR"
	if (window="desktop")
	{
		ControlGet, hwWindow, HWND,, SysListView321, ahk_class Progman
		if !hwWindow
			ControlGet, hwWindow, HWND,, SysListView321, A
			ControlGet, Files, List, % ( selection ? "Selected":"") "Col1",,ahk_id %hwWindow%
			base := SubStr(A_Desktop,0,1)=="\" ? SubStr(A_Desktop,1,-1) : A_Desktop
		loop, Parse, Files, `n, `r
		{
			path := base "\" A_LoopField
			IfExist %path%
			ret .= path "`n"
		}
	}
	else
	{
		if selection
			collection := window.document.SelectedItems
		else
			collection := window.document.Folder.Items
		for item in collection
			ret .= item.path "`n"
	}
return Trim(ret,"`n")
}
;**************** TTOTAL_CMD $ ****************


;QQ,Tim中快速定位聊天位置   {{{2
;Tim
;QQTimFPath := % "E:\Users\Tuutg\Personal\Tencent Files\XXXXXXXXX\FileRecv\"
#If WinActive("ahk_class TXGuiFoundation") or WinActive("ahk_exe Tim.exe")
{
	!1::CoordWinClick(Tim_Start_X, Tim_Start_Y+(1-1)*Tim_Bar_Height)
	!2::CoordWinClick(Tim_Start_X, Tim_Start_Y+(2-1)*Tim_Bar_Height)
	!3::CoordWinClick(Tim_Start_X, Tim_Start_Y+(3-1)*Tim_Bar_Height)
	!4::CoordWinClick(Tim_Start_X, Tim_Start_Y+(4-1)*Tim_Bar_Height)
	!5::CoordWinClick(Tim_Start_X, Tim_Start_Y+(5-1)*Tim_Bar_Height)
	!6::CoordWinClick(Tim_Start_X, Tim_Start_Y+(6-1)*Tim_Bar_Height)
	!7::CoordWinClick(Tim_Start_X, Tim_Start_Y+(7-1)*Tim_Bar_Height)
	!8::CoordWinClick(Tim_Start_X, Tim_Start_Y+(8-1)*Tim_Bar_Height)
	!9::CoordWinClick(Tim_Start_X, Tim_Start_Y+(9-1)*Tim_Bar_Height)
	!0::CoordWinClick(Tim_Start_X, Tim_Start_Y+(10-1)*Tim_Bar_Height)
	!-::CoordWinClick(Tim_Start_X, Tim_Start_Y+(11-1)*Tim_Bar_Height)
	!=::CoordWinClick(Tim_Start_X, Tim_Start_Y+(12-1)*Tim_Bar_Height)

	$!w::
	$!f::
		;这里改成自己对应的路径
		Run,"%COMMANDER_EXE%" /A /T /O /R="%QQTimFPath%"
		Sleep 500
		MyWinWaitActive("ahk_class TTOTAL_CMD")
	return

	;聊天记录翻页，先聚焦一下，才能按别的按键快速按键
	!l::
		WinGetPos, , , w, h, A
		CoordWinClick(w*0.7,h*0.3)
	return

	!j::send,{Down}
	!k::send,{Up}
	!n::send,{PgDn}
	!m::send,{PgUp}{Up}
	!u::send,{End}
	!i::send,{Home}{Up}

	$RButton::	;20240108
		;GV_MouseTimer := 300
		GV_KeyClickAction1 := "SendInput,{Click,Right}"	;系统默认,RButton
		GV_KeyClickAction2 := "SendInput,!{F4}"	;{Escape}
		GV_LongClickAction := "SendInput,{Click,Right}"	;系统默认,RButton
		gosub,Sub_MouseClick123
	return

	; $Space::SendInput,{Blind}{Space}	;恢复{Space}.20240108
	$Space::Space ;恢复空格打字上屏,by Tuutg,20240425
	Escape::SendInput,!{F4}	;{Escape}
}
#If
;****************************************************
;QQ
#If WinActive("ahk_class TXGuiFoundation") or WinActive("ahk_exe QQ.exe")
{
	!1::CoordWinClick(QQ_Start_X, QQ_Start_Y+(1-1)*QQ_Bar_Height)
	!2::CoordWinClick(QQ_Start_X, QQ_Start_Y+(2-1)*QQ_Bar_Height)
	!3::CoordWinClick(QQ_Start_X, QQ_Start_Y+(3-1)*QQ_Bar_Height)
	!4::CoordWinClick(QQ_Start_X, QQ_Start_Y+(4-1)*QQ_Bar_Height)
	!5::CoordWinClick(QQ_Start_X, QQ_Start_Y+(5-1)*QQ_Bar_Height)
	!6::CoordWinClick(QQ_Start_X, QQ_Start_Y+(6-1)*QQ_Bar_Height)
	!7::CoordWinClick(QQ_Start_X, QQ_Start_Y+(7-1)*QQ_Bar_Height)
	!8::CoordWinClick(QQ_Start_X, QQ_Start_Y+(8-1)*QQ_Bar_Height)
	!9::CoordWinClick(QQ_Start_X, QQ_Start_Y+(9-1)*QQ_Bar_Height)
	!0::CoordWinClick(QQ_Start_X, QQ_Start_Y+(10-1)*QQ_Bar_Height)
	!-::CoordWinClick(QQ_Start_X, QQ_Start_Y+(11-1)*QQ_Bar_Height)
	!=::CoordWinClick(QQ_Start_X, QQ_Start_Y+(12-1)*QQ_Bar_Height)

	$!w::
	$!f::
		;这里改成自己对应的路径
		Run,"%COMMANDER_EXE%" /A /T /O /R="%QQTimFPath%"
		Sleep 500
		MyWinWaitActive("ahk_class TTOTAL_CMD")
	return

	;聊天记录翻页，先聚焦一下，才能按别的按键快速按键
	!l::
		WinGetPos, , , w, h, A
		CoordWinClick(w*0.7,h*0.3)
	return
	!j::send,{Down}
	!k::send,{Up}
	!n::send,{PgDn}
	!m::send,{PgUp}{Up}
	!u::send,{End}
	!i::send,{Home}{Up}

	$RButton::	;20240108
		;GV_MouseTimer := 300
		GV_KeyClickAction1 := "SendInput,{Click,Right}"	;系统默认,RButton
		GV_KeyClickAction2 := "SendInput,!{F4}"	;{Escape}
		GV_LongClickAction := "SendInput,{Click,Right}"	;系统默认,RButton
		gosub,Sub_MouseClick123
	return

	; $Space::SendInput,{Blind}{Space}	;恢复{Space}.20240108
	$Space::Space ;恢复空格打字上屏,by Tuutg,20240425
	Escape::SendInput,!{F4}	;{Escape}
}
#If
;****************************************************
;****************************************************
;微信PC客户端全局 {{{2	;20240305
#If WinActive("ahk_exe WeChat.exe")
	;~恢复默认功能:~Space,~$Tab打字和用于热字符串(快速联系人)激活键.20240520
	; $Space::SendInput,{Blind}{Space}
	$Space::Space	;修复打汉字时会多一个空格,by Tuutg,20240520

	~$Tab::
		if (A_Cursor = "IBeam")
			SendInput,{Enter}{Tab}	;20240520,中英通用,快速联系人
		else
			SendInput,{Tab}	;20240520
	return

	;双击右键关闭
	$RButton::
		;GV_MouseTimer := 300
		GV_KeyClickAction1 := "SendInput,{Click,Right}"	;系统默认,RButton
		GV_KeyClickAction2 := "SendInput,!{F4}"	;{Escape}.20240105
		GV_MouseButton := 2
		GV_LongClickAction := "SendInput,{Click,Right}"	;系统默认,RButton
		gosub,Sub_MouseClick123
	return

	;修复Caps+X|C|V,剪切|复制时,单按CapsLock退出微信Bug,改成双击退出
	CapsLock::
		Suspend Permit
		;GV_MouseTimer := 300
		GV_KeyClickAction1 := "SendInput,{Tab}"	;修复单按CapsLock退出微信Bug
		GV_KeyClickAction2 := "SendInput,!{F4}"	;{Escape}.20240105
		gosub,Sub_KeyClick123
	return

	; CapsLock & w::gosub,微信文件传输助手

	Escape::SendInput,!{F4}	;{Escape}

#If

;****************************************************

;微信PC非输入模式启用单键上下模式,{S}{F}会影响打字关闭,修复看图E|D失效问题
/*#If WinActive("ahk_exe WeChat.exe") and !WinActive("图片查看 ahk_class ImagePreviewWnd") and !WinActive("微信 ahk_class ImagePreviewLayerWnd")
$e::
if (A_Cursor != "IBeam"){
	SendInput,{BackSpace}{up}   ;上|避免光标跑到打字框,上失效,20240328
}
else{
	CoordWinMove(%A_CaretX%,%A_CaretY%)
	SendInput,{Text}e ;上|恢复搜索框正常打字,20240328
}
return

$d::
if (A_Cursor != "IBeam"){
	SendInput,{BackSpace}{Down} ;下|避免光标跑到打字框,下失效,20240328
}
else{
	CoordWinMove(%A_CaretX%,%A_CaretY%)
	SendInput,{Text}d ;下|恢复搜索框正常打字,20240328
}
return
#If
*/
;****************************************************

;修复微信中在打开|保持等弹出对话框时按caps|+W弹出文件助手的问题,更改为caps|+W=Enter
#If (WinActive("ahk_exe WeChat.exe") and !WinActive("ahk_group GroupDiagOpenAndSave"))
	CapsLock & w::gosub,微信文件传输助手
#If

;****************************************************

; 微信SingleSpace增强模式,约等于Ctrl,Space+EDSF=上下左右,启用设为1
#If (WinActive("ahk_exe WeChat.exe") and (GV_SpaceKeysAsCapsLock==1))
Space & q:: ;到首部|上翻页,20240320
if (GV_KeyClick_Continuous(GV_KeyTimer)){
	if (A_Cursor = "IBeam") or (A_CaretX !="")
		SendInput,^{Home} ;到首部
	else
		SendInput,{PgUp} ;上翻页
}
else{
	SendInput,{Home} ;到首部
}
return

Space & w::	;输入^|&键
	GV_KeyClickAction1 := "gosub,Space_SgW0"  ;单击保存|资管左键
	GV_KeyClickAction2 := "SendInput,{Text}&" ;逻辑与,&
	gosub,Sub_KeyClick123
return

Space & e::	;{Up}|输入_键
	GV_KeyClickAction1 := "SendInput,{Up}"
	GV_KeyClickAction2 := "SendInput,{Text}_"
	gosub,Sub_KeyClick123
return

Space & r::	;输入{del}|=|+键
	GV_KeyClickAction1 := "SendInput,{del}"
	GV_KeyClickAction2 := "SendInput,{Text}+"
	gosub,Sub_KeyClick123
return

Space & t::		;输入{Backspace}|%键
	GV_KeyClickAction1 := "SendInput,{Backspace}"
	GV_KeyClickAction2 := "SendInput,`|" ;逻辑或,|
	gosub,Sub_KeyClick123
return

;***********************************************

Space & a:: ;到末尾|上翻页,20240320
if (GV_KeyClick_Continuous(GV_KeyTimer)){
	if (A_Cursor = "IBeam") or (A_CaretX !="")
		SendInput,^{End} ;到末尾
	else
		SendInput,{PgDn} ;下翻页
}
else{
	SendInput,{End} ;到末尾
}
return

Space & s::	;输入/(数字/)
	GV_KeyClickAction1 := "SendInput,{Left}" ;{?/}
	GV_KeyClickAction2 := "SendInput,`/"
	gosub,Sub_KeyClick123
return

Space & d::	;单击{Left},双击输入逗(D)号
	GV_KeyClickAction1 := "SendInput,{Down}"
	GV_KeyClickAction2 := "SendInput,`,"
	gosub,Sub_KeyClick123
return

Space & f::	;单击{Right},双击输入分号(F);键
	GV_KeyClickAction1 := "SendInput,{Right}"
	GV_KeyClickAction2 := "SendInput,`;" ;分号
	gosub,Sub_KeyClick123
return

Space & g:: ;输入句号(g).键
	GV_KeyClickAction1 := "SendInput,{Enter}"
	GV_KeyClickAction2 := "SendInput,`."
	gosub,Sub_KeyClick123
return

;***********************************************

Space & z::
	GV_KeyClickAction1 := "gosub,Space_SgZ" ;输入""键
	GV_KeyClickAction2 := "SendInput,`'" 	;',单引号
	gosub,Sub_KeyClick123
return

Space & x::
	GV_KeyClickAction1 := "gosub,Space_SgX"	;剪切
	GV_KeyClickAction2 := "gosub,Space_DbX" ;{}|]
	gosub,Sub_KeyClick123
return

Space & c::
	GV_KeyClickAction1 := "gosub,Space_SgC" ;复制
	GV_KeyClickAction2 := "gosub,Space_DbC" ;()|'>,Listary->cmd命令行
	gosub,Sub_KeyClick123
return

Space & v::
	GV_KeyClickAction1 := "SendInput,^v" ;原版粘贴
	GV_KeyClickAction2 := "gosub,Space_DbV" ;{}|]
	gosub,Sub_KeyClick123
return

Space & b::	;智能判断: 鼠标右(Y)键单击|输入单冒号:|中键,20240403
	GV_KeyClickAction1 := "gosub,Space_SgB0" ;鼠标右(Y)键单击|单冒号:
	GV_KeyClickAction2 := "gosub,Space_DbB0" ;鼠标右(Y)键单击|中键
	gosub,Sub_KeyClick123
return

;***********************************************

Space & F1:: SendInput,{F11} ;加+10
Space & F2:: SendInput,{F12} ;加+10
Space & F3:: SendInput,{F8} ;相加=11
Space & F4:: SendInput,{F7} ;相加=11
Space & F5:: SendInput,{F6} ;相加=11

;***********************************************

Space & 1::	;输入0|!键,相加=11
	GV_KeyClickAction1 := "SendInput,{0}"
	GV_KeyClickAction2 := "SendInput,{Text}!"
	gosub,Sub_KeyClick123
return

Space & 2::	;输入2|@键,相加=11
	GV_KeyClickAction1 := "SendInput,{9}"
	GV_KeyClickAction2 := "SendInput,{Text}@"
	gosub,Sub_KeyClick123
return

Space & 3::	;输入3|#键,相加=11
	GV_KeyClickAction1 := "SendInput,{8}"
	GV_KeyClickAction2 := "SendInput,{Text}#"
	gosub,Sub_KeyClick123
return

Space & 4::	;输入4|$键,相加=11
	GV_KeyClickAction1 := "SendInput,{7}"
	GV_KeyClickAction2 := "SendInput,{Text}$"
	gosub,Sub_KeyClick123
return

Space & 5::	;输入5|%键,相加=11
	GV_KeyClickAction1 := "SendInput,{6}"
	GV_KeyClickAction2 := "SendInput,{Text}%"
	gosub,Sub_KeyClick123
return

Space & 6:: SendInput,{Text}^ ;输入^键
Space & 7:: SendInput,{Text}& ;输入&键
Space & 8:: SendInput,{Text}* ;输入*键
Space & 9:: SendInput,{Text}( ;输入(键
Space & 0:: SendInput,{Text}) ;输入)键

;***********************************************

Space & -:: SendInput,`_ ;输入_键
Space & =:: SendInput,{NumpadAdd} ;输入+键
Space & [:: SendInput,{Text}{ ;输入{键
Space & ]:: SendInput,{Text}} ;输入}键
Space & `;:: SendInput,`: ;输入:键
Space & ':: SendInput,`" ;输入"键
Space & \:: SendInput,`| ;输入|键
Space & /:: SendInput,`? ;输入?键
Space & ,:: SendInput,`< ;输入<键
Space & .:: SendInput,`> ;输入>键

#If
;****************************************************
;微信PC客户端聊天主窗口,增强快速联系人,文件转发,搜索,文件传输助手
#If WinActive("微信 ahk_class WeChatMainWndForPC")
{

	; !a::微信自带截图

	;聚焦搜索框
	$!s::CoordWinClick(100,36)
	!/::CoordWinClick(100,36)

	;点击绿色聊天的数字,未读新消息
	!,::
		CoordMode, Mouse, Window
		Click 28,90 2
		Sleep,100
		Click 180,100
	return

	;聚焦打字框
	$!d::
	!`;::
		WinGetPos, wxx, wxy,wxw,wxh, ahk_class WeChatMainWndForPC
		wxw := wxw - 80
		wxh := wxh - 60
		CoordWinClick(wxw,wxh)
	return

	;点击联系人
	!1::CoordWinClick(WX_Start_X, WX_Start_Y+(1-1)*WX_Bar_Height)
	!2::CoordWinClick(WX_Start_X, WX_Start_Y+(2-1)*WX_Bar_Height)
	!3::CoordWinClick(WX_Start_X, WX_Start_Y+(3-1)*WX_Bar_Height)
	!4::CoordWinClick(WX_Start_X, WX_Start_Y+(4-1)*WX_Bar_Height)
	!5::CoordWinClick(WX_Start_X, WX_Start_Y+(5-1)*WX_Bar_Height)
	!6::CoordWinClick(WX_Start_X, WX_Start_Y+(6-1)*WX_Bar_Height)
	!7::CoordWinClick(WX_Start_X, WX_Start_Y+(7-1)*WX_Bar_Height)
	!8::CoordWinClick(WX_Start_X, WX_Start_Y+(8-1)*WX_Bar_Height)
	!9::CoordWinClick(WX_Start_X, WX_Start_Y+(9-1)*WX_Bar_Height)
	!0::CoordWinClick(WX_Start_X, WX_Start_Y+(10-1)*WX_Bar_Height)

	;这里请修改everything中的书签，确保ev中书签名字保持一致
	$!b::	;20240130
		; Run, "%COMMANDER_PATH%\everything.exe" -bookmark wechatfile
		IfExist,C:\Windows\SysWOW64\
			Run,"%RunCaptX%\Everything\Everything.exe" -bookmark wechatfile
		else
			Run,"%RunCaptX%\Everything\Everything-x86\Everything.exe" -bookmark wechatfile
	return
	;这里请修改tc中搜索条件的目录位置, 改成自己的 E:\Users\Tuutg\Personal\WeChat Files\wxid_7886778867612\FileStorage\File\

	;将tc中默认D:\My Documents\WeChat files\xxx(自己用户名)\FileStorage\MsgAttach改成自己的MsgAttac所在目录全路径
	$!w::
	$!f::
		;Wx_Path = % "E:\Users\Tuutg\Personal\WeChat Files\wxid_7886778867612\FileStorage\File\" . fun_GetFormatTime( "yyyy-MM" )
		;Run,"%COMMANDER_EXE%" /T /O /S /R="%wx_path%"
		;Sleep 500
		;MyWinWaitActive("ahk_class TTOTAL_CMD")

		Run,"%COMMANDER_EXE%" /A /T /O /R="%WechatFpath%"
		Sleep,100
		MyWinWaitActive("ahk_class TTOTAL_CMD")
		TcSendUserCommand("em_loadSearchWechatFile")
		Sleep,500
		SendInput,!s	;TC-打开搜索
		Sleep,2000
		SendInput,!x	;TC-详细模式
	return
;****************************************************

	;点右键选删除
	$!x::	;打X,删除
		;SendInput,{RButton}
		SendInput,{Click,Right}
		Sleep,200
		SendInput,{Up 2}{Enter}
		Sleep,500
		SendInput,{Enter}
	return

	;点右键选撤回
	$!z::
		SendInput,{RButton}
		Sleep,200
		SendInput,{Down 2}{Enter}
	return

	;点右键一键复制并跳到聊天打字框
	$!c::
		SendInput,{Click,Right}
		Sleep 200
		SendInput,{Up 1}{Enter}
		WinGetPos, wxx, wxy,wxw,wxh, ahk_class WeChatMainWndForPC
		wxw := wxw - 80
		wxh := wxh - 60
		CoordWinClick(wxw,wxh)
	return

	;聚焦表情窗口
	!e::
		send,!e
		sleep,500
		send,{Tab 3}{Right}
	return

	;聊天记录翻页，先聚焦一下，才能按别的按键快速按键
	!l::
		WinGetPos, , , w, h, A
		CoordWinClick(w*0.7,h*0.3)
	return

	!j::send,{Down}
	!k::send,{Up}
	!n::send,{PgDn}
	!m::send,{PgUp}{WheelUp}
	!u::send,{End}
	!i::send,{Home}{WheelUp}

	;(图片)点右键另存为|在文件夹中显示
	$MButton::
		SendInput,{Click,Right}
		Sleep 200
		SendInput,{Up 3}{Enter}
	return

	;(文件)点右键另存为|在文件夹中显示
	$+MButton::
		SendInput,{Click,Right}
		Sleep 200
		SendInput,{Up 4}{Enter}
	return

	;点右键一键转发,适用图片/视频/文字 | 点右键一键收藏,适用非图片文件
	$!v::
	$!LButton::
		SendInput,{Click,Right}
		Sleep 200
		SendInput,{Down 3}{Enter}
	return

	;点右键一键转发,适用非图片文件 | 点右键一键复制,适用图片/视频
	;$!b::
	$!MButton::
		SendInput,{Click,Right}
		Sleep 200
		SendInput,{Down 2}{Enter}
	return
;****************************************************
	;...聊天信息
	CapsLock & f::
		WinGetPos, x, y,lengthA,hightA, A
		CoordWinClick(lengthA-18,35)
	return

	;...聊天记录
	CapsLock & s::
		WinGetPos, x, y,lengthA,hightA, A
		CoordWinClick(lengthA-18,35)
		Sleep,200
		WinActivate ahk_class SessionChatRoomDetailWnd
		Sleep,500
		Click,130,130
	return

	CapsLock & g::SendInput,{Enter}	;恢复单独功能,不要Ctr+G

}
#If
;****************************************************
;微信订阅号 {{{2
#If WinActive("CWebviewHostWnd ahk_class CWebviewControlHostWnd")
{
	!,::
		MyWinWaitActive("ahk_class WeChatMainWndForPC")
		CoordMode, Mouse, Window
		Click 28,90 2
		Sleep,100
		Click 180,100
	Return
}
#If
;****************************************************
;一键文件传输助手
微信文件传输助手:
	SendInput, ^f	;定位搜索框
	Sleep,50
	SendInput, ^a	;选中搜索框
	Sleep,50
	SendInput, {Del}  ;清空搜索框
	Sleep,50
	SendInput, {Text}文件传输助手 ;AutoInput("文件传输助手")
	Sleep,600 ;等待出现文件传输助手,不要改动
	SendInput, {Shift}{Enter}
	Click, 170,100
return
;****************************************************
;微信看图器
;#If WinActive("图片查看 ahk_exe WeChat.exe")
#If WinActive("图片查看 ahk_class ImagePreviewWnd") or WinActive("微信 ahk_class ImagePreviewLayerWnd")
{
	XButton1::SendInput,{Right}
	XButton2::SendInput,{Left}
	$!WheelUp::SendInput,{Left}
	$!WheelDown::SendInput,{Right}
	Space & WheelUp::SendInput,{Left}
	Space & WheelDown::SendInput,{Right}

	$h::SendInput,{Left}
	$l::SendInput,{Right}
	$k::Click, 121, 16 	 ;+/放大
	$j::Click, 154, 16 	 ;-/缩小

	$`;::SendInput,{Esc}           ;分号=Esc

	;双击右键关闭
	RButton::
		GV_MouseTimer := 400
		GV_KeyClickAction1 := "Send,{RButton}"
		GV_KeyClickAction2 := "Send,{Escape}"
		GV_MouseButton := 2
		GV_LongClickAction := "Send,{Escape}"
		GoSub,Sub_MouseClick123
	return

	$MButton::
	CapsLock & g::
	~$Enter::gosub,Sub_MaxRestore ;最大化

	;对视频内容点击播放
	$Space::
		WinGetPos, x, y,lengthA,hightA, A
		;MouseMove, % lengthA/2 ,hightA/2
		CoordWinClick(lengthA/2,hightA/2)
	return

	$q::Click, 80, 16 	 ;预览
	$w::Click, 340, 16 	 ;OCR/提取文字
	$Up:: ;+/放大,20240321
	$e::Click, 121, 16 	 ;+/放大
	$r::Click, 233, 16 	 ;旋转
	$t::Click, 305, 16 	 ;翻译

	$a::	;20240424
		Click, 405, 16 ;另存,有二维码时
		Click, 373, 16 ;另存,无二维码时
	return

	$m::	;20240424
		Click, 373, 16 ;二维码,有二维码时
	return

	$s::SendInput,{Left} ;<--
	$Down::	;-/缩小,20240321
	$d::Click, 154, 16 	 ;-/缩小
	$f::SendInput,{Right} ;-->
	$g::Click, 189, 16 	 ;1:1=左键双击

	$z::		 ;收藏
		SendInput,{Click,Right}
		Sleep,200
		SendInput,{Up 3}{Enter}
	return

	$x::SendInput,{Esc} ;Esc

	$c::				 ;复制
		SendInput,{Click,Right}
		Sleep,200
		SendInput,{Down 1}{Enter}
	return

	$v::				;转发
	$!v::
	$!LButton::
		SendInput,{Click,Right}
		Sleep,200
		SendInput,{Down 4}{Enter}
	return

	$b::Click, 265, 18 ;编辑
}
#If
;****************************************************
;微信图片编辑器,在非全屏模式下,相对坐标法,以马赛克为中心.20240105
#If WinActive("图片编辑 ahk_class EditPicWnd") and (A_Cursor != "IBeam")
	XButton1::WheelUp		;+/放大
	XButton2::WheelDown		;-/缩小

	$h::WheelUp	;+/放大
	$k::WheelUp	;+/放大
	$j::WheelDown	;-/缩小
	$L::WheelDown	;-/缩小

	$e::WheelUp		;+/放大
	$d::WheelDown	;-/缩小
	$s::SendInput,{Blind]^z ;撤销
	$f::SendInput,{Blind]^y ;恢复

	$MButton::
		SendInput,{Click}	;点击激活
		Sleep,100
		gosub,Sub_MaxRestore ;最大化
	return

	$q::	;(铅)画笔
		WinGetPos, x, y,lengthA,hightA, A
		CoordWinClick(lengthA/2-40*1,hightA+20)
	return

	$w::	;箭头
		WinGetPos, x, y,lengthA,hightA, A
		CoordWinClick(lengthA/2-40*2,hightA+20)
	return

	; $e::WheelUp		;+/放大

	$r::	;矩形|方框
		WinGetPos, x, y,lengthA,hightA, A
		CoordWinClick(lengthA/2-42.5*5,hightA+20)
	return

	$t::	;文字
	$!t::
		WinGetPos, x, y,lengthA,hightA, A
		CoordWinClick(lengthA/2+45*1,hightA+20)
	return


	$a::	;另存为
		WinGetPos, x, y,lengthA,hightA, A
		CoordWinClick(lengthA/2+39.5*6,hightA+20)
	return

	; $s::SendInput,{Blind]^z ;撤销

	; $d::WheelDown	;-/缩小

	; $f::SendInput,{Blind]^y ;恢复

	$g::	;裁剪
		WinGetPos, x, y,lengthA,hightA, A
		CoordWinClick(lengthA/2+44.5*2,hightA+20)
	return


	$z::	;马赛克
		WinGetPos, x, y,lengthA,hightA, A
		CoordWinClick(lengthA/2+40*0,hightA+20)
	return

	$x::	;马赛克
		WinGetPos, x, y,lengthA,hightA, A
		CoordWinClick(lengthA/2+40*0,hightA+20)
	return

	$c::	;椭圆
		WinGetPos, x, y,lengthA,hightA, A
		CoordWinClick(lengthA/2-42.5*4,hightA+20)
	return

	$v::	;转发
		WinGetPos, x, y,lengthA,hightA, A
		CoordWinClick(lengthA/2+39*5,hightA+20)
	return

	$b::	;表情
		WinGetPos, x, y,lengthA,hightA, A
		CoordWinClick(lengthA/2-42*3,hightA+20)
	return

	~$Enter::
	CapsLock & g::	;SendInput,{Enter}	;打钩
		WinGetPos, x, y,lengthA,hight
		CoordWinClick(lengthA/2+41.5*8,hightA+20)
	return
#If
;****************************************************
;微信中的页面,增加单键模式
#If WinActive("ahk_group 微信中的页面") and (A_CaretX ="") and (A_Cursor != "IBeam")
{
	d::SendInput,{PgDn}
	e::SendInput,{PgUp}
	s::SendInput,{Home}
	f::SendInput,{End}

	j::SendInput,{PgDn}
	k::SendInput,{PgUp}
	h::SendInput,{Home}
	l::SendInput,{End}

	u::SendInput,{End}
	i::SendInput,{Home}

	`;::send,{Esc}
	RButton::
		GV_MouseTimer := 400
		GV_KeyClickAction1 := "Send,{RButton}"
		GV_KeyClickAction2 := "Send,{Escape}"
		GV_MouseButton := 2
		GV_LongClickAction := "Send,{Escape}"
		GoSub,Sub_MouseClick123
	return

	a::SendInput,{End}
	q::SendInput,{Home}

	$!e::	;右键>用默认浏览器打开
		WinGetPos, x, y,lengthA,hightA
		CoordWinClick(lengthA-118,26)
		SendInput,{Down 3}{Enter}
		CoordWinMove(lengthA/2,hightA)
	return
}
#If
;****************************************************
;微信聊天记录
#IfWinActive ahk_class FileManagerWnd ahk_exe WeChat.exe
{
	`;::send,{Esc}
	RButton::
		GV_MouseTimer := 400
		GV_KeyClickAction1 := "Send,{RButton}"
		GV_KeyClickAction2 := "Send,{Escape}"
		GV_MouseButton := 2
		GV_LongClickAction := "Send,{Escape}"
		GoSub,Sub_MouseClick123
	return
}
;****************************************************
;微信中转发的聊天记录
#If WinActive("ahk_class ChatRecordWnd") and (A_Cursor != "IBeam")
$MButton::	;点击发送
$Enter::
	CoordMode, Mouse, Window
	Click, 380 400 1
return

	`;::send,{Esc}
	RButton::
		GV_MouseTimer := 400
		GV_KeyClickAction1 := "Send,{RButton}"
		GV_KeyClickAction2 := "Send,{Escape}"
		GV_MouseButton := 2
		GV_LongClickAction := "Send,{Escape}"
		GoSub,Sub_MouseClick123
	return
#If
;****************************************************
;微信中转发文件窗口
#If WinActive("ahk_class SelectContactWnd") and (A_Cursor != "IBeam")
$MButton::
$Enter::gosub,点击发送A
#If
;****************************************************
点击发送A:
	CoordMode, Mouse, Window
	Click, 380 400 1
return
;****************************************************
;从系统中拖动文件到微信窗口发送
#If WinActive("ahk_class DragAttachWnd") and (A_Cursor != "IBeam")
$MButton::
$Enter::gosub,点击发送B
#If
;****************************************************
点击发送B:
	CoordMode, Mouse, Window
	Click, 100 250 1
return
;****************************************************
;设置标签 ahk_class StandardConfirmDialog
#If WinActive("ahk_class StandardConfirmDialog") and (A_Cursor != "IBeam")
$MButton::
$Enter::
	CoordMode, Mouse, Window
	Click, 235 195 1
return
#If
;****************************************************
;设置备注和标签 ahk_class WeUIDialog
#If WinActive("ahk_class WeUIDialog") and (A_Cursor != "IBeam")
$MButton::
$Enter::
	CoordMode, Mouse, Window
	Click, 125 505 1
return
#If
;****************************************************
;****************************************************
;QQ Tim中查看照片,在非全屏模式下,相对坐标法
#If WinActive("图片查看 ahk_class TXGuiFoundation") and (A_Cursor != "IBeam")
{
	XButton1::SendInput,{Left}
	XButton2::SendInput,{Right}
	$!WheelUp::SendInput,{Left}
	$!WheelDown::SendInput,{Right}

	$h::SendInput,{Left}
	$l::SendInput,{Right}
	$s::SendInput,{Left}
	$f::SendInput,{Right}

	`;::SendInput,!{F4} ;=esc,退出

	Space & WheelUp::SendInput,{Left}
	Space & WheelDown::SendInput,{Right}

	RButton::
		GV_MouseTimer := 400
		GV_KeyClickAction1 := "Send,{RButton}"
		GV_KeyClickAction2 := "Send,{Escape}"
		GoSub,Sub_MouseClick123
	return


	$q::	;(铅)画笔
		WinGetPos, x, y,lengthA,hightA, A
		CoordWinClick(lengthA/2-48.5*6,hightA-35)
	return

	$w::	;文字
		WinGetPos, x, y,lengthA,hightA, A
		CoordWinClick(lengthA/2-46*5,hightA-35)
	return

	$e::WheelUp		;+/放大

	$r::	;旋转
		WinGetPos, x, y,lengthA,hightA, A
		CoordWinClick(lengthA/2+38*4,hightA-35)
	return

	$t::	;翻译
	$!t::
		WinGetPos, x, y,lengthA,hightA, A
		CoordWinClick(lengthA/2-43.5*4,hightA-35)
	return
;****************************************************
	$a::	;另存为
		WinGetPos, x, y,lengthA,hightA, A
		CoordWinClick(lengthA/2+41.5*7,hightA-35)
	return

	; $s::SendInput,{Left}

	$d::WheelDown	;-/缩小

	; $f::SendInput,{Right}

	$g::	;1:1
		WinGetPos, x, y,lengthA,hightA, A
		CoordWinClick(lengthA/2+45*2,hightA-35)
	return
;****************************************************
	$z::	;定位聊天
		SendInput,{Click,Right}
		Sleep,100
		SendInput,{Up 2}{Enter}
	return

	$x::	;裁剪
		WinGetPos, x, y,lengthA,hightA, A
		CoordWinClick(lengthA/2+41.5*6,hightA-35)
	return

	$c::	;右键>复制
		SendInput,{Click,Right}
	Sleep,100
	SendInput,{Down 1}{Enter}
	return

	$v::	;右键>转发
		SendInput,{Click,Right}
		Sleep,100
		SendInput,{Down 2}{Enter}
	return

	$b::	;编辑
		WinGetPos, x, y,lengthA,hightA, A
		CoordWinClick(lengthA/2+40*5,hightA-35)
	return

	$MButton::gosub,Sub_MaxRestore ;最大化
	CapsLock & g::gosub,Sub_MaxRestore ;最大化
;****************************************************
;QQ Tim中查看照片编辑器,在非全屏模式下,相对坐标法,启用空格模式
	$^1::
	CapsLock & 1::
	Space & a::	;矩形/椭圆
		WinGetPos, x, y,lengthA,hightA, A
		CoordWinClick(lengthA/2-48.5*6,hightA-35)
	return

	$^2::
	CapsLock & 2::
	Space & s::	;椭圆
		WinGetPos, x, y,lengthA,hightA, A
		CoordWinClick(lengthA/2-48.5*5,hightA-35)
	return

	$^3::
	CapsLock & 3::
	Space & d:: ;标箭头
		WinGetPos, x, y,lengthA,hightA, A
		CoordWinClick(lengthA/2-48.5*4,hightA-35)
	return

	$^4::
	CapsLock & 4::
	Space & f::	;铅画笔|记号笔
		WinGetPos, x, y,lengthA,hightA, A
		CoordWinClick(lengthA/2-48.5*3,hightA-35)
	return

	$^5::
	CapsLock & 5::
	Space & g::	;马赛克
		WinGetPos, x, y,lengthA,hightA, A
		CoordWinClick(lengthA/2-50*2,hightA-35)
	return
;****************************************************
	$^9::
	CapsLock & 9::
	Space & 2::
	Space & w::
	CapsLock & w::SendInput,^s ;保存 ctrl+S

	~$Enter::
	Space & e::	;完成
		WinGetPos, x, y,lengthA,hightA, A
		CoordWinClick(lengthA/2+41.5*7,hightA-35)
	return

	Space & r::	;重做 ctrl+Y

	$^6::#
	CapsLock & 6::
	Space & 5::
	Space & t:: ;文字
		WinGetPos, x, y,lengthA,hightA, A
		CoordWinClick(lengthA/2-55*1,hightA-35)
	return

	$^7::
	CapsLock & 7::
	Space & 4::
	Space & b:: ;标序号
		WinGetPos, x, y,lengthA,hightA, A
		CoordWinClick(lengthA/2-10,hightA-35)
	return

	$^8::
	CapsLock & 8::
	Space & 3::
	Space & z::SendInput,^z	;撤销 ctrl+z
	; CapsLock & z::SendInput,^z ;撤销,Capstg默认

	$^0::
	CapsLock & 0::
	Space & 1::
	Space & v::	;发送到手机
		WinGetPos, x, y,lengthA,hightA, A
		CoordWinClick(lengthA/2+48*3,hightA-35)
	return
;****************************************************
	Space & q::	;编辑|X退出编辑
	Space & x::	;编辑|X退出编辑
		WinGetPos, x, y,lengthA,hightA, A
		CoordWinClick(lengthA/2+49.7*4,hightA-35)
	return

	Space & c::SendInput,{Blind}^c ;复制 ctrl+c
	$!c::SendInput,^+c ;复制纯文本ctrl+shift+c

	Space & y::	;重做 ctrl+Y
	$!+z::ControlSend,,^y ;重做,屏蔽火绒弹窗拦截冲突
}
#If
;****************************************************
;****************************************************
;F4menu中
#If WinActive("F4Menu ahk_class F4Menu")
{
	$Space::SendInput,{Enter}
}
#If
;****************************************************
;****************************************************
;telegram {{{2
#If WinActive("ahk_exe Telegram.exe")
{
	!/::CoordWinClick(150,52)
	!1::CoordWinClick(TG_Start_X, TG_Start_Y+(1-1)*TG_Bar_Height)
	!2::CoordWinClick(TG_Start_X, TG_Start_Y+(2-1)*TG_Bar_Height)
	!3::CoordWinClick(TG_Start_X, TG_Start_Y+(3-1)*TG_Bar_Height)
	!4::CoordWinClick(TG_Start_X, TG_Start_Y+(4-1)*TG_Bar_Height)
	!5::CoordWinClick(TG_Start_X, TG_Start_Y+(5-1)*TG_Bar_Height)
	!6::CoordWinClick(TG_Start_X, TG_Start_Y+(6-1)*TG_Bar_Height)
	!7::CoordWinClick(TG_Start_X, TG_Start_Y+(7-1)*TG_Bar_Height)
	!8::CoordWinClick(TG_Start_X, TG_Start_Y+(8-1)*TG_Bar_Height)
	!9::CoordWinClick(TG_Start_X, TG_Start_Y+(9-1)*TG_Bar_Height)
	!0::CoordWinClick(TG_Start_X, TG_Start_Y+(10-1)*TG_Bar_Height)

	$RButton::
		;GV_MouseTimer := 300
		GV_KeyClickAction1 := "SendInput,{Click,Right}"	;系统默认,RButton
		GV_KeyClickAction2 := "SendInput,!{F4}"	;{Escape}
		GV_LongClickAction := "SendInput,{Click,Right}"	;系统默认,RButton
		gosub,Sub_MouseClick123
	return
}
#If
;****************************************************
;****************************************************
;IDM的下载对话框中，提取url链接，然后用MPC播放 {{{2
#If WinActive("下载文件信息 ahk_class #32770 ahk_exe IDMan.exe")
;单击右键,双击右键下载 , 右键长按Idm2Mpv(Mpv播放)
$RButton::
{
	;GV_MouseTimer := 300
	GV_KeyClickAction1 := "RButton"	;单击,系统默认,RButton
	GV_KeyClickAction2 := "SendInput,!ss"	;双击,点击下载
	GV_MouseButton := 2
	GV_LongClickAction := "gosub,Idm2Mpv"	;长按,用MPV播放
	gosub,Sub_MouseClick123
return
}

$!v::gosub,Idm2Mpv	;用MPV播放

Idm2Mpv:
{
	ControlGetText,Out,Edit1
	WinClose A
	;Run,%COMMANDER_PATH%\Tools\MPC\mpc.exe "%Out%"
	Run,%COMMANDER_PATH%\Plugins\WLX\vlister\mpv.exe "%Out%"
return
}

$!w::	;点{Enter}/保存->Listary跳转目标路径->开始下载
CapsLock & w::	;ahk_group GroupDiagOpenAndSave,已经定义,20231229
{
	if (GV_KeyClick_Continuous(GV_KeyTimer)){
	SendInput,+{Enter} ;IDM直接下载保存
	}
	else {
		CoordMode, Mouse, Window
		Click, 453,100
		Sleep,500
		SendInput,^g			;QuickJump或Listary跳转转目标路径
		Sleep,100
		; ControlClick,Button1	;单击保存,开始下载
	}
}
return

;点{Enter}/保存>自动复制文件名>搜索本地目录>跳转>粘贴文件名+补扩展名
$!f::	;ahk_group GroupDiagOpenAndSave,已经定义,20231229
$!d::
$MButton::	;2023/12/15 00:40
{
	WinActivate,下载文件信息 ahk_class #32770 ahk_exe IDMan.exe
	;1.提取并处理文件名和扩展名
	ControlGetText,Out,Edit4,下载文件信息 ahk_class #32770 ;全文件名
	; 使用正则表达式提取文件名和扩展名
	; Longfilename := RegExReplace(Out, ".*\\([^\\]*\\)\\..*", "$1")
	Longfilename :=GetFileInfo(Out,2)	;获取文件全名,带后缀
	Filename :=GetFileInfo(Out,3)	;只获取文件名,20240412
	Extension := GetFileInfo(Longfilename,4)	;只获取扩展名,20240412
	; EzTip(Extension,1)
	; 使用正则表达式去掉路径部分,去掉路径部分扩展名
	; ShortFilename := RegExReplace(Longfilename, "^.*\\", "")
	ShortFilename := RegExReplace(Longfilename, "\[www.ghxi.com\]", "")
	ClipBoard = %ShortFilename%	;带后缀,20240412
	EzTip(ShortFilename,1)
	; ListaryQuery := RegExReplace(ShortFilename, "\.([^.]*)$", "") ;去掉扩展名
	; 使用正则表达式将文件名中的所有数字、减号、下划线替换为空
	gosub,QueryReplace	;by Tuutg,20240412,带后缀

	;2.点击...(浏览)->Listary跳转目标路径->粘贴文件名
	CoordMode, Mouse, Window
	Click, 453,100	;点击...(浏览)
	Sleep,100
	; SendInput,^a	;全选(Quicker方法)
	; SendInput,#!s	;Listary复制粘贴文件名(Quicker设定)
	SendInput,^g	;QuickJump或Listary跳转转目标路径
	Sleep,1000	;延时等待,另存为窗口打开
	; SendInput,{Blind}^v	;粘贴文件名
	SendInput,{Text}%Clipboard%	;粘贴文件名,带后缀

	;3.激活Listary搜索框->输入搜索词->定位到下载目录
	gosub,Listary	;按2次Ctrl,激活Listary搜索框
	Sleep,100
	SendInput,{Text}%ListaryQuery% %Extension%	;20240412,输入搜索词,带后缀
	; SendInput,{Text}%ListaryQuery%		;带后缀,缩小搜索范围,20240516
	Sleep,1000	;延时等待,搜索词匹配目标对象
	; SendInput,{Right}	;适配Listary5
	; Sleep,1000 ;延时等待,搜索词匹配目标对象
	SendInput,{Blind}^{Enter}	;适用Listary5.0|6.0版本,定位到下载目录
	Sleep,1000

	;4.粘贴文件名->鼠标移动到保存按钮->开始下载
	WinActivate,另存为 ahk_class #32770 ahk_exe IDMan.exe
	SetControlDelay -1
	ControlFocus,Edit1,另存为 ahk_class #32770 ahk_exe IDMan.exe
	ControlSend,Edit1,{Del},另存为 ahk_class #32770 ahk_exe IDMan.exe
	Sleep,1000	;延时等待,清空不正确的文件名
	; SendInput,{Blind}^v	;粘贴文件名
	SendInput,{Text}%Clipboard%	;粘贴文件名,带后缀
	Sleep,100
	WinGetPos,x,y,lengthA,hightA,另存为 ahk_class #32770 ahk_exe IDMan.exe
	Sleep,100
	CoordWinMove(lengthA-170,hightA-35) ;保存按钮
}
return

;20240324
Space & c::SendInput,^{Home}^+{End}^c
; CapsLock & c::SendInput,^{Home}^+{End}^c

Space & v::SendInput,^{Home}^+{End}^v{Enter}
; CapsLock & v::SendInput,^{Home}^+{End}^v

; $Space::SendInput,{Blind}{Space}	;恢复默认打字功能
$Space::Space ;恢复空格打字上屏,by Tuutg,20240425
#If

;****************************************************
; 使用正则表达式将文件名中的所有数字、减号、下划线替换为空
QueryReplace:
	ListaryQuery := RegExReplace(Filename, "[0-9]", " ")	 ;无后缀,扩大搜索范围,20240412
	; ListaryQuery := RegExReplace(ShortFilename, "[0-9]", "") ;带后缀,缩小搜索范围,20240516
	ListaryQuery := RegExReplace(ListaryQuery, "[v,V]", "")
	ListaryQuery := RegExReplace(ListaryQuery, "_[v,V]", "")
	ListaryQuery := RegExReplace(ListaryQuery, "-[v,V]", "")
	ListaryQuery := RegExReplace(ListaryQuery, "_[m,M]od", "")
	ListaryQuery := RegExReplace(ListaryQuery, "-[m,M]od", "")
	ListaryQuery := RegExReplace(ListaryQuery, "[s,S]etup", "")
	ListaryQuery := RegExReplace(ListaryQuery, "[b,B]it", "")
	ListaryQuery := RegExReplace(ListaryQuery, "[p,P]ortable", "")
	ListaryQuery := RegExReplace(ListaryQuery, "[g,G]reen", "")
	ListaryQuery := RegExReplace(ListaryQuery, "[s,S]ingle", "")
	ListaryQuery := RegExReplace(ListaryQuery, "[m,M]aster", "")
	ListaryQuery := RegExReplace(ListaryQuery, "[m,M]ain", "")
	ListaryQuery := RegExReplace(ListaryQuery, "[\-\.\_\(\)]", " ")
	; EzTip(ListaryQuery,1)
	Sleep,100
return
;****************************************************

;IDM的下载完成对话框中，提取文件信息，然后跳转到tc {{{2
#If WinActive("下载完成 ahk_class #32770 ahk_exe IDMan.exe")
{
$!w::
$!f::
$MButton::
$RButton::
	ControlGetText,Out,Edit4
	WinClose A
	Run,"%COMMANDER_EXE%" /A /T /O /R="%Out%"
	Sleep 500
	MyWinWaitActive("ahk_class TTOTAL_CMD")
return
}
#If

;IDM主界面,双击中键,打开目录.提前是设置双击打开属性 {{{2
#If WinActive("Internet Download Manager ahk_class #32770 ahk_exe IDMan.exe")
{
$!f:: ; 修改成自己的下载目录
	Run,"%COMMANDER_EXE%" /A /T /O /R="G:\TDownload\360Download\IDM\"
	Sleep 500
	MyWinWaitActive("ahk_class TTOTAL_CMD")
return

$!w::
MButton::
	SendInput,{Click 2}	;左键双击,打开
return

$F2::
$!r::SendInput,^m	;20240408
	; SendInput,{Click,Right}
	; Sleep,100
	; SendInput,{Down 4}{Enter}
; return
}
#If

;IDM主界面>中键>文件属性对话框中，提取文件信息，然后跳转到tc
#If WinActive("文件属性 ahk_class #32770 ahk_exe IDMan.exe")
{
$!w::
$!f::
MButton::
	ControlGetText,Out,Edit1
	WinClose A
	Run,"%COMMANDER_EXE%" /A /T /O /R="%Out%"
	Sleep 500
	MyWinWaitActive("ahk_class TTOTAL_CMD")
return
}
#If

;IDM文件属性对话框中,全选=全选+复制
#If WinActive("ahk_group Group_ableCtrlAC")
$!a::
$^a::
	SendInput,{Home}+{End}	;全选
	; SendInput,^a	;全选
	Sleep 100
	SendInput,^c	;复制
	Sleep 300
return

;20240324
Space & c::SendInput,^{Home}^+{End}^c
; CapsLock & c::SendInput,^{Home}^+{End}^c

Space & v::SendInput,^{Home}^+{End}^v{Enter}
; CapsLock & v::SendInput,^{Home}^+{End}^v

; $Space::SendInput,{Blind}{Space}	;恢复默认打字功能
$Space::Space ;恢复空格打字上屏,by Tuutg,20240425
#If
;****************************************************
;****************************************************
;360X浏览器的下载对话框中，提取url链接，文件名 {{{2
#If WinActive("下载任务 ahk_class Chrome_WidgetWin_2")
$RButton::
	;GV_MouseTimer := 300
	GV_KeyClickAction1 := "SendInput,RButton"	;系统默认,RButton
	GV_KeyClickAction2 := "SendInput,{Click,390, 250}"	;点击下载
	GV_MouseButton := 2
	GV_LongClickAction := "SendInput,{Click,390, 250}"	;点击下载
	gosub,Sub_MouseClick123
return

;点{Enter}/保存>自动复制文件名>搜索本地目录>跳转>粘贴文件名+补扩展名
$!f::	;ahk_group GroupDiagOpenAndSave,已经定义,,20231229
$!d::
$MButton::	;2023/12/15 00:40
{
	WinActivate,建下载任务 ahk_class Chrome_WidgetWin_2
	SendInput,^a^c	;获取文件全名,带后缀,20240430
	;1.提取并处理文件名
	Longfilename :=GetFileInfo(ClipBoard,2)	;获取文件全名,带后缀
	Filename :=GetFileInfo(ClipBoard,3)	;只获取文件名,20240412
	Extension := GetFileInfo(Longfilename,4) ;只获取扩展名,20240412
	; EzTip(Extension,1)
	; 使用正则表达式去掉路径部分,去掉路径部分扩展名
	; ShortFilename := RegExReplace(Longfilename, "^.*\\", "")
	ShortFilename := RegExReplace(Longfilename, "\[www.ghxi.com\]", "")
	ClipBoard = %ShortFilename%	;带后缀,20240412
	EzTip(ShortFilename,1)
	; ListaryQuery := RegExReplace(ShortFilename, "\.([^.]*)$", "") ;去掉扩展名
	; 使用正则表达式将文件名中的所有数字、减号、下划线替换为空
	gosub,QueryReplace	;by Tuutg,20240412,去掉带后缀

	;2.点击...(浏览)->Listary跳转目标路径->粘贴文件名
	CoordMode, Mouse, Window
	Click, 475,170	;点击...(浏览)
	Sleep,100
	; SendInput,^a	;全选(Quicker方法)
	; SendInput,#!s	;Listary复制粘贴文件名(Quicker设定)
	SendInput,^g	;QuickJump或Listary跳转转目标路径
	Sleep,1000	;延时等待,另存为窗口打开
	; SendInput,{Blind}^v	;粘贴文件名
	SendInput,{Text}%Clipboard%	;粘贴文件名,带后缀

	;3.激活Listary搜索框->输入搜索词->定位到下载目录
	gosub,Listary	;按2次Ctrl,激活Listary搜索框
	Sleep,100
	SendInput,{Text}%ListaryQuery% %Extension%	;20240412,输入搜索词,带后缀
	; SendInput,{Text}%ListaryQuery%		;带后缀,缩小搜索范围,20240516
	Sleep,1000	;延时等待,搜索词匹配目标对象
	; SendInput,{Right}	;适配Listary5
	; Sleep,1000 ;延时等待,搜索词匹配目标对象
	SendInput,{Blind}^{Enter}	;适用Listary5.0|6.0版本,定位到下载目录
	Sleep,1000

	;4.粘贴文件名->鼠标移动到保存按钮->开始下载
	WinActivate,下载 ahk_class #32770
	SetControlDelay -1
	ControlFocus,Edit1,下载 ahk_class #32770
	; ControlSend,Edit1,{Del},下载 ahk_class #32770
	; Sleep,1000	;延时等待,清空不正确的文件名
	; SendInput,{Blind}^v	;粘贴文件名
	; SendInput,{Text}%Clipboard%	;粘贴文件名
	Sleep,100
	WinGetPos,x,y,lengthA,hightA,下载 ahk_class #32770
	Sleep,100
	CoordWinMove(lengthA-170,hightA-35) ;保存按钮
}
return

;20240324
Space & c::SendInput,^{Home}^+{End}^c
; CapsLock & c::SendInput,^{Home}^+{End}^c

Space & v::SendInput,^{Home}^+{End}^v{Enter}
; CapsLock & v::SendInput,^{Home}^+{End}^v

; $Space::SendInput,{Blind}{Space}	;恢复默认打字功能
$Space::Space ;恢复空格打字上屏,by Tuutg,20240425
#If

;360X浏览器的下载完成对话框中,点击第一个(浏览)->下载目标,20240430
#If WinActive("下载 ahk_class Chrome_WidgetWin_2")
$MButton::
	SetTitleMatchMode,3
	MouseGetPos,x,y	;获取鼠标当前坐标,利用Y进行相对盲点
	CoordMode, Mouse, Window
	Click, 612, %y%	;盲点...(浏览|打开目录)
return
#If
;****************************************************
;****************************************************
;TC设定开始
TC_Focus_Edit(){
	if (A_Cursor = "IBeam" ){
		GV_Edit_Mode := 1
	} else if(A_Cursor = "Arrow" ) {
		GV_Edit_Mode := 0
	}
	ControlGetFocus theFocus, A
	return (InStr(theFocus , "Edit") or (GV_Edit_Mode = 1))
}
;****************************************************

fun_TCselectFileByNum(n){
	if TC_Focus_Edit(){
		SendInput,%n%
	}
	else {
		;如果是0就获取总共几个
		if(n=0){
			;1000 to get active panel: 1=left, 2=right (32/64)
			tcLeftRight := fun_TcGet(1000)
			sMsg := % 1000 + tcLeftRight
			;1001/1002 to get number of items in left/right list (32/64)
			n := fun_TcGet(sMsg) - 1
		}
	ControlGetFocus, Ctrl, AHK_CLASS TTOTAL_CMD
	PostMessage, 0x19E, %n%, 1, %Ctrl%, AHK_CLASS TTOTAL_CMD
	SendInput,{Enter}
	}
}
;****************************************************
;判断当前tc中是否正在显示着收藏夹菜单
fun_TcExistHotMenu(){
	Dlg_HWnd := WinExist("ahk_class #32768 ahk_exe TOTALCMD.EXE")
	return Dlg_HWnd
}
;****************************************************
fun_TcGet(n)
{
	SendMessage,1074, %n%, 0, , Ahk_class TTOTAL_CMD
	return % ErrorLevel
}
;****************************************************
ClickAndLongClick(timeout = 200) { ;
	tout := timeout/1000
	key := RegExReplace(A_ThisHotkey,"[\*\~\$\#\+\!\^]")
	loop {
		t := A_TickCount
		KeyWait %key%
		Pattern .= A_TickCount-t > timeout
		KeyWait %key%,DT%tout%
	if (ErrorLevel)
		return Pattern
	}
}
;0单击
;00双击
;01就短按+长按
;001就是双击+长按
;****************************************************

Sub_TcSrcActivateLastTab:
	TcSendPos(5001)
	TcSendPos(3006)
return
;****************************************************
;Totalcmd备注中快速星号评级 {{{2
#If WinActive("文件备注 ahk_class TCmtEditForm ahk_exe totalcmd.exe")
{
	!0::
	!1::
	!2::
	!3::
	!4::
	!5::
		ControlFocus, TMyMemo1
		ControlGetText, preText, TMyMemo1
		cnt := SubStr(A_ThisHotkey,2)
		text := StrRepeat("★", cnt) . StrRepeat("☆", 5-cnt)
		if StrLen(preText) >0
		text .= "`r`n" preText
		ControlSetText, TMyMemo1, %text%
	return
}
#If

StrRepeat(str, count){
	res =
	loop, % count{
	res .= str
	}
	return res
}
;****************************************************
;totalcmd备注中快速星号评级 {{{2
#If WinActive("文件备注批量操作 ahk_class AutoIt v3 GUI ahk_exe tc_multi_comment_edit.exe")
{
!0::
!1::
!2::
!3::
!4::
!5::
	ControlFocus, Edit1
	ControlGetText, preText, Edit1
	cnt := SubStr(A_ThisHotkey,2)
	text := StrRepeat("★", cnt) . StrRepeat("☆", 5-cnt)
	if StrLen(preText) >0
		text .=  "`r`n" preText
	ControlSetText,  Edit1, %text%
	return
}
#If

;Totalcmd中快搜 {{{2
EndKey(key){
	StringRight, k, key, 1
	StringLower,k,k
	return % k
}

#If WinActive("QUICKSEARCH ahk_class TQUICKSEARCH")
{
	;大部分快捷键是ctrl引导,快搜模式下按下按快捷键的同时会先esc
	~LControl::
		Input, OneKey, T0.5 L1, cxvabdefghijklmnopqrstuwyz
		Switch ErrorLevel
		{
			Case "Timeout":
				return
			Case "EndKey:C","EndKey:X","EndKey:V":
				k := EndKey(ErrorLevel)
				Sendinput,^%k%
			Case "EndKey:A","EndKey:Y","EndKey:K","EndKey:E","EndKey:R","EndKey:T":
				k := EndKey(ErrorLevel)
				Sendinput,{Esc}
				sleep,200
				Sendinput,^%k%
			Default:
		}
	return

	;快搜输入框中不能直接数字，就用空格加数字，也可以先按caps取消输入框状态再单独按数字
	;眼睛适合辨认的也就前5
	Space & 1::
		SendInput,{Esc}
		Sleep,100
		fun_TCselectFileByNum(1)
	return

	Space & 2::
		SendInput,{Esc}
		Sleep,100
		fun_TCselectFileByNum(2)
	return

	Space & 3::
		SendInput,{Esc}
		Sleep,100
		fun_TCselectFileByNum(3)
	return

	Space & 4::
		SendInput,{Esc}
		Sleep,100
		fun_TCselectFileByNum(4)
	return

	Space & 5::
		SendInput,{Esc}
		Sleep,100
		fun_TCselectFileByNum(5)
	return

	Space & 6::
		SendInput,{Esc}
		Sleep,100
		fun_TCselectFileByNum(6)
	return

	Space & 7::
		SendInput,{Esc}
		Sleep,100
		fun_TCselectFileByNum(7)
	return

	Space & 8::
		SendInput,{Esc}
		Sleep,100
		fun_TCselectFileByNum(8)
	return

	Space & 9::
		SendInput,{Esc}
		Sleep,100
		fun_TCselectFileByNum(9)
	return

	Space & 0::
		SendInput,{Esc}
		Sleep,100
		fun_TCselectFileByNum(0)
	return
;****************************************************
	;左手alt+e作为F4
	$!e::
		BlockInput On
		SendInput,{Esc}
		Sleep,100
		SendInput,{Alt Up}
		Sleep,100
		SendInput,{F4}
		BlockInput Off
	return

	;左手alt+s作为F3
	!s::
		BlockInput On
		Sendinput,{Esc}
		Sleep,100
		Sendinput,{Alt up}
		Sleep,300
		Sendinput,{F3}
		BlockInput Off
	return

	;避免聚焦在输入框中而无法生效
	`;::
		SendInput,{Esc}
		Sleep,100
		SendInput,{F4}
	return

	CapsLock & Enter::return
	;CapsLock & Space::return

	; $Space::SendInput,{Blind}{Space}	;恢复默认打字功能
	$Space::Space ;恢复空格打字上屏,by Tuutg,20240425
	$^Space::^Space
	$+Space::+Space

	$+s::SendInput,^s ;;激活过滤,by Tuutg,20240419
	CapsLock & w::SendInput,^s ;激活过滤,by Tuutg,20240419
}
#If

;totalcmd中收藏夹菜单 {{{2
#If WinActive("ahk_class #32768 ahk_exe TOTALCMD.EXE")
{
	1::Sendinput,1
	2::Sendinput,2
	3::Sendinput,3
	4::Sendinput,4
	5::Sendinput,5
	6::Sendinput,6
	7::Sendinput,7
	8::Sendinput,8
	9::Sendinput,9
	0::Sendinput,0
}
#If

;****************************************************

;Eztc totalcmd中快捷键,20231122 {{{2
#If WinActive("Total Commander ahk_class TTOTAL_CMD") 	;TC主窗口

	$^,::TcSendUserCommand("cm_SelectAll")	;全选,同Ctrl+A
	Space & ,::TcSendUserCommand("cm_RestoreSelection") ;恢复选择
	; CapsLock & b::TcSendUserCommand("cm_ClearAll")  ;取消选择
	CapsLock & b::gosub,Sub_KeyCapsLockB	;复制无后缀文件名,20240401
	; Tab & b::TcSendUserCommand("em_BeyondCompare4")	;文件比较,20240401

	$F1::TcSendUserCommand("cm_MultiRenameFiles")   ;批量重命名,同Ctrl+M
	$F2::TcSendUserCommand("cm_RenameSingleFile")	;同F2,重命名
	$F3::ControlSend,,{F3}			;查看,cm_List,屏蔽截图等
	$F4::ControlSend,,{F4}			;F4,cm_Edit,屏蔽讯飞语音
	$F5::ControlSend,,{F5}			;F5,cm_Copy,屏蔽讯飞语音
	$F6::ControlSend,,{F6}			;F6,cm_RenMov,屏蔽讯飞语音
	$F7::ControlSend,,{F7}			;F7,cm_MkDir,屏蔽讯飞语音
	$F8::ControlSend,,{F8}			;F8,cm_Delete,屏蔽讯飞语音
	$F9::ControlSend,,{F9}			;F9,MPV,屏蔽讯飞语音
	$F10::ControlSend,,{F10}			;F10,ACDSee,屏蔽讯飞语音

	Escape & f4::ControlSend,,!{F3}		;同F4,cm_Edit
	CapsLock & Enter::Send,^{Enter}		;同Ctrl+Enter,用Nircmd调用外部定义方式打开
	;CapsLock & Space:: SendInput,{Space}		;改用ListaryPro搜索名字

	$!q::TcSendUserCommand("cm_SrcQuickview")	;查看模式
	; $!q::ControlSend,,!q	;查看模式,用ControlSend,屏蔽anytext
	; $!w					;Win资管->TC|智能跳转
	; $!e::TcSendUserCommand("cm_Edit")		;同F4,cm_Edit
	$!r::TcSendUserCommand("cm_RenameSingleFile")	;同F2,重命名
	$^r::TcSendUserCommand("cm_RereadSource")	;刷新
	$!t::TcSendUserCommand("cm_SwitchSeparateTree")	;树状模式
	$!y::TcSendUserCommand("em_TcFS2F6WithEnter")	;F6|移动,20240518

	$!a::TcSendUserCommand("cm_SelectAll")	;全择,同Ctrl+A
	$!s::TcSendUserCommand("em_StartExplorer") 	;TC->Win资管
	$!d::TcSendUserCommand("cm_EditPath") 	;聚焦地址栏
	$!f::TcSendUserCommand("em_EverythingByFilename")	;Everyting

	CapsLock & g::
		if (GV_KeyClick_Continuous(GV_KeyTimer)){	;树状视图
			TcSendUserCommand("cm_SrcTree") ;树状视图
		}
		else {	;单击{Enter},确定
			; ControlSend,,{Enter}
			SendInput,{Enter}	;20240606
		}
	return

	$!g::  ;新建文件(夹)|树状视图,屏蔽Ra插件管理|goole翻译
		if (GV_KeyClick_Continuous(GV_KeyTimer)){	;^N新建文件(夹)
			TcSendUserCommand("em_NewFiles")
			WinActivate,NewFiles ahk_class TfomMain
			WinShow,NewFiles ahk_class TfomMain
		}
		else {	;Alt+G,聚焦驱动器栏|和Alt+D,聚焦地址栏是一对
			TcSendUserCommand("cm_SrcPathFocus")
			; Sleep,100
			; SendInput,{Down}	;效果明显,但导致双击无效,注释了
		}
	return

	CapsLock & s::	;20240322,后退目录|F6,cm_RenMov,无弹窗|快速移动
	if (GV_KeyClick_Continuous(GV_KeyTimer)){	;F6,cm_RenMov
		SendInput,{Left}	;后退目录
	}
	else {
		if (A_Cursor ="IBeam") or (A_CaretX !=""){
			SendInput,{Left}	;光标左移,20240328
		}
		else {
			TcSendUserCommand("cm_RenMov") ;TC-F6,移动到对侧
			Sleep,100
			SendInput,{Enter}
		}
	}
	return

	CapsLock & f::	;20240322,进入目录|F5,cm_Copy,无弹窗|快速复制
	if (GV_KeyClick_Continuous(GV_KeyTimer)){
		SendInput,{Right}	;进入目录
	}
	else {
		if (A_Cursor ="IBeam") or (A_CaretX !=""){
			SendInput,{Right}	;光标右移,20240328
		}
		else {
			TcSendUserCommand("cm_Copy") ;TC-F5,复制到对侧
			Sleep,100
			SendInput,{Enter}
		}
	}
	return

	$!+z::ControlSend,,!+z		;桌面文件夹,屏蔽火绒弹窗拦截冲突
	$+!^z::TcSendUserCommand("cm_OpenDesktop") 	;桌面文件夹
	$!+x::TcSendUserCommand("em_ToFUpx") ;用FUpx压缩exe等文件
	$!^x::TcSendUserCommand("em_ToUpx") ;用Upx压缩exe等文件
	$!z::TcSendUserCommand("cm_SrcComments")	;备注模式
	$!x::TcSendUserCommand("cm_SrcLong")		;详细信息模式
	$!c::TcSendUserCommand("cm_SrcThumbs")	;缩略图模式
	; $+!c::ControlSend,,^+c			;复制完整路径
	$#!c::ControlSend,,^+n			;复制文件名
	$^`::TcSendUserCommand("cm_DirectoryHotlist")	;常用文件夹
	$!v::TcSendUserCommand("cm_DirectoryHotlist")	;常用文件夹
	$!b::TcSendUserCommand("cm_DirBranch")	;展开所有文件夹

	;tc中使用数字按键来进行快速打开和快速切换标签
	;开启之后如需继续使用快搜，那快搜起始键不能是数字
	;需要打开或关闭直接修改下行的注释即可

	^!+d::
		GV_TotalcmdToggleJumpByNumber := !GV_TotalcmdToggleJumpByNumber
		if(GV_TotalcmdToggleJumpByNumber == 1)
			ToolTip 已启用TC中数字键跳转功能
		else
			ToolTip 已关闭TC中数字键跳转功能
		Sleep 2000
		ToolTip
	return

	1::
		if !GV_TotalcmdToggleJumpByNumber or fun_TcExistHotMenu() or TC_Focus_Edit()
			SendInput,1
		else {
			p := ClickAndLongClick()
			if (p = "0") {
				;单击
				fun_TCselectFileByNum(1)
			} else if (p = "00") {
				;双击
				TcSendPos(5001)
			} else if (p = "1") {
				;长按
				TcSendPos(5001)
				TcSendPos(2001)
			}
		}
	return

	2::
		if !GV_TotalcmdToggleJumpByNumber or fun_TcExistHotMenu() or TC_Focus_Edit()
			SendInput,2
		else {
			p := ClickAndLongClick()
			if (p = "0"){
				;单击
				fun_TCselectFileByNum(2)
			}else if (p = "00"){
				;双击
				TcSendPos(5002)
			}else if (p = "1"){
				;长按
				TcSendPos(5002)
				TcSendPos(2001)
			}
		}
	return

	3::
		if !GV_TotalcmdToggleJumpByNumber or fun_TcExistHotMenu() or TC_Focus_Edit()
			SendInput,3
		else {
			p := ClickAndLongClick()
			if (p = "0") {
				;单击
				fun_TCselectFileByNum(3)
			} else if (p = "00") {
				;双击
				TcSendPos(5003)
			} else if (p = "1") {
				;长按
				TcSendPos(5003)
				TcSendPos(2001)
			}
		}
	return

	4::
		if !GV_TotalcmdToggleJumpByNumber or fun_TcExistHotMenu() or TC_Focus_Edit()
			SendInput,4
		else {
			p := ClickAndLongClick()
			if (p = "0") {
				;单击
				fun_TCselectFileByNum(4)
			} else if (p = "00") {
				;双击
				TcSendPos(5004)
			} else if (p = "1") {
				;长按
				TcSendPos(5004)
				TcSendPos(2001)
			}
		}
	return

	5::
		if !GV_TotalcmdToggleJumpByNumber or fun_TcExistHotMenu() or TC_Focus_Edit()
			SendInput,5
		else {
			p := ClickAndLongClick()
			if (p = "0") {
				;单击
				fun_TCselectFileByNum(5)
			} else if (p = "00") {
				;双击
				TcSendPos(5005)
			} else if (p = "1") {
				;长按
				TcSendPos(5005)
				TcSendPos(2001)
			}
		}
	return

	6::
		if !GV_TotalcmdToggleJumpByNumber or fun_TcExistHotMenu() or TC_Focus_Edit()
			SendInput,6
		else {
			p := ClickAndLongClick()
			if (p = "0") {
				;单击
				fun_TCselectFileByNum(6)
			} else if (p = "00") {
				;双击
				TcSendPos(5006)
			} else if (p = "1") {
				;长按
				TcSendPos(5006)
				TcSendPos(2001)
			}
		}
	return

	7::
		if !GV_TotalcmdToggleJumpByNumber or fun_TcExistHotMenu() or TC_Focus_Edit()
			SendInput,7
		else {
			p := ClickAndLongClick()
			if (p = "0") {
				;单击
				fun_TCselectFileByNum(7)
			} else if (p = "00") {
				;双击
				TcSendPos(5007)
			} else if (p = "1") {
				;长按
				TcSendPos(5007)
				TcSendPos(2001)
			}
		}
	return

	8::
		if !GV_TotalcmdToggleJumpByNumber or fun_TcExistHotMenu() or TC_Focus_Edit()
			SendInput,8
		else {
			p := ClickAndLongClick()
			if (p = "0") {
				;单击
				fun_TCselectFileByNum(8)
			} else if (p = "00") {
				;双击
				TcSendPos(5008)
			} else if (p = "1") {
				;长按
				TcSendPos(5008)
				TcSendPos(2001)
			}
		}
	return

	9::
		if !GV_TotalcmdToggleJumpByNumber or fun_TcExistHotMenu() or TC_Focus_Edit()
			SendInput,9
		else {
			p := ClickAndLongClick()
			if (p = "0") {
				;单击
				fun_TCselectFileByNum(9)
			} else if (p = "00") {
				;双击
				TcSendPos(5009)
			} else if (p = "1") {
				;长按
				TcSendPos(5009)
				TcSendPos(2001)
			}
		}
	return

	0::
		if !GV_TotalcmdToggleJumpByNumber or fun_TcExistHotMenu() or TC_Focus_Edit()
			SendInput,0
		else {
			p := ClickAndLongClick()
			if (p = "0") {
				;单击
				fun_TCselectFileByNum(0)
			} else if (p = "00") {
				;双击
				TcSendPos(5001)
				TcSendPos(3006)
			} else if (p = "1") {
				;长按
				TcSendPos(5001)
				TcSendPos(3006)
				TcSendPos(2001)
			}
		}
	return


	;避免中文输入法的问题,取消选择
	,::
		ControlGetFocus, TC_CurrentControl, A
		;TInEdit1 地址栏和重命名 Edit1 命令行
		if (RegExMatch(TC_CurrentControl, "TMyListBox1|TMyListBox2"))
			TcSendPos(524) ;cm_ClearAll
		else
			SendInput,`,
	return

	; CapsLock & y::SendInput,{AppsKey}	;光标处文件
	CapsLock & y::SendInput,{Click,Right}	;任何位置

	; /*
	[::SendInput,{Home}{Down}

	]::SendInput,{End}
	; */

	;复制到对面选中目录
	$!+F5::
		SendInput,{Tab}^+c{Tab}{F5}
		Sleep,500
		SendInput,^v
		Sleep,500
		SendInput,{Enter 2}
	return

	;移动到对面选中目录
	$!+F6::
		SendInput,{Tab}^+c{Tab}{F6}
		Sleep,500
		SendInput,^v
		Sleep,500
		SendInput,{Enter 2}
	return

	;加上剪贴板中内容改名
	;ez版本中还是tcfs2实现
	$^F2::
		SendInput,+{F6}
		Sleep,300
		SendInput,{Right}
		Sleep,300
		SendInput,{Space}^v
		Sleep,300
		;sendInput,{Enter}
	Return

	$^+F2:: ;更新文件名中日期
		SendInput,{F2}
		Sleep,100
		ControlGetText,OldName,TInEdit1,ahk_class TTOTAL_CMD
		Sleep,100
		NewName:= RegExReplace(OldName,"\d\d-\d\d-\d\d",fun_GetFormatTime("yy-MM-dd"))
		Sleep,100
		ControlSetText,TInEdit1,%NewName%
		Sleep,100
		SendInput,{Enter}
	Return

	;cm_OpenDirInNewTabOther    中键点击，在对面新标签中复制打开
	$MButton::
		if (A_Cursor = "Arrow") or (A_CaretX =""){	;20240606
			SendInput,{Click}	;点击激活
			Sleep 50
			TcSendPos(3004)		;对面新标签
		}
		else {
			SendInput,{Enter}	;文本模式,确认,可用于重命名
		}
	return

	;cm_OpenDirInNewTab     alt+中键点击，在新标签中复制打开
	$!MButton::	;^MButton和Quicker冲突改成$!MButton
	~LButton & MButton::
		SendInput,{Click}	;点击激活
		Sleep 50
		TcSendPos(3003)
	return

	;cm_OpenDirIncloesTab   Ctrl+Shift+中键点击，关闭全部标签
	$^+MButton::
	$^!MButton::
	; $+!MButton::
		SendInput,^+w
		Sleep 50
		SendInput,{Enter}
	return

	$+!MButton::SendInput,^!p	;打印文件,20240424

	;左手alt+e作为F4
	$!e::
		BlockInput On
		SendInput,{Esc}
		Sleep,100
		SendInput,{Alt Up}
		Sleep,100
		SendInput,{F4}
		BlockInput Off
	return

	;左手alt+s作为F3
	; !s::
		; BlockInput On
		; Sendinput,{Esc}
		; Sleep,100
		; Sendinput,{Alt up}
		; Sleep,100
		; Sendinput,{F3}
		; BlockInput Off
	; return

	; 双击右键，发送退格，返回上一级目录,需配合设定鼠标模式为NC右键.  20231122
	; ~RButton::
	; 	KeyWait,RButton
	; 	KeyWait,RButton, d T0.4
	; 	if ! Errorlevel
	; 	{
	; 		SendInput,{BackSpace}
	; 	}
	; return

	;花号的作用
	; $^`::TC_azHistory()
	$!`::TC_azHistory()
	;`::SendInput,{Enter}
	;`::SendInput,{Appskey}

	$!j::TC_azHistory()

	;智能对话框跳转
	$!w::
		Dlg_HWnd := WinExist("ahk_group GroupDiagOpenAndSave")
		if Dlg_HWnd
		;IfWinExist ahk_group GroupDiagOpenAndSave
		{
			WinGetTitle, Dlg_Title, ahk_id %Dlg_HWnd%
				if RegExMatch(Dlg_Title,"新建|选择|保存|另存|存储|打开|上传|导入|导出|插入|浏览|Open|Save|Select|并入|查找|发布|更改|输入|输出|替换|位置|Acrobat|PDF|XPS|下载"){
					;msgbox "这是保存对话框"
					orgClip:=ClipboardAll
					Clipboard =
					;Postmessage, TC_Msg, CM_CopyFullNamesToClip,0,, ahk_class TTOTAL_CMD
					TcSendPos(CM_CopyFullNamesToClip)
					ClipWait, 1
					selFiles := Clipboard
					Clipboard:=orgClip
					selFilesArray := StrSplit(selFiles, "`n","`r")
				if selFilesArray.length() > 1{
					selFiles:=selFilesArray[1]
					EzTip("对话框是保存类型，只认第一个文件",1)
				}

				StringGetPos OutputVar, selFiles,`\,R1
				StringMid, filePath, selFiles,1, OutputVar+1
				StringMid, fileName, selFiles,OutputVar+2,StrLen(selFiles)-OutputVar

				IfWinNotActive, %Dlg_Title% ahk_id %Dlg_HWnd%, , WinActivate, %Dlg_Title% ahk_id %Dlg_HWnd%
					WinWaitActive, %Dlg_Title% ahk_id %Dlg_HWnd%
				if !ErrorLevel
				{
						ControlGetText, orgFileName,Edit1
						ControlFocus, Edit1,
						Sleep 200
						Send,{BackSpace}
						Sleep 300
						SetKeyDelay, 10,10
						ControlSetText, Edit1, %filePath%
						Sleep 900
						Send,{Enter}
						Sleep 500
						if StrLen(fileName) > 0{
							ControlSetText, Edit1, %fileName%, A
						}
						else {
							ControlSetText, Edit1, %orgFileName%, A
						}
				}
			}
			else {
				;msgbox "打开对话框"
				orgClip:=ClipboardAll
				Clipboard =
				;Postmessage, TC_Msg, CM_CopyFullNamesToClip,0,, ahk_class TTOTAL_CMD
				TcSendPos(CM_CopyFullNamesToClip)
				ClipWait, 1
				selFiles := Clipboard
				Clipboard:=orgClip

				selFilesArray := StrSplit(selFiles, "`n","`r")
				quote:=(selFilesArray.length() > 1) ? """" : ""
				selFiles=
				loop % selFilesArray.MaxIndex()
				{
					this_file := selFilesArray[A_Index]
					selFiles=%selFiles% %quote%%this_file%%quote%
				}
				IfWinNotActive, %Dlg_Title% ahk_id %Dlg_HWnd%, , WinActivate, %Dlg_Title% ahk_id %Dlg_HWnd%
					WinWaitActive, %Dlg_Title% ahk_id %Dlg_HWnd%
				if !ErrorLevel
				{
					Sleep 300
					SetKeyDelay, 10,10
					ControlSetText, Edit1, %selFiles%, A
				}
			}
			gosub,ForceSelfReload
		}
		else {
			EzTip("系统当前没有打开或保存对话框",2)
		}
	return

	;Ctrl+D->User个人
	$+MButton::
		TcSendPos(3001)
		ControlSend,,^d
		Sleep 50
		SendInput,{Text}s
		Sleep 50
	return

	$^z::	;撤销
		SetControlDelay -1	;提高可靠性
		;点击窗口内指定坐标
		ControlClick, x100 y100, ahk_class Progman, FolderView,,, NA
		ControlClick, x100 y100, ahk_class WorkerW, FolderView,,, NA
		;向控件发送热键
		ControlSend, SysListView321,^z, ahk_class Progman, FolderView
		ControlSend, SysListView321,^z, ahk_class WorkerW, FolderView
	return

	$^!z::	;重做
		SetControlDelay -1	;提高可靠性
		;点击窗口内指定坐标
		ControlClick, x100 y100, ahk_class Progman, FolderView,,, NA
		ControlClick, x100 y100, ahk_class WorkerW, FolderView,,, NA
		;向控件发送热键
		ControlSend, SysListView321,^y, ahk_class Progman, FolderView
		ControlSend, SysListView321,^y, ahk_class WorkerW, FolderView
	return

	; $Space::SendInput,{Blind}{Space}	;恢复默认打字功能
	$Space::Space ;恢复空格打字上屏,by Tuutg,20240425

	$^+a::ControlSend,,^+a	;屏蔽搜狗输入法,20240327
	$^+b::ControlSend,,^+b	;屏蔽搜狗输入法,20240327,文件比较
	$^+c::ControlSend,,^+c	;屏蔽搜狗输入法,20240327
	$^+d::ControlSend,,^+d	;屏蔽搜狗输入法,20240327
	$^+e::ControlSend,,^+e	;屏蔽搜狗输入法,20240327
	$^+f::ControlSend,,^+f	;屏蔽搜狗输入法,20240327
	$^+g::ControlSend,,^+g	;屏蔽搜狗输入法,20240327
	$^+h::ControlSend,,^+h	;屏蔽搜狗输入法,20240327
	$^+i::ControlSend,,^+i	;屏蔽搜狗输入法,20240327
	$^+j::ControlSend,,^+j	;屏蔽搜狗输入法,20240327
	$^+k::ControlSend,,^+k	;屏蔽搜狗输入法,20240327
	$^+l::ControlSend,,^+l	;屏蔽搜狗输入法,20240327
	$^+m::ControlSend,,^+m	;屏蔽搜狗输入法,20240327
	$^+n::ControlSend,,^+n	;屏蔽搜狗输入法,20240327
	$^+o::ControlSend,,^+o	;屏蔽搜狗输入法,20240327
	$^+p::ControlSend,,^+p	;屏蔽搜狗输入法,20240327
	$^+q::ControlSend,,^+q	;屏蔽搜狗输入法,20240327
	$^+r::ControlSend,,^+r	;屏蔽搜狗输入法,20240327
	$^+s::ControlSend,,^+s	;屏蔽搜狗输入法,20240327
	$^+t::ControlSend,,^+t	;屏蔽搜狗输入法,20240327
	$^+u::ControlSend,,^+u	;屏蔽搜狗输入法,20240327
	$^+v::ControlSend,,^+v	;屏蔽搜狗输入法,20240327
	$^+w::ControlSend,,^+w	;屏蔽搜狗输入法,20240327
	$^+x::ControlSend,,^+x	;屏蔽搜狗输入法,20240327
	$^+y::ControlSend,,^+y	;屏蔽搜狗输入法,20240327
	$^+z::ControlSend,,^+z	;屏蔽搜狗输入法,20240327
	$+!s::ControlSend,,+!s	;屏蔽Everything,文件定位

	;em_ExtractWith7Zip     alt+Shift左键点击，解压光标下当前文件
	$!+LButton::	;20240407,解压当前文件
		GV_KeyClickAction1 := "SendInput,"TcSendUserCommand("em_ExtractWith7Zip")""
		GV_KeyClickAction2 := "gosub,TC_FilesAttrib"	;文件属性
		GV_MouseButton := 1
		GV_LongClickAction := "gosub,TC_FilesAttrib"	;文件属性
		gosub,Sub_MouseClick123
	return

	;em_To7zip    alt+Shift右键点击，压缩光标下当前文件
	$!+RButton::	;根据选择对象选用合适的压缩命令
		SendInput,^c
		Sleep,300
		Clipboard = %Clipboard%	;纯文本全文件名.by Tuutg,20240625
		Extension := GetFileInfo(Clipboard,4)	;只获取扩展名
		Length := StrLen(Extension)	;统计扩展名的字符数量
		; EzTip(Extension,2) 	;测试获取的扩展名
		if (Extension = "" or Length >=10)	;用扩展名作为判断条件,常用扩展名<10
			TcSendUserCommand("em_To7zip")	;压缩目录,压缩后文件名无源文扩展名
		else
			TcSendUserCommand("em_PackFiles_7z_Here") ;压缩文件,压缩后文件名无源文件扩展名
	return

	CapsLock & w:: ;双击关闭/单击标签|确定,by Tuutg,20240419
	if (GV_KeyClick_Continuous(GV_KeyTimer)){
		SendInput,^+w		;关闭标签
		EzTip("全部活动标签已关闭!",1)
		return
	}
	else {
		if (A_Cursor = "IBeam") or (A_CaretX !=""){
			SendInput,{Enter} ;单击, 确定|OK
		}
		else
		{
			if (GV_ToggleCapsMouseMove==1)	;鼠标移动模式
				SendInput,{Click} ;鼠标,单击,20240419
			else
				SendInput,^w ;鼠标,单击,20240419
			return
		}
	}
	return

	$+s:: ;双击关闭/单击标签|确定,by Tuutg,20240419
		if (A_Cursor = "IBeam") or (A_CaretX !=""){
			SendInput,{Text}S	;大写S
		}
		else
		{
			if (GV_KeyClick_Continuous(GV_KeyTimer)){
				TcSendUserCommand("cm_GotoDriveS") ;进入S盘
			}
			else {
				SendInput,^s ;打开文件搜索面板
			}
			return
		}
	return

	$+g:: ;进入G盘/进入自动任意盘,by Tuutg,20240519
		if (A_Cursor = "IBeam") or (A_CaretX !=""){
			SendInput,{Text}G ;大写S
		}
		else
		{
			if (GV_KeyClick_Continuous(GV_KeyTimer)){
				TcSendUserCommand("cm_GotoDriveZ") ;进入Z盘
			}
			else {
				TcSendUserCommand("cm_GotoDriveG") ;进入G盘
			}
			return
		}
	return

#If

TC_FilesAttrib:		;文件属性,避免干扰
	SendInput,!{Enter}
	Sleep 50
	ControlSend,,{Esc}, ahk_class #32770 ahk_exe 7zG.exe
	Sleep 50
	ControlSend,,{Esc}, ahk_class #32770 ahk_exe 7zG.exe
return

#If WinActive("ahk_class TTOTAL_CMD") and MouseUnder("(TMy|LCL)ListBox[123]")
	;长按左键，等于F4
	~$LButton::
		;GV_MouseTimer := 300
		GV_KeyClickAction1 := "SendInput,{LButton}"
		GV_KeyClickAction2 := "SendInput,{Enter}"
		GV_MouseButton := 1
		GV_LongClickAction := "SendInput,{Click}{F4}"
		gosub,Sub_ButtonLongPress
	return
#If

; 排除TC主窗口,Caps+Q|A,重定义,Home|End. by Tuutg,20240421
#If (WinActive("ahk_exe TOTALCMD64.EXE") or WinActive("ahk_exe TOTALCMD.EXE")) and !WinActive("Total Commander ahk_class TTOTAL_CMD")
CapsLock & q::SendInput,{Home}
CapsLock & a::SendInput,{End}
CapsLock & s::SendInput,{Left}
CapsLock & f::SendInput,{Right}
#If

; 双击右键，发送退格，返回上一级目录,配合设定鼠标模式为Win左键. 20240418
#If (WinActive("ahk_class TTOTAL_CMD") or WinActive("强化版 ahk_exe TOTALCMD.EXE")) and !WinActive("ahk_group Group_disableRButtonBackspace")
$RButton::
	; GV_MouseTimer := 300
	GV_KeyClickAction1 := "SendInput,{RButton}"	;系统默认,RButton
	GV_KeyClickAction2 := "SendInput,{BackSpace}"	;退回上一级目录
	GV_MouseButton := 2
	GV_LongClickAction := "SendInput,{RButton}"	;系统默认,RButton
	gosub,Sub_MouseClick123
return
#If

MouseUnder(Controls){
	MouseGetPos,,,, Control
	if RegExMatch(Control, Controls)
		return, true
}


TcSendPos(Number)
{
	PostMessage 1075, %Number%, 0, , AHK_CLASS TTOTAL_CMD
}

;#If WinActive("ahk_class TTOTAL_CMD")
	;#0::TcSendUserCommand("em_To7zip")
	;return
;#If

TcSendUserCommand(strCommand) ;string
{
	VarSetCapacity(CopyDataStruct, 3*A_PtrSize, 0) ;Set up the structure's memory area.
	dwData := Asc("E") + 256 * Asc("M")
	NumPut(dwData, CopyDataStruct, 0)
	cbData := StrPutVar(strCommand, strCommand, "cp0")
	NumPut(cbData, CopyDataStruct, A_PtrSize) ;OS requires that this be done.
	NumPut(&strCommand, CopyDataStruct, 2*A_PtrSize) ;Set lpData to point to the string itself.
	SendMessage, 0x4a, 0, &CopyDataStruct,, ahk_class TTOTAL_CMD ;0x4a is WM_COPYDATA. Must use Send not Post.
}

StrPutVar(string, ByRef var, encoding)
{
	;Ensure capacity.
	VarSetCapacity( var, StrPut(string, encoding) * ((encoding="utf-16"||encoding="cp1200") ? 2 : 1) )
	;Copy or convert the string.
	return StrPut(string, &var, encoding)
}

#If WinActive("批量重命名 ahk_class TMultiRename")
{
	F1::SendInput,!p{tab}{Enter}e

	$MButton::
	Escape & F1::
		SendInput,{F10}e
	return
}
#If
;****************************************************
;MButton盲点默认是“确定”或者OK的按钮，by tuutg,20240329
#If WinActive("ahk_class #32770") and (!WinActive("ahk_group GroupDiagOpenAndSave") and !WinActive("ahk_group MButtonClickPassWord") and !WinActive("ahk_group Group_CQCCAPassWord"))
$MButton::SendInput,{Enter}	;点OK按钮,支持保存对话框.
#If
;****************************************************
; MButton盲点默认不是“确定”或者OK的按钮，by tuutg,2024/01/09 22:08
#If WinActive("ahk_group Group_MButtonClickOk")
$MButton::
	;GV_MouseTimer := 300
	GV_KeyClickAction1 := "gosub,MButtonClickOk" ;OK按钮,被Listary拦截了
	GV_KeyClickAction2 := "SendInput,{Enter}"	 ;OK按钮
	GV_MouseButton := 2
	GV_LongClickAction := "gosub,uTools" ;长400ms,uTools
	gosub,Sub_MouseClick123
return
#If
;****************************************************
MButtonClickOk:
	try{
		SendInput,{Enter}
		Sleep,100
		SetTitleMatchMode RegEx
		SetTitleMatchMode Slow
		ControlClick, i)确定|.*确定|确定.*|全部.*|OK.*|是.*|.*Y|.*A|置入.*|, A
	} catch e{
		ControlClick, Button1, A
		Sleep,100
		ControlClick, Button2, A
		Sleep,100
		ControlClick, TButton1, A
	}
return
;************************************************************************
;F7新建文件(夹),盲点“确定”或者OK按钮，by tuutg,20240115
#If WinActive("ahk_class TfomMain")
	CapsLock & w::SendInput,^{Enter}	;换行
	$MButton::SendInput,{Enter}			;确定

	$RButton::
		;GV_MouseTimer := 300
		GV_KeyClickAction1 := "SendInput,{Click,Right}" ;系统默认,RButton
		GV_KeyClickAction2 := "SendInput,{Esc}"	;取消
		GV_MouseButton := 2
		GV_LongClickAction := "SendInput,{Click,Right}" ;系统默认,RButton
		gosub,Sub_MouseClick123
	return
#If
;************************************************************************
;totalcmd中快速切换到搜索备注 {{{2
#If WinActive("搜索文件 ahk_class TFindFile")
{
!b::
	Sendinput,^{Tab 2}
	Sleep,100
	Sendinput,!i
	Sleep,100
	Sendinput,{Tab 5}
return
}
#If
;************************************************************************
;合格证U_Key,自动输入密码登入 {{{2
#If WinActive("ahk_group Group_CQCCAPassWord")
	$MButton::
	CapsLock & g::
	{
		SendInput,cqcca123
		Sleep,100
		SendInput,{Enter}
	return
	}
#If
;************************************************************************
;阿里云盘分享自动输入密码 {{{2
#If WinActive("阿里云盘分享 ahk_class Chrome_WidgetWin_1")
	$MButton::
	CapsLock & g::
	{
		SendInput,865746
		Sleep,100
		SendInput,{Enter}
	return
	}
#If
;************************************************************************
;Acrobat自动输入文档密码 {{{2
#If WinActive("口令 ahk_class #32770 ahk_exe Acrobat.exe")
	$MButton::
	CapsLock & g::
	{
		SendInput,tuutg2022
		Sleep,100
		SendInput,{Enter 2}
	return
	}
#If
;****************************************************
;****************************************************
;excel中 {{{2
;excel 2010: ahk_class XLMAIN
;excel2013: ahk_class XLMAIN
;WPS 表格-wps11-4合1集成模式, 表格 ahk_exe wpsoffice.exe,打印 ahk_class bosa_sdm_XL9
;WPS_et 双击中键关闭/单击中键缩放100%
#If (WinActive("ahk_exe et.exe") and !WinActive("ahk_group Group_打印"))
CapsLock & w::
$MButton::	;2023/07/30 15:32
if (GV_KeyClick_Continuous(GV_MouseTimer)){	;保存并关闭
	SendInput,^s
	Sleep ,100
	SendInput,^w
	Sleep ,100
	EzTip("文件保存并关闭成功!",1)
}
else {	;100%->保存
	SendInput,^0
	Sleep ,100
	SendInput,^s
	Sleep ,100
	;SendInput,^{Home}
	;EzTip("文件保存成功!",1)
}
return

^#a::	;另存为CVS
	Click, 40, 55, 1
	Sleep, 50
	ControlSend, ,{Text}am, WPS 表格 ahk_class XLMAIN
	Sleep, 100
	WinActivate, 另存为 ahk_class #32770
	Sleep, 100
	ControlSend, Edit1, {Tab}, 另存为 ahk_class #32770
	Sleep, 100
	Click, 408, 551, 1
	Sleep, 100
	Click, 314, 717, 1	;CVS
	Sleep, 100
	Click, 770, 520, 0	;保存(s)
	Sleep, 100
	; WinActivate, 另存为 ahk_class #32770
	; ControlSend, ,{Alt}{y}{enter}, WPS 表格 ahk_class XLMAIN
	; Sleep, 100
	; WinActivate, 另存为 ahk_class #32770
	; ControlSend, ,{Alt}{n}{enter}, WPS 表格 ahk_class XLMAIN
return

^#s::	;另存为xlsx
	Click, 40, 55, 1
	Sleep, 100
	ControlSend, ,{Text}a, WPS 表格 ahk_class XLMAIN
	Sleep, 100
	Click, 315, 315, 1
	WinActivate, 另存为 ahk_class #32770
	Sleep, 100
	Click, 770, 520, 0	;保存(s)
return

#If

;****************************************************
;排除打印组,!WinActive("ahk_group Group_打印"),20240330
#If (WinActive("ahk_group Office_Excel") and !WinActive("ahk_group Group_打印"))
CapsLock & u::SendInput,^{End}
CapsLock & i::SendInput,^{Home}

Space & r::
CapsLock & r:: ;删除|行/列,20240319
if (GV_KeyClick_Continuous(GV_KeyTimer)){
	SendInput,{Click,Right}
	Sleep 50
	SendInput,{Text}d ;删除行/列
}
else
	SendInput,{Del} ;通用删除
return

;单击快速定位到|双击选择非空单元格,20240329
Space & e::
CapsLock & e:: ;20240329
	GV_KeyClickAction1 := "SendInput,^{Up}"	;^{Up}
	GV_KeyClickAction2 := "SendInput,{Blind}^+{Up}"
	gosub,Sub_KeyClick123
return

Space & d::
CapsLock & d:: ;20240329
	GV_KeyClickAction1 := "SendInput,^{Down}" ;^{Down}
	GV_KeyClickAction2 := "SendInput,{Blind}^+{Down}"
	gosub,Sub_KeyClick123
return

Space & s::
CapsLock & s:: ;20240329
	GV_KeyClickAction1 := "SendInput,^{Left}" ;^{Left}
	GV_KeyClickAction2 := "SendInput,{Blind}^+{Left}"
	gosub,Sub_KeyClick123
return

Space & f::
CapsLock & f:: ;20240329
	GV_KeyClickAction1 := "SendInput,^{Right}" ;^{Right}
	GV_KeyClickAction2 := "SendInput,{Blind}^+{Right}"
	gosub,Sub_KeyClick123
return

; CapsLock & g:: ;Enter|定位^g,20231122

CapsLock_B:
$!m::
CapsLock & b::		;Ctrl+M|合并居中
if (WinActive("ahk_exe et.exe")){
	SendInput, ^m
}
else {	;ahk_exe EXCEL.EXE,20240408
	; SendInput, !hz2y7{Enter}	;适用半屏模式
	; SendInput, !hyh{Enter}	;适用全屏模式
	SendInput, !05{Enter}	;通用,要EZtools支持
}
return

Space & q::	;20240329
CapsLock & q::
if (GV_KeyClick_Continuous(GV_KeyTimer)){
	if (WinActive("ahk_exe EXCEL.EXE")){	;同一工作簿表间切换
		SendInput, ^{PgUp}
	}
	else {	;ahk_exe et.exe,同一工作簿表间切换
		Send, {Control Down }
		Sleep, 100
		ControlSend, EXCEL71, {PgUp}
		Sleep, 100
		Send, {Control Up}
	}
	return
}
else {
	if (A_Cursor ="IBeam") or (A_CaretX !=""){
		SendInput,{Home}	;{Home}|行首
	}
	else {
		; SendInput,^+{Tab}	;工作簿标签间切换
		SendInput,^{Home}	;跳转到单元格首格,20240329
	}
}
return

Space & a::	;20240329
CapsLock & a::
if (GV_KeyClick_Continuous(GV_KeyTimer)){	;同一工作簿表间切换
	if (WinActive("ahk_exe EXCEL.EXE")){
		SendInput, ^{PgDn}
	}
	else {	;ahk_exe et.exe,同一工作簿表间切换
		Send, {Control Down}
		Sleep, 100
		ControlSend, EXCEL71, {PgDn}
		Sleep, 100
		Send, {Control Up}
	}
}
else {
	if (A_Cursor ="IBeam") or (A_CaretX !=""){
		SendInput,{End}	;{End}|行末
	}
	else {
		; SendInput,^{Tab}	;工作簿标签间切换
		SendInput,^{End}	;跳转到单元格末格,20240329
	}
}
return

CapsLock & z:: 	 ;插入复制行|列
	SendInput,{Blind}^c
	Sleep 300
	SendInput,{AppsKey}
	Sleep 100
	ControlSend,,e
return

$!q::ControlSend,,!q	;检索内容,自带快捷键

$!w::	;鼠标置于MS_Office Tab标签上,右键定位到文件夹
if WinActive("ahk_exe EXCEL.EXE"){
	Click,Right, ,Down
	Sleep,100
	Click,Right, ,Up
	Sleep,100
	ControlSend, ,{Up 9}
	Sleep,100
	ControlSend, ,{Enter}
}
else {
	Click, Right, , Down
	Sleep,100
	Click, Right, , Up
	Sleep,100
	ControlSend, , {Up 5}
	Sleep,100
	ControlSend, , {Enter}
}
return

$!e::	;字体居中
if (WinActive("ahk_exe EXCEL.EXE")){
	SendInput,!hyc	;字体居中
	SendInput,!hyf	;字体居中
}
else {
	SendInput,!hac	;字体居中
}
return

$!r::	;当前日期
if (WinActive("ahk_exe EXCEL.EXE")){
	SendInput,!hyc	;字体居中
	SendInput,!hyf	;字体居中
	Sleep,100
	SendInput,^`;	;当前日期
}
else {
	SendInput,!hac	;字体居中
	Sleep,100
	SendInput,^`;	;当前日期
}
return
$!t::ControlClick, Edit1	;只适用于MS_Office,定位到Edit1

$!a::SendInput,^a	;全选
$!s::SendInput,!ohr	;重命名工作表
$!d::	;SendInput,^e	;智能填充,20240507
	GV_KeyClickAction1 := "SendInput,^d" ;向下填充
	GV_KeyClickAction2 := "SendInput,^e" ;智能填充
	gosub,Sub_KeyClick123
return

$!f:: ;SendInput,^f	;查找|!g->定位
$^f::
	Clipboard_Save = %ClipboardAll%		;20240403
	Clipboard:=""
	SendInput,^c	;复制
	Sleep,300	;延时等待剪切板
	SendInput,^f
	Sleep,100
	WinActivate,查找 ahk_class bosa_sdm_XL9
	Sleep,100
	ControlSend,,{Delete},查找 ahk_class bosa_sdm_XL9
	Sleep,400
	ControlSend,,{Text}%Clipboard%,查找 ahk_class bosa_sdm_XL9
	Sleep,100
	Clipboard = %Clipboard_Save%	;恢复原来剪切板,外来数据,20240403
return

$!LButton::	;单元格内换行,by Tuutg,20240518
$!g:: ;SendInput,!{Enter} ;单元格内换行
	GV_KeyClickAction1 := "SendInput,!{Enter}" ;格内换行
	GV_KeyClickAction2 := "SendInput,^1" ;设置单元格格式
	gosub,Sub_KeyClick123
return

$!z::SendInput,^z	;撤销
$!+z::ControlSend,,^y	;重做,屏蔽火绒弹窗拦截冲突
$!x::SendInput,!el	;只适用于MS_Office,删除工作表

FrontColor:	;20240528
$!c::	;字体颜色|;SendInput,{F2}^+{Home}^c{Esc}	;复制单元格纯文本
if (WinActive("ahk_exe EXCEL.EXE")){
	WinGetPos,xw,yw,WA,HA,A
	MouseGetPos,xpos, ypos
	if (WA > A_ScreenWidth/2){	;全屏模式
		Click ,355,110,1	;字体颜色
		Sleep,100
		Click ,320,115,1	;背景白色

	}
	else {	;非全屏模式
		Click ,315,115,1	;字体颜色
		Sleep,100
		Click ,285,115,1	;背景白色
	}
	Sleep,100
	CoordWinMove(xpos,ypos)	;光标复位
}
else {
	WinGetPos,xw,yw,WA,HA,A
	MouseGetPos,xpos, ypos
	SendInput,!h!
	Click ,360,115,1	;字体颜色
	Sleep,100
	Click ,325,115,1	;表格颜色
	Sleep,100
	CoordWinMove(xpos,ypos)	;光标复位
}
return

$!v::SendInput,!hfp ;格式刷
$!b::SendInput,^b ;加粗字体

;****************************************************

;筛选
;f3::Postmessage, 0x111, 447, 0, , a
$F3::SendInput,^+l	;ctrl+shift+L

;添加单元格
^=::SendInput,^+=
$!i::SendInput,!his	;只适用于MS_Office,新建工作表

;详细编辑
!;::
{
	ControlClick, EXCEL<1
	SendInput,{Home}
}
return

;自行调整行高
![::
try{
	ox := ComObjActive("Excel.Application")
	ox.Application.Selection.EntireRow.AutoFit
}
catch e{
	;出错就用传统快捷键
	SendInput,!ora
}
return

;自行调整列宽
!]::
try{
	ox := ComObjActive("Excel.Application")
	ox.Application.Selection.EntireColumn.AutoFit
}
catch e{
	SendInput,!oca
}
return

;默认快捷键
;$!WheelUp::SendInput,!{PgUp}
;$!WheelDown::SendInput,!{PgDn}
$!WheelUp::SendInput,{Left 8}
$!WheelDown::SendInput,{Right 8}
$+WheelUp::SendInput,{Left}
$+WheelDown::SendInput,{Right}

;先保存,双击中键关闭/单击中键缩放100%
$MButton::gosub,中键设定_Office
$!MButton::SendInput,{F12} ;另存为
; $+RButton::SendInput,{Enter} ;Enter,处理中文输入法英文上屏,用Caps+g

;电自合格证自动打印,适用于MS_Excel,20240515
Tab & MButton::
$+MButton::
	if !WinActive("ahk_class XLMAIN")
		WinActivate,ahk_class XLMAIN	;20240606

	MouseGetPos,Xp,Yp	;第一行第七列坐标,G1单元格
	; 弹出输入对话框获取起始值和结束值
	InputBox,StartValue,请填写车辆数据初始序号
	if ErrorLevel  ; 用户取消
		return
	InputBox,EndValue,请填写车辆终止数据序号
	if ErrorLevel  ; 用户取消
		return
	; MsgBox,%StartValue%,%EndValue%, %Xp%,%Xp%

	; 确保起始值小于等于结束值
	if (%StartValue% > %EndValue%) {
		EzTip("起始值必须小于等于结束值!",1)
		return
	}

	; 初始化Excel COM对象
	Excel := ComObjActive("Excel.Application")
	if !Excel {
		Excel := ComObjCreate("Excel.Application")
		Excel.Visible := true ; 让Excel可见，以便观察操作过程
	}
	Workbook := Excel.ActiveWorkbook
	Sheet := Workbook.ActiveSheet

	;将起始值写入第一行第七列（G1单元格）
	CoordWinXClick(Xp,Yp,1,1)
	Sleep, 500
	SendInput,%StartValue%{Enter}	;刷新数据和二维码
	Sleep, 1500
	CellValue :=Ceil(Sheet.Range("C5").value)
	Sleep, 500
	CoordWinXClick(305,355,1,1)	;定位到C5单元格
	Sleep, 500
	SendInput,^c	;复制Vin
	Sleep, 1500

	; 循环打印直到达到结束值
	Loop {
		; 打印当前工作表
		; Excel.ActivePrinter := "Adobe PDF" ; 设置默认打印机
		; Sheet.PrintOut ; 执行打印
		Send,{Ctrl down}p{Ctrl Up} ; 执行打印
		Sleep, 1000    ; 等待打印对话框出现
		Send, {ENTER}  ; 确认打印
		WinWaitActive, PDF ahk_class #32770
		SetControlDelay -1
		ControlSend,Edit1,{Del 30},PDF ahk_class #32770
		Sleep,1000	;延时等待,清空不正确的文件名
		ControlFocus,Edit1,PDF ahk_class #32770
		Controlclick,Edit1,PDF ahk_class #32770
		; MsgBox,%CellValue%--%Clipboard%
		SendInput,{Text}%CellValue%	;粘贴文件名,带后缀
		Sleep,1000	;延时等待,正确的文件名
		ControlSend,Edit1,{Enter},PDF ahk_class #32770  ; 确认打印
		Sleep, 2000    ; 等待打印完成（这里简单等待2秒作为示例)
		WinWaitClose,PDF ahk_class #32770
		EzTip(CellValue打印完成!,1)

		; 数字自增1->刷新数据和二维码
		StartValue += 1  ; 数字自增1
		CoordWinXClick(Xp,Yp,1,1) ;定位到G1单元格
		Sleep, 500
		SendInput,%StartValue%{Enter}	;刷新数据和二维码
		Sleep, 1500
		CellValue :=Ceil(Sheet.Range("C5").value)
		Sleep, 500
		CoordWinXClick(305,355,1,1)	;定位到C5单元格
		Sleep, 500
		SendInput,^c	;复制Vin
		Sleep, 1500

		; 检查是否达到结束值
		if (StartValue > EndValue) {
			break
		}
	}
return

;****************************************************

;左手通用模式,Space模式,单击带Ctrl效果,双击Ctrl+Shfit效果.20240329
; Space & q:: ;到首部|上翻页,20240320
; if (GV_KeyClick_Continuous(GV_KeyTimer)){
	; if (A_Cursor = "IBeam") or (A_CaretX !="")
		; SendInput,^{Home} ;到首部
	; else
		; SendInput,{PgUp} ;上翻页
; } else{
	; SendInput,{Home} ;到首部
; }
; return

Space & w::	;单击保存
	GV_KeyClickAction1 := "gosub,Space_SgWInExcel" ;保存|=
	GV_KeyClickAction2 := "SendInput,{Text}&" ;逻辑与,&
	gosub,Sub_KeyClick123
return

; Space & e::	;{Up}|输入-键
	; GV_KeyClickAction1 := "{Up}"
	; GV_KeyClickAction2 := "SendInput,{Text}-"	;-
	; gosub,Sub_KeyClick123
; return

; Space & r::	;输入{del}|=|+键
	; GV_KeyClickAction1 := "SendInput,{del}"
	; GV_KeyClickAction2 := "SendInput,{NumpadAdd}" ;+
	; gosub,Sub_KeyClick123
; return

Space & t:: ;输入{Backspace}
	GV_KeyClickAction1 := "SendInput,{Backspace}" ;{Backspace}
	GV_KeyClickAction2 := "SendInput,{Text}|" ;逻辑或,|
	gosub,Sub_KeyClick123
return

;***********************************************

; Space & a:: ;到末尾|上翻页,20240320
; if (GV_KeyClick_Continuous(GV_KeyTimer)){
	; if (A_Cursor = "IBeam") or (A_CaretX !="")
		; SendInput,^{End} ;下翻页
	; else
		; SendInput,{PgDn} ;下翻页
; } else{
	; SendInput,{End} ;到末尾
; }
; return

; Space & s::	;单击{Left}
	; GV_KeyClickAction1 := "SendInput,{Left}"
	; GV_KeyClickAction2 := "SendInput,{NumpadDiv}" ;/除法
	; gosub,Sub_KeyClick123
; return

; Space & d::	;单击{Down}
	; GV_KeyClickAction1 := "SendInput,{Down}"
	; GV_KeyClickAction2 := "SendInput,`," ;逗号
	; gosub,Sub_KeyClick123
; return

; Space & f:: ;单击{Right}
	; GV_KeyClickAction1 := "SendInput,{Right}"
	; GV_KeyClickAction2 := "SendInput,{Text};" ;分号
	; gosub,Sub_KeyClick123
; return

Space & g:: ;SendInput,{Enter} ;输入{Enter}
	if (GV_ToggleAotuSaveMode==1){ ;自动保存,20240323
		GV_KeyClickAction1 := "SendInput,{Enter}^s" ;Enter|保存
		GV_KeyClickAction2 := "SendInput,{Text}." ;小数点.|句号
		gosub,Sub_KeyClick123
	}
	else {
		GV_KeyClickAction1 := "SendInput,{Enter}" ;Enter|不保存
		GV_KeyClickAction2 := "SendInput,{Text}." ;小数点.|句号
		gosub,Sub_KeyClick123
	}
return

;***********************************************

Space & z:: ;输入Ctrl+Z
	GV_KeyClickAction1 := "SendInput,^z" ;撤销
	GV_KeyClickAction2 := "SendInput,`"`"" ;""
	gosub,Sub_KeyClick123
return

Space & x::gosub,Space_SgX ;剪切+内容显示
	GV_KeyClickAction1 := "gosub,Space_SgX" ;剪切
	GV_KeyClickAction2 := "SendInput,{NumpadMult}" ;*(乘号X)
	gosub,Sub_KeyClick123
return

Space & c::	;复制+内容显示|字体颜色
	GV_KeyClickAction1 := "gosub,Space_SgC" ;复制
	GV_KeyClickAction2 := "gosub,FrontColor" ;字体颜色
	gosub,Sub_KeyClick123
return

Space & v:: ;粘贴
	GV_KeyClickAction1 := "SendInput,{Blind}^v" ;粘贴
	GV_KeyClickAction2 := "SendInput,`(`)" ;()
	gosub,Sub_KeyClick123
return

Space & b::	;鼠标右(Y)键单击|合并居中,20240330
	GV_KeyClickAction1 := "gosub,Space_SgBInExcel" ;合并居中|冒号:
	GV_KeyClickAction2 := "SendInput,{Click,Right}" ;鼠标右(Y)键
	gosub,Sub_KeyClick123
return

$Space::SendInput,{Blind}{Space} ;恢复空格打字上屏
; $Space::Space ;恢复空格打字上屏,by Tuutg,20240425

;***********************************************

Space & 1:: SendInput,{0} ;相加=11
Space & 2:: SendInput,{9} ;相加=11
Space & 3:: SendInput,{8} ;相加=11
Space & 4:: SendInput,{7} ;相加=11
Space & 5:: SendInput,{6} ;相加=11
Space & 6:: SendInput,{5} ;相加=11
Space & 7:: SendInput,{4} ;相加=11
Space & 8:: SendInput,{3} ;相加=11
Space & 9:: SendInput,{2} ;相加=11
Space & 0:: SendInput,{1} ;相加=11

;***********************************************

Space & F1:: SendInput,{F11} ;加+10
Space & F2:: SendInput,{F12} ;加+10
Space & F3:: SendInput,{F8} ;相加=11
Space & F4:: SendInput,{F7} ;相加=11
Space & F5:: SendInput,{F6} ;相加=11

;***********************************************

Space & -:: SendInput,`_ ;输入_键
Space & =:: SendInput,{NumpadAdd} ;输入+键
Space & [:: SendInput,{Text}{ ;输入{键
Space & ]:: SendInput,{Text}} ;输入}键
Space & `;:: SendInput,`: ;输入:键
Space & ':: SendInput,`" ;输入"键
Space & \:: SendInput,`| ;输入|键
Space & /:: SendInput,`? ;输入?键
Space & ,:: SendInput,`< ;输入<键
Space & .:: SendInput,`> ;输入>键

Space & ~LButton::SendInput,^#!2	;键鼠配合,左半屏幕

#If

Space_SgWInExcel:
if (A_Cursor ="IBeam"){
		SendInput,{Text}=	;{Text}=,函数
	}
	else {
		SendInput,{Click}^s	;单击&保存,20240330
	}
return
;****************************************************
Space_SgBInExcel:
if (A_Cursor ="IBeam"){
		SendInput,{Text}:	;{Text}:,选择范围
	}
	else {
		gosub,CapsLock_B	;合并居中,20240408
	}
return

;****************************************************
;****************************************************
;word中,word2013: ahk_class OpusApp, {{{2
;WPS 文字-wps11-4和1集成模式,文字 ahk_exe wpsoffice.exe
;排除打印组,!WinActive("ahk_group Group_打印"),20240330
#If (WinActive("ahk_group Office_Word") and !WinActive("ahk_group Group_打印"))

	CapsLock & o:: ;簿间切换<-
	CapsLock & q:: ;簿间切换<-
	if WinActive("ahk_exe wps.exe") or WinActive("ahk_exe wpsoffice")
	{
		SendInput,^+{Tab}
	}
	else
	{
		SendInput,^+{F6}
	}
	return

	CapsLock & p:: ;簿间切换->
	CapsLock & a:: ;簿间切换->
	if WinActive("ahk_exe wps.exe") or WinActive("ahk_exe wpsoffice")
	{
		SendInput,^{Tab}
	}
	else
	{
		SendInput,^{F6}
	}
	return

	CapsLock & b::^h ;Ctrl+H,替换

	;!q::	;检索内容,自带快捷键
	$!w::	;鼠标置于MS_Office Tab标签上,右键定位到文件夹
	if WinActive("ahk_exe WINWORD.EXE"){
		Click, Right, , Down
		Sleep,100
		Click, Right, , Up
		Sleep,100
		ControlSend, , {Up 9}
		Sleep,100
		ControlSend, , {Enter}
		Sleep,100
	}
	else {
		Click, Right, , Down
		Sleep,100
		Click, Right, , Up
		Sleep,100
		ControlSend, , {Up 5}
		Sleep,100
		ControlSend, , {Enter}
		Sleep,100
	}
	return

	$!e::SendInput,^e	;字体居中
	$!r::SendInput,^r	;右对齐
	; $!t::             ;工具

	$!a::SendInput,^a	;全选
	$!s::SendInput,^s	;保存文件
	$!d::ControlSend,,^d	;字体设置
	$!f::SendInput,^f	;查找
	$!g::SendInput,^g	;定位

	$!z::SendInput,^z	;撤销
	$!+z::ControlSend,,^y	;重做,屏蔽火绒弹窗拦截冲突
	$!x::SendInput,^x	;剪切
	$!c::SendInput,^c	;复制
	$!v::SendInput,!hfp ;格式刷
	$!b::SendInput,^b 	;加粗

	;先保存,双击中键关闭/单击中键缩放100%
	$MButton::gosub,中键设定_Office
	$!MButton::SendInput,{F12} ;另存为
	; $+RButton::SendInput,{Enter} ;Enter,处理中文输入法英文上屏,用Caps+g

#If
;****************************************************
;****************************************************
;PPT中,PPT2013: ahk_class PPTFrameClass,ahk_exe POWERPNT.EXE {{{2
;WPS PPT-wps11-4和1集成模式,演示 ahk_exe wpsoffice.exe
;排除打印组,!WinActive("ahk_group Group_打印"),20240330
#If (WinActive("ahk_group Office_PPT") and !WinActive("ahk_group Group_打印"))
	CapsLock & o:: ;簿间切换<-
	CapsLock & q:: ;簿间切换<-
	if WinActive("ahk_exe wps.exe") or WinActive("ahk_exe wpsoffice")
	{
		SendInput,^+{Tab}
	}
	else
	{
		SendInput,^+{F6}
	}
	return

	CapsLock & p:: ;簿间切换->
	CapsLock & a:: ;簿间切换->
	if WinActive("ahk_exe wps.exe") or WinActive("ahk_exe wpsoffice")
	{
		SendInput,^{Tab}
	}
	else
	{
		SendInput,^{F6}
	}
	return

	CapsLock & b::^h ;Ctrl+H,替换

	;!q::	;检索内容,自带快捷键
	$!w::	;鼠标置于MS_Office Tab标签上,右键定位到文件夹
	if WinActive("ahk_exe WINWORD.EXE"){
		Click, Right, , Down
		Sleep,100
		Click, Right, , Up
		Sleep,100
		ControlSend, , {Up 9}
		Sleep,100
		ControlSend, , {Enter}
		Sleep,100
	}
	else {
		Click, Right, , Down
		Sleep,100
		Click, Right, , Up
		Sleep,100
		ControlSend, , {Up 5}
		Sleep,100
		ControlSend, , {Enter}
		Sleep,100
	}
	return

	$!e::SendInput,^e	;字体居中
	$!r::SendInput,^r	;右对齐
	; $!t::             ;工具

	$!a::SendInput,^a	;全选
	$!s::SendInput,^s	;保存文件
	$!d::ControlSend,,^d	;字体设置
	$!f::SendInput,^f	;查找
	$!g::SendInput,^g	;定位

	$!z::SendInput,^z	;撤销
	$!+z::ControlSend,,^y	;重做,屏蔽火绒弹窗拦截冲突
	$!x::SendInput,^x	;剪切
	$!c::SendInput,^c	;复制
	$!v::SendInput,!hfp ;格式刷
	$!b::SendInput,^b 	;加粗

	;先保存,双击中键关闭/单击中键缩放100%
	$MButton::gosub,中键设定_Office
	$!MButton::SendInput,{F12} ;另存为
	; $+RButton::SendInput,{Enter} ;Enter,处理中文输入法英文上屏,用Caps+g

#If

;****************************************************
;****************************************************
;Tg Everything中 {{{2
;audio:  搜索音频文件.
;zip:	搜索压缩文件.
;doc:	搜索文档文件.
;exe:	搜索可执行文件.
;pic:	搜索图片文件.
;video:	 搜索视频文件.
;folder: 仅匹配文件夹.
;8-tf-  文本文件
;9-ct-  内容搜索
;cf-    重复文件
;mt-    音频-作者专辑标题
#If WinActive("ahk_class EVERYTHING") or WinActive("ahk_class EVERYTHING_(1.5a)")
{
	$!q::ControlSend,,!p ;预览

	;打开文件夹,默认^{Enter},并在TC中定位到文件,即光标在文件上;20240113
	$!w::	;激活Tc并选中,即光标在文件上.by Tuutg,20240625
	CapsLock & w::gosub,EvToTcAClick ;激活Tc并选中,即光标在文件上

	$!e::EverythingChooseType("exe")	;5过滤可执行文件
	$!r::EverythingChooseType("folder")	;6过滤文件夹
	$!t::EverythingChooseType("pic")	;7过滤图片


	$!a::EverythingChooseType("audio")	;2过滤音频

	;一键打开路径,跳入Win资管
	$!s::SendInput,^e
	~$MButton::SendInput,^e

	$!d::	;聚焦地址栏|4过滤文档. by Tuutg,20240625
		if (GV_KeyClick_Continuous(GV_KeyTimer)){
			EverythingChooseType("doc")	;搜索框过滤文档
		}
		else {
			ControlSend,,!d	;聚焦地址栏,TC和系统通用
		}
	return

	; $!f:: ;预留一键Ev搜索
	CapsLock::				;20240330,筛选
	$!g::ControlClick, ComboBox1 ,,,,, NA	;筛选


	$!z::EverythingChooseType("zip")	;3压缩文件
	; $!x::EverythingChooseType("tf")	;9文本文件|与TC一致,详细视图
	; $!c::					;与TC一致,缩微视图,20240623
	$!v::EverythingChooseType("video")	;8过滤视频
	$!b::EverythingChooseType("")		;1过滤所有

	;一键复制路径
	$^+c::ControlSend,,^+c	;屏蔽干扰,20240625
	$!MButton::ControlSend,,^+c	;屏蔽干扰,20240625

	CapsLock & 1::
		GV_KeyClickAction1 := "SendInput,!1"	;1过滤所有
		GV_KeyClickAction2 := "SendInput,!0"	;0内容搜索
		gosub,Sub_KeyClick123
	return

	CapsLock & 2::
		GV_KeyClickAction1 := "SendInput,!2"	;2过滤音频
		GV_KeyClickAction2 := "SendInput,!9"	;9文本文件
		gosub,Sub_KeyClick123
	return

	CapsLock & 3::
		GV_KeyClickAction1 := "SendInput,!3"	;3压缩文件
		GV_KeyClickAction2 := "SendInput,!8"	;8过滤视频
		gosub,Sub_KeyClick123
	return

	CapsLock & 4::
		GV_KeyClickAction1 := "SendInput,!4"	;4过滤文档
		GV_KeyClickAction2 := "SendInput,!7"	;7过滤图片
		gosub,Sub_KeyClick123
	return

	CapsLock & 5::
		GV_KeyClickAction1 := "SendInput,!5"	;5过滤可执行文件
		GV_KeyClickAction2 := "SendInput,!6"	;6过滤文件夹
		gosub,Sub_KeyClick123
	return

	CapsLock & 6::SendInput,!6	;6过滤文件夹
	CapsLock & 7::SendInput,!7	;7过滤图片
	CapsLock & 8::SendInput,!8	;8过滤视频
	CapsLock & 9::SendInput,!9	;9文本文件
	CapsLock & 0::SendInput,!0	;0内容搜索

	;自动调整列宽
	$+MButton::SendInput,^{NumpadAdd}

	; ~LButton::	;20240326
		; GV_LongClickAction := "Send,{Click}{F4}"
		; GV_MouseButton := 1
		; gosub,Sub_ButtonLongPress
	; return

	`;::	;by Tuutg,20240623
	$F4::	;与TC一致,编辑|打开|F4Menu
		SendInput,{Click,Right}
		Sleep,100
		SendInput,{F}{Enter}
	return

	CapsLock & f::	;goin TC,右键>打开路径
		if MouseUnder("SysListView321"){
			SendInput,{Click,Right}{Down 2}
		} else{
			SendInput,{Right}
		}
	return

	$RButton::	;20240320
		;GV_MouseTimer := 300
		GV_KeyClickAction1 := "SendInput,{Click,Right}" ;系统默认,RButton
		GV_KeyClickAction2 := "SendInput,!{F4}"		;退出|关闭
		GV_MouseButton := 2
		GV_LongClickAction := "SendInput,{Click,Right}" ;系统默认,RButton
		gosub,Sub_MouseClick123
	return
}
#If

EverythingChooseType(ft){
	ControlGetText, searching, Edit1, A
	searching := ft . ":" . searching
	ControlSetText, Edit1, %searching%, A
	Sleep,500
	SendInput,{End}
}

EvToTcAClick:	;激活Tc并选中,即光标在文件上
	Clipboard := ""
	SetTitleMatchMode RegEx
	SetTitleMatchMode Slow
	ControlSend,,{Click},ahk_class EVERYTHING.*
	ControlSend,,^+c,ahk_class EVERYTHING.*
	Sleep,500
	; EzTip(Clipboard,1)
	SendInput,^{Enter}
	Run,"%COMMANDER_EXE%" /A /T /O /R="%Clipboard%"
	Sleep,500
	MyWinWaitActive("ahk_class TTOTAL_CMD")
return
;****************************************************
;****************************************************
;MPV播放器中 {{{2
#If WinActive("ahk_exe MPV.exe")
{
	;速度控制
	XButton1 & WheelUp::SendInput,[
	XButton2 & WheelUp::SendInput,[
	XButton1 & WheelDown::SendInput,]
	XButton2 & WheelDown::SendInput,]

	;4速切换
	XButton2 & RButton::SendInput,\
	XButton1 & RButton::SendInput,\

	;上一个/下一个
	XButton1::SendInput,.
	XButton2::SendInput,,

	$^RButton::ControlSend,,!x ;恢复默认1.00播放速度
	$!f::ControlSend,,!f ;在TC中定位到当前播放的文件
	$F1::ControlSend,,{F1} ;键位图|帮助
}
#If
;****************************************************
;****************************************************
;Qsel启动器 {{{2
#If WinActive("Qsel ahk_class WindowClass_0")
{
	XButton1::SendInput,{Tab}
	XButton2::SendInput,{BackSpace}
	WheelDown::SendInput,{Tab}
	WheelUp::SendInput,{BackSpace}
	Space & WheelDown::SendInput,{Tab}
	Space & WheelUp::SendInput,{BackSpace}
	$Space::Space	;恢复空格打字上屏,by Tuutg,20240425

	$RButton::
		;GV_MouseTimer := 300
		GV_KeyClickAction1 := "SendInput,{Click,Right}"
		GV_KeyClickAction2 := "SendInput,{Escape}"
		gosub,Sub_MouseClick123
	return
}
#If
;****************************************************
;****************************************************
;IrfanView 看图器 {{{2	by tuutg 20231209
#If (WinActive("ahk_class IrfanView") and (A_Cursor != "IBeam"))
{
	;IrfanView自身支持ctrl+滚轮，但alt更好按，也不用多想到底哪一个按键
	$q::	;by tuutg 2024/01/09
	$i::SendInput,{Home}
	; $w::	;软件默认,认幻灯片模式
	$e::	;by tuutg 2024/01/09
	$!WheelUp::SendInput,{NumpadAdd}	;+|放大
	; $r::	;软件默认,Rotate right,右旋|R90
	; $t::	;软件默认,缩略图模式

	$a::	;by tuutg 2024/01/09
	$u::SendInput,{End}

	$s::SendInput,{PgUp}	;by tuutg 20231209
	$f::SendInput,{PgDn}	;by tuutg 20231209
	$y::SendInput,{PgDn}
	;如果是动画，先按g暂停动画图片了后再按jk
	$j::SendInput,{PgDn}
	$k::SendInput,{PgUp}

	$d::	;by tuutg 2024/01/09
	$!WheelDown::SendInput,{NumpadSub}	;-|缩小
	.::SendInput,{NumpadAdd}	;-|缩小
	,::SendInput,{NumpadSub}	;+|放大
	`;::SendInput,{Esc}	;分号
	$g::	;by Tuutg,20240526
	$/::SendInput,^h	;原始大小|1:1

	$z::	;by Tuutg,20240526
	$\::SendInput,+w	;Fit|适应窗口大小
	$x::SendInput,!ii	;图片信息
	; $c::SendInput,^c	;默认,Capture dialog
	; $v::	;软件默认,垂直翻转
	; $h::	;软件默认,水平翻转
	; $b::	;软件默认,批量处理

	$1::SendInput,!ofn{Enter}	;按文件名排序
	$4::SendInput,!ofd{Enter}	;按日期排序
	$5::SendInput,!of{Up}{Enter}	;反向排序

	$!r::SendInput,{F2}	;重命名
	$!e::SendInput,!fo	;打开图片
	$!a::SendInput,+fs	;另存为 by tuutg 20231209

	$!1::SendInput,+1	;用PS打开,编辑图片,by Tuutg,20240528
	$!2::SendInput,+2	;TC资管打开图片目录
	$!w::SendInput,+2	;TC资管打开图片目录
	$!3::SendInput,+3	;用Jpeg打开查看图片
	$!g::SendInput,+3	;用Jpeg打开查看图片

	$!s::		;系统资管打开图片目录,by Tuutg,20240528
		SendInput,{Click}	;点击激活
		WinGetActiveTitle,Title	;必须设定显示全路径
		; FilePath := RegExReplace(Title, "[ - IrfanView (Zoom)]", "")
		; 使用正则表达式匹配路径
		PathRegex := "^(.*?)\s-\s.*$"
		; 解析字符串以获取路径
		FilePath := RegExReplace(Title, PathRegex, "$1")
		; 输出提取的路径
		; MsgBox , %FilePath%
		Run, Explorer.exe /select`, "%FilePath%"	;资管定位
	return
}
#If
;****************************************************
#If (WinActive("ahk_class IrfanViewThumbnails") and (A_Cursor != "IBeam"))	;缩略图模式
{
	j::SendInput,{Down}
	k::SendInput,{Up}
	h::SendInput,{Left}
	l::SendInput,{Right}
	d::SendInput,{Down}	;by tuutg 2024/01/09
	e::SendInput,{Up}	;by tuutg 2024/01/09
	s::SendInput,{Left}	;by tuutg 2024/01/09
	f::SendInput,{Right} ;by tuutg 2024/01/09
	x::ControlClick SysTreeView321 ;移动焦点至目录树
}
#If
;****************************************************
;****************************************************
;JPEGView  看图器 {{{2
#if WinActive("ahk_exe JPEGView.exe")
{
	!WheelDown::SendInput,{NumpadSub} ;-
	!WheelUp::SendInput,{NumpadAdd} ;+
	$!e::SendInput,^o	;打开图片

	$!s::SendInput,^w	;系统资管打开图片目录
	$!w::SendInput,^w	;系统资管打开图片目录
	$!r::SendInput,{F2}	;重命名

	$RButton::	;20240320
		;GV_MouseTimer := 300
		GV_KeyClickAction1 := "SendInput,{Click,Right}" ;系统默认,RButton
		GV_KeyClickAction2 := "SendInput,!{F4}"  ;保存|关闭
		GV_MouseButton := 2
		GV_LongClickAction := "SendInput,{Click,Right}" ;系统默认,RButton
		gosub,Sub_MouseClick123
	return

	$!1::SendInput,^e	;用PS打开,编辑图片,by Tuutg,20240528
	$!2::SendInput,w	;TC资管打开图片目录
	$!3::SendInput,+e	;用Imagine打开查看图片
	$!g::SendInput,+e	;用Imagine 打开查看图片
	$!4::SendInput,q	;用IrfanView打开查看图片
}
#if
;****************************************************
;****************************************************
;XnView MP 看图器 {{{2  ;by Tuutg,20240528
#If WinActive("XnView MP ahk_exe xnviewmp.exe") and (A_Cursor !="IBeam")
	;双击右键，发送退格，返回上一级目录 , 同资管一致
	$RButton::
		;GV_MouseTimer := 300
		GV_KeyClickAction1 := "SendInput,{Click,Right}"
		GV_KeyClickAction2 := "SendInput,{BackSpace}"
		GV_MouseButton := 2
		GV_LongClickAction := "SendInput,{Enter}"
		gosub,Sub_MouseClick123
	return
;****************************************************
	CapsLock & q::
		if (GV_KeyClick_Continuous(GV_KeyTimer)){
			ControlSend,,{PgUp} ;双击Ctrl+↑
		}
		else {
			ControlSend,,*	;单击1:1-100%显示
		}
	return

	$q::ControlSend,,!1 ;PS打开/需设置!1=PS
	$w::ControlSend,,!2 ;用TC打开文件,TG版自定义
	$!w::ControlSend,,!w ;用资源管理器打开文件,TG版自定义
	$!s::ControlSend,,!w ;用资源管理器打开文件,TG版自定义

	CapsLock & w::ControlSend,,^s ;保存
	$e::ControlSend,,{NumpadAdd} ;+号-放大
	;CapsLock & e::SendInput,{Up}
	$r::ControlSend,,+r ;右旋|R90
	;CapsLock & r::SendInput,{Del}
	$t::SendInput,+d ;加字
	;CapsLock & t::SendInput,{BackSpace}

	CapsLock & a::
		if (GV_KeyClick_Continuous(GV_KeyTimer)){
			ControlSend,,{PgDn} ;双击Ctrl+↓
		}
		else {
			ControlSend,,^+s ;另存
		}
	return
	$a::ControlSend,,^+s ;另存

	;CapsLock+s/f = <-/->, 上下翻页
	CapsLock & s::
	$s::ControlSend,,{Left} ;<-|PgUp
	CapsLock & f::
	$f::ControlSend,,{Right} ;->|PgDn

	$d::ControlSend,,`- ;-号-缩小
	CapsLock & d::SendInput,{Down} ;-号-缩小

	$g::ControlSend,,*	;单击1:1-100%显示

	$z::ControlSend,,{NumpadDiv} ;缩放到适应窗口
	$!+z::ControlSend,,^y	;重做,屏蔽火绒弹窗拦截冲突
	$x::ControlSend,,+x ;查看器模式-裁剪
	$!c::ControlSend,,!c ;复制到'''
	; $v::ControlSend,,v ;水平翻转,by Tuutg,20240530
	$!v::ControlSend,,!m ;移动到'''
	$b::ControlSend,,+l ;左旋|L90,和通用模式不同
	CapsLock & b::ControlSend,,+s ;大小调整

	$!LButton::ControlSend,,!{Enter} ;属性

	; ~$MButton::ControlSend,,^s ;保存文件
	$!e::ControlSend,,^o ;打开文件,Ctrl+O
#If
;****************************************************
;****************************************************
;ImagineW ahk_exe Imagine.exe {{{2	;by Tuutg,20240530
#If WinActive("ahk_class ImagineW")
$^!s::SendInput,!ft	;选择扫描仪
#If
;****************************************************
;****************************************************
;SumatraPDF {{{2
; in SumatraPDF-settings.txt edit FullPathInTitle = true
#if (WinActive("ahk_class SUMATRA_PDF_FRAME") and (A_Cursor != "IBeam"))
{
	$q::SendInput,^k		;显示命令面板
	$w::SendInput,^+{NumpadSub}	;向左旋转
	$e::SendInput,{NumpadAdd}	;+,放大
	$r::SendInput,^+{NumpadAdd}	;向右旋转
	$t::SendInput,{F8}		;显示/隐藏工具栏

	$a::SendInput,^s		;保存|另存
	$s::SendInput,{Text}p	;上一页
	$d::SendInput,{NumpadSub}	;-,缩小
	$f::SendInput,{Text}n		;下一页
	$g::SendInput,^g		;转到页面

	$z::SendInput,^0		;适合页面
	$x::SendInput,{F12}		;显示/隐藏书签
	$c::SendInput,{F11}		;进入/退出全屏
	$v::SendInput,{F5}		;进入/退出演示
	$b::SendInput,^b		;加入书签

	$!f::SendInput,^f		;页面查找
	$!g::ControlSend,,^f	;页面查找

	;调用F4menu来打开当前文件，如果一种扩展名有多个程序关联则是进行选择
	$!e::
		ClipSaved := ClipboardAll
		Clipboard =
		;调用sumatrapdf内对CmdCopyFilePath定义的快捷键来获取当前文件名
		;不在sumatra中设置不同程序打开方式是因为不能绿化，比不上F4menu中直观和集中管理
		Sendinput,^!+c
		ClipWait, 1
		pdfPath := Clipboard
		Run,%COMMANDER_PATH%\TOOLS\F4Menu\F4Menu.exe "%pdfPath%"
		Clipboard := ClipSaved
	Return

	;简单固定用pdf-xchangeEditor来打开当前文件
	$!w::
		ClipSaved := ClipboardAll
		Clipboard =
		sendinput,^!+c
		ClipWait, 1
		pdfPath := Clipboard
		;Run,D:\Tools\Office\pdf-xchangeEditor\PDFXEdit.exe "%pdfPath%"
		Run,%SOFTDIR%\Office\pdf-xchangeEditor\PDFXEdit.exe "%pdfPath%"
		;Run,%COMMANDER_PATH%\TOOLS\F4Menu\F4Menu.exe "%pdfPath%"
		Clipboard := ClipSaved
	Return

	;用其他软件打开编辑(E),如Acrobat
	$!d::
		Acrobat_Path =D:\Program Files (x86)\Adobe\Acrobat DC 2022\Acrobat DC\Acrobat\Acrobat.exe
		WinGetActiveTitle, title
		Clipboard= % title
		filenamenew := RegExReplace(Clipboard, "(.*(\.chm|\.pdf|\.epub)+).*","$1")
		if RegExMatch(filenamenew, ".pdf$"){
			Run, "%Acrobat_Path%" "%filenamenew%", ..\, Max,
			; Run, "FoxitReaderPortable.exe" "%filenamenew%", ..\, Max,
		}
		else if RegExMatch(filenamenew, ".chm$"){
			Run, "hh.exe" "%filenamenew%", ..\, Max,
		}
	return

	$^+w::SendInput,{Click,Right}{Up 2}{Enter} ;关闭其他标签

	CapsLock & w:: ;关闭标签|关闭其他标签,20240528
		GV_KeyClickAction1 := "SendInput,^w" ;关闭当前标签
		GV_KeyClickAction2 := "SendInput,{Click,Right}{Up 2}{Enter}" ;关闭其他标签
		gosub,Sub_KeyClick123
	return
}
#if
;****************************************************
;****************************************************
;稻壳阅读器 DOCBOX_PDF_FRAME {{{2 , 20240528
#if (WinActive("ahk_class DOCBOX_PDF_FRAME") and (A_Cursor != "IBeam"))
{
	$q::ControlSend,,^+h	;收藏夹管理,控件发送,屏蔽输入法拦截
	$w::SendInput,^+{NumpadSub}	;向左旋转
	$e::SendInput,{NumpadAdd}	;+,放大
	$r::SendInput,^+{NumpadAdd}	;向右旋转
	$t:: ;SendInput,{F8}		;显示/隐藏工具栏
		SendInput,{Click}	;点击激活
		WinGetPos,x,y,lengthA,hightA,A
		Sleep,100
		CoordWinMove(lengthA-100,25) ;保存按钮
		SendInput,{Click,Right}{Up 6}{Enter}
	return

	$a::SendInput,{Click,Right}{Up 11}{Enter}	;保存|另存
	$s::SendInput,{Text}p	;上一页
	$d::SendInput,{NumpadSub}	;-,缩小
	$f::SendInput,{Text}n		;下一页
	$g::SendInput,^g		;转到页面

	$z::SendInput,^0		;适合页面
	$x::SendInput,{F12}		;显示/隐藏书签
	$!x::SendInput,{F7}		;显示/隐藏收藏夹
	$c::SendInput,{F11}		;进入/退出全屏
	$v::SendInput,{F5}		;进入/退出演示|幻灯片
	$b::					;右键加入书签
		SendInput,{Click,Right}
		Sleep , 50
		SendInput,{Up 7}{Enter}
	return

	$!f::SendInput,^f		;页面查找
	$!g::ControlSend,,^f	;页面查找

	;用其他软件打开编辑(E),如Acrobat
	$!e::
	$!d::
		Acrobat_Path =D:\Program Files (x86)\Adobe\Acrobat DC 2022\Acrobat DC\Acrobat\Acrobat.exe
		SendInput,{Click,Right}{Up 8}{Enter}	;右键复制路径
		filenamenew := RegExReplace(Clipboard, "(.*(\.chm|\.pdf|\.epub)+).*","$1")
		; MsgBox , %filenamenew%
		if RegExMatch(filenamenew, ".pdf$"){
			Run, "%Acrobat_Path%" "%filenamenew%", ..\, Max,
			; Run, "FoxitReaderPortable.exe" "%filenamenew%", ..\, Max,
		}
		else if RegExMatch(filenamenew, ".chm$"){
			Run, "hh.exe" "%filenamenew%", ..\, Max,
		}
	return

	CapsLock & w:: ;关闭标签|关闭其他标签,20240528
		GV_KeyClickAction1 := "SendInput,^w" ;关闭当前标签
		GV_KeyClickAction2 := "SendInput,{Click,Right}{Up 2}{Enter}" ;关闭其他标签
		gosub,Sub_KeyClick123
	return
}
#if
;****************************************************
;****************************************************
;快速目录切换 {{{2
;收藏的目录，
;最近使用的目录,20240417
#If WinActive("ahk_group GroupDiagOpenAndSave") and !WinActive("跳至页面 ahk_exe Acrobat.exe") and !WinActive("ahk_group Group_CodeFind")
;QuickLook预览,20240401
$!q::	;20240402
	if WinExist(ahk_exe QuickLook.exe)
		gosub, QuickLook	;QuickLook预览
return

;从对话框中切换到tc，在tc中再选文件，然后alt+w再回来
$!w::gosub,Sub_SendCurDiagPath2Tc

;解决中文输入法输入英文上屏|点击保存|打开按钮,替代Caps+g
CapsLock & w::SendInput,{Enter}

$!e::SendInput,^+n	;新建文件夹，20240417
; $!r::Listary,历史文件夹
; $!t::预留
;****************************************************
$!a::ControlSend,,^a	;全选,替代^A
; $!s::	;和Ra搜索冲突,屏蔽注释掉

;替代原Listary->弹出菜单|中键(默认用做OK),20240413
Space & s::
if (A_Cursor ="IBeam") or (A_CaretX !=""){
	GV_KeyClickAction1 := "SendInput,{Left}"
	GV_KeyClickAction2 := "SendInput,`/" ;{?/}键
	gosub,Sub_KeyClick123
}
else {
	SendInput,!#s	;要在Listary中设定!#s=中键弹出菜单
}
return

; $!d::	;系统自带,定位到地址栏

;复制文件名->Listary自搜索->自动跳转
$<!MButton::
$!f::gosub,Listary_CyJp

;直接用tc中的地址（已经选好）
$!g::gosub,Sub_SendTcCurPath2Diag
;***************************************************
CapsLock & g:: ;SendInput,^g ;QuickJump或Listary跳转目标路径
if (GV_KeyClick_Continuous(GV_KeyTimer)){
	SendInput,{Enter}	;点击保存|打开按钮
}
else {
	; SendInput,^g	;QuickJump或Listary跳转转目标路径
	gosub,Space_SgGinDiagOAS ;动态:Enter|Listary跳转到目标路径,20240430
}
return
;****************************************************
$!z::SendInput,^z	;撤销,20240417
$+!z::SendInput,^y	;恢复,20240417
; $!x::	;预留
; $!c:: ;C-compress压缩,20240417
; $!v::Listary,智能命令
; Space & v::SendInput,^{Home}^+{End}^v{Enter}	;原版粘贴|+Enter
; CapsLock & v::SendInput,^v{Enter}	;原版,20240411
; $!b::预留Listary,浏览程序安装目录
;****************************************************
$Space::	;键位复用,20240402
if (A_Cursor ="IBeam") or (A_CaretX !=""){
	SendInput,{Blind}{Space}	;恢复空格打字
}
else {
	if WinExist(ahk_exe QuickLook.exe){
		gosub, QuickLook	;QuickLook预览
	}
	else if WinExist(ahk_exe Listary.exe) {
		gosub, Listary_CyJp ;Listary_CyJp
	}
	else {
		SendInput,{Blind}{Space}
	}
}
return
;****************************************************
$+MButton:: ;单击保存/确定/打开按钮/IDM下载
	ControlSend, Edit1, {Enter}
	Sleep,100
	if WinActive("下载文件信息 ahk_class #32770") ;IDM下载
		ControlClick,Button1
return
;****************************************************
;打开|保存对话框中,CapsLock=Listary_CyJp,快速自动跳转到Listary中
CapsLock::	;键位复用,文件名对话框做为分界,文本模式已上为{Esc},其他激活Listary
	Suspend Permit
	WinGetPos, x0, y0, w0, h0, A
	if (A_Cursor ="IBeam") or (A_CaretX !=""){
		if (A_CaretY<(h0-150)){
			SendInput,{Esc}	;20240417,纵坐标控制ESC,在文件名控件以上
		}
		else {
			gosub,Listary_CyJp	;20240418,纵坐标在文件名控件及以下
		}
	}
	else {
		GV_KeyClickAction1 := "gosub,Listary_CyJp" ;自搜索->自动跳转
		GV_KeyClickAction2 := "gosub,Listary"	;双击Ctrl激活Listary
		gosub,Sub_KeyClick123
	}
return
;****************************************************
$MButton::	;20240329,盲点默认按钮,一键快速确认|保存|打开
	;GV_MouseTimer := 300
	GV_KeyClickAction1 := "gosub,MButtonInIdm"	;OK|Save|默认按钮
	GV_KeyClickAction2 := "SendInput,{MButton}"	;Listary|中键菜单
	GV_MouseButton := 3
	GV_LongClickAction := "SendInput,{MButton}"	;Listary|中键菜单
	gosub,Sub_MouseClick123
return
#If
;****************************************************
Listary_CyJp:	;复制文件名->Listary自搜索->自动跳转
	; Clipboard=
	Sleep,100
	ControlGetText,Out,Edit1,A ; 复制文件名
	; Clipboard=%Out%

	;1.缩短文件名便于匹配更多结果
	Longfilename :=GetFileInfo(Out,2)	;获取文件全名,带后缀
	Filename :=GetFileInfo(Out,3)	;只获取文件名,20240412
	Extension := GetFileInfo(Longfilename,4)	;只获取扩展名,20240412
	; EzTip(extension,1)
	; 使用正则表达式去掉路径部分,去掉路径部分扩展名
	; ShortFilename := RegExReplace(Longfilename, "^.*\\", "")
	ShortFilename := RegExReplace(Longfilename, "\[www.ghxi.com\]", "")
	ClipBoard = %ShortFilename%	;带后缀,20240412
	EzTip(ShortFilename,1)
	; 使用正则表达式将文件名中的所有数字、减号、下划线替换为空
	gosub,QueryReplace	;by Tuutg,20240412,带后缀

	;2.激活Listary搜索框->输入搜索词->定位到下载目录
	;SendInput,#!s	;Listary复制粘贴文件名(Quicker-LY设定好#!s)
	SendInput,^g	;QuickJump或Listary跳转到资管当前路径
	gosub,Listary	;双击Ctrl激活Listary搜索框
	Sleep,1000	;延时等待,激活Listary搜索框
	; SendInput,{Text}%ListaryQuery% %Extension%	;20240412,输入搜索词,带后缀
	SendInput,{Text}%ListaryQuery%	;20240412,输入搜索词,无后缀,人工搜索,匹配更多目录
	Sleep,1000	;延时等待,搜索词匹配目标对象
	; ControlSend,,{Blind}^{Enter},SearchBarWindow ahk_exe Listary.exe ;适配Listary6.3.预留人工确定,20240330
	; SendInput,{Blind}^{Enter}	;适配Listary5.0,在Listary5中不可靠,改右键方式.预留人工确定,20240330
	; ControlSend,,{Right},ahk_exe Listary.exe	;适配Listary5
	; Sleep,1000
	; ControlSend,,{Enter},ahk_exe Listary.exe	;适配Listary5
return
;****************************************************
MButtonInIdm:	;Idm一步到下载,20240408
	if WinActive("另存为 ahk_class #32770 ahk_exe IDMan.exe"){
		SendInput,{Enter}{Text}s	;一步到为,自动下载
	}
	else {
		SendInput,{Enter}	;默认确认
	}
return
;****************************************************
;****************************************************
#if WinActive("ahk_group GroupDiagOpenAndSave") or WinActive("ahk_exe explorer.exe") or WinActive("ahk_group Group_explorer")
	$!n::	;by Tuutg,20240619
	$F7:: ;空白处右键/新建文件夹,可获取光标处取文件名,模仿TC,20240323
		Clipboard_Save = %ClipboardAll%
		SendInput,^c
		Sleep,300
		Clipboard_New = %Clipboard%
		Clipboard:=GetFileInfo(Clipboard,3)	;只获光标处取文件名
		Sleep,300
		if WinActive("ahk_exe explorer.exe"){
			SendInput,^+n	;新建文件夹
			Sleep,300
			if (Clipboard_Save==Clipboard_New){
				;光标在空白处
				illegalChars := "[\\\\/:*?\""<>|]"	;去除重命名非法字符
				Clipboard_New :=RegExReplace(Clipboard_New,illegalChars," ")
				SendInput,{Text}%Clipboard_New%-0	;新建文件夹-0
			}
			else {
				; SendInput,{Blind}^v{End}{Text}-0	;光标处文件名+尾数0
				SendInput,{Text}%Clipboard%	;光标处文件名+尾数0
				Sleep 100
				SendInput,{End}{Text}-0	;光标处文件名+尾数0
			}
			return
		}
		else {
			SendInput,{Click,Right}
			Sleep 100
			ControlSend,,{Text}{w},ahk_class #32768
			Sleep 100
			ControlSend,,{Text}{f},ahk_class #32768
			Sleep 100
			if (Clipboard_Save==Clipboard_New){
				;光标在空白处
				illegalChars := "[\\\\/:*?\""<>|]"	;去除重命名非法字符
				Clipboard_New :=RegExReplace(Clipboard_New,illegalChars," ")
				SendInput,{Text}%Clipboard_New%-0	;新建文件夹-0
			}
			else {
				; SendInput,{Blind}^v{End}{Text}-0	;光标处文件名+尾数0
				SendInput,{Text}%Clipboard%	;光标处文件名+尾数0
				Sleep 100
				SendInput,{End}{Text}-0	;光标处文件名+尾数0
			}
			return
		}
		Clipboard =%clipboard_save% 	;还原初始剪贴板
	return
;****************************************************
	$F8:: ;快速删除,Win7自动确认,20240323
		SendInput,{Delete}
		Sleep 100
		if (A_OSVersion in WIN_2003, WIN_XP, WIN_7)
		{
			SendInput,{Enter}	;Win1X中del删除不用确认
		}
	return
;****************************************************
	$F3::gosub, QuickLook ;QuickLook预览或;用做贴图
;****************************************************
	$^+F2::	;剪切板重命名,2023/12/07 18:40
	FileName := Clipboard ; 保存剪贴板内容到变量中
	if (FileName != " "){ ;使用剪贴板内容重命名
		Send, {F2} ; 激活重命名命令
		Sleep, 50 ; 等待重命名框出现
		illegalChars := "[\\\\/:*?\""<>|]"	;去除重命名非法字符
		Clipboard :=RegExReplace(FileName,illegalChars," ")
		Send, %Clipboard% ; 输入新的文件名
		Sleep, 50 ; 等待重命名完成
		Clipboard := " " ;清空剪贴板
	}
	return
;****************************************************
	$^F2:: ;追加剪切板重命名
	{
		SendInput,{F2}
		Sleep,100
		SendInput,{Right}
		Sleep,100
		SendInput,{NumpadSub}^v
		Sleep,100
		;SendInput,{Enter}
	}
	return
;****************************************************
	$!c:: ;C-compress压缩,右键>解|压,20240417,需要安装Rar软件
	$^e:: ;一键解压|压缩,类名:ahk_class #32768. by Tuutg,20240625
	{
		SendInput,{Click,Right}
		Sleep 100	;系统右键,用ControlSend,避免Listary拦截菜单选项输入
		ControlSend,,{Text}{w},ahk_class #32768	;防止激活Listary,拦截输入
		Sleep 100
		ControlSend,,{Text}{e},ahk_class #32768	;防止激活Listary,拦截输入
		Sleep 100
		ControlSend,,{Text}{t},ahk_class #32768	;防止激活Listary,拦截输入
		Sleep 100
	}
	return
;****************************************************
	$!MButton:: ;一键隐藏文件
	{
		SendInput,!{Enter}
		Sleep,100
		ControlClick, x35 y45, 属性 ahk_class #32770
		Sleep,250
		SendInput,!h
		Sleep,100
		SendInput,{Enter}
		; Sleep,100
		; SendInput,{Enter}
	}
	return
;****************************************************
	;双击右键，发送退格，返回上一级目录, Win10:SendInput,!{Up}|{BackSpace}
	$RButton::
		;GV_MouseTimer := 300
		GV_KeyClickAction1 := "SendInput,{Click,Right}"	;系统默认,RButton
		GV_KeyClickAction2 := "SendInput,{BackSpace}"	;退回上一级目录
		GV_MouseButton := 2
		GV_LongClickAction := "SendInput,{ESC}"
		gosub,Sub_MouseClick123
	return
;****************************************************
	$!RButton::SendInput,!{Right}	;前进下一级目录
	$!LButton:: ;SendInput,!{Left}	;退回上一级目录
		;GV_MouseTimer := 300
		GV_KeyClickAction1 := "SendInput,!{Left}" ;退回上一级目录
		GV_KeyClickAction2 := "SendInput,!{Enter}" ;文件属性
		GV_MouseButton := 1
		GV_LongClickAction := "SendInput,{ESC}"
		gosub,Sub_MouseClick123
	return
#If

;****************************************************
QuickLook: ;QuickLook预览
{
	Sleep,100
	SendInput,^c
	Sleep,300
	clip:=
	clip:=Clipboard
	Sleep,100
	Run, "%RunCaptX%\SoftDir\QuickLook\QuickLook.exe" "%clip%"
	Sleep,100
	clip:=
}
return
;****************************************************
;****************************************************
;在TC中打开对话框的路径
Sub_SendCurDiagPath2Tc:
	WinActivate, ahk_class TTOTAL_CMD
	/*
	;WinGetText, CurWinAllText
	;;MsgBox, The text is:`n%CurWinAllText%
	;loop, Parse, CurWinAllText, `n, `r
	;{
		;if RegExMatch(A_loopField, "^地址: "){
		;curDiagPath := SubStr(A_loopField,4)
		;break
		;}
	;}
	;WinActivate, ahk_class TTOTAL_CMD
	;ControlSetText, Edit1, cd %curDiagPath%, ahk_class TTOTAL_CMD
	;Sleep 900
	;ControlSend, Edit1,{Enter}, ahk_class TTOTAL_CMD
	*/
return

;将tc中路径发送到对话框
Sub_SendTcCurPath2Diag:
	;开关： 将剪贴板中内容作为文件名
	B_Clip2Name := false
	;开关： 是否改大对话框
	B_ChangeDiagSize := false ;不改变对话框大小

	ControlGetText, orgFileName,Edit1

	;先获取TC中当前路径
	clip:=Clipboard
	Clipboard =
	TcSendPos(CM_CopySrcPathToClip)
	;TcSendPos(CM_CopyFullNamesToClip)

	ClipWait, 1
	tcSrcPath := Clipboard
	Clipboard:=clip

	;处理例如根目录c:\就不用额外添加\
	if(SubStr(tcSrcPath, StrLen(tcSrcPath))!="`\"){
		tcSrcPath := tcSrcPath . "`\"
	}

	ControlFocus, Edit1,
	Sleep 200
	SendInput,{BackSpace}
	Sleep 300
	SetKeyDelay, 10,10
	ControlSetText, Edit1, %tcSrcPath%
	Sleep 900
	SendInput,{Enter}
	Sleep 500

	if(B_Clip2Name){
		ControlSetText, Edit1, %clip%,A
	}
	else {
		ControlSetText, Edit1, %orgFileName%
	}

	;ControlSetText, Edit1, %text%,A

	if(B_ChangeDiagSize){
		;WinGetPos, xTB, yTB,lengthTB,hightTB, ahk_class Shell_TrayWnd
		;改变对话框大小，省事就直接移动到100,100的位置，然后85%屏幕大小，否则就要详细结算任务栏在上下左右的位置
		WinMove, A,,80,80, A_ScreenWidth * 0.85, A_ScreenHeight * 0.85
	}
return

;构建对话框中菜单
Sub_Menu2Diag:
	;左边历史
	;右边历史
	;hotdir
return

;Totalcmd历史记录 {{{2
;添加按照ini读取的启动菜单，接管`按键
;剪贴板增强
;固定文本条目增强
;#Persistent
TC_azHistory()
{
	if RegExMatch(COMMANDER_EXE, "i)totalcmd64\.exe$")
	{
		TCListBox := "LCLListBox"
		TCEdit := "Edit2"
		TInEdit := "TInEdit1"
		TCPanel1 := "Window1"
		TCPanel2 := "Window11"
		TCPathPanel := "TPathPanel2"
	}
	else
	{
		TCListBox := "TMyListBox"
		TCEdit := "Edit1"
		TInEdit := "TInEdit1"
		TCPanel1 := "TPanel1"
		TCPanel2 := "TMyPanel8"
		TCPathPanel := "TPathPanel1"
	}

	;<cm_ConfigSaveDirHistory>
	TcSendPos(582)
	Sleep, 200
	history := ""
	TCINI := COMMANDER_PATH . "\wincmd.ini"
	;msgbox % TCINI
	tcLeftRight := fun_TcGet(1000)
	;msgbox % tcLeftRight
	if tcLeftRight = 1
	{
		IniRead, history, %TCINI%, LeftHistory
		if RegExMatch(history, "RedirectSection=(.+)", HistoryRedirect)
		{
			StringReplace, HistoryRedirect1, HistoryRedirect1, `%COMMANDER_PATH`%, %COMMANDER_PATH%
			IniRead, history, %HistoryRedirect1%, LeftHistory
		}
	}
	else if tcLeftRight = 2
	{
		IniRead, history, %TCINI%, RightHistory
		if RegExMatch(history, "RedirectSection=(.+)", HistoryRedirect)
		{
			StringReplace, HistoryRedirect1, HistoryRedirect1, `%COMMANDER_PATH`%, %COMMANDER_PATH%
			IniRead, history, %HistoryRedirect1%, RightHistory
		}
	}
	history_obj := []
	Global history_name_obj := []
	;loop, Parse, history, `n
	;max := A_index
	loop, Parse, history, `n
	{
		idx := RegExReplace(A_loopField, "=.*$")
		value := RegExReplace(A_loopField, "^\d\d?=")
		;避免&被识别成快捷键
		value := RegExReplace(value, "\t.*$")
		name := StrReplace(value, "&", ":＆:")
		;msgbox % name
		if RegExMatch(Value, "::\{20D04FE0\-3AEA\-1069\-A2D8\-08002B30309D\}\|")
		{
			name := RegExReplace(Value, "::\{20D04FE0\-3AEA\-1069\-A2D8\-08002B30309D\}\|")
			value := 2122
		}
		if RegExMatch(Value, "::\|")
		{
			name := RegExReplace(Value, "::\|")
			value := 2121
		}
		if RegExMatch(Value, "::\{21EC2020\-3AEA\-1069\-A2DD\-08002B30309D\}\\::\{2227A280\-3AEA\-1069\-A2DE\-08002B30309D\}\|")
		{
			name := RegExReplace(Value, "::\{21EC2020\-3AEA\-1069\-A2DD\-08002B30309D\}\\::\{2227A280\-3AEA\-1069\-A2DE\-08002B30309D\}\|")
			value := 2126
		}
		if RegExMatch(Value, "::\{208D2C60\-3AEA\-1069\-A2D7\-08002B30309D\}\|") ;NothingIsBig的是XP系统，网上邻居是这个调整
		{
			name := RegExReplace(Value, "::\{208D2C60\-3AEA\-1069\-A2D7\-08002B30309D\}\|")
			value := 2125
		}
		if RegExMatch(Value, "::\{F02C1A0D\-BE21\-4350\-88B0\-7367FC96EF3C\}\|")
		{
			name := RegExReplace(Value, "::\{F02C1A0D\-BE21\-4350\-88B0\-7367FC96EF3C\}\|")
			value := 2125
		}
		if RegExMatch(Value, "::\{26EE0668\-A00A\-44D7\-9371\-BEB064C98683\}\\0\|")
		{
			name := RegExReplace(Value, "::\{26EE0668\-A00A\-44D7\-9371\-BEB064C98683\}\\0\|")
			value := 2123
		}
		if RegExMatch(Value, "::\{645FF040\-5081\-101B\-9F08\-00AA002F954E\}\|")
		{
			name := RegExReplace(Value, "::\{645FF040\-5081\-101B\-9F08\-00AA002F954E\}\|")
			value := 2127
		}
		name := "&" . Chr(idx+65) . " " . name
		history_obj[idx] := name
		history_name_obj[name] := value
	}
	Menu, az, UseErrorLevel
	Menu, az, add
	Menu, az, deleteall
	MaxItem := 26
	loop, %MaxItem%
	{
		idx := A_Index - 1
		name := history_obj[idx]
		Menu, az, Add, %name%, azHistorySelect
	}
	Menu, az, Add, [&4] 关闭,azHistoryDeleteAll	;by tuutg 2024/01/09
	ControlGetFocus, TLB, ahk_class TTOTAL_CMD
	ControlGetPos, xn, yn, wn, , %TLB%, ahk_class TTOTAL_CMD
	Menu, az, show, %xn%, %yn%
}

azHistoryDeleteAll:
	Menu, az, DeleteAll
return

azHistorySelect:
	azHistorySelect()
return

azHistorySelect()
{
	Global history_name_obj
	if ( history_name_obj[A_ThisMenuItem] = 2122 ) or RegExMatch(A_ThisMenuItem, "::\{20D04FE0\-3AEA\-1069\-A2D8\-08002B30309D\}")
		TcSendPos(cm_OpenDrives)
	else if ( history_name_obj[A_ThisMenuItem] = 2121 ) or RegExMatch(A_ThisMenuItem, "::(?!\{)")
		TcSendPos(cm_OpenDesktop)
	else if ( history_name_obj[A_ThisMenuItem] = 2126 ) or RegExMatch(A_ThisMenuItem, "::\{21EC2020\-3AEA\-1069\-A2DD\-08002B30309D\}\\::\{2227A280\-3AEA\-1069\-A2DE\-08002B30309D\}")
		TcSendPos(cm_OpenPrinters)
	else if ( history_name_obj[A_ThisMenuItem] = 2125 ) or RegExMatch(A_ThisMenuItem, "::\{F02C1A0D\-BE21\-4350\-88B0\-7367FC96EF3C\}") or RegExMatch(A_ThisMenuItem, "::\{208D2C60\-3AEA\-1069\-A2D7\-08002B30309D\}\|") ;NothingIsBig的是XP系统，网上邻居是这个调整
		TcSendPos(cm_OpenNetwork)
	else if ( history_name_obj[A_ThisMenuItem] = 2127 ) or RegExMatch(A_ThisMenuItem, "::\{645FF040\-5081\-101B\-9F08\-00AA002F954E\}")
		TcSendPos(cm_OpenRecycled)
	else
	{
		ThisMenuItem := StrReplace(A_ThisMenuItem, ":＆:", "&")
		ThisMenuItem := RegExReplace(ThisMenuItem, "^&[A-Z] ")
		TcSendPos(CM_EditPath)
		Sleep,300
		ControlSetText, %TInEdit%, %ThisMenuItem%, ahk_class TTOTAL_CMD
		Sleep,300
		ControlSend, %TInEdit%, {Enter}, ahk_class TTOTAL_CMD
	}
}

MenuHandler:
	MsgBox You selected %A_ThisMenuItem% from the Menu %A_ThisMenu%.
return
;快速目录切换$
;*************** 各程序快捷键或功能$ ****************


;****************【初始化菜单|设置】*****************
;****************       Menu^       *****************
CreatTrayMenu:
	Menu,Tray,NoStandard
	Menu,Tray,add,设置Capstg(&T),Settings_Gui
	Menu,Tray,add,Edit编辑脚本(&E),Menu_Edit
	Menu,Tray,add,Window Spy(&C),Menu_Debug
	Menu,Tray,add,AHK网络帮助文档|Caps+F1(&H),Menu_Document
	Menu,Tray,add,Open自身监视(&F),Menu_Open
	Menu,Tray,add
	Menu,Tray,add,开启或关闭随系统自动启动(&Q),Menu_AutoStart
	Menu,Tray,add,添加或去除绿软SoftDir变量(&D),Menu_SoftDir
	Menu,Tray,add
	Menu,Tray,add,拾遗补缺的绿化(&G),Menu_GreenPath
	Menu,Tray,add
	Menu,Tray,add,重启脚本|Caps+F5(&R),Menu_Reload
	Menu,Tray,add
	Menu,Tray,add,暂停热键(&S),Menu_Suspend
	Menu,Tray,add,暂停脚本(&A),Menu_Pause
	Menu,Tray,add,退出脚本(&X),Menu_Exit
return


Menu_Open:
	ListLines
return

Menu_Edit:
	;Edit
	;Run,%COMMANDER_PATH%\Tools\notepad\Notepad3.exe %A_ScriptFullPath%
	Run,%RunCaptX%\SoftDir\AutoHotkey\SciTE\SciTE.exe "%A_ScriptFullPath%"
	;Run,%COMMANDER_PATH%\Tools\AutoHotkey\SciTE\SciTE.exe "%A_ScriptFullPath%"
return

Menu_Debug:
	Run,%COMMANDER_PATH%\AU3_Spy.exe
return

; CapsLock & F1::
Menu_Document:
	;Run,hh.exe %COMMANDER_PATH%\AutoHotkey.chm
	Run,https://wyagd001.github.io/zh-cn/docs/
return

Menu_Reload:
	gosub,ForceSelfReload
return

Menu_AutoStart:
	if A_Is64bitOS
		SetRegView 64
	; RegRead, OutputVar, HKEY_LOCAL_MACHINE, Software\Microsoft\Windows\CurrentVersion\Run, Capsez
	RegRead, OutputVar, HKEY_LOCAL_MACHINE, Software\Microsoft\Windows\CurrentVersion\Run, Capstg
	if OutputVar
	{
		; RegDelete, HKEY_LOCAL_MACHINE, Software\Microsoft\Windows\CurrentVersion\Run, Capsez
		RegDelete, HKEY_LOCAL_MACHINE, Software\Microsoft\Windows\CurrentVersion\Run, Capstg
		EzTip("已关闭Capstg随系统自动启动",2)
	}
	else
	{
		; RegWrite, REG_SZ, HKEY_LOCAL_MACHINE, Software\Microsoft\Windows\CurrentVersion\Run, Capsez, "%A_AhkPath%"
		RegWrite, REG_SZ, HKEY_LOCAL_MACHINE, Software\Microsoft\Windows\CurrentVersion\Run, Capstg, "%A_AhkPath%"
		EzTip("已设置Capstg随系统自动启动",2)
	}
return

Menu_SoftDir:
	if A_Is64bitOS
		SetRegView 64
		RegRead, OutputVar, HKEY_LOCAL_MACHINE, SYSTEM\CurrentControlSet\Control\Session Manager\Environment, SoftDir
	if OutputVar
	{
		RegDelete, HKEY_LOCAL_MACHINE, SYSTEM\CurrentControlSet\Control\Session Manager\Environment, SoftDir
		EzTip("已去掉SoftDir环境变量",2)
	}
	else
	{
		RegWrite, REG_SZ, HKEY_LOCAL_MACHINE, SYSTEM\CurrentControlSet\Control\Session Manager\Environment, SoftDir, %SOFTDIR%
		EzTip("已添加SoftDir环境变量",2)
	}
return

;Menu_RightMenu:
	;if A_Is64bitOS
		;SetRegView 64
	;RegRead, OutputVar, HKEY_LOCAL_MACHINE, SYSTEM\CurrentControlSet\Control\Session Manager\Environment, SoftDir
	;if OutputVar
	;{
		;RegDelete, HKEY_LOCAL_MACHINE, SYSTEM\CurrentControlSet\Control\Session Manager\Environment, SoftDir
		;eztip("已去掉SoftDir环境变量",10)
	;}
	;else
	;{
		;RegWrite, REG_SZ, HKEY_LOCAL_MACHINE, SYSTEM\CurrentControlSet\Control\Session Manager\Environment, SoftDir, %SOFTDIR%
		;eztip("已添加SoftDir环境变量",10)
	;}
;return


Menu_GreenPath:
	;1、newfile中的模板，Tools\NewFiles\NewFiles.ini
	p := COMMANDER_PATH . "\Tools\NewFiles\Templates"
	IniWrite, %p%, %COMMANDER_PATH%\Tools\NewFiles\NewFiles.ini, FileList,TemplatePath

	;2、everything的Everything.ini
	p := "$exec(""" . COMMANDER_PATH . "\" . COMMANDER_NAME . """ /A /T /O /R=""%1"")"
	IniWrite, %p%, %COMMANDER_PATH%\Everything.ini, Everything,open_folder_command2
	IniWrite, %p%, %COMMANDER_PATH%\Everything.ini, Everything,open_path_command2
	p := "$exec(""" . COMMANDER_PATH . "\Tools\F4Menu\F4Menu.exe"" ""%1"")"
	IniWrite, %p%, %COMMANDER_PATH%\Everything.ini, Everything,explore_command2
	; IniWrite, %p%, %COMMANDER_PATH%\Everything.ini, Everything,explore_path_command2

	;3、tcmatch.ini
	p := "Long description@" . COMMANDER_PATH . "\Plugins\WDX\FileDiz\FileDiz.wdx"
	IniWrite, %p%, %COMMANDER_PATH%\tcmatch.ini, wdx, wdx_text_plugin3
return

Menu_Suspend:
	Menu,tray,ToggleCheck,暂停热键(&S)
	Suspend
return

Menu_Pause:
	Menu,tray,ToggleCheck,暂停脚本(&A)
	Pause
return

Menu_Exit:
	ExitApp
return

Quit:
	ExitApp
return
;****************       Menu$       *****************

;****************       Vim^        *****************
;vim: textwidth=120 wrap tabstop=4 shiftwidth=4
;vim: foldmethod=marker fdl=0
;****************       Vim$        *****************

;****************  Settings_Gui^    *****************
Settings_Gui:
	Critical, On ;防止短时间内打开多次界面出现问题
	Thread, NoTimers,true
	HotKeyFlag:=MenuVarFlag:=OpenExtFlag:=AdvancedConfigFlag:=false
	GUI_WIDTH_66=700
	TAB_WIDTH_66=680
	GROUP_WIDTH_66=660
	GROUP_LISTVIEW_WIDTH_66=650
	GROUP_CHOOSE_EDIT_WIDTH_66=580
	GROUP_ICON_EDIT_WIDTH_66=550
	MARGIN_TOP_66=15
	Gui,66:Destroy
	Gui,66:default
	Gui,66:+Resize
	Gui,66:Margin,30,20
	Gui,66:Font,,Microsoft YaHei

	;CapsTg基本设置
	Gui,66:Add,Tab3,x10 y10 w%TAB_WIDTH_66%,CapsTg基本设置
	Gui,66:Tab,1

	;1.RunCaptX安装目录
	Gui,66:Add,GroupBox,xm-10 y+18 w%GROUP_WIDTH_66% h60, 1. RunCaptX安装目录, 默认D:\RunCaptX, 不要改动位置.如放U盘运行,同步调整1-2-3项
	Gui,66:Add,Button,xm yp+18 GSetRunCaptX,RunCaptX安装目录
	Gui,66:Add,Edit,x+11 yp+2 w482 r1 vRunCaptX,%RunCaptX%

	;2.TOTALCMD安装目录
	Gui,66:Add,GroupBox,xm-10 y+18 w%GROUP_WIDTH_66% h60, 2. TOTALCMD(64).exe所在目录, 默认D:\RunCaptX\SoftDir\Totalcmd\SoftDir\totalcmd_ez
	Gui,66:Add,Button,xm yp+18 GSetTcDir,TOTALCMD安装目录
	Gui,66:Add,Edit,x+11 yp+2 w498 r1 vTcDir,%COMMANDER_PATH%

	;3.Tg截图保存目录
	Gui,66:Add,GroupBox,xm-10 y+18 w%GROUP_WIDTH_66% h60, 3. Tg截图保存目录, 默认D:\RunCaptX\PixPinTg截图, 可自定义
	Gui,66:Add,Button,xm yp+18 GSetScreenShotDir,Tg截图保存目录
	Gui,66:Add,Edit,x+11 yp+2 w525 r1 vScreenShotDir,%ScreenShotPath%

	;4.QQ|Tim文件接收目录
	Gui,66:Add,GroupBox,xm-10 y+18 w%GROUP_WIDTH_66% h60, 4. QQ|Tim文件接收目录(带\), 默认X:\Users\Personal\Tencent Files\自己的QQ号\FileRecv\
	Gui,66:Add,Button,xm yp+18 GSetQQTimFPath,QQ|Tim文件接收目录
	Gui,66:Add,Edit,x+11 yp+2 w495 r1 vQQTimFPath,%QQTimFPath%

	;5.微信|wechat文件接收目录
	Gui,66:Add,GroupBox,xm-10 y+18 w%GROUP_WIDTH_66% h60, 5. 微信|wechat文件接收目录(带\), 默认X:\Users\XXX\Personal\WeChat Files\
	Gui,66:Add,Button,xm yp+18 GSetWechatFpath,微信|wechat文件接收目录
	Gui,66:Add,Edit,x+11 yp+2 w475 r1 vWechatFpath,%WechatFpath%

	;6.微信|wechat安装路径
	Gui,66:Add,GroupBox,xm-10 y+18 w%GROUP_WIDTH_66% h60, 6. wechat.exe所在路径, 默认D:\Program Files (x86)\Tencent\WeChat\WeChat.exe.如不是请填写正确的微信安装路径
	Gui,66:Add,Button,xm yp+18 GSetWx_Path,微信|wechat安装路径
	Gui,66:Add,Edit,x+11 yp+2 w498 r1 vWx_Path,%Wx_Path%

	;7.启用光标下滚轮
	Gui,66:Add,GroupBox,xm-10 y+18 w%GROUP_WIDTH_66% h60, 7. 启用光标下滚轮,(Win7设置为1,Win7以上设置为0)
	Gui,66:Add,Text,xm yp+18 ,启用光标下滚轮
	Gui,66:Add,Edit,x+11 yp+2 w540 r1 vWheelOnCursor,%GV_ToggleWheelOnCursor%

	;8.启用自动保存,GV_ToggleAotuSaveMode=1
	Gui,66:Add,GroupBox,xm-10 y+18 w%GROUP_WIDTH_66% h60, 8. 启用自动保存功能,(启用设置为1,按Tab|Caps|Ctrl+G|Space+G|Enter激活自动保存,适用编程,office,wps等)
	Gui,66:Add,Text,xm yp+18 ,启用自动保存
	Gui,66:Add,Edit,x+11 yp+2 w540 r1 vAotuSaveMode,%GV_ToggleAotuSaveMode%

	;9.电摩合格证车型配置序号,20240531
	Gui,66:Add,GroupBox,xm-10 y+18 w%GROUP_WIDTH_66% h60, 9. 电摩合格证车型配置序号,默认:02,两位数字,见车型配置对照表
	Gui,66:Add,Text,xm yp+18 ,电摩合格证车型配置序号
	Gui,66:Add,Edit,x+11 yp+2 w485 r1 vVehicleModelNum0,%VehicleModelNum0%

	;10.最下方4个按键,y控制垂直方向位置
	Gui,66:Add,Button,x150 y630 w75  ggOK,确定(&S)
	Gui,66:Add,Button,x250 y630 w100 ggOpen,打开配置文件(&F)
	Gui,66:Add,Button,x375 y630 w100 ggSpic,配置简图|帮助(&T)
	Gui,66:Add,Button,x500 y630 w75  ggCancel,取消(&C)

	Gui,66:Tab	;即后续添加的控件将不属于前面那个选项卡控件
	Gui,66:Show, w%GUI_WIDTH_66% h670 ;整体高度
	Critical,Off
return

;按键对应动作的跳转标签
;1.RunCaptX安装目录
SetRunCaptX:
	FileSelectFolder, dir, , 1
	if(dir){
		GuiControl,, RunCaptX, %dir%
	}
	Gui,66:Submit, NoHide
return

;2.TOTALCMD安装目录
SetTcDir:
	FileSelectFolder, dir, , 0
	if(dir){
		GuiControl,, TcDir, %dir%
	}
	Gui,66:Submit, NoHide
return

;3.Tg截图保存目录
SetScreenShotDir:
	FileSelectFolder, dir, , 1
	if(dir){
		GuiControl,, ScreenShotDir, %dir%
	}
	Gui,66:Submit, NoHide
return

;4.QQ|Tim文件接收目录
SetQQTimFPath:
	FileSelectFolder, dir, , 1
	if(dir){
		GuiControl,, QQTimFPath, %dir%
	}
	Gui,66:Submit, NoHide
return

;5.微信|wechat文件接收目录
SetWechatFpath:
	FileSelectFolder, dir, , 1
	if(dir){
		GuiControl,, WechatFpath, %dir%
	}
	Gui,66:Submit, NoHide
return

;6.微信|wechat安装目录
SetWx_Path:
	FileSelectFolder, dir, , 1
	if(dir){
		GuiControl,, Wx_Path, %dir%
	}
	Gui,66:Submit, NoHide
return

;8.最下方4个按键
gOK:
	Critical, On
	Thread, NoTimers,True
	Gui,66: Submit

	;获取配置文件变量赋值
	GuiControlGet, RunCaptX, , RunCaptX
	GuiControlGet, COMMANDER_PATH, , TcDir
	GuiControlGet, ScreenShotPath, , ScreenShotDir
	GuiControlGet, QQTimFPath, , QQTimFPath
	GuiControlGet, WechatFpath, , WechatFpath
	GuiControlGet, Wx_Path, , Wx_Path
	GuiControlGet, GV_ToggleWheelOnCursor, , WheelOnCursor
	GuiControlGet, GV_ToggleAotuSaveMode, , AotuSaveMode
	GuiControlGet, VehicleModelNum0, ,VehicleModelNum0

	FileDelete, %INI%

	;修改后保存配置文件,自动重启
	IniWrite, %RunCaptX%,   			%INI%, 基本设置, RunCaptX
	IniWrite, %COMMANDER_PATH%, 		%INI%, 基本设置, COMMANDER_PATH
	IniWrite, %ScreenShotPath%, 		%INI%, 基本设置, ScreenShotPath
	IniWrite, %QQTimFPath%,     		%INI%, 基本设置, QQTimFPath
	IniWrite, %WechatFpath%,    		%INI%, 基本设置, WechatFpath
	IniWrite, %Wx_Path%,        		%INI%, 基本设置, Wx_Path
	IniWrite, %GV_ToggleWheelOnCursor%, %INI%, 基本设置, GV_ToggleWheelOnCursor
	IniWrite, %GV_ToggleAotuSaveMode%, %INI%, 基本设置, GV_ToggleAotuSaveMode	;20240325,自动保存
	IniWrite, %VehicleModelNum0%,		%INI%,基本设置,VehicleModelNum0	;20240531,车型配置序号
	;MsgBox TOTALCMD安装目录: %COMMANDER_PATH%

	gosub, Menu_Reload
	Critical, Off
return

gOpen:
	Ini_Run(INI)
return

gCancel:
	Gui,66:Destroy
return

gSpic:
	Run, %RunCaptX%\Capstg帮助\Capstg目录自定义设置\Capstg目录自定义设置.jpg
	Run, %RunCaptX%\Capstg帮助\Capstg键鼠增强键位图\核心用法帮助文档.pdf
return
;****************************************************
Ini_Run(ini){
	try {
		if(!FileExist(ini)){
			MsgBox,16,%ini%,没有找到配置文件：%ini%
		}
			Run,"%ini%"
	} catch {
		Run,notepad.exe "%ini%"
	}
}

Label_ReadINI: ; 读取INI配置文件
	if !FileExist(INI)
	gosub,Label_Init_INI

	; 读取基本设置文件

	IniRead, RunCaptX,    	  %INI%, 基本设置, RunCaptX,   	   D:\RunCaptX
	IniRead, COMMANDER_PATH,  %INI%, 基本设置, COMMANDER_PATH, D:\RunCaptX\SoftDir\Totalcmd\SoftDir\totalcmd_ez
	IniRead, ScreenShotPath,  %INI%, 基本设置, ScreenShotPath, D:\RunCaptX\PixPinTg截图
	IniRead, QQTimFPath	,     %INI%, 基本设置, QQTimFPath,     E:\Users\Tuutg\Personal\Tencent Files\542114344\FileRecv\
	IniRead, WechatFpath,     %INI%, 基本设置, WechatFpath,    E:\Users\Tuutg\Persona\WeChat Files\
	IniRead, Wx_Path	,     %INI%, 基本设置, Wx_Path,        D:\Program Files\Tencent\WeChat\WeChat.exe

	IniRead, GV_ToggleWheelOnCursor, %INI%, 基本设置, GV_ToggleWheelOnCursor, 0	;1-打开
	IniRead, GV_ToggleAotuSaveMode, %INI%, 基本设置, GV_ToggleAotuSaveMode, 0	;20240325,自动保存

	IniRead, VehicleModelNum0, %INI%, 基本设置, VehicleModelNum0, 02	;20240531,车型配置序号,电摩合格证打印系统
return

Label_Init_INI:	; 初始化配置文件INI
	FileAppend, [基本设置]`n, %INI%
	FileAppend, RunCaptX :=D:\RunCaptX`n, %INI%
	FileAppend, COMMANDER_PATH :=D:\RunCaptX\SoftDir\Totalcmd\SoftDir\totalcmd_ez`n, %INI%
	FileAppend, ScreenShotPath :=D:\RunCaptX\PixPinTg截图`n, %INI%

	FileAppend, QQTimFPath :=E:\Users\Tuutg\Personal\Tencent Files\542114344\FileRecv\`n, %INI%
	FileAppend, WechatFpath :=E:\Users\Tuutg\Persona\WeChat Files\`n, %INI%
	FileAppend, Wx_Path :=D:\Program Files\Tencent\WeChat\WeChat.exe`n, %INI%
	FileAppend, GV_ToggleWheelOnCursor :=0`n, %INI%	;1-打开
	FileAppend, GV_ToggleAotuSaveMode :=0`n, %INI%	;20240325,自动保存

	FileAppend, VehicleModelNum0 :=02`n,%INI%	;20240531,合格证车型配置序号
return
;****************************************************
;****************  Settings_Gui$    *****************


;*********** Tuutg自定义应用增强例子开始^ ***********
#If WinActive("ahk_group Group_Code")
$!w::
	SendInput,^s	;单击, 保存
	Sleep,100
	SendInput,#!d    ;Ra_XiaoYao_plus设定好#!d为定位文件目录动作
return

CapsLock & w:: ;{Enter}/保存
	if (GV_KeyClick_Continuous(GV_KeyTimer)){
		SendInput,^s ;双击保存并提示
		EzTip("文件保存成功!",1)
		SendInput,^w ;关闭标签/窗口
	}
	else if(ahk_class ConsoleWindowClass){
		SendInput,{Enter} ;单击,Enter
	}else {
		; SendInput,{Enter} ;单击,Enter
		SendInput,{Click}^s ;单击, 保存
	}
return

$!q::
$!e:: ;用e作为注释键,因此比较熟练.
	if (GV_KeyClick_Continuous(GV_KeyTimer)){
		;ControlSend,, ^q ;取消之前的注释代码,SciTEWindow
		;ToolTip,在400毫秒内连续按下了两次.
		SendInput,{End}	;从行末起跳,行首位置统一
		Sleep,100
		SendInput,{Home} ;到代码段的行首
		SendInput,{Del 2} ;删除注释说明的符号
	}
	else {
		;ControlSend,, ^q ;只按一次的话,行首加注释说明的符号
		SendInput,{Home} ;到代码段的行首
		SendInput,{Insert}`;{Space}	;+注释说明的符号
	}
return

$!r::
	if WinActive("Code ahk_class Chrome_WidgetWin_1") and (!WinActive("ahk_exe BCompare.exe") or !WinActive("ahk_exe uc.exe") or !WinActive("ahk_exe WinMergeU.exe"))
		SendInput,!r	;当前软件默认,VS查找,启用正则.在比较软件中有单独定义,20240328
	else
		SendInput,^r
return

; $!t::ControlSend,,!t	;当前软件默认

CapsLock & q:: ;文段首|Home,行首
	if (GV_KeyClick_Continuous(GV_KeyTimer)){
		SendInput,^{Home} ;文段首
	}
	else {
		SendInput,{Home} ;只按一次的话,行首
	}
return
;****************************************************
CapsLock & a:: ;文段末|End,行末
	if (GV_KeyClick_Continuous(GV_KeyTimer)){
		SendInput,^{End} ;文段末
	}
	else {
		SendInput,{End} ;只按一次的话,行末
	}
return

$!a::SendInput,^a   ;全选
; $!s::SendInput,^s	;改为,全局Ra_搜索
$!f::SendInput,^f{F3}	;查找|下一个
$!d::SendInput,^d	;复制当前行
; CapsLock & g::^g	;改为{Enter}
$!g::SendInput,^g	;定位|跳转到行号
$!b::SendInput,^h	;查找|替换

$!z::SendInput,^z	;撤销
$!+z::ControlSend,,^y	;重做,屏蔽火绒弹窗拦截冲突
$!x::ControlSend,,!x	;当前软件默认
$!c::ControlSend,,!c	;当前软件默认,VS查找,启用大小写
$!v::					;VS Code代码格式化
	if (WinActive("ahk_exe Code.exe")){
		SendInput,+!f
	}else {
		ControlSend,,!v	;当前软件默认
	}
return

CapsLock & b::
	if (WinActive("ahk_exe Code.exe")){
		SendInput,^b	;Notepad--,目录比较|VS Code,主侧栏
	}
return

; $+RButton::SendInput,{Enter} ;Enter,处理中文输入法英文上屏,用Caps+g

$MButton::					 ;单击, 保存|Enter
	if (WinActive("ahk_exe BCompare.exe") or WinActive("ahk_exe uc.exe") or WinActive("ahk_exe WinMergeU.exe")){
		if WinActive("更新 ahk_exe BCompare.exe"){
			ControlClick,TUiBitBtn1	;立即同步(&Y)	;20240520
			Sleep,100
			SendInput,{Text}a
		}
		SendInput,^s
		Sleep,100
		SendInput,{Enter}
	}else {
		SendInput,^s	;单击, 保存认
	}
return

;增强代码编辑器,单击CapsLock={Tab},双击退出|返回英文,要在KBLAutoSwitch中设定{Rshift}=英文;20240325
CapsLock::
	Suspend Permit
	if ((GV_ToggleAotuSaveMode==1) and !WinActive("ahk_exe BCompare.exe") and !WinActive("ahk_exe uc.exe") and !WinActive("ahk_exe WinMergeU.exe")){
		;通用编写自动保存场景,默认为{Tab},带自动保存,20240325
		GV_KeyClickAction1 := "SendInput,{Tab}^s" ;Tab后|保存
		; GV_KeyClickAction1 := "SendInput,{Enter}^s" ;Enter后保存|默认按钮
		; GV_KeyClickAction2 := "SendInput,{Esc}{Rshift}" ;退出|重置英文
		; GV_KeyClickAction2 := "SendInput,{Tab}{Rshift}" ;Tab|重置英文
		GV_KeyClickAction2 := "SendInput,{Enter}{Rshift}^s" ;回车|重置英文
		gosub,Sub_KeyClick123
	}
	else if ((WinActive("ahk_exe BCompare.exe") and !WinActive("更新 ahk_exe BCompare.exe")) or WinActive("ahk_exe uc.exe") or WinActive("ahk_exe WinMergeU.exe")){
		;BCompare通用场景,默认为{Esc},无自动保存,20240325
		GV_KeyClickAction1 := "SendInput,{Esc}" ;Esc退出
		; GV_KeyClickAction1 := "SendInput,{Enter}^s" ;Enter后保存|默认按钮
		; GV_KeyClickAction2 := "SendInput,{Esc}{Rshift}" ;退出|重置英文
		; GV_KeyClickAction2 := "SendInput,{Tab}{Rshift}" ;Tab|重置英文
		GV_KeyClickAction2 := "SendInput,{Enter}{Rshift}^s" ;回车|重置英文
		gosub,Sub_KeyClick123
	}
	else if WinActive("更新 ahk_exe BCompare.exe"){	;by Tuutg,20240425
		;BCompare同步场景,默认为{Esc}^w,无自动保存
		GV_KeyClickAction1 := "SendInput,^s{Esc}^w" ;保存|Esc退出并关闭标签
		; GV_KeyClickAction1 := "SendInput,{Enter}^s" ;Enter后保存|默认按钮
		; GV_KeyClickAction2 := "SendInput,{Esc}{Rshift}" ;退出|重置英文
		; GV_KeyClickAction2 := "SendInput,{Tab}{Rshift}" ;Tab|重置英文
		GV_KeyClickAction2 := "SendInput,{Enter}{Rshift}^s" ;回车|重置英文
		gosub,Sub_KeyClick123
	}
	else if WinActive("uTools ahk_class Chrome_WidgetWin_1"){ ;by Tuutg,20240518
		;uTools通用场景,默认为{Esc}^w,无自动保存
		GV_KeyClickAction1 := "SendInput,{Esc}^w" ;Esc退出并关闭标签
		; GV_KeyClickAction1 := "SendInput,{Enter}^s" ;Enter后保存|默认按钮
		; GV_KeyClickAction2 := "SendInput,{Esc}{Rshift}" ;退出|重置英文
		; GV_KeyClickAction2 := "SendInput,{Tab}{Rshift}" ;Tab|重置英文
		GV_KeyClickAction2 := "SendInput,{Enter}{Rshift}" ;回车|重置英文
		gosub,Sub_KeyClick123
	}
	else {
		;其他通用编写场景,默认为{Tab},无自动保存,20240325
		GV_KeyClickAction1 := "SendInput,{Tab}" ;Tab后|保存
		; GV_KeyClickAction1 := "SendInput,{Enter}^s" ;Enter后保存|默认按钮
		; GV_KeyClickAction2 := "SendInput,{Esc}{Rshift}" ;退出|重置英文
		; GV_KeyClickAction2 := "SendInput,{Tab}{Rshift}" ;Tab|重置英文
		GV_KeyClickAction2 := "SendInput,{Enter}{Rshift}" ;回车|重置英文
		gosub,Sub_KeyClick123
	}
return

#If
;****************************************************
;****************************************************
; /*;SciTEWindow
#If WinActive("ahk_class SciTEWindow")

	$!q::ControlSend,,^q    ; 注释
	$!w::
		SendInput,^s	;单击, 保存
		Sleep,100
		; SendInput,#!d  ;Ra_XiaoYao_plus设定好#!d为定位文件目录动作
		SendInput,^2 	;右键定位到文件目录
	return

	CapsLock & w:: ;SendInput,{Click}^s/单击&保存
	if (GV_KeyClick_Continuous(GV_KeyTimer)){
		SendInput,^s ;双击保存并提示
		EzTip("文件保存成功!",1)
		SendInput,^w ;关闭标签/窗口
	}
	else if(WinActive("ahk_class ConsoleWindowClass")){
		SendInput,{Enter} ;单击,Enter
	}   else {
		SendInput,{Click}^s ;单击,保存
	}
	return

	$!e::ControlSend,,^q    ; 注释
	$!r::          ; Run/F5
		Send,{F7}
		Sleep,100
		Send,{F5}  	; Click,395,60
	return
	;$!t          ; 工具

	CapsLock & q::	;文段首|Home,行首
	if (GV_KeyClick_Continuous(GV_KeyTimer)){
		SendInput,^{Home} ;文段首
	}
	else {
		SendInput,{Home} ;只按一次的话,行首
	}
	return
;****************************************************
	CapsLock & a:: ;Home|End,行末
	if (GV_KeyClick_Continuous(GV_KeyTimer)){
		SendInput, ^{End} ;文段末
	}
	else {
		SendInput,{End} ;只按一次的话,行末
	}
	return

	$!a::SendInput,^a    ; 全选
	$!s::          ; 右键多开
		Click, ,  Right, 1
		Sleep,100
		SendInput,{Down 9}
		Sleep,100
		SendInput,{Enter}
	return

	$!d::SendInput,^d	; 复制当前行

	^f::	; 查找|光标定位到搜索框,20240328
	$!f::	;SendInput,^f
		SendInput,^f
		sleep,100
		ControlGetPos,x,y,w,h,Edit2 ;搜索框
		CoordWinDbClick(x,y)
		SendInput,{F3}	;自动查找下一个,20240530
	return

	$!g::SendInput,^g	; 定位|跳转到行号

	$!z::SendInput,^z	; 撤销
	$!+z::ControlSend,,^y	;重做,屏蔽火绒弹窗拦截冲突
	$!x::ControlClick,Button4	; 清除标记
	$!c::SendInput,^3	; 右键运行选区代码
	$!v::Click,485,60	; 代码格式化.之前为Button2

	$!b:: ;SendInput,^h	; 替换|查找,20240328
		SendInput,^h
		sleep,100
		ControlGetPos,x,y,w,h,Edit4 ;搜索框
		CoordWinDbClick(x,y)
	return

	; CapsLock & b::    ; 右键定位到#include文件
		; Click, ,  Right, 1
		; Sleep,100
		; SendInput,{Up 5}
		; Sleep,100
		; SendInput,{Enter}
	; return

	; $+RButton::SendInput,{Enter} ;Enter,处理中文输入法英文上屏,用Caps+g
	~$MButton::SendInput,^s	;单击, 保存
	$!MButton:: SendInput,!{Enter}   ; 跳转到定义

	;增强代码编辑器,单击CapsLock={Tab},双击退出|返回英文,要在KBLAutoSwitch中设定{Rshift}=英文
	CapsLock::	;20240324
		Suspend Permit
		if MouseUnder("Edit*"){ ;查找|替换场景,默认为{Enter}
			; GV_KeyClickAction1 := "SendInput,{Tab}^s" ;Tab后|保存
			GV_KeyClickAction1 := "SendInput,{Enter}" ;Enter后保存|默认按钮
			; GV_KeyClickAction2 := "SendInput,{Esc}{Rshift}" ;退出|重置英文
			GV_KeyClickAction2 := "SendInput,{Tab}{Rshift}" ;Tab|重置英文
			; GV_KeyClickAction2 := "SendInput,{Enter}{Rshift}" ;回车|重置英文
			gosub,Sub_KeyClick123
		}
		else if (GV_ToggleAotuSaveMode==1) and !MouseUnder("Edit*"){
			;通用编写场景,默认为{Tab},带自动保存,20240325,排除代码比较软件
			GV_KeyClickAction1 := "SendInput,{Tab}^s" ;Tab后|保存
			; GV_KeyClickAction1 := "SendInput,{Enter}^s" ;Enter后保存|默认按钮
			; GV_KeyClickAction2 := "SendInput,{Esc}{Rshift}" ;退出|重置英文
			; GV_KeyClickAction2 := "SendInput,{Tab}{Rshift}" ;Tab|重置英文
			GV_KeyClickAction2 := "SendInput,{Enter}{Rshift}^s" ;回车|重置英文
			gosub,Sub_KeyClick123
		}
		else {	;通用编写场景,默认为{Tab},无自动保存,20240325
			GV_KeyClickAction1 := "SendInput,{Tab}" ;Tab后|保存
			; GV_KeyClickAction1 := "SendInput,{Enter}^s" ;Enter后保存|默认按钮
			; GV_KeyClickAction2 := "SendInput,{Esc}{Rshift}" ;退出|重置英文
			; GV_KeyClickAction2 := "SendInput,{Tab}{Rshift}" ;Tab|重置英文
			GV_KeyClickAction2 := "SendInput,{Enter}{Rshift}" ;回车|重置英文
			gosub,Sub_KeyClick123
		}
	return

	;Enter激活启用保存,20240325
	Enter::
		if (GV_ToggleAotuSaveMode==1) and !MouseUnder("Edit*")
			SendInput,{Enter}^s
		else
			SendInput,{Enter}
	return

#If
*/
;****************************************************
;****************************************************
#If WinActive("ahk_group Group_MButtonDoubleClose")
::zz::SendInput,^w
;****************************************************
中键设定_Acrobat:	;单击中键缩放100%/双击中键关闭
~$MButton::
	if (GV_KeyClick_Continuous(GV_MouseTimer)){
		SendInput,^s
		Sleep ,100
		SendInput,^w
		Sleep ,100
		EzTip("文件保存并关闭成功!",1)
	}
	else {
		SendInput,^s
		Sleep ,100
		SendInput,^0	;单击中键缩放100%
		Sleep ,100
	}
return
#If
;****************************************************
;****************************************************
#If WinActive("͉ahk_class Photoshop")		;适用于Photoshop
	$^#!d::		;左键另存为PDF|动作设定^+{F12}
	$+!LButton::
	$^+LButton::
		SendInput,!fa
		WinWaitActive, 另存为 ahk_class #32770
		ControlClick, ComboBox2
		ControlSend, ComboBox2, {Home}{Down 13}{Enter}, 另存为 ahk_class #32770
		Sleep, 1000
		SendInput,^g
	return

	$^#!j::
	$+!MButton::
	$^+MButton:: ;中键另存为JPG|动作设定^+{F11}
		SendInput,!fa
		WinWaitActive, 另存为 ahk_class #32770
		ControlClick, ComboBox2
		ControlSend, ComboBox2, {Home}{Down 9}{Enter}, 另存为 ahk_class #32770
		Sleep, 1000
		SendInput,^g
	return

	$^#!p::		;^+!u ,快速导出png
	$+!RButton::
	$^+RButton::
		SendInput,!fe{Enter}
		Sleep, 1000
		SendInput,^g
	return

	CapsLock & w:: ;双击关闭/单击保存
	if (GV_KeyClick_Continuous(GV_KeyTimer)){
		SendInput,^s		;先保存
		Sleep,100
		SendInput,^w		;关闭窗口 by Tuutg 2022/11/13
		EzTip("文件保存并关闭成功!",1)
		return
	}
	else {
		if (A_Cursor = "IBeam") or (A_CaretX !=""){
			SendInput,{Click}^s ;单击, 保存
		}
		else
		{
			SendInput,{Click}^s ;鼠标,单击,20240418
		}
		return
	}
	return

;****************************************************

	;Space单独模式,约等Ctrl
	~$Space::	;修复打汉字时会多一个空格
		if (A_Cursor ="IBeam") or (A_CaretX !=""){
			SendInput,{Space}{BackSpace}	;修复打汉字时会多一个空格
		} else {
			return	;~默认抓手工具,20240402
		}
	return

	Space & q::SendInput,^o  ;打开,20240329

	Space & w::	;Ctrl+W,新建|编组
		GV_KeyClickAction1 := "SendInput,^n" ;新建
		GV_KeyClickAction2 := "SendInput,^s" ;编组
		gosub,Sub_KeyClick123
	return

	Space & e::SendInput,^e  ;Ctrl+E,合并复制
	Space & r::SendInput,^+!r ;旋转图像
	Space & t::SendInput,^t ;Ctrl+t,自由变换
;****************************************************
	Space & a::SendInput,^a ;Ctrl+A,全部选择
	Space & s::SendInput,^`/ ;Ctrl+/,锁定图层
	Space & d::ControlSend,,^d ;Ctrl+D,取消选择
	Space & f::SendInput,^j ;Ctrl+J,复制图层
	CapsLock & g::	;自动保存,20240323
	Space & g:: ;Ctrl+G,对象编组|{Enter}
		GV_KeyClickAction1 := "gosub,Space_SgGInPS"
		GV_KeyClickAction2 := "SendInput,^g" ;编组
		gosub,Sub_KeyClick123
	return
;****************************************************
	Space & z::SendInput,^z ;Ctrl+Z,撤销操作

	Space & x:: ;SendInput,^+x ;液化
		GV_KeyClickAction1 := "SendInput,^x"
		GV_KeyClickAction2 := "SendInput,^+x"
		gosub,Sub_KeyClick123
	return

	Space & c:: ;SendInput,^!i ;复制|图像大小
		GV_KeyClickAction1 := "gosub,Space_SgCInPS"
		GV_KeyClickAction2 := "SendInput,^!i"
		gosub,Sub_KeyClick123
	return

	Space & v:: ;SendInput,^`, ;粘贴|Ctrl+,显示图层
		GV_KeyClickAction1 := "SendInput,^v"
		GV_KeyClickAction2 := "SendInput,^`,"
		gosub,Sub_KeyClick123
	return

	Space & b::		;Photoshop智能对象_替换内容>跳转
	CapsLock & b::
	if (GV_KeyClick_Continuous(GV_MouseTimer)){
		SendInput,^+{F2} ;需要相应动作配合并设定快捷键
		Sleep,100
		SendInput,{Text}c
		Sleep,100
		SendInput,^g	;Listary跳转
;****************************************************
		; SendInput,{Click}	;AHK方法,通用
		; Sleep,100
		; SendInput,{Click,Right}
		; Sleep,100
		; SendInput,{Down 14}
		; Sleep,500
		; SendInput,{Enter}
		; Sleep,100
		; SendInput,^g	;Listary跳转
	}
	else {
		SendInput,{Click}	;AHK方法,通用
		Sleep,100
		SendInput,{Click,Right}
		Sleep,100
		SendInput,{Down 14}
		Sleep,500
		SendInput,{Enter}
		Sleep,100
		SendInput,^g	;Listary跳转
;****************************************************
		; SendInput,^+{F2} ;需要相应动作配合并设定快捷键
		; Sleep,100
		; SendInput,{Text}c
		; Sleep,100
		; SendInput,^g	;Listary跳转
	}
	return

	Space & F5::SendInput,+{F5} ;填充图层
#If
;****************************************************
Space_SgCInPS:
	if (A_Cursor = "IBeam"){
		SendInput,^a^c ;文字输入模式
		Sleep, 300
	}
	else{
		SendInput,^c ;复制
		Sleep, 300
		SendInput,^v ;粘贴
	}
return

Space_SgGInPS:	;自动保存,20240323
	if (GV_ToggleAotuSaveMode==1){
		if (A_Cursor = "IBeam"){
			SendInput,{Enter} ;文字输入模式
			Sleep,100
			SendInput,^s ;自动保存
		}
		else{
			SendInput,^g ;对象编组
			Sleep,100
			SendInput,^s ;自动保存
		}
	}
	else {
		if (A_Cursor = "IBeam"){
			SendInput,{Enter} ;文字输入模式
		}
		else{
			SendInput,^g ;对象编组
		}
	}
return
;****************************************************
;****************************************************
#If WinActive("ahk_class #32770 ahk_group Group_WPS")
	$!f::	;模仿Listary, 跳转目标路径
	$MButton::
	; CapsLock & g::	;Win7打开,Win11关闭(Listary6)
		; 方法1,Utools
		Click, 75, 80 Left, 1
		Sleep, 100
		Click, 550, 300 Middle, Down
		Sleep, 500
		Click, 550, 300 Middle, Up
		Sleep, 100
		; Click, 465, 365 Left, 1 ;适用uTools,4.0.1以下
		Click, 550, 435 Left, 1	;适用uTools,4.4.1以上
		Sleep, 100
		SendInput, {Enter}
		Sleep, 100
		SendInput, {Enter}
	return

	CapsLock & g::SendInput,^g	;QuickJump或Listary
#If
;****************************************************
;****************************************************
#If WinActive("PCHunter ahk_class #32770")
	CapsLock & r:: ;文件页->右键,强制删除
		SendInput,{Click,Right}
		Sleep,500
		ControlSend, , {Down 4}{Enter}
	return

	CapsLock & t:: ;进程页->右键,强制关闭进程树
		SendInput,{Click,Right}
		Sleep,500
		SendInput, {Down 12}{Enter 2}
	return

	CapsLock & d:: ;进程页->右键,定位到hunter目录
		SendInput,{Click,Right}
		Sleep,500
		SendInput, {Up 2}{Enter}
	return

	$F5::
	CapsLock & w:: ;进程页->右键,刷新
		SendInput,{Click,Right}
		Sleep,500
		SendInput, {Down 1}{Enter}
	return

	CapsLock & q:: ;进程页->右键,查看进程文件属性
		SendInput,{Click,Right}
		Sleep,500
		SendInput, {Up 3}{Enter}
	return

	CapsLock & c:: ;进程页->右键,复制进程名
		SendInput,{Click,Right}
		Sleep,500
		SendInput, {Up 7}{Enter}
	return

	CapsLock & v:: ;进程页->右键,复制进程路径
		SendInput,{Click,Right}
		Sleep,500
		SendInput, {Up 8}{Enter}
	return
#If
;****************************************************
;****************************************************
#If WinActive("ahk_exe acad.exe")
	;布局之间切换
	;CapsLock & q::SendInput,^{PgUp}
	;CapsLock & a::SendInput,^{PgDn}
	$F3::^f	;F3对象捕捉	;屏蔽截图占用F3
	$!w:: ;定位到文件夹	;2023/08/01 02:10
	{
		WinGetTitle, Title_acad, A
		Title_RegEx1:= LTrim(Title_acad, OmitChars := "AutoCAD 2022 - [")
		Title_RegEx2:= RTrim(Title_RegEx1, OmitChars := "]")
		;MsgBox, %Title_RegEx2%
		SendInput,!{Space}	;EVERYTHING
		Sleep, 100
		SendInput,%Title_RegEx2%{Enter}
		Sleep, 100
		SendInput,^{Enter}
		WinMinimize, A
	}
	return
#If
;****************************************************
;****************************************************
#If WinActive("ahk_exe SUMo.exe")
	$MButton:: ;打开文件夹
	{
		SendInput,{Click,Right}
		Sleep,100
		SendInput, {Up 4}{Enter}
	}
	return
#If
;****************************************************
;****************************************************
#If WinActive("极连快传 ahk_exe munify.exe")
	;右键在文件夹中显示
	$MButton::
	{
		SendInput,{Click,Right}
		Sleep 100
		SendInput ,{Up 1}{Enter}
		Sleep 500
	}
	return
;****************************************************
	;右键删除
	!d::
	$!MButton::
	{
		SendInput,{Click,Right}
		Sleep 100
		SendInput,{Down 1}{Enter}
		Sleep 100
	}
	return
;****************************************************
	!f::
	{
		360JL_path = % "G:\TDownload\360Download\360下载\Doc\360JiLianFiles\" . fun_GetFormatTime("yyyy-MM")
		Run,"%COMMANDER_EXE%" /A /T /O /R="%360JL_path%"
		Sleep 500
		MyWinWaitActive("ahk_class TTOTAL_CMD")
	}
	return
#If
;****************************************************
;****************************************************
;股票软件
#If WinActive("ahk_exe mainfree.exe")	;东方财富
	$!f:: ;可转债
		Click, 285, 18	;点击菜单行情（Q）
		SendInput, {Down 14}
		Sleep,100
		SendInput, {Right 1}
		Sleep,100
		SendInput, {Down 8}
		Sleep,100
		SendInput, {Right 1}
		Sleep,100
		SendInput, {Down 1}{Enter}
	return
;****************************************************
	$F3::ControlSend,,{F3}	;F3=上证, F4=深证
	~$MButton::Send,{F6} ;自选股
	$!z::Send, {Insert}{Enter}	;一键加入自选股
	CapsLock & z::Send,{Insert}	;加入自选股/板块
	CapsLock & b::	;标记①
		SendInput,{Click,Right}
		SendInput,{Down 7}
		SendInput,{Right}
		SendInput,{Down 2}{Enter}
	return
	CapsLock & w::Send,{F7} ;条件选股
;****************************************************
	$!w:: ;导出数据
	$!c:: ;导出数据
		SendInput,{Click,Right}
		SendInput,{Down 8}
		SendInput,{Right}{Down}{Enter}
		;Click, 415 100,导出对话 ahk_class #32770
	return

	CapsLock & q::SendInput,{PgUp}	;20240301
	CapsLock & a::SendInput,{PgDn}	;20240301

	$RButton::
		;GV_MouseTimer := 300
		GV_KeyClickAction1 := "SendInput,{Click,Right}"
		GV_KeyClickAction2 := "SendInput,{Escape}"
		GV_MouseButton := 2
		GV_LongClickAction := "SendInput,{Enter}"
		gosub,Sub_MouseClick123
	return
#If
;****************************************************
;****************************************************
#If WinActive("ahk_exe Tdxw.exe")	;通达信
	$!f:: ;可转债Send,kzz{Enter}
		Click, 10 10
		Send, {Down 4}{Right 1}
		Send, {Down 1}{Right 1}
		Send, {Up 5}{Right 1}
		Send, {Up 6}{Enter}
	return
;****************************************************
	$F3::ControlSend,,{F3}	;F3=上证, F4=深证
	~$MButton::Send,{F6} ;自选股
	$!z::ControlSend,,!z	;一键加入自选股
	CapsLock & z::Send,^z	;加入自选股/板块
	; $!+z::ControlSend,,^y	;重做,屏蔽火绒弹窗拦截冲突
	CapsLock & b::	;标记①
		SendInput,{Click,Right}
		SendInput,{Up 6}
		SendInput,{Right}
		SendInput,{Down 1}{Enter}
	return
	CapsLock & w::Send,^t ;条件选股
;****************************************************
	$!w:: ;导出数据
	$!c:: ;导出数据
		Click, 10 10
		Send, {Down 1}{Right 1}
		Send, {Down 2}{Enter}	;Send, 34{Enter}
	return
;****************************************************
	$!t:: ;股池
	$!g:: ;股池
		SendInput, {Text}78
		Sleep,100
		SendInput, {Enter}
	return
;****************************************************
	$!s:: ;下载数据
		Click, 10 10
		Send, {Down 1}{Right 1}
		Send, {Up 6}{Enter}
		WinActivate, 数据下载 ahk_class #32770
		Sleep,1500
		SetControlDelay -1
		ControlClick, Button1,数据下载 ahk_class #32770,,,, NA
		Sleep,100
		ControlClick, Button9,数据下载 ahk_class #32770,,,, NA
	return

	CapsLock & q::SendInput,{PgUp}	;20240301
	CapsLock & a::SendInput,{PgDn}	;20240301

	$RButton::
		;GV_MouseTimer := 300
		GV_KeyClickAction1 := "SendInput,{Click,Right}"
		GV_KeyClickAction2 := "SendInput,{Escape}"
		GV_MouseButton := 2
		GV_LongClickAction := "SendInput,{Enter}"
		gosub,Sub_MouseClick123
	return
#If
;****************************************************
;****************************************************
;打开扫描仪进行扫描
#If WinActive("ahk_exe explorer.exe") or WinActive("ahk_class AcrobatSDIWindow")
/*	$!#c::	;系统扫描仪进行扫描
	{
		WinActivate, Program Manager ahk_class Progman
		Click, 854, 30 Left, , Down
		Sleep,50
		Click, 854, 30 Left, , Up
		Sleep,50
		SendInput ,{AppsKey}
		Sleep,1000
		ControlSend, SysListView321, {t Down}, Program Manager ahk_class Progman
		Sleep,50
		ControlSend, SysListView321, {t Up}, Program Manager ahk_class Progman
		Sleep, 200
		ControlSend, SysListView321, {Enter Down}, Program Manager ahk_class Progman
		Sleep,50
		ControlSend, SysListView321, {Enter Up}, Program Manager ahk_class Progman
		Sleep,50

		WinActivate, 新扫描 ahk_class #32770
		Click, 1115, 780, 0
		ControlSend, Button1, {s Down}, 新扫描 ahk_class #32770
		Sleep,50
		ControlSend, Button1, {s Up}, 新扫描 ahk_class #32770
		Sleep, 16000

		WinActivate, 导入图片和视频 ahk_class #32770
		Click, 148, 134 Left, , Down
		Sleep,50
		Click, 148, 134 Left, , Up
		Sleep,50
		ControlSend, Edit1, {Tab Down}, 导入图片和视频 ahk_class #32770
		Sleep,50
		ControlSend, Edit1, {Tab Up}, 导入图片和视频 ahk_class #32770
		Sleep,50
		ControlSend, Button1, {m Down}, 导入图片和视频 ahk_class #32770
		Sleep,50
		ControlSend, Button1, {m Up}, 导入图片和视频 ahk_class #32770
		Sleep,5000

		scan_path = % "E:\Users\Tuutg\Pictures\扫描文件\" . fun_GetFormatTime("yyyy-MM-dd")
		Run , "%scan_path%"
		Sleep 500
		Run,"%COMMANDER_EXE%" /A /T /O /R="%scan_path%"
		Sleep 500
		MyWinWaitActive("ahk_class TTOTAL_CMD")
	}
	return
*/
;****************************************************
	;打开Adobe PDF进行扫描
	$^#+s::
	IfWinNotExist, ahk_class AcrobatSDIWindow
	{	;路径依各自实际安装位置修改
		Software="D:\Program Files (x86)\Adobe\Acrobat DC 2022\Acrobat DC\Acrobat\Acrobat.exe"
		Run, %Software%
		Sleep,1000
		gosub AdobePDF_scan
	}
	else
	{
		gosub AdobePDF_scan
	}
	return
#If
;****************************************************
AdobePDF_scan:
	WinActivate ahk_class AcrobatSDIWindow
	Sleep,250
	ControlClick, AVL_AVView49	;主页|扫描
	Sleep,100
	SendInput,!v
	Sleep,500
	SendInput,{Text}t		;点击工具菜单
	Sleep,100
	SendInput,{Down 6}
	Sleep,100
	SendInput,{Right}
	Sleep,100
	SendInput,{Enter}
	Sleep,100
	WinGetPos, x, y,lengthA,hightA, A
	CoordWinClick(lengthA/2+85,415)	; 扫描文档
	Sleep,100
	CoordWinClick(lengthA/2-20,595)	; 开始扫描
	Sleep,100
	CoordWinClick(lengthA/2-170,788)	; 扫描
	Sleep, 22000
	; 保存
	scan_path := "E:\Users\Tuutg\Pictures\扫描文件\PDF扫描\"	;路径依各自实际安装位置修改
	WinGetTitle, Title, ahk_class AcrobatSDIWindow
	ControlFocus, AVL_AVView35, ahk_class AcrobatSDIWindow
	Sleep 100
	SendInput,^s
	Sleep,100
	ControlFocus,Edit2,另存为 PDF ahk_class #32770
	SendInput,%scan_path%
	Sleep,100
	SendInput,{Enter}
	ControlFocus,Edit1,另存为 PDF ahk_class #32770
	Title1:= RTrim(Title, OmitChars := " .PDF - Adobe Acrobat Pro DC (32-bit) " ) . "-" . fun_GetFormatTime( "yy-MM-dd HH-mm-ss" )
	Title2:= "Scan" . "-" . fun_GetFormatTime( "yy-MM-dd HH-mm-ss" )
	SendInput,%Title2%
	Sleep,500
	ControlClick,Button4,另存为 PDF ahk_class #32770
	; 打开扫描保存目录
	Run , "%scan_path%"
return
;****************************************************
;****************************************************
;采用右键|Alt+菜单方式操作,Acrobat-PDF
#If WinActive("ahk_class AcrobatSDIWindow")
$!g::ControlSend,,^f	;页面查找

$!s:: ;AdobePDF_图章
{
	ControlClick, AVL_AVView49
	Sleep,100
	Click,140,38		;点击视图菜单
	Sleep,500
	SendInput,{Text}t		;点击工具菜单
	Sleep,100
	SendInput,{up 5}		;AdobePDF_图章
	Sleep,100
	SendInput,{Right}
	Sleep,100
	SendInput,{Enter}
return
}
;****************************************************
$!b:: ;AdobePDF_保护
{
	ControlClick, AVL_AVView49
	Sleep,100
	Click,140,38		;点击视图菜单
	Sleep,500
	SendInput,{Text}t		;点击工具菜单
	Sleep,100
	SendInput,{up 2}		;AdobePDF_保护
	Sleep,100
	SendInput,{Right}
	Sleep,100
	SendInput,{Enter}
	Sleep,100
return
}
;****************************************************
; Space & b::
CapsLock & b:: ;AdobePDF_替换页面
{
	SendInput,{Click,Right}
	Sleep,100
	SendInput,{Text}r	;右键>替换页面
	Sleep, 200
	SendInput,^g	;QuickJump或Listary跳转
	Sleep,100
	SendInput,{tab}
	Sleep,2000
	SendInput,{Down 18}
	Sleep,100
	SendInput,{Enter}
return
}
;****************************************************
; Space & x::
CapsLock & x:: ;右键剪切|AdobePDF_页面标签
if GV_KeyClick_Continuous(GV_KeyTimer)
{
	SendInput,{Click,Right}
	Sleep,50
	SendInput,{Text}t	;右键剪切
	Sleep,50
	SendInput,{Enter}
}
else {
	SendInput,{Click,Right}
	Sleep,100
	SendInput,{Text}b	;页面标签
	Sleep,100
}
return
;****************************************************
; Space & c::
CapsLock & c::	;原版|右键复制,by Tuutg,20240416
	if (A_Cursor != "IBeam"){
		gosub,Sub_ClipAppend ;原版复制
	}
	else {
		SendInput,{Click,Right}
		Sleep,50
		SendInput,{Text}c	;右键复制
		Sleep,50
		SendInput,{Enter}
	}
return
;****************************************************
; Space & v::
CapsLock & v::	;原版|右键粘贴,by Tuutg,20240416
	if (A_Cursor = "IBeam"){
		SendInput,^v{Enter}	;原版粘贴
	}
	else {
		SendInput,{Click,Right}
		Sleep,50
		SendInput,{Text}p	;右键粘贴
		Sleep,50
		SendInput,{Enter}
	}
return
;****************************************************
; Space & t::
CapsLock & t:: ;AdobePDF_提取页面
{
	SendInput,{Click,Right}
	Sleep,100
	SendInput,{Text}x	;右键>提取页面
	Sleep, 200
	SendInput,!e
	Sleep,100
	SendInput,{Enter}
	Sleep,100
	SendInput,^g	;QuickJump或Listary跳转
	Sleep,100
return
}
;****************************************************
; Space & g::
CapsLock & g::^+i	;AdobePDF_插入页面
;****************************************************
$^+c::		;从剪切板插入页面
{
	SendInput,{Click,Right}
	Sleep,100
	SendInput,{Text}nc	;右键从剪切板插入页面
	SendInput,{Enter}
return
}
;****************************************************
$!e::SendInput,!ei	;AdobePDF_菜单->编辑->编辑文本图像
;****************************************************
;D->J->P,文件快速导出
$+!LButton::	;PDF在jpg前,所以用LButton
$^+LButton::
$^#!d::SendInput,!fhr ;另存为较小的PDF

$+!MButton::	;中键
$^+MButton::
$^#!j::SendInput,!ftij	;导出为JPG

$+!RButton::
$^+RButton::
$^#!p::SendInput,!ftip	;导出png

$^#!w::SendInput,!ftww ;导出为word
$^#!e::SendInput,!ftse ;导出为excel
$^#!t::SendInput,!ftt ;导出为PPt
$^#!c::SendInput,!ftc ;导出为文本
$^#!l::SendInput,!ftl ;导出为纯文本

CapsLock & w:: ;关闭当前标签|恢复关闭标签,20240528
	GV_KeyClickAction1 := "SendInput,^w"	;关闭当前标签
	GV_KeyClickAction2 := "SendInput,!+t"	;恢复关闭标签
	gosub,Sub_KeyClick123
return

#If

;****************************************************
;Acrobat-PDF,单字母自定义快捷键设定
#If WinActive("ahk_class AcrobatSDIWindow") and (A_Cursor != "IBeam")
{
~$q::SendInput,^5	;显示/隐藏线条粗细
~$w::SendInput,^+{NumpadSub}	;向左旋转
~$e::SendInput,^{NumpadAdd}	;+,放大
~$r::SendInput,^+{NumpadAdd}	;向右旋转
~$t::SendInput,{F8}		;显示/隐藏工具栏,20240528

~$a::SendInput,^s		;保存|另存
~$s::SendInput,{PgUp}	;上一页
~$d::SendInput,^{NumpadSub}	;-,缩小
~$f::SendInput,{PgDn}	;下一页
~$g::SendInput,^+n		;转到页面

~$z::SendInput,^0		;适合页面
~$x::SendInput,^r		;显示/隐藏标尺,20240528
~$c::SendInput,^l		;进入/退出全屏
~$v::SendInput,{F4}		;左-进入/退出导航窗格
~$b::SendInput,+{F4}	;右-显示/隐藏工具窗格
}
#If

;****************************************************
;****************************************************
;Snipaste
#If WinActive("Snipaste ahk_exe Snipaste.exe")
{
	CapsLock & 1::SendInput,^1	;矩形/椭圆
	CapsLock & 2::SendInput,^2	;折线
	CapsLock & 3::SendInput,^3	;画笔
	CapsLock & 4::SendInput,^4	;记号笔
	CapsLock & 5::SendInput,^5	;马赛克
	CapsLock & 6::SendInput,^6	;文字

	Space & a::SendInput,^1	;矩形/椭圆
	Space & s::SendInput,^2	;折线
	Space & d::SendInput,^3	;画笔
	Space & f::SendInput,^4	;记号笔
	Space & g::SendInput,^5	;马赛克
	Space & m::SendInput,^5	;马赛克

	Space & r::SendInput,^5	;马赛克

	Space & 5::
	Space & t::SendInput,^6	;文字

	Space & y::SendInput,^y	;重做 ctrl+Y
	Space & z::SendInput,^z	;撤销 ctrl+Z
	; CapsLock & z::SendInput,^z	;撤销,Capstg默认
	$!+z::ControlSend,,^y	;重做,屏蔽火绒弹窗拦截冲突
	Space & x::SendInput,{Enter}	;{Enter}
	Space & c::SendInput,^c	;复制 ctrl+c
	$!c::SendInput,^c	;复制 ctrl+c
	Space & v::SendInput,^y	;重做 ctrl+Y
	Space & b::SendInput,^3 	;画笔
	Space & q::SendInput,^4	;记号笔

	Space & w::
	CapsLock & w::SendInput,^s	;保存 ctrl+S

	; $Space::SendInput,{Blind}{Space} ;恢复默认打字上屏功能
	$Space::Space ;恢复空格打字上屏,by Tuutg,20240425

	$RButton::
		;GV_MouseTimer := 300
		GV_KeyClickAction1 := "SendInput,{Click,Right}" ;系统默认,RButton
		GV_KeyClickAction2 := "SendInput,^c{Escape}{Enter}"	;快速退出
		GV_MouseButton := 2
		GV_LongClickAction := "SendInput,{Click,Right}" ;系统默认,RButton
		gosub,Sub_MouseClick123
	return
}
#If
;****************************************************
#If WinActive("图像 ahk_class #32770")
	; $Space::SendInput,{Blind}{Space}	;恢复{Space}打字功能.20240108
	$Space::Space ;恢复空格打字上屏,by Tuutg,20240425
#If
;****************************************************
;只在PixPin钉图界面有效,启用Space模式
#If WinActive("PixPin ahk_exe PixPin.exe")
{	; 适用于1.2.0.0以下版本
	; ΔX :=(lengthA-x)/i, ΔY :=hightA-y
	; ::PixPinToolsClick(1)	;打钩
	; ::PixPinToolsClick(2)	;ctrl+Y
	; ::PixPinToolsClick(3)	;ctrl+Z
	; ::PixPinToolsClick(4)	;橡皮擦
	; ::PixPinToolsClick(5)	;文字
	; ::PixPinToolsClick(6)	;马赛克
	; ::PixPinToolsClick(7)	;记号笔
	; ::PixPinToolsClick(8)	;铅画笔
	; ::PixPinToolsClick(9)	;标序号
	; ::PixPinToolsClick(10) ;标箭头
	; ::PixPinToolsClick(11) ;折线
	; ::PixPinToolsClick(12) ;矩形/椭圆
;****************************************************
	$^1::
	CapsLock & 1::
	; Space & a::PixPinToolsClick(12) ;矩形/椭圆
	Space & a::SendInput,+1	;矩形/椭圆

	$^2::
	CapsLock & 2::
	; Space & s::PixPinToolsClick(11) ;折线
	Space & s::SendInput,+2	;折线

	$^3::
	CapsLock & 3::
	; Space & d::PixPinToolsClick(10) ;标箭头
	Space & d::SendInput,+3 ;标箭头

	$^4::
	CapsLock & 4::
	; Space & f::PixPinToolsClick(9) ;标序号
	Space & f::SendInput,+4 ;标序号

	$^5::
	CapsLock & 5::
	; Space & g::PixPinToolsClick(8) ;铅画笔
	Space & g::SendInput,+5 ;铅画笔

	$!g::SendInput,^!g ;GIF录制
;****************************************************
	$^6::
	Space & 5::
	CapsLock & 6::
	; Space & b::PixPinToolsClick(7) ;记号笔
	Space & b::SendInput,+6 ;记号笔

	$^7::
	Space & 4::
	CapsLock & 7::
	; Space & r::PixPinToolsClick(6) ;马赛克
	Space & m::SendInput,+7 ;马赛克
	Space & r::SendInput,+7 ;马赛克

	$^8::
	Space & 3::
	CapsLock & 8::
	; Space & t::PixPinToolsClick(5) ;文字
	Space & t::SendInput,+8 ;文字

	$^9::
	Space & 2::
	CapsLock & 9::
	; Space & e::PixPinToolsClick(4) ;橡皮擦
	Space & e::SendInput,+9 ;橡皮擦

	Space & w::
	CapsLock & w::SendInput,^s ;保存 ctrl+S

	Space & q::SendInput,+5 ;铅(q)画笔
;****************************************************
	Space & z::SendInput,^z	;撤销 ctrl+z
	; CapsLock & z::SendInput,^z ;撤销,Capstg默认
	Space & x::SendInput,^!x ;长截屏
	Space & c::SendInput,^c ;复制 ctrl+c
	$!c::SendInput,^+c ;复制纯文本ctrl+shift+c
	Space & v::	;重做 ctrl+Y
	Space & y::	;重做 ctrl+Y
	$!+z::ControlSend,,^y ;重做,屏蔽火绒弹窗拦截冲突
	; Space & b::SendInput,+6 ;记号笔
;****************************************************
	; $Space::SendInput,{Blind}{Space} ;恢复默认打字上屏功能
	$Space::Space ;恢复空格打字上屏,by Tuutg,20240425

	$RButton::
		;GV_MouseTimer := 300
		GV_KeyClickAction1 := "SendInput,{Click,Right}" ;系统默认,RButton
		GV_KeyClickAction2 := "SendInput,^c{Escape}{Enter}"	;快速复制并退出
		GV_MouseButton := 2
		GV_LongClickAction := "SendInput,{Click,Right}" ;系统默认,RButton
		gosub,Sub_MouseClick123
	return
}
#If
;****************************************************
	~$F1::	;截图|贴图OCR|复制
		SendInput,{F1}  ; 发送Pixpin截图快捷键

		Clipboard := "" ; 清空剪切板

		Loop
			{
				If GetKeyState("LButton", "P") || GetKeyState("MButton", "P")
				{
					Break
				}
				Sleep, 100
			}

		; 等待鼠标弹起
		Loop
		{
			If !GetKeyState("LButton", "P") && !GetKeyState("MButton", "P")
			{
				Break
			}
			Sleep, 100
		}

		WinGetPos, x, y,lengthA,hightA, A
		Sleep, 100
		Click, lengthA/2, hightA/2
		Sleep, 100
		SendInput,^c	;复制 ctrl+c
		Sleep, 300
	return
;****************************************************
;****************************************************
;Listary 5 选项, 双击右键盲点确定/取消/应用
#If WinActive("Listary 选项 ahk_class Listary_WidgetWin_0") or WinActive("Listary 选项 ahk_class Listary_WidgetWin_1")
{
	$RButton::
	{
		;GV_MouseTimer := 300
		GV_KeyClickAction1 := "SendInput,{Click,Right}" ;RButton|Listary5
		GV_KeyClickAction2 := "gosub,Escape_Listary" ;点击取消|Listary5
		GV_MouseButton := 2
		GV_LongClickAction := "SendInput,{Click,Right}" ;RButton|Listary5
		gosub,Sub_MouseClick123
	}
	return

	$!a::SendInput,^a^c	;全选,20240621

	$MButton::	;by Tuutg,20240419
		;GV_MouseTimer := 300
		GV_KeyClickAction1 := "gosub,Apply_Listary" ;点击应用|Listary5
		GV_KeyClickAction2 := "gosub,Ok_Listary" ;点击确定|Listary5
		GV_MouseButton := 2
		GV_LongClickAction := "gosub,Apply_Listary" ;点击应用|Listary5
		gosub,Sub_MouseClick123
	return
}
#If
;****************************************************
Ok_Listary:
	CoordMode, Mouse, Window
	Click , 550,670
return

Escape_Listary:
	CoordMode, Mouse, Window
	Click , 645,670
return

Apply_Listary:	;by Tuutg,20240419
	CoordMode, Mouse, Window
	MouseGetPos, x, y, w, h, A
	Click , 740, 670
	Sleep,100
	Click , %x%, %y%
return
;****************************************************
;Listary搜索框增强,Listary5|6通用
#If WinActive("ahk_group Group_ListarySearchBox")
	CapsLock & r::	 ;Ctrl+A/清空搜索框
		SendInput,^a
		Sleep, 100
		SendInput,{Del}
	return
;****************************************************
	;默认^{Enter},在默认资管(win系统)中定位到文件,即光标在文件上;20240114
	$!s::SendInput,^{Enter}
	$MButton::SendInput,^{Enter}
	CapsLock & w::SendInput,^{Enter}
;****************************************************
	;打开文件夹,默认^{Enter},并在TC中定位到文件,即光标在文件上,;20240113
	$!w:: ;选中,即光标在文件上
	; CapsLock & w:: ; 用idm,快速save
		Clipboard := ""
		SendInput,^+c	;复制文件路径
		Sleep,500
		; EzTip(Clipboard,1)
		;这里改成自己对应的路径
		Run,"%COMMANDER_EXE%" /A /T /O /R="%Clipboard%"
		Sleep,500
		MyWinWaitActive("ahk_class TTOTAL_CMD")
	return
;****************************************************
	;Listary搜索框中,单击Caps={Ctrl 2},双击快速切换到ListaryPro6.3的文件窗口
	CapsLock::
		Suspend Permit
		if (WinActive("Listary ahk_exe Listary.exe")){ ;在Listary6.3文件搜索窗口
			GV_KeyClickAction1 :="gosub,ListaryFileToLauncher" ;文件搜索->启动器
			;GV_KeyClickAction1 :="SendInput,{Ctrl}"  ;ListaryPro5,3次^,TC启动器
			GV_KeyClickAction2 :="gosub,Listary" ;ListaryPro6.3,退出文件搜索窗口
			gosub,Sub_KeyClick123
		}
		else {	;不在Listary6.3文件搜索窗口
			GV_KeyClickAction1 :="gosub,Listary" ;ListaryPro5,3次^,TC启动器
			;GV_KeyClickAction1 :="SendInput,{Ctrl}" ;ListaryPro5,3次^,TC启动器
			GV_KeyClickAction2 :="gosub,Listary" ;ListaryPro6.3,4次^文件搜索窗口
			gosub,Sub_KeyClick123
		}
	return
;****************************************************
	$RButton::
		;GV_MouseTimer := 300
		GV_KeyClickAction1 := "SendInput,{Click,Right}" ;系统默认,RButton
		GV_KeyClickAction2 := "SendInput,{Escape}"  ;发送取消|退出
		GV_MouseButton := 2
		GV_LongClickAction := "SendInput,{Click,Right}" ;系统默认,RButton
		gosub,Sub_MouseClick123
	return

	/*;QuickLook预览,win7 and Win10,Listary5打开,Listary6关闭
	$!q::	;QuickLook预览
		Clipboard := ""
		SendInput,^+c	;复制文件路径
		Sleep,500
		; EzTip(Clipboard,1)
		; 这里改成自己对应的路径
		Run,"%RunCaptX%\SoftDir\QuickLook\QuickLook.exe" "%Clipboard%"
	return
	*/

	$Tab::	;双击tab,从Listary搜索框跳转回打开|保存对话框.20240520
		; GV_KeyTimer := 150
		GV_KeyClickAction1 := "SendInput,{Tab}"	;原版Tab,切换对象
		GV_KeyClickAction2 := "gosub,GotoDiagOpenAndSave"
		gosub,Sub_KeyClick123
	return
#if

ListaryFileToLauncher:	;适配Listary6.3文件搜索窗口
	WinGetPos,x,y,lengthA,hightA,Listary ahk_exe Listary.exe
	; EzTip(lengthA-170,1)
	CoordWinClick(lengthA-170,15) ;返回启动器按钮
return

GotoDiagOpenAndSave:	;从Listary搜索框跳转回打开|保存对话框
	WinActivate, ahk_class #32770
	SendInput,{Click}	;点击激活
return
;****************************************************
;主要适配Listary5.在Listary6部分失效
#If WinActive("ahk_exe Listary.exe")
	$!q::ControlSend,,!q	;工具栏->预览,Listary6打开,Listary5关闭
	; $!w::ControlSend,,!w  ;动  作->打开文件夹
	$!e::ControlSend,,!e	;动  作->关键字
	; $!r::ControlSend,,!r	;工具栏->最近文档
	$!t::ControlSend,,!t	;动  作->Edit Notepad++

	; $!a::SendInput,!v	;工具栏->智能命令|Atom
	; $!s::ControlSend,,!s	;动  作->发送到..
	$!d::SendInput,^d	;工具栏->收藏
	; $!f::ControlSend,,!f	;动  作->打开方式
	$!g::ControlSend,,!g	;工具栏->切换到程序安装目录

	$!z::ControlSend,,^d	;工具栏->收藏
	; $!x::ControlSend,,!x	;预留给Anytext
	$!c::ControlSend,,!c	;打开动作|=鼠标右键
	; $!v::ControlSend,,!v ;工具栏->智能命令
	$!b::ControlSend,,!b	;工具栏->浏览程序安装目录|Bash
#If
;****************************************************
;****************************************************
#If WinActive("uTools ahk_class Chrome_WidgetWin_1")
	CapsLock & r::	 ;Ctrl+A/清空搜索框
		SendInput,^a
		Sleep, 100
		SendInput,{Del}
	return
#If
;****************************************************
#If WinActive("文本代码对比 ahk_class Chrome_WidgetWin_1") 	;uTools_diff
{
	CapsLock & q:: ;向上翻页
	{
	if (GV_KeyClick_Continuous(GV_KeyTimer)){
		SendInput,{PgUp} ;向上翻页
	}
	else {
		Click,460,80 ;上一处差异
	}
	return
	}
;****************************************************
	CapsLock & a:: ;向下翻页
	{
	if (GV_KeyClick_Continuous(GV_KeyTimer)){
		SendInput,{PgDn} ;向下翻页
	}
	else {
		Click,500,80 ;下一处差异
	}
	return
	}
}
#If
;****************************************************
;****************************************************
;模擬滑鼠點擊圖片范例+控制網頁物件(預設為點擊),20240410
; $^+#z::ClickPicture("D:\RunCaptX\RunPlugins\Test\wxtz\wxtz.png", 1, 0,false,false)
; $^+#z::WebElementAction(" .btn-box .select > span","Input","查询",100)

;模擬滑鼠點擊圖片
ClickPicture(ImageFilePath,ClickCount:=1,Speed:=0,return:=true,ShowError:=true){
	pos:=GetPicturePosition(ImageFilePath)
	if %pos%{
		posX:=pos[1]
		posY:=pos[2]
		ClickPosition(posX,posY,ClickCount,Speed,,return)
		return [posX,posY]
	}else {
	if %ShowError% {
		MsgBox 畫面中找不到圖片`n %ImageFilePath%
	}
	return false
	}
}
;****************************************************
;模擬滑鼠點擊
ClickPosition(posX,posY,ClickCount:=1,Speed:=0,CoordMode:="Screen",Return:=true){
	;若使用相對模式
	if (CoordMode="Relative"){
		CoordMode,Mouse,Screen
		MouseGetPos, posX_i, posY_i ;儲存原來的滑鼠位置
		;根據點擊次數是否為零來使用MouseClick或MouseMove
	if %ClickCount%{
		MouseClick,,%posX%,%posY%,%ClickCount%,%Speed%,,R ;點擊相對位置
	}else {
		MouseMove, %posX%, %posY%,%Speed%
	}
	;若使用其他模式
	}else {
		CoordMode,Mouse,%CoordMode%
		MouseGetPos, posX_i, posY_i ;儲存原來的滑鼠位置
		;根據點擊次數是否為零來使用MouseClick或MouseMove
	if %ClickCount%{
		MouseClick,,%posX%,%posY%,%ClickCount%,%Speed%
	}else {
		MouseMove, %posX%, %posY%,%Speed%
	}
	}
	;是否點擊後返回
	if %return%{
		MouseMove, %posX_i%, %posY_i%,%Speed%
	}
	return
}
;****************************************************
;獲取圖片的位置
GetPicturePosition(ImageFilePath){
	Gui,add,picture,hwndmypic,%ImageFilePath%
	ControlGetPos,,,width,height,,ahk_id %mypic%
	CoordMode Pixel
	ImageSearch, FoundX, FoundY, 0, 0, A_ScreenWidth, A_ScreenHeight,%ImageFilePath%
	CoordMode Mouse
	if %FoundX%{
		return [FoundX+width/2,FoundY+height/2]
	} else {
		return FoundX
	}
}
;****************************************************
;****************************************************
;控制網頁物件(預設為點擊)
WebElementAction(selector,action:="Click",value:=0,delay:=300){
	;設置獲取網頁物件語法
	element_str=document.querySelector("%selector%")
	;設置動作語法
	if (action="Click"){
	action_str:=".click()"
	}else if (action="Focus"){
	action_str:=".focus();" . element_str . ".select()"
	}else if (action="Input"){
	action_str=.value="%value%"
	}else if (action="Select"){
	action_str=.selectedIndex="%value%"
	}
	;生成Javascript語法並在瀏覽器上執行
	javascript_str:=element_str . action_str
	ExecuteJavascriptOnUrlBar(javascript_str,delay)
	Return
}
;****************************************************
;在瀏覽器上執行Javascript, v1.0
ExecuteJavascriptOnUrlBar(str,delay:=300){
	Sleep %delay%
	;聚焦在網址列
	Send ^l
	Sleep %delay%
	;輸入Javascript語法
	AutoInput("_Javascript:" . str . ";void(0);")
	;刪除前綴底線符號，並執行Javascript語法
	Sleep %delay%
	Send {Home}{Delete}{Enter}
	Return
}
;****************************************************
;****************************************************
#If WinActive("ahk_exe tagLyst.exe")
	F1::Click , 25,50	;切换显示左侧边栏
	$F3::^f	;Ctrl+F进行关键字查找
	$!f::SendInput,^n	;添加新的文件或文件夹
	CapsLock & w::^n ;添加新的文件或文件夹
	$!r::SendInput,^{F5}	;重设筛选条件并刷新结果

	CapsLock & r::
	{
		if (GV_KeyClick_Continuous(GV_KeyTimer)){
			SendInput,{F5} ;刷新结果
		}
		else {
			SendInput,{Del}	;删除
		}
	}
	return

	CapsLock & t::
	{
		if (GV_KeyClick_Continuous(GV_KeyTimer)){
			SendInput,+{Del} ;彻底删除
		}
		else {
			SendInput,{BackSpace}	;上级目录
		}
	}
	return

	$!s::SendInput,^{Enter}	;资管打开当前选中文件

	$RButton::
	{
		;GV_MouseTimer := 300
		GV_KeyClickAction1 := "SendInput,{Click,Right}" ;系统默认,RButton
		GV_KeyClickAction2 := "SendInput,{BackSpace}" ;上级目录
		GV_MouseButton := 2
		GV_LongClickAction := "SendInput,{Click,Right}" ;点击取消
		gosub,Sub_MouseClick123
	}
	return

	CapsLock & f::Click , 1090,650	;导入文件 2023/07/10 16:43
	CapsLock & s::Click , 1000,650	;引用文件 2023/07/10 16:43
	CapsLock & g::Click , , 2	 ;左键双击,打开

	~Enter::
	~MButton::
	{
		ClickPicture("%RunCaptX%\RunPlugins\Test\tag\完成.png", 1, 0,true,false)	;完成
		ClickPicture("%RunCaptX%\RunPlugins\Test\tag\确定.png", 1, 0,true,false)	;确定
		ClickPicture("%RunCaptX%\RunPlugins\Test\tag\保存.png", 1, 0,true,false)	;保存
		ClickPicture("%RunCaptX%\RunPlugins\Test\tag\S&O.png", 1, 0,true,false)		;保存并打开
		ClickPicture("%RunCaptX%\RunPlugins\Test\tag\Clink.png", 1, 0,true,false)	;创建引用链接
		;ClickPicture("%RunCaptX%\RunPlugins\Test\tag\S&M.png", 1, 0,true,false)	;开始移动
	}
	return
#If
;****************************************************
;****************************************************
MouseNotUnder(Controls){
	MouseGetPos,,,, Control
	if Control != Controls
	return, true
}
;****************************************************
;****************************************************
#If WinActive("ahk_class 360ExplorerFrame") or WinActive("ahk_exe Q-Dir.exe") or WinActive("ahk_exe Q-Dir32.exe")
	$Space::
	if ((A_CaretX ="") and (A_Cursor != "IBeam")){	 ;非输入模式
		gosub, QuickLook	;QuickLook预览
	}
	else {			;输入模式
		SendInput,{Space}
	}
	return
#If
;****************************************************
;****************************************************
;浏览器点击关闭按钮,将浏览器窗口最小化到任务栏
; 定义一个函数，用于将浏览器窗口最小化
; 定义一个函数，用于模拟鼠标点击
ClickMouse(x, y)
{
	Click, x, y, 1 ; 1 表示左键点击
}

#If WinActive("ahk_group Group_browser")
	WinGetPos, x, y, lengthA, hightA, A
	CloseButtonX := %lengthA%-33 ; 根据实际情况调整关闭按钮的X坐标
	CloseButtonY := %y%+10 ; 根据实际情况调整关闭按钮的Y坐标
	ClickCloseButton := ClickMouse(CloseButtonX, CloseButtonY)
	; 将最小化操作绑定到关闭按钮的点击事件
	^!::ClickMouse(CloseButtonX, CloseButtonY)
	if ErrorLevel = 0 ; 如果模拟点击操作成功（即点击了关闭按钮），则调用最小化函数
	{
		WinMinimize, %BrowserTitle%
	}
#If
;****************************************************
;****************************************************
MyWinRunWaitActive(title){	;激活并左侧半屏显示
	CoordMode, Mouse, Window
	WinWait, %title%
	IfWinNotExist, %title%
	{
		Run, "C:\Windows\explorer.exe"
		WinActivate, %title%
		WinWaitActive, %title%
		WinMove, %title%, , 0, 0, A_ScreenWidth/2-1, A_ScreenHeight-75
	}
	else {
		WinActivate, %title%
		WinWait, %title%
		WinMove, %title%, , 0, 0, A_ScreenWidth/2-1, A_ScreenHeight-75
	}
	return
}
;****************************************************
;****************************************************
;Snipaste黑名单,修复snipaste快捷键强制占用,恢复其他软件,F1,F3默认功能
#If WinActive("ahk_group Group_Code")
	$F1::ControlSend,,{F1}
	$F3::ControlSend,,{F3}
#If
;****************************************************
#If WinActive("ahk_group Group_disableSnipaste")
	; $F1::ControlSend,,{F1}
	$F3::ControlSend,,{F3}
#If
;****************************************************
;Runany黑名单,修复Runany快捷键`强制占用,恢复其他软件`默认功能,如Listary
#If WinActive("ahk_group Group_disableRunany")
	$`::SendInput, `` ;屏蔽Runany快捷键`
#If
;****************************************************
;****************************************************
;GridMove,切换窗口,半屏留空|留空录屏,用于键盘鼠标操作演示

;切换窗口到左侧半屏
^#!a::	;左手模式
^#!Left::	;左右手模式
Tab & LButton::	;键鼠配合
WinMoveToLeftHalf:	;切换窗口到左侧半屏
SendInput,^#!2	;GridMove,键鼠配合
Sleep,200
WinGetPos,x,y,w,h,A
; if ((w >= A_ScreenWidth/2) or (x <= A_ScreenWidth/2) and (h=A_ScreenHeight)){	;20240602
	; SendInput,#{Left}	;系统默认,双手键盘
; }
return
;****************************************************
;切换窗口到右侧半屏
^#!s::	;左手模式
^#!Right::	;左右手模式
Tab & RButton::	;键鼠配合
WinMoveToRightHalf:	;切换窗口到右侧半屏
SendInput,^#!3	;GridMove,键鼠配合
Sleep,200
WinGetPos,x,y,w,h,A
; if ((w >= A_ScreenWidth/2) or (x >= A_ScreenWidth/2) and (h=A_ScreenHeight)){	;20240602
	; SendInput,#{Right}	;系统默认,双手键盘
; }
return
;****************************************************
;排除ahk_class Photoshop,保持默认Space抓手功能不变,20240501
#If !WinActive("ahk_group DisableGridMove")
	Space & LButton::gosub,WinMoveToLeftHalf	;键鼠配合,左半屏幕
	Space & RButton::gosub,WinMoveToRightHalf	;键鼠配合,右半屏幕
#If
;****************************************************
;****************************************************
;任务管理器,(右键->)结束进程(E)
#If WinActive("ahk_exe Taskmgr.exe")
	CapsLock & r::SendInput,{Del}{Enter}	;结束进程(E)
	CapsLock & t::SendInput,{Del}{Enter}	;结束进程(E)

	/*
	CapsLock & r::	 ;右键->结束进程或树(T|E)|区分系统.
		SendInput,{Click,Right}
		Sleep 100	;系统右键,用ControlSend,避免Listary拦截菜单选项输入
		if A_OSVersion in WIN_2003, WIN_XP, WIN_7
		{	;结束进程树(T)
			ControlSend,,{t}{Enter},ahk_class #32768	;防止激活Listary,拦截输入
		}
		else
		{	;结束进程(E)
			ControlSend,,{e},ahk_class #32768	;防止激活Listary,拦截输入
		}
	return
	*/
#If
;****************************************************
;****************************************************
#If WinActive("ahk_exe BCompare.exe")	;Tuutg,20231220
	; $MButton::	;在Group_Code组已定义
		; SendInput,^s{Enter}
		; Sleep,100
		; ControlClick,TUiBitBtn1	;立即同步(&Y)
	; return

	$RButton::
		;GV_MouseTimer := 300
		GV_KeyClickAction1 := "SendInput,{Click,Right}" ;系统默认,RButton
		GV_KeyClickAction2 := "SendInput,{BackSpace}"	;后退目录,BackSpace
		GV_MouseButton := 2
		GV_LongClickAction := "SendInput,{Click,Right}" ;系统默认,RButton
		gosub,Sub_MouseClick123
	return

	$!RButton::SendInput,!{Right}	;前进目录

	$!r::SendInput,{F2}	;重命名|编辑,20240328

	$r::			;重命名|编辑
		if (A_Cursor = "IBeam"){	;输入模式
			SendInput,r
		}
		else {	;非输入模式
			SendInput,{F2}
		}
	return

	CapsLock & f::SendInput,^r	;复制到右边
	CapsLock & s::SendInput,^l	;复制到左边

	$f::
		if (A_Cursor = "IBeam"){	;输入模式
			SendInput,f
		}
		else {	;非输入模式
			SendInput,^r
		}
	return

	$s::
		if (A_Cursor = "IBeam"){	;输入模式
			SendInput,s
		}
		else {	;非输入模式
			SendInput,^l
		}
	return

	$e::
		if (A_Cursor = "IBeam"){	;输入模式
			SendInput,e
		}
		else {	;非输入模式
			SendInput,^p
		}
	return

	$d::
		if (A_Cursor = "IBeam"){	;输入模式
			SendInput,d
		}
		else {	;非输入模式
			SendInput,^n
		}
	return

	$w::	;20240606
		if (A_Cursor = "IBeam"){	;输入模式
			SendInput,w
		}
		else {	;非输入模式
			SendInput,{F5}	;刷新|重载
		}
	return

	$g::	;20240621
		if (A_Cursor = "IBeam"){	;输入模式
			SendInput,g
		}
		else {	;非输入模式
			SendInput,{Enter}	;确认|进入
		}
	return

	$t::	;20240621
		if (A_Cursor = "IBeam"){	;输入模式
			SendInput,t
		}
		else {	;非输入模式
			SendInput,{Del}	;删除|文件
		}
	return

	$a::	;20240606
		if (A_Cursor = "IBeam"){	;输入模式
			SendInput,a
		}
		else {	;非输入模式
			SendInput,^s	;保存文件
		}
	return

	CapsLock & q::SendInput,^p	;上一个差异,20231209
	CapsLock & a::SendInput,^n	;下一个差异,20231209

	$^enter::		;文件夹比较|属性设置
	$!MButton::
		SendInput,{Click,Right}
		Sleep,100
		SendInput,{Up}{Enter}
		Sleep,100
		SendInput,{Text}r
	return

	$!LButton::	;资管打开|编辑
		SendInput,{Click,Right}
		Sleep,100
		SendInput,{up}{Enter}
		Sleep,100
		SendInput,{Text}o
	return

	$!w::SendInput,^s	;保存文件

	CapsLock & w:: 	;保存文件|会话
		if (GV_KeyClick_Continuous(GV_KeyTimer)){
			SendInput,^+s 		;保存会话
		}
		else {
			SendInput,^s 		;保存文件
		}
	return

	CapsLock & b:: 	;保存文件|会话
		if (GV_KeyClick_Continuous(GV_KeyTimer)){
			SendInput,^+b 		;三方比较<-
		}
		else {
			SendInput,^b 		;三方比较->
		}
	return
#If
;****************************************************
#If WinActive("更新 ahk_exe BCompare.exe")
CapsLock & r::	;右键|资管|删除
	SendInput,{Click,Right}	;右键
	Sleep,100
	SendInput,{up 1}{Enter}	;资管
	Sleep,100
	SendInput,{up 2}{Enter}	;删除
return

$!e::	;右键|排除
	SendInput,{Click,Right}	;右键
	Sleep,100
	SendInput,{up 4}{Enter}	;排除
return
#If
;****************************************************
;****************************************************
#If WinActive("ahk_exe uc.exe")
	$MButton::SendInput,^s{Enter}

	$RButton::
		;GV_MouseTimer := 300
		GV_KeyClickAction1 := "SendInput,{Click,Right}" ;系统默认,RButton
		GV_KeyClickAction2 := "SendInput,!{Left}"	;后退目录
		GV_MouseButton := 2
		GV_LongClickAction := "SendInput,{Click,Right}" ;系统默认,RButton
		gosub,Sub_MouseClick123
	return

	; $!RButton::SendInput,!{Right}	;前进目录

	CapsLock & f::SendInput,!{Right}	;复制到右边
	CapsLock & s::SendInput,^{Left}	;复制到左边

	CapsLock & q::SendInput,{F4}	;上一个差异
	CapsLock & a::SendInput,{F3}	;下一个差异

	$^enter::		;会话设置|F5刷新
		SendInput,{Click,Right}
		Sleep,100
		SendInput,{Up}{Enter}
	return

	$!MButton::	;资管打开2
		SendInput,{Click,Right}
		Sleep,100
		SendInput,{Up  4}{Enter}
	return

	$!LButton::	;资管打开1
		SendInput,{Click,Right}
		Sleep,100
		SendInput,{up  5}{Enter}
	return

	$!w::SendInput,^s	;保存文件

	CapsLock & w:: 	;保存文件|会话
		if (GV_KeyClick_Continuous(GV_KeyTimer)){
			SendInput,^+s 		;保存会话
		}
		else {
			SendInput,^s 		;保存文件
		}
	return
#If
;****************************************************
;****************************************************
#If WinActive("ahk_exe WinMergeU.exe")	;Tuutg,20231220
	$MButton::SendInput,^s{Enter}

	$RButton::
		;GV_MouseTimer := 300
		GV_KeyClickAction1 := "SendInput,{Click,Right}" ;系统默认,RButton
		GV_KeyClickAction2 := "SendInput,{BackSpace}"	;后退目录
		GV_MouseButton := 2
		GV_LongClickAction := "SendInput,{Click,Right}" ;系统默认,RButton
		gosub,Sub_MouseClick123
	return

	; $!RButton::SendInput,!{Right}	;前进目录

	CapsLock & f::SendInput,^!{Right}	;复制到右边并前进
	CapsLock & s::SendInput,^!{Left}	;复制到左边并前进

	CapsLock & q::SendInput,!{Up}	;上一个差异
	CapsLock & a::SendInput,!{Down}	;下一个差异

	; $^enter::		;默认程序编辑
	; $!MButton::
		; SendInput,{Click,Right}
		; Sleep,100
		; SendInput,{Up}{Enter}
		; Sleep,100
		; SendInput,{Text}er
	; return

	$!RButton::	;资管打开
		SendInput,{Click,Right}
		Sleep,100
		SendInput,{up 2}{Enter}
		Sleep,100
		SendInput,{Left}{up}{Enter}
	return

	$!w::SendInput,^s	;保存文件

	CapsLock & w:: 	;保存文件|会话
		if (GV_KeyClick_Continuous(GV_KeyTimer)){
			SendInput,!fv 		;保存会话
		}
		else {
			SendInput,^s 		;保存文件
		}
	return
#If
;****************************************************
;****************************************************
RunWechat:	;一键启动微信
	; 定义一个数组来存储微信的安装路径
	wechat1 := "C:\Program Files\Tencent\WeChat\WeChat.exe"
	wechat2 := "D:\Program Files\Tencent\WeChat\WeChat.exe"
	wechat3 := "E:\Program Files\Tencent\WeChat\WeChat.exe"
	wechat4 := "F:\Program Files\Tencent\WeChat\WeChat.exe"
	wechat5 := "G:\Program Files\Tencent\WeChat\WeChat.exe"
	wechat6 := "C:\Program Files (x86)\Tencent\WeChat\WeChat.exe"
	wechat7 := "D:\Program Files (x86)\Tencent\WeChat\WeChat.exe"
	wechat8 := "E:\Program Files (x86)\Tencent\WeChat\WeChat.exe"
	wechat9 := "F:\Program Files (x86)\Tencent\WeChat\WeChat.exe"
	wechat10:= "G:\Program Files (x86)\Tencent\WeChat\WeChat.exe"
	wechat11 := "%Wx_Path%"	;如果不在默认位置, 手动填写安装路径
	wechatPaths:= [wechat1,wechat2,wechat3,wechat4,wechat5,wechat6,wechat7,wechat8,wechat9,wechat10,wechat11]

	; 遍历数组中的每个路径，并启动微信
	loop, % wechatPaths.MaxIndex()
	{
		; 检查当前索引的路径是否存在
		IfExist, % wechatPaths[A_Index]
		{
			Run, % wechatPaths[A_Index]
			Sleep, 1000 ; 等待1秒
		}
	}
return
;****************************************************
;****************************************************
; ACDSee.exe,20231216
#If WinActive("ahk_exe ACDSee.exe")
	CapsLock & s::SendInput,{PgDn}
	CapsLock & f::SendInput,{PgUp}
#If
;****************************************************
;****************************************************
; Win11系统默认图片查看器
#If WinActive(" ‎- 照片 ahk_class ApplicationFrameWindow") and (A_Cursor != "IBeam")
	~$q::SendInput,{Text}f	;显示影片
	~$w::SendInput,^e	;编辑图像
	~$e::SendInput,^{NumpadAdd}	;放大|上
	~$r::SendInput,^r	;旋转图像
	~$t::SendInput,^l	;锁定界面
;****************************************************
	~$a::SendInput,^s    	;另保为
	~$s::SendInput,{Left}	;<-|左
	~$d::SendInput,^{NumpadSub}	;缩小|下
	$f::SendInput,{Right}	;->|右
	~$g::SendInput,^1	;1:1
;****************************************************
	~$z::SendInput,{Text}l	;+|-收藏夹
	~$x::			;调整图像大小
		SendInput,{Click,Right}
		Sleep,50
		SendInput,{Up 6}{Enter}
	return
	~$c::SendInput,^c	;复制
	~$v::			;创建视频
		SendInput,{Click,Right}
		Sleep,50
		SendInput,{Up 2}{Enter}
	return
	~$b::			;在资源管理器打开文件
		SendInput,{Click,Right}
		Sleep,50
		SendInput,{Up 3}{Enter}
	return
;****************************************************
	~$MButton::SendInput,^{0}	;缩放以适应
	$!LButton::SendInput,!{Enter}	;文件信息
	$!WheelUp::SendInput,{Left}	;<-,20231225
	$!WheelDown::SendInput,{Right}	;->,20231225

	$F4::	;打开方式
		SendInput,{Click,Right}
		Sleep,50
		SendInput,{Up 4}{Enter}
	return

	$!w::Click,30 20 1 	;浏览所有文件
#If
;****************************************************
;****************************************************
;Anytext搜索增强,搜索通用.20240116
#If WinActive("ahk_exe ATGUI.exe")
	CapsLock & r::	 ;Ctrl+A/清空搜索框
		SendInput,^a
		Sleep, 100
		SendInput,{Del}
	return
;****************************************************
	;默认^e,在默认资管(win系统)中定位到文件,即光标在文件上;20240114,
	$!s::SendInput,^e
	$MButton::SendInput,^e
;****************************************************
	;打开文件夹,默认^e,并在TC中定位到文件,即光标在文件上,;20240113
	$!w:: ;选中,即光标在文件上
	CapsLock & w:: ; 用idm,快速save
		Clipboard := ""
		SendInput,{Right}
		Sleep,100
		SendInput,{Down 2} ;复制文件路径
		; EzTip(Clipboard,1)
		;这里改成自己对应的路径
		Run,"%COMMANDER_EXE%" /A /T /O /R="%Clipboard%"
		Sleep,500
		MyWinWaitActive("ahk_class TTOTAL_CMD")
	return
;****************************************************
	$RButton::
		;GV_MouseTimer := 300
		GV_KeyClickAction1 := "SendInput,{Click,Right}" ;系统默认,RButton
		GV_KeyClickAction2 := "SendInput,{Escape}"  ;发送取消
		GV_MouseButton := 2
		GV_LongClickAction := "SendInput,{Click,Right}" ;系统默认,RButton
		gosub,Sub_MouseClick123
	return

	$F4::	;F4Menu
		SendInput,{Click,Right}
		Sleep,100
		SendInput,{F}{Enter}
	return

	CapsLock & f::
	if MouseUnder("Qt5QWindowIcon11"){
		SendInput,{Click,Right}{Down}
	} else{
		SendInput,{Right}
	}
return
#if
;****************************************************
;****************************************************
;FileLocatorPro.exe搜索增强,搜索通用.20240116
#If WinActive("ahk_exe FileLocatorPro.exe")
	CapsLock & r::	 ;Ctrl+A/清空搜索框
		SendInput,^a
		Sleep, 100
		SendInput,{Del}
	return
;****************************************************
	;默认+!r,在默认资管(win系统)中定位到文件,即光标在文件上;20240114,
	$!s::SendInput,+!r
	$MButton::SendInput,+!r
;****************************************************
	;打开文件夹,默认^e,并在TC中定位到文件,即光标在文件上,;20240113
	$!w:: ;选中,即光标在文件上
	CapsLock & w:: ; 用idm,快速save
		Clipboard := ""
		SendInput,+!c	;复制文件路径
		; Clipboard=%Clipboard%
		EzTip(Clipboard,1)
		;这里改成自己对应的路径
		Run,"%COMMANDER_EXE%" /A /T /O /R="%Clipboard%"
		Sleep,500
		MyWinWaitActive("ahk_class TTOTAL_CMD")
	return
;****************************************************
	$RButton::
		;GV_MouseTimer := 300
		GV_KeyClickAction1 := "SendInput,{Click,Right}" ;系统默认,RButton
		GV_KeyClickAction2 := "SendInput,{Escape}"  ;发送取消
		GV_MouseButton := 2
		GV_LongClickAction := "SendInput,{Click,Right}" ;系统默认,RButton
		gosub,Sub_MouseClick123
	return

	$F4::	;F4Menu
		SendInput,{Click,Right}
		Sleep,100
		SendInput,{F}{Enter}
	return

	CapsLock & f::
		if MouseUnder("BCGPGridCtrl.*"){
			SendInput,{Click,Right}{Down}
		} else{
			SendInput,{Right}
		}
	return
#if
;****************************************************
;****************************************************
; 火绒-文件解锁->解锁
#If WinActive("文件解锁 ahk_class HRFileUnlock")
	g::
	CapsLock & g::
		WinActivate,文件解锁 ahk_class HRFileUnlock
		Click,570 300 1	;解锁
		Sleep,100
		WinActivate,ahk_class ATL:010AED28
		Click,325 215 1 ;确定
	return
#If
;****************************************************
;****************************************************
; fupx.exe,20240124
#If WinActive("ahk_exe fupx.exe")
	CapsLock & g::
	$NumpadDot::	;压缩|.
		SendInput,{Click,Right}
		Sleep,100
		SendInput,{Down 1}{Enter}
	return

	CapsLock & b::	;20240626
	$`;::	;解压,B不压缩|;
		SendInput,{Click,Right}
		Sleep,100
		SendInput,{Down 2}{Enter}
	return

	CapsLock & r::Click,215 70	;删除
	CapsLock & t::	;清除. 20240626
		SendInput,{Click,Right}
		Sleep,100
		SendInput,{Down 6}
		Sleep,100
		SendInput,{Enter 2}
	return

	$!e::	;by Tuutg,20240626
	$F3::Click,20 220	;检查已压缩

	$!d::	;by Tuutg,20240626
	$F4::Click,20 245	;检查未压缩


	$NumpadAdd::	;+
	$!a::SendInput,^a	;全部选择

	$NumpadSub::	;取消全部选择|-
		SendInput,{Click,Right}
		Sleep,100
		SendInput,{Down 9}{Enter}
	return

	$NumpadMult::	;反向|*
		SendInput,{Click,Right}
		Sleep,100
		SendInput,{Down 11}{Enter}
	return

	$NumpadDiv::	;取消选择|/
		SendInput,{Click,Right}
		Sleep,100
		SendInput,{Down 13}{Enter}
	return
#If
;****************************************************
;****************************************************
;XMind.exe,20240315
#If WinActive("ahk_exe XMind.exe")
Space & q::SendInput,^o 	;文件>打开
Space & w::SendInput,^t		;文件>新建
Space & e::SendInput,^`=	;+,放大
Space & r::SendInput,^r		;前往中心主题
Space & t::SendInput,^+t	;显|隐标签栏

Space & a::SendInput,^+s	;保存|另存
Space & s::SendInput,{PgUp}	;上一页
Space & d::SendInput,^`-	;-,缩小
Space & f::SendInput,{PgDn}	;下一页
Space & g::SendInput,^0^s	;缩放100%

Space & z::SendInput,^+f	;适合页面
Space & x::SendInput,^x 	;显示属性
Space & c::SendInput,^c		;复制
Space & v::SendInput,^v		;删除|不要
Space & b::SendInput,^!b	;标注

!d::SendInput,^d	;复制->粘贴主题

; $Space::SendInput,{Blind}{Space} ;恢复默认打字上屏功能
$Space::Space ;恢复空格打字上屏,by Tuutg,20240425
#If
;****************************************************
;****************************************************
;Win7默认看图器Photo_Lightweight_Viewer,20240313
#If WinActive("ahk_class Photo_Lightweight_Viewer")
$q::SendInput,{Click,Right}{Text}{h} ;显示打开方式
$w::SendInput,^`,		;向左旋转
$e::SendInput,{WheelUp}	;+,放大
$r:: ;SendInput,^`.		;向右旋转
	SendInput,{Click,Right}{l}{Enter}
return
$t::SendInput,{Click,Right}{h}{Enter} ;画图打开

$a::SendInput,!fk		;保存|另存
$s::SendInput,{Left}		;上一页
$d::SendInput,{WheelDown}	;-,缩小
$f::SendInput,{Right}		;下一页
$g::				;定位到系统
	SendInput,{Click,Right}{i}{Enter}
return

$z::SendInput,^!0			;适合页面
$x::SendInput,{Click,Right}{r}{Enter} 	;显示属性
$c::SendInput,^c	;复制
$v::SendInput,{Del}			;删除|不要
$b::SendInput,{Click,Right}{k}{Enter} 	;设为背景

$RButton::
	;GV_MouseTimer := 300
	GV_KeyClickAction1 := "SendInput,{Click,Right}" ;系统默认,RButton
	GV_KeyClickAction2 := "SendInput,!{F4}"	;关闭取消
	GV_MouseButton := 2
	GV_LongClickAction := "SendInput,{Click,Right}" ;系统默认,RButton
	gosub,Sub_MouseClick123
return
#If
;****************************************************
;****************************************************
/*;Win7,ahk_exe mspaint.exe,系统画图,20240315
#If WinActive("ahk_exe mspaint.exe") and (A_CaretX ="") and (A_Cursor != "IBeam")
$q::SendInput,!fo 	;文件>打开
$w::SendInput,!hirol	;向左旋转
$e::SendInput,!vi	;+,放大
$r::SendInput,!hiror	;向右旋转
$t::SendInput,!ht 	;工具>文字

$a::SendInput,!fs	;保存|另存
$s::SendInput,{WheelUp} ;{Left}
$d::SendInput,!vo	;-,缩小
$f::SendInput,{WheelDown} ;{Right}
$g::SendInput,!vm	;缩放100%

$z::SendInput,!hm	;局部缩放
$x::SendInput,!hsh	;形状线条
$c::SendInput,!hec	;编辑颜色
$v::SendInput,!hser	;选择|同PS
$b::SendInput,!hb 	;笔刷工具

~$MButton::Send,^{0} ;缩放以适应
#If
*/
;****************************************************
;/*Win11,ahk_exe mspaint.exe,系统画图,20240317
#If WinActive("ahk_exe mspaint.exe") and (A_CaretX ="") and (A_Cursor != "IBeam")
$q::SendInput,^o 		;文件>打开

$w::			;向左旋转
	BlockInput on
	WinGetPos, x, y,lengthA,hightA, A
	CoordWinMove(lengthA/2,hightA/2)
	SendInput,{Click,Right}
	Sleep,50
	SendInput,{Down 2}
	Sleep,50
	SendInput,{Right}
	Sleep,50
	SendInput,{Down}
	Sleep,50
	SendInput,{Enter}
	BlockInput Off
Return

$!w::SendInput,!w 		;重设大小

$e::SendInput,^{PgUp}	;+,放大

$r::				;向右旋转
	BlockInput on
	WinGetPos, x, y,lengthA,hightA, A
	CoordWinMove(lengthA/2,hightA/2)
	SendInput,{Click,Right}
	Sleep,50
	SendInput,{Down 2}
	Sleep,50
	SendInput,{Right}
	Sleep,50
	SendInput,{Enter}
	BlockInput Off
Return

$t::SendInput,!t 		;工具>铅笔


$a::SendInput,^s		;保存|另存
$s::SendInput,{WheelUp} ;{Left}
$d::SendInput,^{PgDn}	;-,缩小
$f::SendInput,{WheelDown} ;{Right}
$g::SendInput,^0	;缩放100%


$z::SendInput,^1		;适应窗口大小

$x::				;形状线条
	BlockInput on
	SendInput,{Alt Down}
	Sleep,50
	SendInput,{Alt Up}
	SendInput,{Text}sh		;形状线条
	BlockInput Off
Return

$c::				;编辑颜色
	BlockInput on
	SendInput,{Alt Down}
	Sleep,50
	SendInput,{Alt Up}
	SendInput,{Text}ec		;编辑颜色
	BlockInput Off
Return

$v::				;选择|同PS
	BlockInput on
	SendInput,{Alt Down}
	Sleep,50
	SendInput,{Alt Up}
	SendInput,se		;选择|同PS
	BlockInput Off
Return

$b::				;笔刷工具
	BlockInput on
	SendInput,{Alt Down}
	Sleep,50
	SendInput,{Alt Up}
	SendInput,{Text}b		;选择|同PS
	BlockInput Off
Return

$h::				;水平翻转
	BlockInput on
	WinGetPos, x, y,lengthA,hightA, A
	CoordWinMove(lengthA/2,hightA/2)
	SendInput,{Click,Right}
	Sleep,50
	SendInput,{Down 3}
	Sleep,50
	SendInput,{Right}
	Sleep,50
	SendInput,{Enter}
	BlockInput Off
Return

$!v::				;垂直翻转
	BlockInput on
	WinGetPos, x, y,lengthA,hightA, A
	CoordWinMove(lengthA/2,hightA/2)
	SendInput,{Click,Right}
	Sleep,50
	SendInput,{Down 3}
	Sleep,50
	SendInput,{Right}
	Sleep,50
	SendInput,{Down}
	Sleep,50
	SendInput,{Enter}
	BlockInput Off
Return

~$MButton::Send,^1		;缩放以适应
#If
*/
;****************************************************
;****************************************************
;ahk_exe ACDSee.exe,20240313
#If WinActive("ahk_exe ACDSee.exe")
$q::SendInput,^q 		;动作
$w::SendInput,^j{1}{Enter}	;向左旋转
$e::SendInput,{NumpadAdd}	;+,放大
$r::SendInput,^j{3}{Enter}	;向右旋转
$t::SendInput,^e 		;编辑器打开

$a::SendInput,^s		;保存|另存
$s::SendInput,{PgUp}		;上一页
$d::SendInput,{NumpadSub}	;-,缩小
$f::SendInput,{PgDn}		;下一页
$g::SendInput,{Text}f		;全屏

; $z::SendInput,{Text}z	;适合页面
$x::SendInput,^x		;剪切
$c::SendInput,^c	;复制
$v::SendInput,^v		;粘贴
$b::SendInput,^b 		;设为背景
#If
;****************************************************
;****************************************************
;鼠标左|中|右集中增强,2024/03/24 12:39
;1.双击按右键，保存|强制关闭
#If (!WinActive("ahk_exe explorer.exe") and !WinActive("ahk_group Group_explorer") and !WinActive("ahk_group Group_browser") and !WinActive("ahk_class TTOTAL_CMD"))
$RButton::
	;GV_MouseTimer := 300
	GV_KeyClickAction1 := "SendInput,{Click,Right}" ;系统默认,RButton
	GV_KeyClickAction2 := "gosub,SaveAndClose"  ;保存|强制关闭
	GV_MouseButton := 2
	GV_LongClickAction := "SendInput,{Click,Right}" ;系统默认,RButton
	gosub,Sub_MouseClick123
return
#If
;****************************************************
SaveAndClose:
	SendInput,^s	;先保存
	Sleep,100
	SendInput,!{F4}	;强制关闭
return
;****************************************************
;****************************************************
;2.纯鼠标剪切|复制|粘贴,~LButton & X|C|V, 2024/03/24 12:31
#If !WinActive("ahk_class TTOTAL_CMD")
~LButton & x::
	SendInput,^x
	Sleep,300
	StringLeft,clipboard_left,Clipboard,500
	ToolTip,%clipboard_left% ;在鼠标右侧显示clip(Clipboard内容)
	Sleep ,1000
	ToolTip
return

~LButton & c::
~LButton & RButton::
	SendInput,^c	;复制
	Sleep,300
	StringLeft,clipboard_left,Clipboard,500
	ToolTip,%clipboard_left% ;在鼠标右侧显示clip(Clipboard内容)
	Sleep ,1000
	ToolTip
return

~LButton & v::
~LButton & MButton::
	SendInput,^v
return
#If
;****************************************************
;****************************************************

;3.中键盲点简单对方框第一个选项,详见EztC部分代码

;****************************************************
;****************************************************

;************************************************************************
;====================默认功能区6: 简易剪切板历史(文本)===================
;简易剪切板历史(文本)
ClipAppend:
	; 打开文件并读取内容到变量
	FileRead,ClipboardContent,%COMMANDER_PATH%\剪切板\ClipAppend.txt

	; 将剪贴板内容添加到变量的首行
	ClipboardContent = %clipboard%`r`n`r`n%ClipboardContent%

	; 将更新后的内容写入文件
	FileDelete, %COMMANDER_PATH%\剪切板\ClipAppend.txt
	FileAppend, %ClipboardContent%, %COMMANDER_PATH%\剪切板\ClipAppend.txt
Return

;************************************************************************
;===================默认功能区7: 快速输入自定义函数======================
;微信搜索框>输入联系人>点击联系人>跳转到聊天输入框
WeChatInput(InputStr){
	;聚焦搜索框
	if WinActive("ahk_class WeChatMainWndForPC"){
		Click,95 40
	}
	else {
		Click,55 30
	}
	SendInput, ^f	;定位搜索框
	Sleep,50
	SendInput, ^a	;选中搜索框
	Sleep,50
	SendInput, {Del}  ;清空搜索框
	Sleep,50
	SendInput, {Text}%InputStr%
	Sleep,600
	OutputDebug, "找到"
	SendInput, {Shift}{Enter}
	OutputDebug, "进入"
	Click, 170,100	 ;跳转到聊天输入框
return
}
;************************************************************************
;=======================默认功能区8: 快速输入:短语=======================
;快速输入:
;快速输入,全局通用.e.g. 连续输入tdw, 即可打印当前时间
:*o?:tdaw::	;Todaynow(当前时间),20240403
	;？号表示在单词中也会替换，e.g. tdw → V2019.....
	FormatTime, CurrentDateTime,, yyyy/MM/dd HH:mm:ss
	Send %CurrentDateTime%
Return
:o:jxca::
	;？号表示在单词中也会替换，e.g. Vtdw → V2019.....
	AutoInput("江西诚安新能源科技有限公司")
	; Sendinput, {Enter}
Return

:o:mma::
	;？号表示在单词中也会替换，e.g. Vtdw → V2019.....
	AutoInput("ca2020")
	; Sendinput, {Enter}
Return

:o:tel::
:o:ted::
	;？号表示在单词中也会替换，e.g. Vtdw → V2019.....
	AutoInput("180....0986")
	; Sendinput, {Enter}
Return

:o:eml::
:o:edx::
	;？号表示在单词中也会替换，e.g. Vtdw → V2019.....
	AutoInput("xds.2008@163.com")
	; Sendinput, {Enter}
Return
:o:tgv::
	;？号表示在单词中也会替换，e.g. Vtdw → V2019.....
	AutoInput("tuutg@live.cn")
	; Sendinput, {Enter}
Return

:o?:xdsf::
	;？号表示在单词中也会替换，e.g. Vtdw → V2019.....
	AutoInput("寻冬生")
	; Sendinput, {Enter}
Return

:o:gxw::
	Sendinput, #r
	Sleep, 100
	AutoInput("\\xds-yf")
	Sendinput, {Enter}
Return

:?:exr::
	;？号表示在单词中也会替换，e.g. Vtdw → V2019.....
	AutoInput("explorer.exe")
	; Sendinput, {Enter}
Return

:o:hbb::
	;？号表示在单词中也会替换，e.g. Vtdw → V2019.....
	AutoInput("已汇报")
	; Sendinput, {Enter}
Return

:o:ttg::
:o:tgg::
	;？号表示在单词中也会替换，e.g. Vtdw → V2019.....
	AutoInput("Tuutg")
	; Sendinput, {Enter}
Return

;************************************************************************
;====================默认功能区8: 微信联系人快速输入=====================
/*;微信联系人快速输入,按空格或回车,自动跳到对应联系人,要改成自己的微信联系人
;举例 :o:yl::--yl-快速输入简称, WeChatInput("杨磊")--杨磊 自己的微信联系人
#If WinActive("ahk_exe WeChat.exe") ;这里不要改动,下面{ }的改成自己的微信联系人
{
:o:yy::
:o:yl::      ;输入yl,按空格或回车,自动跳到对应联系人聊天输入框
	WeChatInput("杨磊")     ;点击输入框,输入联系人
Return

:o:wc::
:o:wz::
:o:wj::
	WeChatInput("文件传输助手")
Return

:o:drb::
	WeChatInput("订阅号")
Return

:o:gzf::
	WeChatInput("顾志丰")
Return

:o:yz::
	WeChatInput("杨总")
Return

:o:qx::
	WeChatInput("江西诚安～全喜线束工作沟通群")
Return

:o:xa::
	WeChatInput("诚安&小安车厂技术对接群")
Return

:o:yg::
	WeChatInput("印工@无锡晶汇")
Return

:o:zgq::
	WeChatInput("诚安职工群")
Return

:o:rdb::
	WeChatInput("诚安技术部")
Return

:o:qz::
	WeChatInput("钱总@减震厂")
Return

:o:cag::
:o:caq::
	WeChatInput("诚安管理人员群")
Return

:o:tt::
	WeChatInput("唐凯 *电动车电缆*")
Return

:o:ff::
	WeChatInput("叶一峰")
Return

:o:yy::
	WeChatInput("杨艳")
Return

:o:qg::
	WeChatInput("浅光暖笑@欧阳敏")
Return

:o:dsb::
	WeChatInput("诚安碟刹泵协调@吉强")
Return

:o:xh::
	WeChatInput("辛虹")
Return





}
#If ;这里不要改动,上面大{ }的改成自己的微信联系人
*/
;****************************************************
;****************************************************
;单键上下左右增强模式,适配QuickLook,EVERYTHING等软件
;QuickLook.exe,20240318
#If WinActive("ahk_exe QuickLook.exe") and (A_Cursor = "IBeam")
$e::SendInput,{Up}		;上
$d::SendInput,{Down}	;下
$s::SendInput,{Left}	;左
$f::SendInput,{Right}	;右

$k::SendInput,{Up}		;上
$j::SendInput,{Down}	;下
$h::SendInput,{Left}	;左
$l::SendInput,{Right}	;右
#If
;****************************************************
#If WinActive("ahk_exe QuickLook.exe") and (A_Cursor != "IBeam")
$e::SendInput,{WheelUp}	  ;放大
$k::SendInput,{WheelUp}	  ;放大
$d::SendInput,{WheelDown} ;缩小
$j::SendInput,{WheelDown} ;缩小

$s::SendInput,{Left}	;左
$f::SendInput,{Right}	;右
$h::SendInput,{Left}	;左
$l::SendInput,{Right}	;右
#If
;****************************************************
;****************************************************
;Everything.exe,20240326
#If WinActive("ahk_class EVERYTHING") and (A_Cursor != "IBeam") and (A_CaretX = "")
~$e::SendInput,{BackSpace}{Up}	;上|~,避免光标跑出搜索框无法打字,20240328
~$d::SendInput,{BackSpace}{Down}	;下|~,避免光标跑出搜索框无法打字,20240328
$s::SendInput,{Click,Right}{Down 3}{Enter} 	;右键->(资管)打开路径
$f::SendInput,{Click,Right}{Down 2}{Enter} 	;右键->(TC)浏览路径
#If
;****************************************************
;恢复打字,防止鼠标移动后无法打字,20240326
#If WinActive("ahk_class EVERYTHING") and ((A_Cursor = "IBeam") or (A_CaretX != ""))
$e::SendInput,{Text}e	;e
$d::SendInput,{Text}d	;d
$s::SendInput,{Text}s	;s
$f::SendInput,{Text}f	;f
#If
;****************************************************
;****************************************************
;视频剪辑组,如剪映,剪映国际版和Pr等.单独Space模式,20240404
;恢复输入框,I型光标|箭头+输入框同时存在都可打字功能,20240404
#If WinActive("ahk_group MediaEditor_SgSpace") and ((A_Cursor = "IBeam") or ((A_CaretX = "") and (A_Cursor = "Arrow")))

q::SendInput,q
w::SendInput,w
e::SendInput,e
r::SendInput,r
t::SendInput,t

; ****************************************************

a::SendInput,a
s::SendInput,s
d::SendInput,d
f::SendInput,f
g::SendInput,g

; ****************************************************

z::SendInput,z
x::SendInput,x
c::SendInput,c
v::SendInput,v
b::SendInput,b

#If

;****************************************************
;箭头光标为原版正常模式,其他光标为Tg定制模式,20240423
#If WinActive("ahk_group MediaEditor_SgSpace") and !(A_Cursor = "IBeam")

;CapsLock:上一个标记,下一个标记,时间轴→ ←快移,和时间轴相关
CapsLock & e::SendInput,!+m ;上一个标记

CapsLock & d::SendInput,+m ;下一个标记

CapsLock & s::SendInput,+{Left} ;←快移

CapsLock & f::SendInput,+{Right} ;→快移

CapsLock & g::	;字幕拆分|拆行
	GV_KeyClickAction1 := "SendInput,{Enter}" ;字幕拆分
	GV_KeyClickAction2 := "SendInput,^{Enter}" ;字幕拆行
	gosub,Sub_KeyClick123
return

;****************************************************

;Space:音量↑↓,轨道+|-
Space & q::	;输入{Home}键
	GV_KeyClickAction1 := "SendInput,{Home}" ;首帧
	GV_KeyClickAction2 := "SendInput,+p" ;预览轴开关|和P对称
	gosub,Sub_KeyClick123
return

Space & w::	;{i}|{M}键
	GV_KeyClickAction1 := "SendInput,{m}" ;M标记
	GV_KeyClickAction2 := "SendInput,{i}" ;区域入点
	gosub,Sub_KeyClick123
return

Space & e::SendInput,^`.	;音量+0.1db

Space & r::	;变速面板|区域出点
	GV_KeyClickAction1 := "SendInput,^r" ;变速面板
	GV_KeyClickAction2 := "SendInput,{o}" ;区域出点
	gosub,Sub_KeyClick123
return

Space & t::		;删除|主轨吸附
	GV_KeyClickAction1 := "SendInput,{BackSpace}"	;删除
	GV_KeyClickAction2 := "SendInput,+{BackSpace}"	;主轨吸附
	gosub,Sub_KeyClick123
return

;****************************************************

Space & a::SendInput,{End}	;尾帧

Space & s::SendInput,{NumpadSub}	;(数字|-号)|轨道缩小

Space & d::SendInput,^`,	;音量-0.1db

Space & f::SendInput,{NumpadAdd}	;(数字|+号)|轨道放大

Space & g:: ;输入句号(g).键
	GV_KeyClickAction1 := "SendInput,^g" ;创建组合
	GV_KeyClickAction2 := "SendInput,^+g" ;解除组合
	gosub,Sub_KeyClick123
return

;****************************************************

Space & z::
	GV_KeyClickAction1 := "SendInput,^z" ;输入^z,撤销
	GV_KeyClickAction2 := "SendInput,^+z" ;输入^+z,恢复
	gosub,Sub_KeyClick123
return

Space & x::	;20240423
	GV_KeyClickAction1 := "SendInput,+x" ;以片段选定区域
	GV_KeyClickAction2 := "SendInput,!x" ;取消选定区域
	gosub,Sub_KeyClick123
return

Space & c::
	GV_KeyClickAction1 := "SendInput,^c" ;复制
	GV_KeyClickAction2 := "SendInput,^k" ;分割
	gosub,Sub_KeyClick123
return

Space & v::
	GV_KeyClickAction1 := "SendInput,^v" ;粘贴
	GV_KeyClickAction2 := "SendInput,^n" ;新建
	gosub,Sub_KeyClick123
return

Space & b::	;全屏
	GV_KeyClickAction1 := "SendInput,^f" ;软件全屏
	GV_KeyClickAction2 := "SendInput,`~" ;播放器全屏
	gosub,Sub_KeyClick123
return

#If

;****************************************************
;20240407,增加Space+数字和符号输入模式
#If WinActive("ahk_group MediaEditor_SgSpace")

Space & F1:: SendInput,{F11} ;加+10
Space & F2:: SendInput,{F12} ;加+10
Space & F3:: SendInput,{F8} ;相加=11
Space & F4:: SendInput,{F7} ;相加=11
Space & F5:: SendInput,{F6} ;相加=11

; $Space::SendInput,{Blind}{Space} ;恢复默认打字上屏功能
$Space::Space ;恢复空格打字上屏,by Tuutg,20240425

;单击CapsLock退出全屏,双击退出到草稿
CapsLock::
	Suspend Permit
	;GV_MouseTimer := 300
	GV_KeyClickAction1 := "SendInput,^s!x^+x{ESC}" ;退出全屏|取消选定区域
	GV_KeyClickAction2 := "SendInput,^s!{F4}" ;退到草稿|Alt+F4|关闭
	gosub,Sub_KeyClick123
return

Space & 1::	;输入0|!键,相加=11
	GV_KeyClickAction1 := "SendInput,{0}"
	GV_KeyClickAction2 := "SendInput,{Text}!"
	gosub,Sub_KeyClick123
return

Space & 2::	;输入2|@键,相加=11
	GV_KeyClickAction1 := "SendInput,{9}"
	GV_KeyClickAction2 := "SendInput,{Text}@"
	gosub,Sub_KeyClick123
return

Space & 3::	;输入3|#键,相加=11
	GV_KeyClickAction1 := "SendInput,{8}"
	GV_KeyClickAction2 := "SendInput,{Text}#"
	gosub,Sub_KeyClick123
return

Space & 4::	;输入4|$键,相加=11
	GV_KeyClickAction1 := "SendInput,{7}"
	GV_KeyClickAction2 := "SendInput,{Text}$"
	gosub,Sub_KeyClick123
return

Space & 5::	;输入5|%键,相加=11
	GV_KeyClickAction1 := "SendInput,{6}"
	GV_KeyClickAction2 := "SendInput,{Text}%"
	gosub,Sub_KeyClick123
return

;****************************************************

Space & 6:: SendInput,{Text}^ ;输入^键
Space & 7:: SendInput,`& ;输入&键
Space & 8:: SendInput,`* ;输入*键
Space & 9:: SendInput,`( ;输入(键
Space & 0:: SendInput,`) ;输入)键

Space & -:: SendInput,`_ ;输入_键
Space & =:: SendInput,{NumpadAdd} ;输入+键
Space & [:: SendInput,{Text}{ ;输入{键
Space & ]:: SendInput,{Text}} ;输入}键
Space & `;:: SendInput,`: ;输入:键
Space & ':: SendInput,`" ;输入"键
Space & \:: SendInput,`| ;输入|键
Space & /:: SendInput,`? ;输入)键
Space & ,:: SendInput,`< ;输入)键
Space & .:: SendInput,`> ;输入)键

#If

;****************************************************

;单键模式,基本同Space模式,分割点↑↓播放速度→←,不同的键位用Q,A,Z,X,C,V,B.排除输入框,I型光标
#If WinActive("ahk_group MediaEditor_SgSpace") and !(A_Cursor = "IBeam") and (A_CaretX = "")
q::	;输入{Home}键
	GV_KeyClickAction1 := "SendInput,{Home}" ;首帧
	GV_KeyClickAction2 := "SendInput,{End}" ;尾帧
	gosub,Sub_KeyClick123
return

w::	;{i}|{M}键
	GV_KeyClickAction1 := "SendInput,{m}" ;M标记
	GV_KeyClickAction2 := "SendInput,{i}" ;区域入点
	gosub,Sub_KeyClick123
return

e::SendInput,{Up}	;上一个分割点

r::	;变速面板|区域出点
	GV_KeyClickAction1 := "SendInput,^r" ;变速面板
	GV_KeyClickAction2 := "SendInput,{o}" ;区域出点
	gosub,Sub_KeyClick123
return

t::	;删除|主轨吸附
	GV_KeyClickAction1 := "SendInput,{BackSpace}"	;删除
	GV_KeyClickAction2 := "SendInput,+{BackSpace}"	;主轨吸附
	gosub,Sub_KeyClick123
return

;****************************************************

a::	;向右全选|向左全选
	; GV_KeyClickAction1 := "SendInput,{Text}a" ;向右全选
	GV_KeyClickAction2 := "SendInput,+a" ;向左全选
	gosub,Sub_KeyClick123
return

$+s::SendInput,{Text}s	;吸附开关,20240423
$!s::SendInput,{Text}s	;吸附开关,20240423
s::SendInput,{Text}j	;后退加速播放

d::SendInput,{Down} ;下一个分割点

f::SendInput,{Text}l ;前进加速播放

g:: ;创建组合|解除组合
	GV_KeyClickAction1 := "SendInput,^g" ;创建组合
	GV_KeyClickAction2 := "SendInput,^+g" ;解除组合
	gosub,Sub_KeyClick123
return

;****************************************************

z::
	GV_KeyClickAction1 := "SendInput,+z" ;全局预览缩放+z
	GV_KeyClickAction2 := "SendInput,!+z" ;播放器适应窗口!+z
	gosub,Sub_KeyClick123
return

x::
	GV_KeyClickAction1 := "SendInput,x+x" ;以片段选定区域PR:X,剪映+X
	; GV_KeyClickAction1 := "gosub,TCursor" ;吸附开关,谐音X
	GV_KeyClickAction2 := "SendInput,x+x" ;以片段选定区域PR:X,剪映+X
	gosub,Sub_KeyClick123
return

c::
	; GV_KeyClickAction1 := "SendInput,{c}" ;鼠标分割模式
	GV_KeyClickAction2 := "SendInput,^k" ;分割
	gosub,Sub_KeyClick123
return

v::
	; GV_KeyClickAction1 := "SendInput,{v}" ;鼠标选择模式
	GV_KeyClickAction2 := "SendInput,^n" ;新建草稿
	gosub,Sub_KeyClick123
return

b::
	GV_KeyClickAction1 := "SendInput,^i" ;导入
	GV_KeyClickAction2 := "SendInput,^m" ;导出
	gosub,Sub_KeyClick123
return

#If

TCursor:	;Test,测试光标类型
	MsgBox,%A_Cursor%
	MsgBox,%A_CaretX%
return
;****************************************************
;****************************************************
;Enter激活自动保存,20240325;自动保存,只在如Code|Office|Wps系列软件启用
#If WinActive("ahk_group OfficeAndWPS") or WinActive("ahk_group Group_Code") or WinActive("ahk_group EnterableAutoSave")
	$Enter::
	if (GV_ToggleAotuSaveMode==1) and !WinActive("ahk_group Group_CodeFind")
		SendInput,{Enter}^s
	else
		SendInput,{Enter}
	return
#If
;****************************************************
;****************************************************
; 自动刷新工信部合格证信息管理系统
#If WinActive("ahk_group Group_browser")
	~$MButton::
	~F5::	;20240408
	Loop{
		WinActivate,A
		Sleep, 1000
		WinShow,A
		Sleep, 1000
		SendInput,{Click}	;点击激活
		Sleep, 1000
		ControlSend,,{F5},A
		Sleep,180000
	}
#If
;****************************************************
;****************************************************
;WPS-PDF专业版 {{{2	,20240528
;金山PDF专业版: pdf ahk_class QWidget ahk_exe wpspdf.exe
;采用右键|Alt+菜单方式操作
#If WinActive("ahk_group Kingsoft_PDF")

$!s::SendInput,!+3 ;签名|批注

;****************************************************
; $!b:: 		;金山PDF_保护
;****************************************************
; Space & x::
; CapsLock & x::	;原版剪切
;****************************************************
; Space & c::
; CapsLock & c::	;原版复制
;****************************************************
; Space & v::
; CapsLock & v::	;原版粘贴
;****************************************************
CapsLock & t::SendInput,^!e ;金山PDF_提取页面

CapsLock & g::SendInput,^!t	;金山PDF_插入页面
$!g::SendInput,^!i	;金山PDF_插入空白页面

$^+c::SendInput,^+b	;从剪切板插入页面

CapsLock & b::SendInput,!f5 ;金山PDF_替换页面
;****************************************************
;D->J->P,文件快速导出
$+!LButton::	;PDF在jpg前,所以用LButton
$^+LButton::
$^#!d::SendInput,+{F10} ;另存为较小的PDF

$+!MButton::	;中键
$^+MButton::
$^#!j::SendInput,!4	;图片转为PDF

$+!RButton::
$^+RButton::
$^#!p::SendInput,!3	;导出png

$^#!w::SendInput,^+w ;导出为word

$^#!e::SendInput,^+x ;导出为excel

$^#!t::SendInput,^+p ;导出为PPt

$^#!c::SendInput,!5	;导出为纯文本,PDF专业版
$^#!l::SendInput,!5	;导出为纯文本,PDF专业版
	; CoordWinClick(40,65)
	; SendInput,{Down 4}
	; Sleep,100
	; SendInput,{Right}{Up 2}
; return

;****************************************************

$!f::SendInput,^f		;页面查找

;用其他软件打开编辑(E),如Acrobat
; $!e::
	; Acrobat_Path =D:\Program Files (x86)\Adobe\Acrobat DC 2022\Acrobat DC\Acrobat\Acrobat.exe
	; WinGetActiveTitle, title
	; Clipboard= % title
	; filenamenew := RegExReplace(Clipboard,"(.*(\.chm|\.pdf|\.epub)+).*","$1")
	; if RegExMatch(filenamenew, ".pdf$"){
		; Run, "%Acrobat_Path%" "%filenamenew%", ..\, Max,
		; Run, "FoxitReaderPortable.exe" "%filenamenew%", ..\, Max,
	; }
	; else if RegExMatch(filenamenew, ".chm$"){
		; Run, "hh.exe" "%filenamenew%", ..\, Max,
	; }
; return

CapsLock & w:: ;关闭当前标签|恢复关闭标签,20240528
	GV_KeyClickAction1 := "SendInput,^w"	;关闭当前标签
	GV_KeyClickAction2 := "SendInput,!+t"	;恢复关闭标签
	gosub,Sub_KeyClick123
return

#If
;****************************************************
;金山PDF专业版: pdf ahk_class QWidget ahk_exe wpspdf.exe
;4合1集成版1: pdf ahk_class Qt5QWindowIcon
;4合1集成版2: pdf ahk_exe wpsoffice.exe
#if WinActive("ahk_group Kingsoft_PDF") and (A_Cursor != "IBeam")
{	;20240330
	$q::SendInput,!+2		;显示缩微图
	$w::SendInput,^+{NumpadSub}	;向左旋转-
	$e::SendInput,^`=			;+,放大
	$r::SendInput,^+{NumpadAdd}	;向右旋转+
	$t::			;显示/隐藏工具栏
		WinGetPos, x, y,WA,HA, A
		CoordWinClick(WA-22,65)
	return

	$a::SendInput,^s		;保存|另存
	$s::SendInput,{PgUp}	;上一页
	$d::SendInput,^`-		;-,缩小
	$f::SendInput,{PgDn}	;下一页
	$g::			;进入/退出播放|幻灯片
		if WinActive("pdf ahk_class QWidget"){		;专业版
			SendInput,^l
		}
		else if WinActive("pdf ahk_exe wpsoffice.exe"){	;集成版
			SendInput,+{F5}
		}
		else {	;集成版
			SendInput,+{F5}
		}
	return

	$z::SendInput,^0		;适合页面
	$x::SendInput,!+1		;显示/隐藏书签
	$c::SendInput,{F11}		;进入/退出全屏
	$v::			;鼠标选择模式切换|同PS_V
		; MsgBox,%A_Cursor%
		if WinActive("pdf ahk_exe wpsoffice.exe"){
			GV_KeyClickAction1 := "SendInput,^h" ;手型光标
			GV_KeyClickAction2 := "SendInput,^r" ;箭头光标
			gosub,Sub_KeyClick123
		}
		else if WinActive("pdf ahk_class QWidget"){ ;专业版
			GV_KeyClickAction1 := "SendInput,!1" ;手型光标
			GV_KeyClickAction2 := "SendInput,!2" ;箭头光标
			gosub,Sub_KeyClick123
		}
		else {
			GV_KeyClickAction1 := "SendInput,^h" ;手型光标
			GV_KeyClickAction2 := "SendInput,^r" ;箭头光标
			gosub,Sub_KeyClick123
		}
	return

	$b::SendInput,^b		;新建书签
}
#if
;****************************************************
;****************************************************
; PotPlayer播放
; #if WinActive("ahk_class PotPlayer") or WinActive("ahk_class PotPlayer64")

	; $q::SendInput,^k		;显示命令面板
	; $w::SendInput,^+{NumpadSub}	;向左旋转
	; $e::SendInput,{NumpadAdd}	;+,放大
	; $r::SendInput,^+{NumpadAdd}	;向右旋转
	; $t::SendInput,{F12}	;显示/隐藏书签

	; $a::SendInput,^s		;保存|另存
	; $s::SendInput,{Text}p		;上一页
	; $d::SendInput,{NumpadSub}	;-,缩小
	; $f::SendInput,{Text}n		;下一页
	; $g::SendInput,^g		;转到页面

	; $z::SendInput,^0		;适合页面
	; $x::SendInput,{F8}	;显示/隐藏工具栏
	; $c::SendInput,{F11}	;进入/退出全屏
	; $v::SendInput,{F5}	;进入/退出演示
	; $b::SendInput,^b		;加入书签

; #if
;****************************************************
;****************************************************
;机动车合格证 ahk_exe CertificateSystemClient.exe,20240411

; 自动登录电摩合格证信息管理系统,并右半屏幕,by Tuutg,20240515
#If WinActive("登录合格证信息管理系统 ahk_exe CertificateSystemClient.exe")
	$MButton::
	CapsLock & g::
		WinActivate, 登录合格证信息管理系统 ahk_exe CertificateSystemClient.exe
		Sleep,100
		SendInput,{Text}admin
		Sleep,100
		SendInput,{Enter}
		WinWaitActive,机动车合格证 ahk_exe CertificateSystemClient.exe
		Sleep, 1000
		SendInput, SendInput,^#!3	;右半屏幕,by Tuutg,20240515
	return
#If
;****************************************************
#if WinActive("机动车合格证 ahk_exe CertificateSystemClient.exe")
CapsLock & v::gosub,PastePureText	;纯文本粘贴+Enter

CapsLock & w::
	CoordWinClick(50,40)	;只保存不打印
	Sleep, 500
	SendInput,{Enter}
return

$!w::
$!LButton::
$!MButton::
Tab & MButton::
StartNewData:	;点新增合格证
	WinActivate, 机动车合格证 ahk_exe CertificateSystemClient.exe
	CoordWinXClick(70,42,1,1)	;单击操作菜单
	CoordWinXClick(40,75,1,1)	;点新增合格证,by Tuutg,20240515
	WinWaitActive,机动车合格证 ahk_class WindowsForms10.Window.8.app.0.a0f91b_r30_ad1
	Sleep, 1000
	SendInput, {Click}	;激活新增合格证窗口
	SendInput, SendInput,^#!3	;右半屏幕

	Sleep, 500
	gosub,StartAutoInput	;开始参数自动填写
return

$!e::
$^p::
$+MButton::
Space & MButton::
StartAutoPrint:
	WinActivate, 机动车合格证 ahk_exe CertificateSystemClient.exe
	CoordWinClick(125,40)	;保存并打印,by Tuutg,20240515

	if ErrorLevel  ; 用户取消了操作。
	{
	   return  ; 退出程序
	}

	SendInput,{Enter}	;关闭进口弹窗
	Sleep, 500
	WinWaitActive, 将打印输出另存为 ahk_class #32770
	Sleep, 1000
	gosub,PastePureText	;粘贴文件名

	FileName := Clipboard	;20240531
	if FileName :=
	{
		return
	}

	;检查文件名是否包含非法字符
	if ContainsIllegalCharacters(FileName)
	{
		; MsgBox, 文件名包含非法字符。
		return
	}

	Sleep, 1500
	SendInput,{Enter}
	EzTip("保存并打印成功",1)

	WinWaitActive,机动车合格证 ahk_exe CertificateSystemClient.exe
	WinGetPos,X, Y, W, H, 机动车合格证 ahk_exe CertificateSystemClient.exe
	Sleep, 1000
	Click, 133, 153, 2	;双击全选合格证编号
	SendInput,^c	;复制编号便于更新台账
	Sleep, 250

	; Click, W-22, 12, 1	;点击关闭窗口
	SendInput,{Blind}!{F4} ;关闭窗口
	Sleep, 100

	WinActivate, 诚安杰羊电摩-2.xlsx - WPS Office ahk_class XLMAIN
	Sleep, 500
	Click, 105, 260, 1	;合格证完整编号
	SendInput, {Blind}^v	;更新合格证编号
	Sleep, 100
	Click, 730, 760, 1	;下一个数据
	Sleep, 500

	gosub,StartNewData	;点新增合格证
return

$^g::
$!g::
~$MButton::	;请输入车型序号,开始参数自动填写,20240425
StartAutoInput:
	WinActivate, 机动车合格证 ahk_exe CertificateSystemClient.exe	;车型序号
	SendInput, SendInput,^#!3	;右半屏幕,by Tuutg,20240515

	; IniRead, VehicleModelNum0, %INI%, 基本设置, VehicleModelNum0, 02	;20240531,车型配置序号
	Sleep, 100
	InputBox,VehicleModelNum,请手动填写车型序号并回车,,,,,,,,,%VehicleModelNum0%	;20240425
	; InputBox,Month,请输入发证月份,两位数字,举例01-1月份	;20240515
	; InputBox,Date,请输入发证日期,两位数字,举例15-15日	;20240515
	WinWaitActive,机动车合格证 ahk_exe CertificateSystemClient.exe
	Sleep, 100
	VehicleModelNum0 :=VehicleModelNum
	IniWrite, %VehicleModelNum0%, %INI%, 基本设置, VehicleModelNum0	;20240531,车型配置序号
	Sleep, 500

	if (VehicleModelNum == "") {
		EzTip("用户取消操作!",1)
		return
	}

	Click, 245, 80, 2	;车型序号
	Sleep, 100
	ControlSend, WindowsForms10.Edit.app.0.a0f91b_r30_ad153, %VehicleModelNum%, 机动车合格证 ahk_exe CertificateSystemClient.exe
	Sleep, 500
	ControlSend, WindowsForms10.Edit.app.0.a0f91b_r30_ad153, {Enter}, 机动车合格证 ahk_exe CertificateSystemClient.exe
	Sleep, 1500

	SendInput,{Click, 405 180}{End}	;查看车辆型号,by Tuutg,20240531
	Sleep, 250

	WinActivate, 诚安杰羊电摩-2.xlsx - WPS Office ahk_class XLMAIN
	Clipboard := ""
	Click, 220, 260, 1	;合格证编号
	SendInput, {Blind}^c
	Sleep, 1000
	if (Clipboard == "")
		return	;excel复制到数据停止

	WinActivate, 机动车合格证 ahk_exe CertificateSystemClient.exe
	Click, 170, 156, 1	;合格证编号
	Sleep, 500
	SendInput, {Text}%Clipboard%
	Sleep, 1000

	WinActivate, 诚安杰羊电摩-2.xlsx - WPS Office ahk_class XLMAIN
	Click, 450, 260, 1	;车身颜色
	SendInput,  {Blind}^c
	Sleep, 1000
	WinActivate, 机动车合格证 ahk_exe CertificateSystemClient.exe
	Click, 120, 210, 1	;车身颜色
	Sleep, 500
	ControlSend, WindowsForms10.Edit.app.0.a0f91b_r30_ad141, {Text}%Clipboard%, 机动车合格证 ahk_exe CertificateSystemClient.exe
	Sleep, 1000

	WinActivate, 诚安杰羊电摩-2.xlsx - WPS Office ahk_class XLMAIN
	Click, 560, 260, 1	;电动机编号
	SendInput,  {Blind}^c
	Sleep, 1000
	WinActivate, 机动车合格证 ahk_exe CertificateSystemClient.exe
	Click, 260, 240, 1	;电动机编号
	Sleep, 500
	ControlSend, WindowsForms10.Edit.app.0.a0f91b_r30_ad140, {Text}%Clipboard%, 机动车合格证 ahk_exe CertificateSystemClient.exe
	Sleep, 1000

	WinActivate, 诚安杰羊电摩-2.xlsx - WPS Office ahk_class XLMAIN
	Click, 325, 260, 1	;VIN编号
	SendInput, {Blind}^c
	Sleep, 1000
	WinActivate, 机动车合格证 ahk_exe CertificateSystemClient.exe
	Sleep, 500
	Click, 175, 235, 2	;双击全选VIN编号,by Tuutg,20240515
	Sleep, 1000
	ControlSend, WindowsForms10.Edit.app.0.a0f91b_r30_ad147, {Del 2}, 机动车合格证 ahk_exe CertificateSystemClient.exe
	Sleep, 1000
	ControlSend, WindowsForms10.Edit.app.0.a0f91b_r30_ad147, {Text}%Clipboard%, 机动车合格证 ahk_exe CertificateSystemClient.exe
	Sleep, 1000
	SendInput,{Click, 130 235}{End}	;查看VIN编号,by Tuutg,20240515
	Sleep, 250


	;发证月份, by Tuutg,20240515
	WinActivate, 机动车合格证 ahk_exe CertificateSystemClient.exe
	Click, 710, 129, 1	;发证月份03
	Sleep, 1000
	; ControlSend, WindowsForms10.Edit.app.0.a0f91b_r30_ad151, {Text}%Month%{Enter}, 机动车合格证 ahk_exe CertificateSystemClient.exe
	ControlSend, WindowsForms10.Edit.app.0.a0f91b_r30_ad151, {Text}05{Enter}, 机动车合格证 ahk_exe CertificateSystemClient.exe
	Click, 730, 129, 1	;发证日期,31
	; ControlSend, WindowsForms10.Edit.app.0.a0f91b_r30_ad151, {Text}%Date%{Enter}, 机动车合格证 ahk_exe CertificateSystemClient.exe
	ControlSend, WindowsForms10.Edit.app.0.a0f91b_r30_ad151, {Text}31{Enter}, 机动车合格证 ahk_exe CertificateSystemClient.exe
	Sleep, 1000
	Click, 605, 410, 1	;发证月份03
	; ControlSend, WindowsForms10.Edit.app.0.a0f91b_r30_ad151, {Text}%Month%{Enter}, 机动车合格证 ahk_exe CertificateSystemClient.exe
	ControlSend, WindowsForms10.Edit.app.0.a0f91b_r30_ad151, {Text}05{Enter}, 机动车合格证 ahk_exe CertificateSystemClient.exe
	Sleep, 1000
	Click, 625, 410, 1	;发证月份31
	; ControlSend, WindowsForms10.Edit.app.0.a0f91b_r30_ad151, {Text}%Date%{Enter}, 机动车合格证 ahk_exe CertificateSystemClient.exe
	ControlSend, WindowsForms10.Edit.app.0.a0f91b_r30_ad151, {Text}31{Enter}, 机动车合格证 ahk_exe CertificateSystemClient.exe
	Sleep, 1000


	;6个否
	ControlSend, WindowsForms10.Window.b.app.0.a0f91b_r30_ad18, {Click}{Down 2}{Enter}, 机动车合格证 ahk_exe CertificateSystemClient.exe
	Sleep, 1000
	ControlSend, WindowsForms10.Window.b.app.0.a0f91b_r30_ad15, {Click}{Down}{Enter}, 机动车合格证 ahk_exe CertificateSystemClient.exe
	Sleep, 1000
	ControlSend, WindowsForms10.Window.b.app.0.a0f91b_r30_ad14, {Click}{Down}{Enter}, 机动车合格证 ahk_exe CertificateSystemClient.exe
	Sleep, 1000
	ControlSend, WindowsForms10.Window.b.app.0.a0f91b_r30_ad13, {Click}{Down}{Enter}, 机动车合格证 ahk_exe CertificateSystemClient.exe
	Sleep, 1000
	ControlSend, WindowsForms10.Window.b.app.0.a0f91b_r30_ad12, {Click}{Down}{Enter}, 机动车合格证 ahk_exe CertificateSystemClient.exe
	Sleep, 1000
	ControlSend, WindowsForms10.Window.b.app.0.a0f91b_r30_ad11, {Click}{Down}{Enter}, 机动车合格证 ahk_exe CertificateSystemClient.exe

	CoordWinClick(50,40)	;只保存不打印
	Sleep, 1000
	SendInput,{Enter}		;关闭保存弹窗
	; Sleep, 1000
	; gosub,StartAutoPrint	;自动开始打印
return

;从机动车合格证信息管理系统导入数据到Excel,by Tuutg,20240515
$!d::
	WinActivate, 机动车合格证 ahk_exe CertificateSystemClient.exe
	Click, 175, 235, 2	;双击全选VIN编号
	Sleep, 100
	SendInput,  {Blind}^c
	Sleep, 1000
	WinActivate, 诚安杰羊电摩-2.xlsx - WPS Office ahk_class XLMAIN
	Click, 325, 260, 1	;VIN编号
	SendInput,{Text}%Clipboard%
	Sleep, 100
	SendInput,{Tab}
	WinActivate, 机动车合格证 ahk_exe CertificateSystemClient.exe
	Sleep, 1000
	Click, 70, 210, 1	;车身颜色
	SendInput,  {Blind}^+{End}
	Sleep, 1000
	SendInput,  {Blind}^c
	Sleep, 1000
	WinActivate, 诚安杰羊电摩-2.xlsx - WPS Office ahk_class XLMAIN
	Click, 450, 260, 1	;车身颜色
	SendInput,{Text}%Clipboard%
	Sleep, 100
	SendInput,{Tab}
	WinActivate, 机动车合格证 ahk_exe CertificateSystemClient.exe
	Sleep, 1000
	Click, 260, 240, 2	;电动机编号
	Sleep, 100
	SendInput,  {Blind}^c
	Sleep, 1000
	WinActivate, 诚安杰羊电摩-2.xlsx - WPS Office ahk_class XLMAIN
	Click, 560, 260, 1	;电动机编号
	SendInput,{Text}%Clipboard%
	Sleep, 100
	SendInput,{Tab}{Enter}
return

;数据操作-VIN筛选,by Tuutg,20240515
$!s::
$F3::
	WinActivate, 机动车合格证 ahk_exe CertificateSystemClient.exe
	Click, 320, 40, 1	;数据操作
	Sleep, 100
	Click, 25, 85, 1	;筛选
	WinActivate, 过滤条件生成器 ahk_class WindowsForms10.Window.8.app.0.a0f91b_r30_ad1
	WinGetPos, x0, y0, w0, h0, 过滤 ahk_class WindowsForms10.Window.8.app.0.a0f91b_r30_ad1
	Sleep, 100
	Click, w0/2, h0/2, 1
	Sleep, 100
	Click, 32, 40, 1	;点击+
	Sleep, 1500
	Click, 70, 65, 1	;选VIN
	Sleep, 100
	SendInput,{Down 11}	;VIN
	Sleep, 100
	SendInput,{Enter}	;确认
	Sleep, 100
	Click, 145 65, 1	;选条件
	SendInput,{Down 1}	;点击=
	Sleep, 100
	SendInput,{Enter}	;确认
	Sleep, 100
	Click, 180, 260, 1	;VIN
	Sleep, 100
	SendInput,{Text}%Clipboard%
	Sleep, 100
	; SendInput,!o	;确认
	Click, 245, 350, 0	;确认
return

;上传数据,by Tuutg,20240602
$!c::
	CoordWinClick(335,40)	;上传数据
	Sleep, 500
	WinActivate, 上传申请 ahk_exe CertificateSystemClient.exe
	CoordWinClick(35,105)	;上传数据
	WinActivate, 申请原因 ahk_exe CertificateSystemClient.exe
	CoordWinClick(35,85)	;上传原因
	Sleep, 500
	SendInput,{Text}系统原因
	Sleep, 500
	CoordWinMove(60,360)	;确定上传
return

#if

; 函数来检查文件名是否包含非法字符
ContainsIllegalCharacters(FileName)
{
	; 列出所有非法字符
	illegalChars := ":*?""<>|"

	; 检查文件名中是否包含任何非法字符
	Loop, Parse, illegalChars
	{
	if InStr(FileName, A_LoopField)
	{
		return true
	}
	}
	return false
}

;****************************************************
#if WinActive("机动车合格证信息管理系统 ahk_exe CertificateSystemClient.exe")
CapsLock & r::	;强制删除数据
	SendInput,{Click,Right}
	Sleep, 100
	SendInput,{Up 1}{Enter}	;20240601
	Sleep, 300
	WinActivate,操作确认 ahk_class #32770 ahk_exe CertificateSystemClient.exe
	SendInput,{Tab 1}
	Sleep, 200
	SendInput,{Enter 2}
return

$^a::	;全选数据
	SendInput,{Click,Right}
	Sleep, 100
	SendInput,{Down 1}{Enter}	;20240601
return

$!a::	;全选|取消
	GV_KeyClickAction1 := "SendInput,{Click,Right}{Down 1}{Enter}" ;全选数据
	GV_KeyClickAction2 := "SendInput,{Click,Right}{Up 1}{Enter}" ;取消全选
	gosub,Sub_KeyClick123
return
#if
;****************************************************
;****************************************************
;信息检测,20240416
>^CapsLock::
Loop
{
	Sleep, 100
	WinGetTitle, Title, A
	WinGetClass, class, A
	WinGetPos, , , w0, h0, A
	MouseGetPos, x0, y0, WhichWindow, WhichControl
	ControlGetPos, x, y, w, h, %WhichControl%, ahk_id %WhichWindow%
	ToolTip, %Title%`t%class%`nMX%X0%`tMY%Y0%`nWW%W0%`tWH%H0%`n%A_Cursor%`t%WhichControl%`nCX%X%`tCY%Y%`nCW%W%`tCH%H%`nA_X:%A_CaretX%`tA_Y:%A_CaretY%
}
;****************************************************
;****************************************************
/**
 * 将鼠标移动到光标的位置,20240416
 */
MoveMouseToCaret() {
  x :=A_CaretX
  y :=A_CaretY
  if (StrLen(x) | StrLen(y)) {
	; Tip(A_SendMode "|" A_CoordModeMouse) ; 每次执行热键, 这两个都会重置为默认值
	SendMode,Event
	CoordMode,Mouse,Window
	MouseMove,x, y, 100
  } else {
	MouseToActiveWindowCenter()
  }
}
;****************************************************
/**
 * 移动鼠标到活动窗口中心,20240416
 */
MouseToActiveWindowCenter() {
  WinGetPos,X, Y, W, H, A
  MouseMove,X + W / 2, Y + H / 2
}
;****************************************************
;****************************************************
;向日葵远程控制本机,中键输入登入密码,20240520
#if WinActive("向日葵远程控制 ahk_class OrayUI")
~$MButton::
	SendInput,{Text}tuutg624908	;本机登入密码
	sleep,50
	SendInput,{Enter}
#if

#if WinActive("XDS-YF-抚州 ahk_class OrayUI")	;20240616
~$MButton::
	SendInput,{Text}tuutg86	;他机登入密码
	sleep,50
	SendInput,{Enter}
#if

;向日葵远程控制他机(4楼合格证PC),中键输入登入密码,20240603
#if WinActive("4楼合格证PC ahk_class OrayUI")
~$MButton::
	SendInput,{Text}Tuutg2024	;他机登入密码
	sleep,50
	SendInput,{Enter}
#if
;****************************************************
;****************************************************
;SLDWORKS一键快速导出|另存为其他格式,20240603
#if WinActive("ahk_exe SLDWORKS.exe")
$^+a::	;快速导出Ai
$^#!a::	;快速导出Ai
	SldWorksSaveAs("{a 3}")
return

$^+d::	;左键另存为Dwg
$^#!d::	;左键另存为Dwg
$+!LButton::
$^+LButton::
	SldWorksSaveAs("{Text}d")
return

$^+f::	;中键快速导出PDF
$^#!f::	;中键快速导出PDF
$+!MButton::
$^+MButton::
	SldWorksSaveAs("{a 5}")
return

$^+j::	;右键另存为JPG
$^#!j::	;右键另存为JPG
$+!RButton::
$^+RButton::
	SldWorksSaveAs("{Text}j")
return

$^+i::
$^#!i::	;快速导出IGES
	SldWorksSaveAs("{i 3}")
return

$^+p::
$^#!p::	;快速导出PSD
	SldWorksSaveAs("{a 4}")
return

$!e::SendInput,!fb	;出版到eDrawings,用Alt+_字母菜单
#if
;****************************************************
SldWorksSaveAs(Key){	;20240520,修改Key可应用到其他软件
	SendInput,!fa	;另存为,Alt+_字母菜单
	WinWaitActive, 另存为 ahk_class #32770
	ControlClick, ComboBox2	;弹出文件类型选择
	Sleep, 100
	ControlSend, ComboBox2, %Key%, 另存为 ahk_class #32770
	Sleep, 100
	ControlSend, ComboBox2, {Enter}, 另存为 ahk_class #32770
	Sleep, 1000
	SendInput,^g	;Listary路径跳转
}
;****************************************************
;****************************************************
#if WinActive("ahk_class #32770")
$!a::SendInput,^a	;全选 by Tuutg,20240628
#if
;****************************************************
;****************************************************
;*********** Tuutg自定义应用增强例子结束$ ***********
;********************************************************************
;********************************************************************
;*********** 用户自定义应用增强例子开始^ ***********
; “例子”位置,进行自定义修改,请开始你的表演.





















;********** 用户自定义应用增强例子结束$ ************